import {
    Component,
    OnInit,
    ViewEncapsulation,
    AfterViewInit,
    Input,
    Output
} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { AppState } from '../app.service';

// Import services
import {HttpServices} from '../services/http.services';
import {ValidationService} from '../services/validation.service';

import * as _ from 'lodash';

// import '../loader/slick.loader.ts';

declare var $: any;

@Component({
  /**
   * The selector is what angular internally uses
   * for `document.querySelectorAll(selector)` in our index.html
   * where, in this case, selector is the string 'home'.
   */
    selector: 'login',
    templateUrl: '../views/login/login.html',
    styleUrls: [ '../../assets/css/_login.scss', '../../assets/css/_plugin.scss' ],
  /**
   * We need to tell Angular's Dependency Injection which providers are in our app.
   */
    providers: [
        HttpServices,
        ValidationService
    ],

  /**
   * Every Angular template is first compiled by the browser before Angular runs it's compiler.
   */
})
export class LoginComponent implements OnInit, AfterViewInit {
    loginFrm: FormGroup;
    loginStatus: any = {
        text: '',
        class: ''
    }

    checkPhone: boolean = false;
    loginForm: boolean = false;

    phone: any = '';
    digit: any = '';
    fullname: any = '';

    month: any = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    day: any = [];

    reedem: any = [];

    checkIn: any;
    numbCheck: any;

    enterName: boolean = false;
    enterBirth: boolean = false;
    enterMail: boolean = false;
    done: boolean = false;

    errMsg: any;

    nameFrm: FormGroup;
    mailFrm: FormGroup;

    updateRes: any = [];
    checkinDOne: any =[];
    host: any = '';

    constructor(
    public appState: AppState,
    private _router: Router,
    private _titleService: Title,
    private _httpService : HttpServices,
    ) {}

    public ngOnInit() {
        if(localStorage.getItem('token')){
            this.loginForm = false;
            this.checkPhone = true;

            for(let i = 1; i <= 31; i++){
                this.day.push(i);
            }

        } else {
            this.loginForm = true;
        }
    }

    ngAfterViewInit() {
        this.loginFrm = new FormGroup({
            username: new FormControl('', Validators.compose([Validators.required, ValidationService.NoWhitespaceValidator])),
            password: new FormControl('', Validators.compose([Validators.required, ValidationService.NoWhitespaceValidator])),
            domain: new FormControl('', Validators.compose([Validators.required, ValidationService.NoWhitespaceValidator])),
            host: new FormControl('.go-checkin.com')
        });

    }

    // Login form
    submitLogin(event: any, frmData: any){
        let thisComp = this;
        if(this.loginFrm.valid){
            let loginParam = {
                username: frmData.username,
                password: frmData.password
            };

            // console.log(loginParam);
            let urlParam = 'http://'+ frmData.domain + frmData.host + '/api/v1/user/login';
            this.host = frmData.host;

            this._httpService.postAPINoToken(urlParam, loginParam).subscribe(
                res => {
                    let token = JSON.parse(res._body);

                    localStorage.setItem('token', JSON.stringify(token.result['token']));
                    localStorage.setItem('domain', JSON.stringify(frmData.domain));

                    this.loginStatus.text = 'Login successfully.';
                    this.loginStatus.class = 'text-success';

                    setTimeout(function() {
                        thisComp.loginStatus.text = '';
                        thisComp.loginStatus.class = '';

                        thisComp.loginForm = false;
                        thisComp.checkPhone = true;

                        // thisComp.getReedem();
                    }, 1000);

                    this.getReedem();
                },
                error => {
                    console.log(error);

                    this.loginStatus.text = 'Login failed.';
                    this.loginStatus.class = 'text-danger';

                    setTimeout(function() {
                        thisComp.loginStatus.text = '';
                        thisComp.loginStatus.class = '';
                    }, 1000);
                },
                () => {
                    console.log('Login successfully.');
                }
            )
        } else {
            for (let i in this.loginFrm.controls) {
                this.loginFrm.controls[i].markAsTouched();
            }
        }
    }

    getReedem(){
        if(localStorage.getItem('domain')){
            let domain = JSON.parse(localStorage.getItem('domain'));

            let urlParam = 'http://'+ domain + this.host + '/api/v1/redeem/list';

            if(localStorage.getItem('token')){
                let token = JSON.parse(localStorage.getItem('token'));

                this._httpService.getAPI(urlParam, token).subscribe(
                    res => {
                        console.log(res);
                    },
                    error => {

                    },
                    () => {

                    }
                );
            }
        }
    }
    // Enter phone number
    enterNumb(event: any, numb: any){
        let country, city, number;

        if(this.phone.length >= 0 && this.phone.length <= 10){
            if(numb.length == 1 && (numb == '1' || numb == '0')) {
                this.phone = '';
            } else {
                this.phone += numb;
            }
        }
        if(this.phone.length == 10){
            country = 1;
            city = this.phone.slice(0, 3);
            number = this.phone.slice(3);

            if (country == 1) {
                country = "";
            }

            number = number.slice(0, 3) + '-' + number.slice(3);

            this.phone = (country + " (" + city + ") " + number).trim();
        }

    }

    delNumb(event: any){
        let val = $('.ipt-phone input').val();
        if(val.length > 0){
            $('.ipt-phone input').val(val.substring(0, val.length - 1))
            this.phone = this.phone.substring(0, this.phone.length - 1);
        }
    }

    // Enter phone
    submitPhone(event: any){
        // console.log(this.phone)
        if(_.trim(this.phone) != ''){
            let phoneParam = this.phone;
            phoneParam = phoneParam.replace('(', '');
            phoneParam = phoneParam.replace(')', '');
            phoneParam = phoneParam.replace('-', '');
            phoneParam = phoneParam.replace(' ', '');

            if(localStorage.getItem('domain')){
                let domain = JSON.parse(localStorage.getItem('domain'));

                let urlParam = 'http://'+ domain + this.host +'/api/v1/customer/check-in?phone=' + phoneParam;

                if(localStorage.getItem('token')){
                    let token = JSON.parse(localStorage.getItem('token'));

                    this._httpService.getAPI(urlParam, token).subscribe(
                        res => {
                            console.log(res);
                            this.checkIn = res.result;
                            this.checkPhone = false;
                            this.enterName = true;

                            this.checkIn['number-checkin'] = parseInt(this.checkIn['number-checkin']);
                            this.numbCheck = this.checkIn['number-checkin'];

                            this.nameFrm = new FormGroup({
                                name: new FormControl('', Validators.compose([Validators.required, ValidationService.NoWhitespaceValidator]))
                            });
                        },
                        error => {
                            this.enterName = false;
                            this.checkPhone = true;
                        },
                        () => {

                        }
                    );
                }
            }
            // this._httpService.postAPI
        }
    }

    // Enter name
    submitName(event: any, frmData: any){
        if(this.nameFrm.valid){
            if(localStorage.getItem('domain')){
                let domain = JSON.parse(localStorage.getItem('domain'));
                let phoneParam = this.phone;
                phoneParam = phoneParam.replace('(', '');
                phoneParam = phoneParam.replace(')', '');
                phoneParam = phoneParam.replace('-', '');
                phoneParam = phoneParam.replace(' ', '');
                let param = {
                    'Customer[full_name]': frmData.name,
                    'phone': phoneParam,
                }

                let urlParam = 'http://'+ domain + this.host +'/api/v1/customer/update';

                if(localStorage.getItem('token')){
                    let token = JSON.parse(localStorage.getItem('token'));

                    this._httpService.postAPI(urlParam, token, param).subscribe(
                        res => {
                            this.updateRes = JSON.parse(res._body);
                            console.log(this.updateRes);
                            if(this.updateRes['status'] == 'OK'){

                                if(this.numbCheck >= 2 && this.updateRes['result']['birthday'] == '1969-12-31'){
                                    this.enterBirth = true;
                                }

                                this.fullname = this.updateRes['result']['full_name'];

                                this.mailFrm = new FormGroup({
                                    mail: new FormControl('', Validators.compose([Validators.required, ValidationService.NoWhitespaceValidator, ValidationService.emailValidator]))
                                });

                                this.enterName = false;
                                if(this.updateRes['result']['email']) {
                                    this.enterMail = false;
                                    this.done = true;
                                } else {
                                    this.enterMail = true;
                                }

                            }
                        },
                        error => {
                        },
                        () => {

                        }
                    );
                }
            }
        } else {
            for (let i in this.nameFrm.controls) {
                this.nameFrm.controls[i].markAsTouched();
            }
        }
    }

    enterEmail(event: any, frmData){
        if(this.mailFrm.valid){
            if(localStorage.getItem('domain')){
                let domain = JSON.parse(localStorage.getItem('domain'));

                let phoneParam = this.phone;
                phoneParam = phoneParam.replace('(', '');
                phoneParam = phoneParam.replace(')', '');
                phoneParam = phoneParam.replace('-', '');
                phoneParam = phoneParam.replace(' ', '');

                let param = {
                    'Customer[full_name]': this.updateRes['result']['full_name'],
                    'Customer[email]': frmData.mail,
                    'phone': phoneParam
                }

                let urlParam = 'http://'+ domain + this.host + '/api/v1/customer/update';

                if(localStorage.getItem('token')){
                    let token = JSON.parse(localStorage.getItem('token'));

                    this._httpService.postAPI(urlParam, token, param).subscribe(
                        res => {
                            this.updateRes = JSON.parse(res._body);
                            console.log(this.updateRes['status']);
                            this.submitCheckin();

                        },
                        error => {
                        },
                        () => {

                        }
                    );
                }
            }
        } else {
            for (let i in this.mailFrm.controls) {
                this.mailFrm.controls[i].markAsTouched();
            }
        }
    }

    // Skip step email
    skipStep(event: any){
        this.submitCheckin();
    }

    submitCheckin(){
        if(localStorage.getItem('domain')){
            let domain = JSON.parse(localStorage.getItem('domain'));

            let phoneParam = this.phone;
            phoneParam = phoneParam.replace('(', '');
            phoneParam = phoneParam.replace(')', '');
            phoneParam = phoneParam.replace('-', '');
            phoneParam = phoneParam.replace(' ', '');

            let urlParam = 'http://'+ domain + this.host + '/api/v1/customer/check-in-submit?phone=' + phoneParam;

            if(localStorage.getItem('token')){
                let token = JSON.parse(localStorage.getItem('token'));

                this._httpService.getAPI(urlParam, token).subscribe(
                    res => {
                        console.log(res);
                        this.checkinDOne = res;
                        console.log(this.checkinDOne);

                        if(this.checkinDOne['status'] == 'OK'){
                            this.enterMail = false;
                            this.done = true;
                        }
                    },
                    error => {
                    },
                    () => {

                    }
                );
            }
        }
    }
}
