/**
 * Draw Circle Chart 
 * Create by     : mnwp
 * Created date  : 10.15.14
 * ======================================================
 * HTML DEMO     :
 * <div class="chrt" id="graph" data-percent="88"></div>
 * @Param - [ elem ] - string - '.chrt'
 */
function drawCircle (elem) {
    if (!$(elem).length) {return;}

    var el = $(elem); // get canvas

    el.each(function (n,e) {
        el = $(this);

        var options = {
            percent:   el.data('percent') || 25,
            size:      el.data('size')    || 158,
            lineWidth: el.data('line')    || 15,
            rotate:    el.data('rotate')  || 0,
            bgColor:   el.data('bgcolor')   || '#dbffeb',
            lineColor: el.data('linecolor') || '#abf587',
            useSpan :  el.data('useSpan')   || false
        }

        var canvas = document.createElement('canvas');
        var span = document.createElement('span');
        span.textContent = options.percent + '%';
            
        if (typeof(G_vmlCanvasManager) !== 'undefined') {
            G_vmlCanvasManager.initElement(canvas);
        }

        var ctx = canvas.getContext('2d');
        canvas.width = canvas.height = options.size;

        if (options.useSpan) {
            el.append(span);
        }        
        el.append(canvas);

        ctx.translate(options.size / 2, options.size / 2); // change center
        ctx.rotate((-1 / 2 + options.rotate / 180) * Math.PI); // rotate -90 deg

        //imd = ctx.getImageData(0, 0, 240, 240);
        var radius = (options.size - options.lineWidth) / 2;

        var drawCircle = function(color, lineWidth, percent) {
                percent = Math.min(Math.max(0, percent || 1), 1);
                ctx.beginPath();
                ctx.arc(0, 0, radius, 0, Math.PI * 2 * percent, false);
                ctx.strokeStyle = color;
                ctx.lineCap = 'butt'; // butt, round or square
                ctx.lineWidth = lineWidth
                ctx.stroke();
        };

        drawCircle(options.bgColor, options.lineWidth, 100 / 100);
        drawCircle(options.lineColor, options.lineWidth, options.percent / 100);
    });    
}
/**
 * RUN Drawing
 */
jQuery(document).ready(function($){
//    drawCircle ('.chrt');
});
