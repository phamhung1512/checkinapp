(function( $ ) {
/**
 * START - ONLOAD - JS
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */

// 1. Show/hide menu
function showHideMenu(){
    // Show/hide parent item
    $('.menu-itm').each(function(){
        $(this).on('click', function(e){
            if($(this).closest('li').hasClass('active')){
                $(this).closest('li').removeClass('active');
                $(this).siblings('.sub').removeClass('active');
            } else {
                $(this).closest('li').siblings('li').removeClass('active');
                $(this).closest('li').siblings('li').find('.sub').removeClass('active');

                $(this).closest('li').addClass('active');
                $(this).siblings('.sub').addClass('active');
            }
        });
    });

    // Sub menu item click
    $('.sub li a').each(function(){
        $(this).on('click', function(e){
            $('.sub li').removeClass('active');
            $(this).closest('li').addClass('active');
        });
    });
}


// 2. Filter list
function filterList(){
    $('.filter-slt .display').each(function(){
        $(this).on('click', function(e){
            if($(this).hasClass('active')){
                $(this).removeClass('active');
                $(this).siblings('.f-lst').removeClass('shw');
            } else {
                $('.f-lst').removeClass('shw');
                $(this).addClass('active');
                $(this).siblings('.f-lst').addClass('shw');
            }
        });
    });

    $('.f-lst li a').each(function(){
        $(this).on('click', function(e){
            $(this).closest('li').siblings('li').find('a').removeClass('active');
            $(this).addClass('active');

            var text = $(this).attr('data-text');

            if(!$(this).hasClass('tag-itm')){
                $(this).closest('.f-lst').siblings('.display').find('b').text(text);
            } else {
                if(text != 'all'){
                    $(this).closest('.f-lst').siblings('.display').find('b').text('Tag: ');
                    $(this).closest('.f-lst').siblings('.display').addClass(text);
                } else {
                    $(this).closest('.f-lst').siblings('.display').removeClass('red-tag yellow-tag');
                    $(this).closest('.f-lst').siblings('.display').find('b').text('Tag: All');
                }
            }
        });
    });

    $(".filter").bind( "clickoutside", function(event){
        $('.f-lst').removeClass('shw');
        $('.display').removeClass('active');
    });
}

// 3. Amcharts
function callAmcharts(){
    AmCharts.makeChart('chartdiv',
        {
            "type": "pie",
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "colors": [
                "#f26858",
                "#41bd45",
                "#4789ff",
                "#8f9aaa",
                "#f0ad4e"
            ],
            "responsive": {
                "enabled": true,
                "rules": [
                    // at 400px wide, we hide legend
                    {
                        "maxWidth": 400,
                        "overrides": {
                            "legend": {
                            "enabled": false
                            }
                        }
                    }
                ]
            },
            "innerRadius": "60%",
            "titleField": "category",
            "startEffect": "easeOutSine",
            "valueField": "column-1",
            "labelText": "",
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                // "markerType": "circle",
                "position": "bottom",
                // "marginLeft": 100,
                "autoMargins": false,
                // "valueWidth": 100
            },
            "titles": [],
            "dataProvider": [
                {
                    "category": "Vip",
                    "column-1": 420
                },
                {
                    "category": "New",
                    "column-1": 550
                },
                {
                    "category": "Regular",
                    "column-1": 2500
                },
                {
                    "category": "At Risk",
                    "column-1": "1400"
                },
                {
                    "category": "Normal",
                    "column-1": "500"
                }
            ]
        }
    );
    AmCharts.makeChart('chartdiv2',
        {
            "type": "pie",
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "colors": [
                "#f26858",
                "#41bd45",
                "#4789ff",
                "#8f9aaa",
                "#f0ad4e"
            ],
            "responsive": {
                "enabled": true,
                "rules": [
                    // at 400px wide, we hide legend
                    {
                        "maxWidth": 400,
                        "overrides": {
                            "legend": {
                            "enabled": false
                            }
                        }
                    }
                ]
            },
            "innerRadius": "60%",
            "titleField": "category",
            "startEffect": "easeOutSine",
            "valueField": "column-1",
            "labelText": "",
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "align": "center",
                // "markerType": "circle",
                "position": "bottom",
                // "marginLeft": 100,
                "autoMargins": false,
                // "valueWidth": 100
            },
            "titles": [],
            "dataProvider": [
                {
                    "category": "Vip",
                    "column-1": 420
                },
                {
                    "category": "New",
                    "column-1": 550
                },
                {
                    "category": "Regular",
                    "column-1": 2500
                },
                {
                    "category": "At Risk",
                    "column-1": "1400"
                },
                {
                    "category": "Normal",
                    "column-1": "1400"
                }
            ]
        }
    );
}

// 4. Open modal
function openModal(){
    $('.create-nw').on('click', function(e){
        var target = $(this).attr('data-tg');

        $('#' + target).addClass('shw');
    });

    // Close modal
    $('.close-c-modal').on('click', function(e){
        $(this).closest('.cre-pro-modal').removeClass('shw');
    });

    // Promotion v2 - open detail modal
    $('.open-dt-modal').each(function(){
        $(this).on('click', function(e){
            var target = $(this).attr('data-target');
            $('#' + target).addClass('shw');
        });
    });

    $('.close-modal-btn').each(function(){
        $(this).on('click', function(e){
            $(this).closest('.cre-pro-modal').removeClass('shw');
        });
    });

}

// 5. Switch text/mail mode
function switchMode(){
    $('.c-ft a').each(function(){
        $(this).on('click', function(e){
            var target = $(this).attr('data-tg');
            
            $('.c-ft a').removeClass('active');
            $('.r-mode').removeClass('active');

            $(this).addClass('active');
            $('#' + target + '-mode').addClass('active');
        });
    });

    //
    $('.switch-btn a').each(function(){
        $(this).on('click', function(e){
            $('.switch-btn a').removeClass('active');

            $(this).addClass('active');
        });
    });
}

// 6. Switcher show modal
function switchModal(){
    $('.switcher .lbl-switch input[type="checkbox"]').each(function(){
        $(this).on('click', function(e){
            var modal = $(this).attr('data-mode');
            if($(this).prop('checked') == true){
                $('#active-' + modal).modal('show');
            } else {
                $('#inactive-' + modal).modal('show');
            }
        });
    });
}

// 7. Time range
function timerange(){
    $('.time-btn').each(function(){
        $(this).on('click', function(e){
            $('.time-btn').removeClass('active');
            $(this).addClass('active');
        });
    });
}

// 8. Overall chart
function overallChart(){
    AmCharts.makeChart('overall-chart',
        {
            "type": "serial",
            "categoryField": "category",
            "colors": [
                "#4789ff",
                "#2a66d2"
            ],
            "startDuration": 1,
            "startEffect": "easeOutSine",
            "usePrefixes": true,
            "categoryAxis": {
                "gridPosition": "start",
                "parseDates": true
            },
            "chartCursor": {
                "enabled": true,
                "cursorColor": "#000000"
            },
            "trendLines": [],
            "graphs": [
                {
                    "balloonText": "[[title]] of [[category]]:[[value]]",
                    "fillAlphas": 1,
                    "id": "AmGraph-1",
                    "title": "graph 1",
                    "type": "column",
                    "valueField": "column-1"
                },
                {
                    "balloonText": "[[title]] of [[category]]:[[value]]",
                    "fillAlphas": 1,
                    "id": "AmGraph-2",
                    "title": "graph 2",
                    "type": "column",
                    "valueField": "column-2"
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "stackType": "regular",
                    "title": ""
                }
            ],
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "useGraphSettings": true
            },
            "titles": [
                {
                    "id": "Title-1",
                    "size": 15,
                    "text": ""
                }
            ],
            "dataProvider": [
                {
                    "category": "6/6/2017",
                    "column-1": 8,
                    "column-2": 5
                },
                {
                    "category": "6/7/2017",
                    "column-1": 6,
                    "column-2": 7
                },
                {
                    "category": "6/8/2017",
                    "column-1": 2,
                    "column-2": 3
                },
                {
                    "category": "6/9/2017",
                    "column-1": "15",
                    "column-2": "5"
                },
                {
                    "category": "6/10/2017",
                    "column-1": "35",
                    "column-2": "25"
                },
                {
                    "category": "6/11/2017",
                    "column-1": "28",
                    "column-2": "18"
                },
                {
                    "category": "6/12/2017",
                    "column-1": "10",
                    "column-2": "30"
                },
                {
                    "category": "6/13/2017",
                    "column-1": "14",
                    "column-2": "8"
                },
                {
                    "category": "6/14/2017",
                    "column-1": "20",
                    "column-2": "16"
                }
            ]
        }
    );
    // Overall chart


    AmCharts.makeChart('reve-chart',
        {
            "type": "serial",
            "categoryField": "category",
            "colors": [
                "#2a66d2",
                "#4789ff"
            ],
            "startDuration": 1,
            "startEffect": "easeOutSine",
            "usePrefixes": true,
            "categoryAxis": {
                "gridPosition": "start",
                "parseDates": true
            },
            "chartCursor": {
                "enabled": true,
                "cursorColor": "#000000"
            },
            "trendLines": [],
            "graphs": [
                {
                    "balloonText": "[[title]] of [[category]]:[[value]]",
                    "fillAlphas": 1,
                    "id": "AmGraph-1",
                    "title": "graph 1",
                    "type": "column",
                    "valueField": "column-1"
                },
                {
                    "balloonText": "[[title]] of [[category]]:[[value]]",
                    "fillAlphas": 1,
                    "id": "AmGraph-2",
                    "title": "graph 2",
                    "type": "column",
                    "valueField": "column-2"
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "title": ""
                }
            ],
            "allLabels": [],
            "balloon": {},
            "legend": {
                "enabled": true,
                "useGraphSettings": true
            },
            "titles": [
                {
                    "id": "Title-1",
                    "size": 15,
                    "text": ""
                }
            ],
            "dataProvider": [
                {
                    "category": "8/1/2017",
                    "column-1": "522045",
                    "column-2": "676445"
                },
                {
                    "category": "8/2/2017",
                    "column-1": "66544",
                    "column-2": "125466"
                },
                {
                    "category": "8/3/2017",
                    "column-1": "287945",
                    "column-2": "310656"
                },
                {
                    "category": "8/4/2017",
                    "column-1": "234897",
                    "column-2": "644200"
                },
                {
                    "category": "8/5/2017",
                    "column-1": "678463",
                    "column-2": "32000"
                },
                {
                    "category": "8/6/2017",
                    "column-1": "156789",
                    "column-2": "641031"
                },
                {
                    "category": "8/7/2017",
                    "column-1": "576341",
                    "column-2": "71354"
                },
                {
                    "category": "8/8/2017",
                    "column-1": "356060",
                    "column-2": "125630"
                },
                {
                    "category": "8/9/2017",
                    "column-1": "131256",
                    "column-2": "102355"
                },
                {
                    "category": "8/10/2017",
                    "column-1": "654687",
                    "column-2": "984566"
                }
            ]
        }
    );
}

// 9. Responsive menu
function responsiveMenu(){
    $('.responsvie-btn').on('click', function(e){
        if($(this).hasClass('open')){
            $(this).removeClass('open');
            $(this).closest('.top-header').siblings('.main-menu').removeClass('shw');
            $(this).find('.fa').removeClass('fa-close').addClass('fa-bars');
        } else {
            $(this).addClass('open');
            $(this).closest('.top-header').siblings('.main-menu').addClass('shw');
            $(this).find('.fa').removeClass('fa-bars').addClass('fa-close');
        }
    });
}

/* ----------------------------------------------- */
/* ----------------------------------------------- */
/* OnLoad Page */
$(document).ready(function($){
    $('[data-toggle="tooltip"]').tooltip();
    drawCircle('.chrt');
    $('.r-click').on('click', function(){
        $('#promotion-performance-report').modal('show');
    });

    // Call date picker
    $('#prom-from').datetimepicker({
        showTodayButton: false,
        format: 'MM/DD/YYYY',
        keepOpen: true
    });
    $('#prom-to').datetimepicker({
        showTodayButton: false,
        format: 'MM/DD/YYYY',
        keepOpen: true
    });
    $('#reminder-date').datetimepicker({
        showTodayButton: false,
        format: 'MM/DD/YYYY',
        keepOpen: true
    });
    $('#special-date').datetimepicker({
        showTodayButton: false,
        format: 'MM/DD/YYYY',
        keepOpen: true
    });
    $('#frm-date').datetimepicker({
        showTodayButton: false,
        format: 'MM/DD/YYYY',
        keepOpen: true
    });
    $('#to-date').datetimepicker({
        showTodayButton: false,
        format: 'MM/DD/YYYY',
        keepOpen: true
    });
    // 1. Show/hide menu
    showHideMenu();
    // 2. Filter list
    filterList();
    // 3. Amcharts
    callAmcharts();
    // callAmcharts(chartdiv2);
    // 4. Open modal
    openModal();
    // 5. Switch text/mail mode
    switchMode();
    // 6. Switcher show modal
    switchModal();
    // 7. Time range
    timerange();
    // 8. Overall chart
    overallChart();
    // 9. Responsive menu
    responsiveMenu();
});
/* OnLoad Window */
var init = function () {

};
window.onload = init;

})(jQuery);
