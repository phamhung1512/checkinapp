(function( $ ) {
/**
 * START - ONLOAD - JS
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */


// Fix header
function fixHheader () {
    let menuHeight = $('.main-menu').height();

    $(window).scroll(function(){
        var scrollTop  = $(window).scrollTop();
        if(scrollTop >= menuHeight){
            $('.main-menu').addClass('fixed');
        } else {
            $('.main-menu').removeClass('fixed');
        }
    });
}

// Client slider
function clientSlider() {
     $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.slider-nav',
        pauseOnFocus: false,
        pauseOnHover: false,
        autoplay: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    arrows: true,
                    asNavFor: '.slider-nav',
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    arrows: true,
                    asNavFor: '.slider-nav',
                }
            },
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ],
    });
    $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        centerMode: true,
        focusOnSelect: true,
        arrows: false,
        pauseOnFocus: false,
        pauseOnHover: false,
        autoplay: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    autoplay: true,
                    arrows: true,
                    asNavFor: '.slider-for',
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    autoplay: true,
                    arrows: true,
                    asNavFor: '.slider-for',
                }
            },
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
}

// How we do it
function howDoItSlide() {
    $('.how-do-slide').slick({
        arrows: false,
        fade: true,
        pauseOnFocus: false,
        pauseOnHover: false,
        autoplay: true,
        dots: true
    });
}

// Show question block
function showQuestion() {
    $('.shw-question').on('click', function(e){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).find('.fa').removeClass('fa-close').addClass('fa-comments');
            $(this).siblings('.q-list').removeClass('shw');
        } else {
            $(this).addClass('active');
            $(this).find('.fa').addClass('fa-close').removeClass('fa-comments');
            $(this).siblings('.q-list').addClass('shw');
        }
    });

    // Click close button
    $('.close-blk').on('click', function(e){
        $('.shw-question').removeClass('active');
        $('.shw-question').find('.fa').removeClass('fa-close').addClass('fa-comments');
        $('.shw-question').siblings('.q-list').removeClass('shw');
    });
}

// Select question
function selectQuestion() {
    $('.q-itm').each(function(){
        $(this).on('click', function(e){
            $('.q-itm').removeClass('selected');
            $(this).addClass('selected');
        });
    });
}

// Menu responsive
function menuResponsive(){
    $('.menu-responsive').on('click', function(e){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).siblings('.menu').removeClass('shw');
        } else {
            $(this).addClass('active');
            $(this).siblings('.menu').addClass('shw');
        }
    });
}


/* ----------------------------------------------- */
/* ----------------------------------------------- */
/* OnLoad Page */
$(document).ready(function($){
    // Pause intro video
    $('#intro-video').get(0).pause();

    $('#intro-clip').on('shown.bs.modal', function () {
        $('#intro-clip').find('video').get(0).play();
    });

    $('#intro-clip').on('hide.bs.modal', function () {
        $('#intro-clip').find('video').get(0).pause();
    });

    // Check position of menu
    let menuHeight = $('.main-menu').height();

    $(window).scroll(function(){
        var scrollTop  = $(window).scrollTop();
        if(scrollTop >= menuHeight){
            $('.main-menu').addClass('fixed');
        } else {
            $('.main-menu').removeClass('fixed');
        }
    });

    // Fix header
    fixHheader ();
    // Client slider
    clientSlider();
    // How we do it
    howDoItSlide();
    // Show question block
    showQuestion();
    // Select question
    selectQuestion();
    // Menu responsive
    menuResponsive();
});
/* OnLoad Window */
var init = function () {

};
window.onload = init;

})(jQuery);
