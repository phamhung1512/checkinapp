(function( $ ) {
/**
 * START - ONLOAD - JS
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */


// Fix header
function fixHheader () {
    var menuHeight = $('.main-menu').height();

    $(window).scroll(function(){
        var scrollTop  = $(window).scrollTop();
        if(scrollTop >= menuHeight){
            $('.main-menu').addClass('fixed');
        } else {
            $('.main-menu').removeClass('fixed');
        }
    });
}

// Client slider
function clientSlider() {
     $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.slider-nav',
        pauseOnFocus: false,
        pauseOnHover: false,
        autoplay: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    arrows: true,
                    asNavFor: '.slider-nav',
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    arrows: true,
                    asNavFor: '.slider-nav',
                }
            },
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ],
    });
    $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        centerMode: true,
        focusOnSelect: true,
        arrows: false,
        pauseOnFocus: false,
        pauseOnHover: false,
        autoplay: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    autoplay: true,
                    arrows: true,
                    asNavFor: '.slider-for',
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    autoplay: true,
                    arrows: true,
                    asNavFor: '.slider-for',
                }
            },
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
}

// How we do it
function howDoItSlide() {
    $('.how-do-slide').slick({
        arrows: false,
        fade: true,
        pauseOnFocus: false,
        pauseOnHover: false,
        autoplay: true,
        dots: true
    });
}

// Show question block
function showQuestion() {
    $('.shw-question').on('click', function(e){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).find('.fa').removeClass('fa-close').addClass('fa-comments');
            $(this).siblings('.q-list').removeClass('shw');
        } else {
            $(this).addClass('active');
            $(this).find('.fa').addClass('fa-close').removeClass('fa-comments');
            $(this).siblings('.q-list').addClass('shw');
        }
    });

    // Click close button
    $('.close-blk').on('click', function(e){
        $('.shw-question').removeClass('active');
        $('.shw-question').find('.fa').removeClass('fa-close').addClass('fa-comments');
        $('.shw-question').siblings('.q-list').removeClass('shw');
    });
}

// Select question
function selectQuestion() {
    $('.q-itm').each(function(){
        $(this).on('click', function(e){
            $('.q-itm').removeClass('selected');
            $(this).addClass('selected');
        });
    });
}

// Menu responsive
function menuResponsive(){
    $('.menu-responsive').on('click', function(e){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).siblings('.menu').removeClass('shw');
        } else {
            $(this).addClass('active');
            $(this).siblings('.menu').addClass('shw');
        }
    });
}

// Home main slider
function mainSlider() {
    $('.m-slide').slick({
        arrows: false,
        fade: true,
        pauseOnFocus: false,
        pauseOnHover: false,
        autoplay: true,
        dots: false,
        autoplaySpeed: 5000
    });

    $('.t-slider').owlCarousel({
        items: 3,
        margin: 15,
        dots: true,
        autoplay: true,
        autoplayHoverPause: true,
        responsive:{
            768:{
                items:3,
            },
            480:{
                items:2,
            },
            320: {
                items: 1
            }
        }
    });
}

// Play modal video
function modalVideo() {
    $('.play-ico').each(function(){
        $(this).on('click', function(e){
            var clip_source = $(this).data('source');

            $('#intro-video').find('#src-video').attr('src', clip_source);
            $("#intro-video")[0].load();
            $('#intro-clip').modal('show');
            $('#intro-clip').find('video').get(0).play();
        });
    });

    // Close modal
    $('.close-modal').on('click', function(e){
        var modal = $(this).data('modal');
        $(modal).modal('hide');
        $('#intro-clip').find('video').get(0).pause();
    });
}

// Play test video
function playTestVideo() {
    $('.play-test').on('click', function (e) {
        $('#testimonial-video').get(0).play();
        $(this).addClass('hide');
    });

    $('#testimonial-video').get(0).addEventListener('pause', function(){
        $('.play-test').removeClass('hide');
    });

    $('#testimonial-video').get(0).addEventListener('play', function(){
        $('.play-test').addClass('hide');
    });
}

/* ----------------------------------------------- */
/* ----------------------------------------------- */
/* OnLoad Page */
$(document).ready(function($){
    // Pause intro video
    // $('#intro-video').get(0).pause();

    // $('#intro-clip').on('shown.bs.modal', function () {
    //     $('#intro-clip').find('video').get(0).play();
    // });

    $('#intro-clip').on('hide.bs.modal', function () {
        $('#intro-clip').find('video').get(0).pause();
    });

    // Check position of menu
    var menuHeight = $('.main-menu').height();

    $(window).scroll(function(){
        var scrollTop  = $(window).scrollTop();
        if(scrollTop >= menuHeight){
            $('.main-menu').addClass('fixed');
        } else {
            $('.main-menu').removeClass('fixed');
        }
    });

    // Fix header
    fixHheader ();
    // Client slider
    clientSlider();
    // How we do it
    howDoItSlide();
    // Show question block
    showQuestion();
    // Select question
    selectQuestion();
    // Menu responsive
    menuResponsive();
    // Home main slider
    mainSlider();
    // Play modal video
    modalVideo();
    // Play test video
    playTestVideo();
});
/* OnLoad Window */
var init = function () {

};
window.onload = init;

})(jQuery);
