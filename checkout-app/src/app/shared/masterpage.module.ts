import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// Import component

import { Sidebar } from '../sidebar';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,

  ],
  declarations: [
    // components
    Sidebar
  ],
  exports: [
    Sidebar,
    CommonModule,
    FormsModule
  ]
})

export class MasterpageModule { }
