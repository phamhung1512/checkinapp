import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterViewInit,
  Input,
  Output
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { AppState } from '../app.service';

// Import services
import * as _ from 'lodash';

// Import services
import { HttpServices } from '../services/http.service';
import { ValidationService } from '../services/validation.service';
import { ParamConfig } from '../services/encode';
import { ParamsList } from '../services/params';

declare var $: any;

@Component({
  selector: 'login',
  templateUrl: '../views/login/login.html',
  providers: [
    HttpServices,
    ValidationService
  ]
})
export class LoginComponent implements OnInit, AfterViewInit {
  loginFrm: FormGroup;
  loginStatus: any = '';

  constructor(
    public appState: AppState,
    private _router: Router,
    private _titleService: Title,
    private _httpService: HttpServices,
  ) {}

  public ngOnInit() {
  }

  ngAfterViewInit() {
    this.loginFrm = new FormGroup({
      username: new FormControl('', Validators.compose([Validators.required, ValidationService.NoWhitespaceValidator])),
      password: new FormControl('', Validators.compose([Validators.required, ValidationService.NoWhitespaceValidator])),
      domain: new FormControl('', Validators.compose([Validators.required, ValidationService.NoWhitespaceValidator])),
      subdomain: new FormControl(ParamsList.domain, Validators.compose([Validators.required, ValidationService.NoWhitespaceValidator])),
    });
  }

  // Login form
  submitLogin(event: any, frmData: any) {
    let thisComp = this;
    if (this.loginFrm.valid) {
      let loginParam = {
        username: frmData.username,
        password: ParamConfig.encodeURI(frmData.password)
      };
      // console.log(loginParam);
      let urlParam = 'http://' + frmData.domain + frmData.subdomain +'/api/v1/user/login';

      this._httpService.postAPINoToken(urlParam, loginParam).subscribe(
        res => {
          let token = JSON.parse(res._body);

          if(token.result.status == 'FAIL'){
            this.loginStatus = 'Login failed!';
          } else {
            this.loginStatus = '';
            localStorage.setItem('checkout-token', JSON.stringify(token.result['token']));
            localStorage.setItem('checkout-domain', JSON.stringify(frmData.domain + frmData.subdomain));

            this._router.navigate(['/checkout/checkout-list']);
          }

        },
        error => {
          console.log(error);

          this.loginStatus = 'Login failed!';
        },
        () => {
          console.log('Login successfully.');
        }
      );
    } else {
      for (let i in this.loginFrm.controls) {
        this.loginFrm.controls[i].markAsTouched();
      }
    }
  }
}
