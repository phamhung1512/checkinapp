import { Routes } from '@angular/router';
import { NoContentComponent } from './no-content';
import  { LoginComponent } from  './login';

import { DataResolver } from './app.resolver';

export const ROUTES: Routes = [
  { path: '', loadChildren: './checkout/checkout.module#CheckoutModule'},
  { path: 'login', component: LoginComponent},
  { path: '**', component: NoContentComponent },
];
