import {
  Component,
  ViewEncapsulation,
  OnInit,
  AfterViewInit,
  Output,
  Input,
  EventEmitter,
  OnChanges,
  SimpleChange
} from '@angular/core';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpServices } from '../services/http.service';

declare var $: any;

@Component({
  selector: 'sidebar-block',
  encapsulation: ViewEncapsulation.None,
  templateUrl: '../views/sidebar/sidebar.template.html',
  providers: [HttpServices],
  outputs: ['changeNotiList']
})
export class Sidebar implements OnInit,
  AfterViewInit {
  @Input() notify: any;
  notiList: any = [];
  noData: any = '';
  totalUnread: any = 0;
  token: any;
  domain: any;
  isAppoint: boolean = false;

  public changeNotiList = new EventEmitter();

  constructor(private _router: Router, private _httpService: HttpServices) { }

  ngOnInit() {
    $('.responsive-btn')
      .on('click', function (e) {
        if ($(this).hasClass('active')) {
          $(this).removeClass('active');
          $(this)
            .find('.fa')
            .removeClass('fa-close active')
            .addClass('fa-bars');
          $(this)
            .closest('.top-header')
            .siblings('.main-menu')
            .removeClass('shw');
        } else {
          $(this).addClass('active');
          $(this)
            .find('.fa')
            .removeClass('fa-bars active')
            .addClass('fa-close');
          $(this)
            .closest('.top-header')
            .siblings('.main-menu')
            .addClass('shw');
        }
      });

    if (localStorage.getItem('checkout-token')) {
      this.token = JSON.parse(localStorage.getItem('checkout-token'));

      if (localStorage.getItem('checkout-domain')) {
        this.domain = JSON.parse(localStorage.getItem('checkout-domain'));
        this.getUserInfo();
      }

      $('.tab-btn')
        .each(function () {
          $(this)
            .on('click', function (e) {
              let target = $(this).data('target');

              $('.tab-btn').removeClass('active');
              $(this).addClass('active');

              $('.tab-itm').removeClass('active');
              $('#' + target).addClass('active');
            });
        });
    }

    // if(localStorage.getItem('config')) {
    //   let config = JSON.parse(localStorage.getItem('config'));
    //   if(config['appointment_feature'] === '1') {
    //     this.isAppoint = true;
    //   } else {
    //     this.isAppoint = false;
    //   }
    // }
  }

  ngAfterViewInit() { }

  ngOnChanges(changes: {
    [propertyName: string]: SimpleChange
  }) {
    if (this.notify) {
      if (this.notify.status === false) {
        this.noData = 'No notify';
        this.notiList = [];
      } else {
        console.log(this.notify.list);
        let res = this.notify.list;

        if (res['result']['notifications'].length != 0) {
          this.noData = '';

          this.notiList = res['result']['notifications'];
          this.totalUnread = res['result']['total_unread'];
        } else {
          this.noData = 'No notify';
          this.totalUnread = 0;
          this.notiList = [];
        }
      }
      // if(this.notify.result['notifications'].length != 0) {   this.notiList =
      // this.notify.result['notifications']; }
    }
  }

  /**
   *
   * 1. Logout
   * -----------------------------
   * 2. Show notifications list
   * -----------------------------
   * 3. Set notifications isRead
   * -----------------------------
   * 4. Set all notifications isRead
   *
   * */

  // 1. Logout
  signout(event: any) {
    localStorage.clear();
    this
      ._router
      .navigate(['/login']);
  }

  // 2. Change menu link
  changeMenuLink(event: any, menu: string) {
    // localStorage.setItem('route', menu);
  }

  // 2. Show notifications list
  showNotiList(event: any) {
    if ($('.noti-list').hasClass('shw')) {
      $('.noti-list').removeClass('shw');
    } else {
      $('.noti-list').addClass('shw');
    }
  }

  // 3. Set notifications isRead
  setIsRead(event: any, id: any) {
    if (this.token !== '') {
      let url = 'http://' + this.domain + '/api/v1/appointment/set-notifications-read';
      let param: any = {
        notifications: id
      };

      this
        ._httpService
        .postAPI(url, this.token, param)
        .subscribe(res => {
          this.changeNotiList.emit({
            reload: true
          });
        }, error => {
          console.log(error);

        }, () => {
          console.log('');
        });
    }
  }

  //4. Set all notifications isRead
  setAllAsRead(event: any) {
    if (this.token !== '') {
      let url = 'http://' + this.domain + '/api/v1/appointment/set-notifications-read';
      for (let i = 0; i < this.notiList.length; i++) {
        if (this.notiList[i].isRead === 0) {
          let param: any = {
            notifications: this.notiList[i].id
          }
          this
            ._httpService
            .postAPI(url, this.token, param)
            .subscribe(res => {
              this.changeNotiList.emit({
                reload: true
              });
            }, error => {
              console.log(error);

            }, () => { //finally
            });
        }
      }
    }
  }

  getUserInfo() {
    if (this.token !== '') {
      let url = 'http://' + this.domain + '/api/v1/user/info';

      this
        ._httpService
        .getAPI(url, this.token)
        .subscribe(res => {
          if (res.result['settings']) {
            let setting = res.result['settings'];
            localStorage.setItem('config', JSON.stringify(res.result['settings']));

            if (setting['appointment_feature'] == '1') {
              this.isAppoint = true;
            } else {
              this.isAppoint = false;
            }
          }
        }, error => {
          console.log(error);
        }, () => {
          console.log('Get checkout list successfully.');
        });
    }
  }
}
