import { Customer } from './customer';
import { Checkout} from './checkout';
import { Appointment} from './appointment';
import * as moment from 'moment';

export class CheckIn {
  public customerInfo: Customer;
  public checkoutInf: Checkout;
  public promotion: any;
  public id: number;
  public status: number;
  public createAt: any;
  public customer_id: number;
  public services: any = [];

  constructor(
  ) {
    this.customerInfo = new Customer();
    this.checkoutInf = new Checkout();
  }

  get createAtTime(){
    return moment(this.createAt).format('hh:mm a').toString().toUpperCase();
  }
  get createAtDate() {
    return moment(this.createAt).format('MM/DD/YYYY');
  }
  get getId() {
    return this.id;
  }
  get serviceName() {
    if(this.services.length != 0) {
      for (let i = 0; i < this.services.length; i++) {
        let tmp = this.services[i]['name'].match(/\b(\w)/g);
        // this.services[i]['tmpName'] = this.services[i]['name'].substring(0, 5);
        this.services[i]['tmpName'] = tmp.join('');

        if(i === this.services.length - 1) {
          return this.services;
        }
      }
    }
  }

}
