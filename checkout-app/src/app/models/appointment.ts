import * as moment from 'moment';

export class Appointment {
  public id: number;
  public startTime: any;
  public endTime: any;
  public price: any;
  public status: any;
  public key: any;
  public booker: any;
  public bookerPhone: any;
  public customerId: any;
  public empId: any;
  public discount: number;

  constructor(
  ) {
  }

  bookingTime() {
    return moment(this.startTime).format('h:mm:ss a');
  }
  bookingDate() {
    return moment(this.startTime).format('MM/DD/YYYY');
  }

}
