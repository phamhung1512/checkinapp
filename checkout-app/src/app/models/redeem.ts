import * as moment from 'moment';

export class Redeem {
  public id;
  public point = 0;
  public content = '';
  public isActive;
  public logicType = '';
  public logicValue = '';
  public createdAt = '';
  public updatedAt = '';

  constructor(
  ) {
  }
}
