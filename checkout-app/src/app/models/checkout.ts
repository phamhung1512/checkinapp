import { Customer } from './customer';
import * as moment from 'moment';

export class Checkout {
  public checkin_id: any;
  public created_at: any;
  public customer_id: number;
  public discount: number;
  public id: any;
  public note: any;
  public point_bonus: any;
  public status: any;
  public pay: any;
  public update_at: any;

  constructor(
  ) {
  }

  get checkoutTime(){
    return moment(this.update_at).format('MM/DD/YYYY');
  }

  get checkoutDate() {
    return moment(this.update_at).format('hh:mm a').toString().toUpperCase();
  }

}
