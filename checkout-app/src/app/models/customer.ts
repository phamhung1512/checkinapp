import * as moment from 'moment';
export class Customer {
  public name = '';
  public phone = '';
  public email = '';
  public point = 0;
  public lifetimePoint = 0;
  public type = '';
  public lastVisited = '';
  public numberCheckin = '';
  public yelp: any;
  public last_review: any;
  public full_review: any = '';
  public id: any;
  public note: any;

  constructor(
  ) {
  }

  get tag() {
    let result = '';
    if (this.yelp) {
      result = 'yellow';
      if (this.yelp.is_elite === 1) {
        result = 'red';
      }
    }
    return result;
  }

  get customerImg() {
    let img = '';
    if (this.yelp) {
      img = this.yelp.image;
    }
    return img;
  }
  get formatPhone() {
    let value = this.phone.trim();
    let country, city, phoneNumber;

    switch (value.length) {
      case 10: // +1PPP####### -> C (PPP) ###-####
        country = 1;
        city = value.slice(0, 3);
        phoneNumber = value.slice(3);
        break;

      case 11: // +CPPP####### -> CCC (PP) ###-####
        country = value[0];
        city = value.slice(1, 4);
        phoneNumber = value.slice(4);
        break;

      case 12: // +CCCPP####### -> CCC (PP) ###-####
        country = value.slice(0, 3);
        city = value.slice(3, 5);
        phoneNumber = value.slice(5);
        break;
      default: return value;
    }

    if (country === 1) {
      country = '';
    }

    phoneNumber = phoneNumber.slice(0, 3) + '-' + phoneNumber.slice(3);

    return (country + ' (' + city + ') ' + phoneNumber).trim();
  }

  get maskPhone() {
    let mask = this.phone.substring(0, 6);
    let show = this.phone.substring(6);
    let country, city, phoneNumber;

    mask = mask.replace(/[0-9]/g, '*');

    let res = mask + show;

    city = res.slice(0, 3);
    phoneNumber = res.slice(3);

    phoneNumber = phoneNumber.slice(0, 3) + ' - ' + phoneNumber.slice(3);

    return ('(' + city + ') ' + phoneNumber).trim();
  }

  get lastReviewDate() {
    if (this.last_review) {
      return moment(this.last_review.time_created).format('MM/DD/YY');
    }
    return "";
  }
  get lastReviewContent() {
    if (this.last_review) {
      var more_text = '';
      if (this.last_review.content.length > 50) {
        more_text = '...';
      }
      return this.last_review.content.substring(0, 50) + more_text;
    }
    return "";
  }
  get lastReviewStarRender() {
    var count = 0;
    var str_render = '';
    if (this.last_review) {
      for (var i = 0; i < 5; i++) {
        if (i < this.last_review.rating) {
          str_render += '<span class="fa fa-star star-rate active"></span>'
        }
        else {
          str_render += '<span class="fa fa-star star-rate"></span>'
        }

      }
    }
    return str_render;
  }

}
