import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterViewInit,
  Input,
  Output, OnChanges, SimpleChange
} from '@angular/core';

import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AppState } from '../../app.service';

// Import services
import * as _ from 'lodash';

// Import services

import * as moment from 'moment';
import 'bootstrap/js/modal';

declare var $: any;

@Component({
  selector: 'toas',
  templateUrl: '../views/toas/toas.html',
  providers: [
  ]
})
export class Toas implements OnInit, AfterViewInit {
  @Input() option: any = {
    position: 'top-right',
    message: '',
    title: '',
    type: '',
    timeout: 3000,
  };

  @Input() isShow: boolean = false;

  classCss: any = 'alert-success';

  constructor(
  ) { }

  public ngOnInit() {

  }

  ngAfterViewInit() {
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    if(this.isShow == true) {

      let thisComp = this;

      if(this.option.position == '') {
        this.option.position = 'top-right';
      }

      switch(this.option.type) {
        case 'success': this.classCss = 'alert-success'; break;
        case 'error': this.classCss = 'alert-danger'; break;
        case 'info': this.classCss = 'alert-info'; break;
        case 'warning': this.classCss = 'alert-warning'; break;
      }

      setTimeout(function() {
        thisComp.isShow = false;
      }, this.option.timeout);
    }
  }
  /**
   *
   *
   */


}
