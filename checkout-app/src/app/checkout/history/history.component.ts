import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterViewInit,
  Input,
  Output
} from '@angular/core';

import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AppState } from '../../app.service';

// Import services
import * as _ from 'lodash';

// Import services
import { HttpServices } from '../../services/http.service';
import { ValidationService } from '../../services/validation.service';
import { ParamConfig } from '../../services/encode';

import { CheckIn } from '../../models/checkin';
import { Customer } from '../../models/customer';
import * as moment from 'moment';
import '../../loader/datepicker.loader';
import 'bootstrap/js/modal';

declare var $: any;

@Component({
  selector: 'history-list',
  templateUrl: '../../views/checkout/history.html',
  providers: [
    HttpServices,
    ValidationService
  ]
})
export class HistoryList implements OnInit, AfterViewInit {
  token = '';
  domain = '';
  checkinList: any = [];
  resData: any = [];
  reloadData: any = [];
  summary: any;
  typeNew: any = 0;
  typeRegular: any = 0;
  typeVip: any = 0;
  totalType: any = 0;

  userDetail: any;
  frmSearch: FormGroup;
  noteFrm: FormGroup;

  isSending: boolean = false;
  noteStatus: any = '';

  dataStatus: any = {
    text: '',
    class: ''
  }

  date: any;

  apiUrl: any = '';

  apiParam: any = {
    url: ''
  }

  constructor(
    public appState: AppState,
    private _router: Router,
    private _titleService: Title,
    private _httpService: HttpServices,
  ) {}

  public ngOnInit() {
    if (localStorage.getItem('checkout-token')) {
      this._router.navigate(['/checkout/history']);

      this.token = JSON.parse(localStorage.getItem('checkout-token'));

      if (localStorage.getItem('checkout-domain')) {
        this.domain = JSON.parse(localStorage.getItem('checkout-domain'));

        this.apiParam.url = 'http://' + this.domain + '/api/v1/check-out/check-out-list-v2';

        this.getHistory();

        this.frmSearch = new FormGroup({
          searchTxt: new FormControl('')
        });

        let thisComp = this;

        $('#filter-date').datetimepicker({
          showTodayButton: false,
          format: 'MM/DD/YYYY',
          keepOpen: true
        });

      }
    } else {
      this._router.navigate(['/login']);
    }
  }

  ngAfterViewInit() {
  }

  // 1. Get history list
  getHistory(date ?: any){
    if (this.token !== '') {
      let url = this.apiParam.url;

      this.dataStatus.text = '';
      this.dataStatus.class = '';

      this._httpService.getAPI(url, this.token).subscribe(
        res => {
          // console.log(res);
          let resArr = [];
          this.checkinList = [];
          this.summary = [];
          this.resData = [];
          if(res.result['check_out']){
            resArr = res.result['check_out'];
            this.resData = res.result['check_out'];
            // console.log(resArr);

            for(let i = 0; i < resArr.length; i++) {
              let tmp = new CheckIn();
              let customerItm = tmp.customerInfo;
              let checkoutInf = tmp.checkoutInf;

              if(resArr[i]['customer']['type'] == 'new') {
                this.typeNew++;
              } else if(resArr[i]['customer']['type'] == 'vip') {
                this.typeVip++;
              } else if(resArr[i]['customer']['type'] == 'regular') {
                this.typeRegular++;
              }

              customerItm.name = resArr[i]['customer']['name'];
              customerItm.phone = resArr[i]['customer']['phone'];
              customerItm.email = resArr[i]['customer']['email'];
              customerItm.point = resArr[i]['customer']['point'];
              customerItm.type = resArr[i]['customer']['type'];
              customerItm.lifetimePoint = resArr[i]['customer']['lifetime_point'];
              customerItm.lastVisited = resArr[i]['customer']['last_visited'];
              customerItm.numberCheckin = resArr[i]['customer']['number_checkin'];
              customerItm.yelp = resArr[i]['customer']['yelp'];

              checkoutInf.update_at = resArr[i]['check_out']['update_at'];
              checkoutInf.pay = resArr[i]['check_out']['total_price'];
              checkoutInf.point_bonus = resArr[i]['check_out']['point_bonus'];
              checkoutInf.id = resArr[i]['check_out']['id'];
              checkoutInf.note = resArr[i]['check_out']['note'];

              tmp['redeemInf'] = resArr[i]['redeem'];
              tmp['survey'] = resArr[i]['survey'];
              tmp['staff'] = resArr[i]['staff'];
              tmp['services'] = this.resData[i]['services'];

              tmp.createAt = resArr[i]['check_in']['create_at'];
              tmp.id = resArr[i]['check_in']['id'];
              tmp.status = resArr[i]['check_in']['status'];
              tmp.promotion = resArr[i]['promotion'];
              tmp.customer_id = resArr[i]['check_in']['customer_id'];

              // console.log(tmp)
              this.checkinList.push(tmp);

              if(i == (resArr.length - 1)) {
                this.totalType = this.typeNew + this.typeVip + this.typeRegular;

                this.typeNew = (this.typeNew / this.totalType) * 100;
                this.typeVip = (this.typeVip / this.totalType) * 100;
                this.typeRegular = (this.typeRegular / this.totalType) * 100;
              }
            }

            this.summary = res.result['summary'];

            // console.log(this.checkinList);
          } else {
            this.dataStatus.text = 'No data';
            this.dataStatus.class = 'text-info';
          }
        },
        error => {
          console.log(error);

          this.dataStatus.text = '';
          this.dataStatus.class = '';
        },
        () => {
          console.log('Get history list successfully.');
        }
      );
    }
  }

  // 2. Filter list
  filterHistory(event) {
    let filter = _.trim($(event.target).val());

    this.dataStatus.text = '';
    this.dataStatus.class = '';

    if(filter != '') {
      let tmpFilter = [];
      for(let i = 0; i < this.resData.length; i++) {
        let tmp = new CheckIn();
        let customerItm = tmp.customerInfo;
        if(this.resData[i]['customer']['type'] == filter) {
          customerItm.name = this.resData[i]['customer']['name'];
          customerItm.phone = this.resData[i]['customer']['phone'];
          customerItm.email = this.resData[i]['customer']['email'];
          customerItm.point = this.resData[i]['customer']['point'];
          customerItm.type = this.resData[i]['customer']['type'];
          customerItm.lifetimePoint = this.resData[i]['customer']['lifetime_point'];
          customerItm.lastVisited = this.resData[i]['customer']['last_visited'];
          customerItm.numberCheckin = this.resData[i]['customer']['number_checkin'];
          customerItm.yelp = this.resData[i]['customer']['yelp'];

          tmp.createAt = this.resData[i]['check_in']['create_at'];
          tmp.id = this.resData[i]['check_in']['id'];
          tmp.status = this.resData[i]['check_in']['status'];
          tmp.promotion = this.resData[i]['promotions'];
          tmp.customer_id = this.resData[i]['check_in']['customer_id'];
          tmpFilter.push(tmp);


        }

        if(i == this.resData.length - 1) {
          if(tmpFilter.length != 0) {
            this.checkinList = tmpFilter;
          } else {
            this.checkinList = [];
            this.dataStatus.text = 'No data';
            this.dataStatus.class = 'text-info';
          }
        }
      }
    } else {
      this.getHistory();
    }
  }

  // 3. Show detail
  showDetail(event: any, id: any) {
    for(let i = 0; i < this.checkinList.length; i++) {
      if (this.checkinList[i]['id'] === id) {
        this.userDetail = this.checkinList[i];
        $('#history-detail').modal('show');
        this.getNote(this.userDetail['customerInfo']['phone']);

        console.log(this.userDetail);
      }
    }
  }

  closeModal(event: any) {
    $('#history-detail').modal('hide');
  }

  search(event: any, frmData: any) {
    if(this.frmSearch.valid) {
      let searchTxt = _.trim($('.search-ipt').val());
      let result = [];

      this.dataStatus.text = '';
      this.dataStatus.class = '';


      this.apiParam.url = 'http://' + this.domain + '/api/v1/check-out/check-out-list-v2?search=' + ParamConfig.encodeURI(frmData.searchTxt);

      this.getHistory();


    } else {

    }
  }
  // 13.1 Sort
  sortCol(event: any, type: any){
    let asc;
    let thisComp = this;

    if($('.col-' + type).find('.fa').hasClass('fa-caret-down')) {
      asc = true;
      $('.col-' + type).find('.fa').removeClass('fa-caret-down').addClass('fa-caret-up');
    } else {
      asc = false;
      $('.col-' + type).find('.fa').removeClass('fa-caret-up').addClass('fa-caret-down');
    }

    switch (type) {
      case 'id': this.sortData('id', asc); break;
      case 'name': this.sortData('name', asc); break;
      case 'checkintime': this.sortData('lastVisited', asc); break;
      case 'point': this.sortData('point', asc); break;
      case 'mem-type': this.sortData('type', asc); break;
      case 'tag': this.sortData('tag', asc); break;
    }
  }
  // 13.2 Sort data
  sortData(type: any, asc: any) {
    if(type === 'id') {
      if(asc === false) {
        this.checkinList = _.sortBy(this.checkinList, function(n){
          return n['id'];
        });
      } else {
        this.checkinList = _.sortBy(this.checkinList, function(n){
          return n['id'];
        }).reverse();
      }
    } else {
      if(asc === false) {
        this.checkinList = _.sortBy(this.checkinList, function(n){
          return n['customerInfo'][type];
        });
      } else {
        this.checkinList = _.sortBy(this.checkinList, function(n){
          return n['customerInfo'][type];
        }).reverse();
      }
    }

  }

  // 14. Save note
  saveNote(event: any, frmData: any, phone: any) {
    if (this.token !== '') {
        let url = 'http://' + this.domain + '/api/v1/customer/update';

        let param = {
          phone: phone,
          'Customer[note]': frmData.note
        };
        this.isSending = true;

        this._httpService.postAPI(url, this.token, param).subscribe(
          res => {
            this.noteStatus = 'Save note successfully.';

            let thisComp = this;

            setTimeout(function() {
              thisComp.noteStatus = '';
            }, 2000);

            this.isSending = false;
          },
          error => {
            console.log(error);
            this.isSending = false;
          },
          () => {
            console.log('Update note successfully.');
          }
        );

    }
  }

  getNote(phone: any) {
    if (this.token !== '') {
        let url = 'http://' + this.domain + '/api/v1/customer/info?phone=' + phone;

        this._httpService.getAPI(url, this.token).subscribe(
          res => {
            let result = res['result']['customer'];

            this.noteFrm = new FormGroup({
              note: new FormControl(result['note'] ? result['note'] : '')
            });
          },
          error => {
            console.log(error);

          },
          () => {
            console.log('Get note successfully.');
          }
        );

    }
  }

  update() {
    // console.log($('#filter-date').val());
    let date = $('#filter-date').val();

    if(date == '') {
      this.apiParam.url = 'http://' + this.domain + '/api/v1/check-out/check-out-list-v2';
    } else {
      this.apiParam.url = 'http://' + this.domain + '/api/v1/check-out/check-out-list-v2?date=' + date + '&limit=100';
    }
    
    this.getHistory();
  }

  filterDate(event: any) {
    $('#filter-date').data("DateTimePicker").show();
  }

  showFullNameServ(event: any) {
    if ($(event.target).hasClass('active')) {
      let txt = $(event.target).attr('data-tmp-name');
      $(event.target).text(txt.toUpperCase());
      $(event.target).removeClass('active');
    } else {
      $(event.target).text($(event.target).attr('data-full-name'));
      $(event.target).addClass('active');
    }
  }
}
