import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';

// Component
import { CheckoutList } from './checkout-list/checkout-list.component';
import { HistoryList } from './history/history.component';
import { Appointment } from './appointment/appointment.component';

const routes: Routes = [
  { path: '', component: CheckoutList},
  { path: 'checkout/checkout-list', component: CheckoutList },
  { path: 'checkout/history', component: HistoryList },
  { path: 'appointment', component: Appointment },

  // { path: 'dashboard/tiles', component: DashboardTiles},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckoutRoute { }
