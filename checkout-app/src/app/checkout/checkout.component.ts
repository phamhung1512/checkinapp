import { Component, ViewEncapsulation, OnInit, AfterViewInit } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import { Router }    from '@angular/router';

declare var $: any;

@Component({
  selector: 'checkout',
  encapsulation: ViewEncapsulation.None,
  templateUrl: '../views/checkout/checkout.html'
})
export class Checkout implements OnInit, AfterViewInit {

  constructor(
    private _titleService: Title,
    private _router: Router
  ) {
    // Check login
    // if(localStorage.getItem('userLog')){
    //     _router.navigate(['dashboard']);
    // } else {
    //     _router.navigate(['login']);
    // }
  }

  ngOnInit() {
    // set Title Page
    // this._titleService.setTitle('AutomationIQ - Dashboard Grid');
  }

  ngAfterViewInit(){
  }
}
