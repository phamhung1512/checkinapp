import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterViewInit,
  Input,
  Output
} from '@angular/core';

import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AppState} from '../../app.service';

// Import services
import * as _ from 'lodash';

// Import services
import {HttpServices} from '../../services/http.service';
import {ValidationService} from '../../services/validation.service';

import {CheckIn} from '../../models/checkin';
import {Redeem} from '../../models/redeem';
import {Customer} from '../../models/customer';
import * as moment from 'moment';
import 'bootstrap/js/modal';

declare var $ : any;

@Component({
  selector: 'checkout-list',
  templateUrl: '../../views/checkout/checkout-list.html',
  providers: [HttpServices, ValidationService]
})
export class CheckoutList implements OnInit,
AfterViewInit {
  token = '';
  domain = '';
  promotionCode : any = '';
  staffCode : any = '';
  disabled : any = '';
  noAmount : any = '';
  warning : any = '';
  sltRedemMsg : any = 'Select 0 redeem(s)';
  searchText : any = '';

  amountInput : any = '';

  totalAmountInput : any = 0;
  total : any = 0;
  pointRedeem : any = 0;
  pricePromoDiscountTotal : any = 0;
  priceRedeemDiscountTotal : any = 0;
  userDetail : any;

  resData : any = [];
  checkinList : any = [];
  redeemList : any = [];
  redeemSlt : any = [];
  promotionSlt : any = undefined;
  reloadData : any = [];

  checkoutStatus : any = {
    class: '',
    text: ''
  }

  dataStatus : any = {
    class: '',
    text: ''
  }

  option : any = {
    position: '',
    message: '',
    title: '',
    type: '',
    timeout: 0
  }

  isShow : boolean = false;

  noteStatus : any = '';

  sending : boolean = false;
  checkHasDot : boolean = false;
  frmSearch : FormGroup;
  noteFrm : FormGroup;

  filterStr : any = '';
  isSending : boolean = false;

  checkout_feature : number;
  checkout_msg : any = '';

  review: any;

  hasStaffCode: any = '';
  timer: any;
  isAppoint: boolean = false;
  isServ: boolean = false;

  constructor(public appState : AppState, private _router : Router, private _titleService : Title, private _httpService : HttpServices,) {}

  public ngOnInit() {
    if (localStorage.getItem('checkout-token')) {
      this
        ._router
        .navigate(['/checkout/checkout-list']);
      this.token = JSON.parse(localStorage.getItem('checkout-token'));

      if (localStorage.getItem('checkout-domain')) {
        this.domain = JSON.parse(localStorage.getItem('checkout-domain'));

        this.getList();
        this.getUserInfo();
        this.frmSearch = new FormGroup({searchTxt: new FormControl('')});

        let thisComp = this;
        this.timer = setInterval(function () {
          thisComp.reloadRealtime();
        }, 10000);
      }
    } else {
      this
        ._router
        .navigate(['/login']);
    }
  }

  ngAfterViewInit() {}

  ngOnDestroy() {
    clearInterval(this.timer);
  }

  /**
   *
   * 1. Get checkout list
   * ----------------------------------
   * 2. Show user info
   * ----------------------------------
   * 3.1 Get redeem list
   * 3.2 Show redeem popup list
   * ----------------------------------
   * 4. Close modal
   * ----------------------------------
   * 5.1 Show confirm checkout
   * 5.2 Cancel confirm checkout
   * 5.3 Click numb button
   * 5.4 Keypress staff code input
   * 5.5 Del numb
   * ----------------------------------
   * 6. Enter amount
   * ----------------------------------
   * 7. Select promotion
   * ----------------------------------
   * 8. Calculate discount
   * ----------------------------------
   * 9. Select redeem
   * ----------------------------------
   * 10. Send checkout
   * ----------------------------------
   * 11. Filter list
   * ----------------------------------
   * 12. Search
   * ----------------------------------
   * 13.1 Sort
   * 13.2 Sort data
   * ----------------------------------
   * 14. Clickout
   * ----------------------------------
   * 15. Reload realtime
   * ----------------------------------
   * 16. Get user info
   *
   */

  // 1. Get checkout list
  getList() {
    if (this.token !== '') {
      let url = 'http://' + this.domain + '/api/v1/check-out/check-in-list';

      this.dataStatus.text = '';
      this.dataStatus.class = '';

      this
        ._httpService
        .getAPI(url, this.token)
        .subscribe(res => {
          this.resData = [];
          this.checkinList = [];

          if (res.result['check_in']) {
            this.resData = res.result['check_in'];
            for (let i = 0; i < this.resData.length; i++) {
              let tmp = new CheckIn();
              let customerItm = tmp.customerInfo;
              let promotion = tmp.promotion;

              let cus = this.resData[i]['customer'];

              customerItm.name = cus['name'];
              customerItm.phone = cus['phone'];
              customerItm.email = cus['email'];
              customerItm.point = cus['point'];
              customerItm.type = cus['type'];
              customerItm.lifetimePoint = cus['lifetime_point'];
              customerItm.lastVisited = cus['last_visited'];
              customerItm.numberCheckin = cus['number_checkin'];
              customerItm.yelp = cus['yelp'];
              customerItm.last_review = cus['last_review'];

              if (cus['last_review']) {
                customerItm.full_review = cus['last_review']['content'];
              }

              tmp.createAt = this.resData[i]['check_in']['create_at'];
              tmp.id = this.resData[i]['check_in']['id'];
              tmp.status = this.resData[i]['check_in']['status'];
              tmp.promotion = this.resData[i]['promotions'];
              tmp.customer_id = this.resData[i]['check_in']['customer_id'];
              tmp.services = this.resData[i]['services'];

              if(this.resData[i]['appointment'] !== null) {
                tmp['start'] = moment(this.resData[i]['appointment']['start_time']).format('h:mm a');
                tmp['start_date'] = moment(this.resData[i]['appointment']['start_time']).format('MM/DD/YYYY');
                tmp['price'] = this.resData[i]['appointment']['price_final'];
                tmp['appointment'] = this.resData[i]['appointment']['start_time'];
              } else {
                tmp['appointment'] = '';
              }

              this
                .checkinList
                .push(tmp);

              if (i == this.resData.length - 1) {
                $('#user-detail').modal('hide');
              }
            }
          } else {
            this.dataStatus.text = 'No data';
            this.dataStatus.class = 'text-info';

            $('#user-detail').modal('hide');
          }
          console.log('Checkin list: ', this.checkinList);
        }, error => {
          console.log(error);

          this.dataStatus.text = 'No data';
          this.dataStatus.class = 'text-info';

        }, () => {
          console.log('Get checkout list successfully.');
        });
    }
  }

  // 2. Show user info
  showInfo(event : any, id : any) {
    $('.service-popup').removeClass('shw');
    $('#user-detail').removeClass('show-serv');

    // console.log(this.checkinList);
    this.total = 0;
    this.totalAmountInput = 0;
    this.redeemSlt = [];
    this.sltRedemMsg = 'Select 0 redeem(s)';
    this.amountInput = '';
    $('.staff-code').addClass('hide');
    $('.total-amount').val('');
    $('.checkout-frm').removeClass('shw');
    $('.calc-frm').removeClass('hide');
    $('.s-code-1').val('');
    $('.s-code-1').removeClass('has-val');
    $('.s-code-2').val('');
    $('.s-code-2').removeClass('has-val');
    $('.s-code-3').val('');
    $('.s-code-3').removeClass('has-val');
    this.staffCode = '';
    $('.prom-slt').val('');

    for (let i = 0; i < this.checkinList.length; i++) {
      if (this.checkinList[i]['id'] === id) {
        this.userDetail = this.checkinList[i];
        this.getRedeem();
        this.testApi(id);
        $('#user-detail').modal('show');
        // console.log(this.checkinList[i]);
        $('.total-amount').removeClass('err');
        $('.total-amount')
          .siblings('.err-msg')
          .removeClass('shw');

        if(this.userDetail['price']) {
          this.amountInput = this.userDetail['price'].toString();
        }

        this.getNote(this.userDetail['customerInfo']['phone']);
      }
    }
    // console.log(this.userDetail);
  }

  // 3.1 Get redeem list
  getRedeem() {
    if (this.token !== '') {
      let url = 'http://' + this.domain + '/api/v1/redeem/list';

      this
        ._httpService
        .getAPI(url, this.token)
        .subscribe(res => {
          // console.log(res.result);
          if (res.result['list']) {

            let list = res.result['list'];
            let tmp = [];

            if (list.length != 0) {
              for (let i = 0; i < list.length; i++) {
                let redeem = new Redeem();

                redeem.id = list[i]['id'];
                redeem.content = list[i]['content'];
                redeem.isActive = list[i]['is_active'];
                redeem.point = list[i]['point'];
                redeem.createdAt = list[i]['created_at'];
                redeem.updatedAt = list[i]['updated_at'];
                redeem.logicType = list[i]['logic_type'];
                redeem.logicValue = list[i]['logic_value'];
                if (this.userDetail.customerInfo.point < redeem.point) {

                  break;
                }
                tmp.push(redeem);
                this.redeemList = tmp;
              }
            }
          }
        }, error => {
          console.log(error);
        }, () => {
          console.log('Get redeem list successfully.');
        });
    }
  }
  // 3.2 Show redeem popup list
  showRedeem(event : any) {
    if ($('.open-slt-redeem').hasClass('active')) {
      $('.open-slt-redeem').removeClass('active');
      $('.open-slt-redeem')
        .siblings('.slt-redeem')
        .removeClass('shw');

      if (this.pointRedeem > this.userDetail.customerInfo.point) {
        this.disabled = 'disabled';
        this.warning = 'Your points is not enough.';
      } else {
        this.disabled = '';
        this.warning = '';
      }
    } else {
      $('.open-slt-redeem').addClass('active');
      $('.open-slt-redeem')
        .siblings('.slt-redeem')
        .addClass('shw');

    }

    if ($('.total-amount').val() == '') {
      this.total = 0;
    }
  }

  // 4. Close modal
  closeModal(event : any) {
    $('#user-detail').modal('hide');
    $('.total-amount').val('');
  }

  // 5.1 Show confirm checkout
  goCheckOut(event : any) {
    let total = _.trim(this.amountInput);

    if (total !== '' && total.match("[0-9.]*")) {
      $('.total-amount').removeClass('err');
      $('.total-amount')
        .siblings('.err-msg')
        .removeClass('shw');
      $(event.target)
        .closest('.calc-frm')
        .addClass('hide');
      $(event.target)
        .closest('.calc-frm')
        .siblings('.staff-code')
        .removeClass('hide');
      $('.checkout-frm').addClass('shw');
    } else {
      $('.total-amount').addClass('err');
      $('.total-amount')
        .siblings('.err-msg')
        .addClass('shw');
      this.total = 0;
    }
  }
  // 5.2 Cancel confirm checkout
  cancelConfirm(event) {
    $(event.target)
      .closest('.staff-code')
      .addClass('hide');
    $(event.target)
      .closest('.staff-code')
      .siblings('.calc-frm')
      .removeClass('hide');
    $('.checkout-frm').removeClass('shw');
    $('.staff-code-ipt').val('');
  }
  // 5.3 Click numb button
  clickNumb(event : any, numb : any) {
    if (this.staffCode.length < 3) {
      this.staffCode += numb;
      let a = [];
      a.push(numb);
      if ($('.s-code-1').val() == '' && $('.s-code-2').val() == '' && $('.s-code-3').val() == '') {
        $('.s-code-1').val(a[0]);
        $('.s-code-1').addClass('has-val');
      } else if ($('.s-code-1').val() != '' && $('.s-code-2').val() == '') {
        $('.s-code-2').val(a[0]);
        $('.s-code-2').addClass('has-val');
      } else if ($('.s-code-1').val() != '' && $('.s-code-2').val() != '') {
        $('.s-code-3').val(a[0]);
        $('.s-code-3').addClass('has-val');
      }
    }
  }
  // 5.4 Keypress staff code input
  enterStaffCode(event : any) {
    let val = _.trim($(event.target).val());

    if (val !== '' && val.match(/^[0-9]{0,2}$/)) {
      this.staffCode = val;
    } else {
      this.staffCode = '';
    }
  }
  // 5.5 Del numb
  delNumb(event : any) {
    let val = $('.staff-code-ipt').val();
    if (this.staffCode >= 0) {
      this.staffCode = this
        .staffCode
        .substring(0, this.staffCode.length - 1);

      if ($('.s-code-1').val() != '' && $('.s-code-2').val() != '' && $('.s-code-3').val() != '') {
        $('.s-code-3').val('');
        $('.s-code-3').removeClass('has-val');
      } else if ($('.s-code-1').val() != '' && $('.s-code-2').val() != '') {
        $('.s-code-2').val('');
        $('.s-code-2').removeClass('has-val');
      } else {
        $('.s-code-1').val('');
        $('.s-code-1').removeClass('has-val');
      }
    }
  }
  // 5.6 Click amount
  clickAmount(event : any, numb : any) {
    if (this.amountInput.length < 9) {
      this.amountInput += numb;

      if (this.amountInput.includes('.')) {
        this.checkHasDot = true;
      } else {
        this.checkHasDot = false;
      }

      if (this.amountInput != '') {
        this.totalAmountInput = parseFloat(this.amountInput);
      } else {
        this.totalAmountInput = 0;
      }
      this.reCalculate();
    }
  }
  // 5.7 Del amount
  delAmount(event : any) {
    if (this.amountInput != '') {
      this.amountInput = this
        .amountInput
        .substring(0, this.amountInput.length - 1);

      if (this.amountInput.includes('.')) {
        this.checkHasDot = true;
      } else {
        this.checkHasDot = false;
      }

      if (this.amountInput != '') {
        this.totalAmountInput = parseFloat(this.amountInput);
      } else {
        this.totalAmountInput = 0;
      }
      this.reCalculate();
    }
  }

  // 6. Enter amount
  enterAmount(event : any) {
    event.stopPropagation();
    event.preventDefault();
    // let amount = parseFloat($(event.target).val()); if ($(event.target).val() !=
    // '') {   this.totalAmountInput = amount; } else {   this.totalAmountInput = 0;
    // } this.reCalculate();
  }

  // 7. Select promotion
  selectPromotion(event : any) {
    this.pricePromoDiscountTotal = 0;
    this.promotionCode = '';
    this.promotionSlt = undefined;
    if ($(event.target).val() !== '') {
      if (this.userDetail.promotion.length !== 0) {
        for (let i = 0; i < this.userDetail.promotion.length; i++) {
          if (this.userDetail.promotion[i]['code'] === $(event.target).val()) {
            // CODE TOAN
            this.promotionCode = this.userDetail.promotion[i]['code'];
            this.promotionSlt = this.userDetail.promotion[i];
            break;
          }
        }
      }
    }

    this.reCalculate(); // Recalc total price
  }

  // 8.1 Calculate discount
  calculateDiscount(type : any, value : any, discount : any) {
    let result = 0;
    if (!_.isNaN(value)) {
      if (type == 'usd') {
        result = parseFloat(discount);
      } else if (type == 'percent') {
        result = value * parseFloat(discount) / 100;
      }
    } else if (_.isNaN(value) || parseFloat(discount) == 0) {
      return 0;
    }
    return result;
  }
  // 8.2 Recalculator
  reCalculate() {

    // Get Total amount Input first
    if (this.amountInput == '') {
      this.totalAmountInput = 0;
    } else {
      this.totalAmountInput = parseFloat(this.amountInput);
    }
    this.pricePromoDiscountTotal = 0;
    this.priceRedeemDiscountTotal = 0;
    this.pointRedeem = 0;
    // console.log(this.promotionSlt); console.log(this.redeemSlt);
    // console.log(this.total); RECACL PROMOTION DISCOUNT
    if (this.promotionSlt !== undefined) {
      let type = this.promotionSlt.logic_type;
      let discount_value = this.promotionSlt.logic_value;
      this.pricePromoDiscountTotal = this.calculateDiscount(type, this.totalAmountInput, discount_value);
    }
    this.totalAmountInput -= this.pricePromoDiscountTotal;
    ////////////
    for (let i = 0; i < this.redeemSlt.length; i++) {
      let type = this.redeemSlt[i]['logicType'];
      let discount_value = this.redeemSlt[i]['logicValue'];
      this.priceRedeemDiscountTotal += this.calculateDiscount(type, this.totalAmountInput, discount_value);
      this.pointRedeem += this.redeemSlt[i]['point'];
    }
    // check point of redeem valid
    if (this.pointRedeem > this.userDetail.customerInfo.point) {
      this.disabled = 'disabled';
      this.warning = 'Your points is not enough.';
    } else {
      this.disabled = '';
      this.warning = '';
    }
    this.total = this.totalAmountInput - this.priceRedeemDiscountTotal;
    if (this.total < 0) {
      this.total = 0;
    }
  }

  // 9. Select redeem
  selectRedem(event : any, id : any) {
    let type = '';
    let value = '';
    let amount = parseFloat($('.total-amount').val());
    let index = _.findIndex(this.redeemList, function (n) {
      return n['id'] === parseInt(id);
    });

    if ($(event.target).prop('checked') == false) {

      _
        .remove(this.redeemSlt, function (n) {
          return n['id'] == id;
        });
      this.sltRedemMsg = 'Select ' + this.redeemSlt.length + ' redeem(s)';
    } else {
      let ind = _.findIndex(this.redeemSlt, function (n) {
        return n['id'] == id;
      });

      if (ind == -1) {
        this
          .redeemSlt
          .push(this.redeemList[index]);
      }

      this.sltRedemMsg = 'Select ' + this.redeemSlt.length + ' redeem(s)';
    }
    this.reCalculate(); // Recalc total price
  }

  // 10. Send checkout
  sendCheckout(event : any) {
    if (this.token !== '') {
      if (this.staffCode != '' || this.hasStaffCode != '') {
        if(this.hasStaffCode != '')
          {
              this.staffCode = this.hasStaffCode;
          }
        let url = 'http://' + this.domain + '/api/v1/check-out/checkout';
        let redeem_list = '';

        if (this.redeemSlt.length != 0) {
          for (let i = 0; i < this.redeemSlt.length; i++) {
            redeem_list += this.redeemSlt[i]['id'] + ',';
          }
        }
        let param = {
          promotion_code: (this.promotionCode != '')
            ? this.promotionCode
            : '',
          point_bonus: Math.round(parseInt(this.total)),
          checkin_id: this.userDetail['id'],
          customer_id: this.userDetail['customer_id'],
          redeem_list: redeem_list.substring(0, redeem_list.length - 1),
          staff_number: parseInt(this.staffCode),
          total_price: this.total,
          discount_value: this.pricePromoDiscountTotal + this.priceRedeemDiscountTotal
        };

        this.sending = true;

        this
          ._httpService
          .postAPI(url, this.token, param)
          .subscribe(res => {
            let thisComp = this;
            let result = JSON.parse(res._body);
            if (result['result']['status'] == 'OK') {
              // this.checkoutStatus.text = 'Checkout successfully.';
              // this.checkoutStatus.class = 'text-success';

              this.staffCode = '';
              this.total = 0
              this.priceRedeemDiscountTotal = 0;
              this.pricePromoDiscountTotal = 0;

              this.sending = false;

              this.getList();

              this.option.message = 'Checkout successfully';
              this.option.type = 'success';
              this.option.timeout = 3000;

              this.isShow = true;

              console.log(this.isShow)

              // setTimeout(function () {   thisComp.checkoutStatus.text = '';
              // thisComp.checkoutStatus.class = '';   $('#user-detail').modal('hide');
              // thisComp.getList(); }, 2000);
            } else {
              this.sending = false;

              // this.checkoutStatus.text = result['result']['message'];
              // this.checkoutStatus.class = 'text-danger'; let thisComp = this;
              // setTimeout(function () {   thisComp.checkoutStatus.text = '';
              // thisComp.checkoutStatus.class = ''; }, 2000);

              this.option.message = result['result']['message'];
              this.option.type = 'error';
              this.option.timeout = 3000;

              this.isShow = true;
            }
          }, error => {
            console.log(error);

            this.sending = false;

            // this.checkoutStatus.text = 'Checkout failed.'; this.checkoutStatus.class =
            // 'text-danger'; let thisComp = this; setTimeout(function () {
            // thisComp.checkoutStatus.text = '';   thisComp.checkoutStatus.class = ''; },
            // 2000);

            this.option.message = 'Checkout failed.';
            this.option.type = 'error';
            this.option.timeout = 3000;

            this.isShow = true;
          }, () => {
            console.log('Get redeem list successfully.');
          });
      } else {

      }
    }
  }

  // 11. Filter list
  filterList(event?: any, data?: string) {
    let filter = _.trim($(event.target).val());
    this.filterStr = filter;

    this.dataStatus.text = '';
    this.dataStatus.class = '';

    if (filter != '' || data != '') {
      if (filter == '' && data != '') {
        filter = data;
        this.filterStr = data;
      }
      let tmpFilter = [];
      for (let i = 0; i < this.resData.length; i++) {
        let tmp = new CheckIn();
        let customerItm = tmp.customerInfo;
        if (this.resData[i]['customer']['type'] == filter) {
          customerItm.name = this.resData[i]['customer']['name'];
          customerItm.phone = this.resData[i]['customer']['phone'];
          customerItm.email = this.resData[i]['customer']['email'];
          customerItm.point = this.resData[i]['customer']['point'];
          customerItm.type = this.resData[i]['customer']['type'];
          customerItm.lifetimePoint = this.resData[i]['customer']['lifetime_point'];
          customerItm.lastVisited = this.resData[i]['customer']['last_visited'];
          customerItm.numberCheckin = this.resData[i]['customer']['number_checkin'];
          customerItm.yelp = this.resData[i]['customer']['yelp'];

          tmp.createAt = this.resData[i]['check_in']['create_at'];
          tmp.id = this.resData[i]['check_in']['id'];
          tmp.status = this.resData[i]['check_in']['status'];
          tmp.promotion = this.resData[i]['promotions'];
          tmp.customer_id = this.resData[i]['check_in']['customer_id'];
          tmpFilter.push(tmp);
        }

        if (i == this.resData.length - 1) {
          if (tmpFilter.length != 0) {
            this.checkinList = tmpFilter;
          } else {
            this.checkinList = [];
            this.dataStatus.text = 'No data';
            this.dataStatus.class = 'text-info';
          }
        }
      }
    } else if (filter == '' && data == '') {
      this.getList();
    }
  }

  // 12. Search
  search(event?: any, frmData?: any) {
    if (this.frmSearch.valid) {
      let searchTxt = _.trim($('.search-ipt').val());
      let result = [];
      this.searchText = frmData.searchTxt;

      this.dataStatus.text = '';
      this.dataStatus.class = '';

      if (frmData.searchTxt == '') {
        if (this.filterStr != '') {
          this.filterList('', this.filterStr);
        } else {
          this.getList();
        }
      } else {
        if (frmData.searchTxt.match(/^[0-9]{0,10}$/)) {
          for (let i = 0; i < this.resData.length; i++) {
            let tmp = new CheckIn();
            let customerItm = tmp.customerInfo;
            if (this.resData[i]['customer']['phone'].includes(frmData.searchTxt)) {
              this.checkinList = [];
              customerItm.name = this.resData[i]['customer']['name'];
              customerItm.phone = this.resData[i]['customer']['phone'];
              customerItm.email = this.resData[i]['customer']['email'];
              customerItm.point = this.resData[i]['customer']['point'];
              customerItm.type = this.resData[i]['customer']['type'];
              customerItm.lifetimePoint = this.resData[i]['customer']['lifetime_point'];
              customerItm.lastVisited = this.resData[i]['customer']['last_visited'];
              customerItm.numberCheckin = this.resData[i]['customer']['number_checkin'];
              customerItm.yelp = this.resData[i]['customer']['yelp'];

              tmp.createAt = this.resData[i]['check_in']['create_at'];
              tmp.id = this.resData[i]['check_in']['id'];
              tmp.status = this.resData[i]['check_in']['status'];
              tmp.promotion = this.resData[i]['promotions'];
              tmp.customer_id = this.resData[i]['check_in']['customer_id'];

              result.push(tmp);
              // this.checkinList.push(tmp);
            }
            if (i == this.resData.length - 1) {
              if (result.length != 0) {
                this.checkinList = result;
              } else {
                this.checkinList = [];
                this.dataStatus.text = 'No data';
                this.dataStatus.class = 'text-info';
              }
            }
          }
        } else {
          for (let i = 0; i < this.resData.length; i++) {
            let tmp = new CheckIn();
            let customerItm = tmp.customerInfo;
            if (this.resData[i]['customer']['name'].toLowerCase().includes(frmData.searchTxt.toLowerCase())) {
              this.checkinList = [];

              customerItm.name = this.resData[i]['customer']['name'];
              customerItm.phone = this.resData[i]['customer']['phone'];
              customerItm.email = this.resData[i]['customer']['email'];
              customerItm.point = this.resData[i]['customer']['point'];
              customerItm.type = this.resData[i]['customer']['type'];
              customerItm.lifetimePoint = this.resData[i]['customer']['lifetime_point'];
              customerItm.lastVisited = this.resData[i]['customer']['last_visited'];
              customerItm.numberCheckin = this.resData[i]['customer']['number_checkin'];
              customerItm.yelp = this.resData[i]['customer']['yelp'];

              tmp.createAt = this.resData[i]['check_in']['create_at'];
              tmp.id = this.resData[i]['check_in']['id'];
              tmp.status = this.resData[i]['check_in']['status'];
              tmp.promotion = this.resData[i]['promotions'];
              tmp.customer_id = this.resData[i]['check_in']['customer_id'];
              // this.checkinList.push(tmp);
              result.push(tmp);
            }
            if (i == this.resData.length - 1) {
              if (result.length != 0) {
                this.checkinList = result;
              } else {
                this.checkinList = [];
                this.dataStatus.text = 'No data';
                this.dataStatus.class = 'text-info';
              }
            }
          }
        }
      }
    } else {}
  }

  // 13.1 Sort
  sortCol(event : any, type : any) {
    let asc;
    let thisComp = this;

    if ($('.col-' + type).find('.fa').hasClass('fa-caret-down')) {
      asc = true;
      $('.col-' + type)
        .find('.fa')
        .removeClass('fa-caret-down')
        .addClass('fa-caret-up');
    } else {
      asc = false;
      $('.col-' + type)
        .find('.fa')
        .removeClass('fa-caret-up')
        .addClass('fa-caret-down');
    }

    switch (type) {
      case 'id':
        this.sortData('id', asc);
        break;
      case 'name':
        this.sortData('name', asc);
        break;
      case 'checkintime':
        this.sortData('lastVisited', asc);
        break;
      case 'point':
        this.sortData('point', asc);
        break;
      case 'mem-type':
        this.sortData('type', asc);
        break;
      case 'tag':
        this.sortData('tag', asc);
        break;
      case 'appointment':
        this.sortData('appointment', asc);
        break;
    }
  }
  // 13.2 Sort data
  sortData(type : any, asc : any) {
    if (type === 'id') {
      if (asc === false) {
        this.checkinList = _.sortBy(this.checkinList, function (n) {
          return n['id'];
        });
      } else {
        this.checkinList = _.sortBy(this.checkinList, function (n) {
          return n['id'];
        }).reverse();
      }
    } else {
      if (asc === false) {
        this.checkinList = _.sortBy(this.checkinList, function (n) {
          return n['customerInfo'][type];
        });
      } else {
        this.checkinList = _.sortBy(this.checkinList, function (n) {
          return n['customerInfo'][type];
        }).reverse();
      }
    }

    if(type == 'appointment') {
      if (asc === false) {
        this.checkinList = _.sortBy(this.checkinList, function (n) {
          return n[type];
        });
      } else {
        this.checkinList = _.sortBy(this.checkinList, function (n) {
          return n[type];
        }).reverse();
      }
    }

  }

  // 14. Clickout
  onClickedOutside(event : any) {
    if ($(event.target).hasClass('redeem-content') || $(event.target).hasClass('control__indicator') || $(event.target).hasClass('redeem-pts') || $(event.target).is('input')) {
      return;
    } else if ($('.open-slt-redeem').hasClass('active')) {
      $('.slt-redeem').removeClass('shw');
      $('.open-slt-redeem').removeClass('active');
    }
  }

  // 15. Reload realtime
  reloadRealtime() {
    if (this.token !== '') {
      let url = 'http://' + this.domain + '/api/v1/check-out/check-in-list';
      console.log('Old: ', this.checkinList);
      let isLast : boolean = false;
      let thisComp = this;

      this.dataStatus.text = '';
      this.dataStatus.class = '';

      this
        ._httpService
        .getAPI(url, this.token)
        .subscribe(res => {
          this.resData = [];
          this.reloadData = [];

          if (res.result['check_in']) {
            this.resData = res.result['check_in'];
            for (let i = 0; i < this.resData.length; i++) {
              let tmp = new CheckIn();
              let customerItm = tmp.customerInfo;
              let promotion = tmp.promotion;

              customerItm.name = this.resData[i]['customer']['name'];
              customerItm.phone = this.resData[i]['customer']['phone'];
              customerItm.email = this.resData[i]['customer']['email'];
              customerItm.point = this.resData[i]['customer']['point'];
              customerItm.type = this.resData[i]['customer']['type'];
              customerItm.lifetimePoint = this.resData[i]['customer']['lifetime_point'];
              customerItm.lastVisited = this.resData[i]['customer']['last_visited'];
              customerItm.numberCheckin = this.resData[i]['customer']['number_checkin'];
              customerItm.yelp = this.resData[i]['customer']['yelp'];
              customerItm.last_review = this.resData[i]['customer']['last_review'];
              tmp.createAt = this.resData[i]['check_in']['create_at'];
              tmp.id = this.resData[i]['check_in']['id'];
              tmp.status = this.resData[i]['check_in']['status'];
              tmp.promotion = this.resData[i]['promotions'];
              tmp.services = this.resData[i]['services'];

              if(this.resData[i]['appointment'] !== null) {
                tmp['start'] = moment(this.resData[i]['appointment']['start_time']).format('h:mm a');
                tmp['start_date'] = moment(this.resData[i]['appointment']['start_time']).format('MM/DD/YYYY');
                tmp['price'] = this.resData[i]['appointment']['price_final'];
                tmp['appointment'] = this.resData[i]['appointment']['start_time'];
              } else {
                tmp['appointment'] = '';
              }

              this
                .reloadData
                .push(tmp);

              if (i == this.resData.length - 1) {
                // this.checkinList = this.reloadData;
                isLast = true;

              }
            }

            if (isLast == true) {
              // Check if an element is removed
              for (let i = 0; i < this.checkinList.length; i++) {
                let index = _.findIndex(this.reloadData, function (n) {
                  return n['id'] == thisComp.checkinList[i]['id'];
                });

                if (index == -1) {
                  _
                    .remove(this.checkinList, function (n) {
                      return thisComp.checkinList[i]['id'];
                    });
                  // Sort and filter apply after reload data
                  if (i == this.checkinList.length - 1) {
                    if (this.filterStr != '') {
                      this.filterList('', this.filterStr);
                    }

                    if (this.searchText != '') {
                      this.search('', this.frmSearch.value);
                    }
                  }
                }

              }

              // Check if element is updated or has new element
              for (let i = 0; i < this.reloadData.length; i++) {
                let index = _.findIndex(this.checkinList, function (n) {
                  return n['id'] == thisComp.reloadData[i]['id'];
                });

                if (index == -1) {
                  this
                    .checkinList
                    .unshift(this.reloadData[i]); // Unshift => insert into head of array
                  // Sort data
                  if (i == this.reloadData.length - 1) {
                    if (this.filterStr != '') {
                      this.filterList('', this.filterStr);
                    }

                    if (this.searchText != '') {
                      this.search('', this.frmSearch.value);
                    }
                  }
                }

              }
            }
            console.log('New: ', this.reloadData);
          } else {
            this.checkinList = [];
            this.dataStatus.text = 'No data';
            this.dataStatus.class = 'text-info';
          }
        }, error => {
          console.log(error);

          this.dataStatus.text = 'No data';
          this.dataStatus.class = 'text-info';
        }, () => {
          console.log('Get realtime successfully.');
        });
    }
  }

  // 16. Save note
  saveNote(event : any, frmData : any, phone : any) {
    if (this.token !== '') {
      let url = 'http://' + this.domain + '/api/v1/customer/update';

      let param = {
        phone: phone,
        'Customer[note]': frmData.note
      };
      this.isSending = true;

      this
        ._httpService
        .postAPI(url, this.token, param)
        .subscribe(res => {
          this.noteStatus = 'Save note successfully.';

          let thisComp = this;

          setTimeout(function () {
            thisComp.noteStatus = '';
          }, 2000);

          this.isSending = false;
        }, error => {
          console.log(error);
          this.isSending = false;
        }, () => {
          console.log('Update note successfully.');
        });

    }
  }

  getNote(phone : any) {
    if (this.token !== '') {
      let url = 'http://' + this.domain + '/api/v1/customer/info?phone=' + phone;

      this
        ._httpService
        .getAPI(url, this.token)
        .subscribe(res => {
          let result = res['result']['customer'];

          this.noteFrm = new FormGroup({
            note: new FormControl(result['note']
              ? result['note']
              : '')
          });
        }, error => {
          console.log(error);

        }, () => {
          console.log('Get note successfully.');
        });

    }
  }

  clearSelectPromotion(event : any) {
    $('input[name="radio-promotion"]').prop('checked', false);

    this.promotionCode = '';
    this.promotionSlt = undefined;

    this.reCalculate();
  }

  // 16. Get user info
  getUserInfo() {
    if (this.token !== '') {
      let url = 'http://' + this.domain + '/api/v1/user/info';

      this
        ._httpService
        .getAPI(url, this.token)
        .subscribe(res => {
          console.log('User info', res);
          if (res.result['settings']) {
            localStorage.setItem('config', JSON.stringify(res.result['settings']));
            let setting = res.result['settings'];
            if (parseInt(setting['checkout_feature']) == 0) {
              this.checkout_feature = 0;
            } else {
              this.checkout_feature = 1;
            }

            if(parseInt(setting['appointment_feature']) == 1) {
              this.isAppoint = true;
            } else {
              this.isAppoint = false;
            }

            if (parseInt(setting['service_feature']) == 1) {
              this.isServ = true;
            } else {
              this.isServ = false;
            }

            if(parseInt(setting['staff_code_active']) == 0) {
              this.hasStaffCode = res.result['staff_info']['code'].toString();
              this.staffCode = this.hasStaffCode
            }

          }
        }, error => {
          console.log(error);
        }, () => {
          console.log('Get checkout list successfully.');
        });
    }
  }

  updateCheckout() {
    if (this.token !== '') {
      let url = 'http://' + this.domain + '/api/v1/check-out/update-checkout-campaign';

      let redeem_list = '';

      if (this.redeemSlt.length != 0) {
        for (let i = 0; i < this.redeemSlt.length; i++) {
          redeem_list += this.redeemSlt[i]['id'] + ',';
        }
      }
      let param = {
        checkin_id: this.userDetail['id'],
        redeem_list: redeem_list.substring(0, redeem_list.length - 1),
        promotion_code: (this.promotionCode != '')
          ? this.promotionCode
          : ''
      }

      this.sending = true;

      this
        ._httpService
        .postAPI(url, this.token, param)
        .subscribe(res => {
          let result = JSON.parse(res._body);
          let thisComp = this;

          setTimeout(function () {
            thisComp.sending = false;
            if (result.result['status'] == 'OK') {
              thisComp.checkout_msg = 'Redeem successfully.';
              $('#user-detail').modal('hide');
            } else {
              thisComp.checkout_msg = 'Redeem failed.';
            }
          }, 2000);
        }, error => {
          console.log(error);
          this.sending = false;
        }, () => {
          console.log('Get checkout list successfully.');
        });
    }
  }

  viewMore(event: any, id: any){
    // console.log('asdddas')
    for (let i = 0; i < this.checkinList.length; i++) {
      if (this.checkinList[i]['id'] === id) {
        this.review = this.checkinList[i];
        $('#review-dt').modal('show');
      }
    }
  }

  closeReview(event: any) {
    $('#review-dt').modal('hide');
  }

  testApi(id: any) {
    if (this.token !== '') {
      let url = 'http://' + this.domain + '/api/v1/check-out/get-info?id=' + id;

      this
        ._httpService
        .getAPI(url, this.token)
        .subscribe(res => {
          console.log(res)
        }, error => {
          console.log(error);
        }, () => {
          console.log('Update note successfully.');
        });

    }
  }

  changeRoute(event: any) {
    console.log(event);
  }

  showServPopup(event: any) {
    $('.service-popup').addClass('shw');
    $('#user-detail').addClass('show-serv');
  }
  closeServPopup(event: any) {
    $('.service-popup').removeClass('shw');
    $('#user-detail').removeClass('show-serv');
  }

  showFullNameServ(event: any) {
    if($(event.target).hasClass('active')) {
      let txt = $(event.target).attr('data-tmp-name');
      $(event.target).text(txt.toUpperCase());
      $(event.target).removeClass('active');
    } else {
      $(event.target).text($(event.target).attr('data-full-name'));
      $(event.target).addClass('active');
    }
  }
}
