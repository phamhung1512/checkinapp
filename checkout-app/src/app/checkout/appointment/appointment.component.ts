import {
    Component,
    OnInit,
    ViewEncapsulation,
    AfterViewInit,
    Input,
    Output
} from '@angular/core';

import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AppState } from '../../app.service';

// Import services
import * as _ from 'lodash';

// Import services
import { HttpServices } from '../../services/http.service';
import { ValidationService } from '../../services/validation.service';
import { ParamConfig } from '../../services/encode';
import { ParamsList } from '../../services/params';

import { CheckIn } from '../../models/checkin';
import { Customer } from '../../models/customer';
import * as moment from 'moment';
import '../../loader/datepicker.loader';
import 'bootstrap/js/modal';
import { setTimeout } from "timers";

declare var $: any;

@Component({
    selector: 'appointment',
    templateUrl: '../../views/appointment/appointment.html',
    providers: [HttpServices, ValidationService]
})
export class Appointment implements OnInit,
    AfterViewInit {

    token = '';
    domain = '';
    label: any = '';

    loading: boolean = false;
    loadCalendar: boolean = false;

    addDetail: FormGroup;
    searchFrm: FormGroup;
    customerFrm: FormGroup;

    isLoaded: boolean = false;
    isShow: boolean = false;

    showCreate: boolean = false;

    appointmentDetail: any;
    serviceList: any = [];
    searchRes: any;
    dayClick: any;
    timeAvailable: any = [];

    appointStatus: any = {
        text: '',
        class: ''
    };

    not_available: any = '';

    msgUpdate: any = '';

    timer: any;

    mask: any = [
        '(',
        /[2-9]/,
        /\d/,
        /\d/,
        ')',
        ' ',
        /\d/,
        /\d/,
        /\d/,
        '-',
        /\d/,
        /\d/,
        /\d/,
        /\d/
    ];

    dateChoose: any;
    showDetail: boolean = false;

    oldData: any;
    showReload: boolean = false;

    notiList: any = {
        status: false,
        list: []
    };
    calendarOptions: any = {
        // height: 'parent', fixedWeekCount: false, defaultDate: '2016-09-12',
        defaultView: 'agendaWeek',
        editable: true,
        eventLimit: 4,
        minTime: '08:00:00',
        maxTime: '20:00:00',
        allDaySlot: false,
        header: {
            left: 'prev,next today new-event',
            center: 'title',
            right: 'month, agendaWeek,agendaDay, listWeek'
        },
        eventDurationEditable: false,
        timezone: 'local',
        slotDuration: '00:15:00',
        slotLabelInterval: '01:00:00',
        navLinks: true, // can click day/week names to navigate views
        events: [],
        views: {

        },
        customButtons: {
            'new-event': {
                text: '+ New Appointment',
                click: () => {
                    let today = new Date();
                    today.setHours(8, 0, 0, 0);
                    this.showCreate = true;
                    this.dateChoose = today;
                }
            }
        },


        eventClick: (calEvent, jsEvent, view) => {
            // this.showDetail = true;
            // this.appointmentDetail = calEvent;
            this.getAppointServices(calEvent.id, calEvent);
        },
        dayClick: (date, jsEvent, view) => {
            let today = new Date();
            // console.log();
            let thisComp = this;

            if (date._d.getTime() > today.getTime() || moment(today).format('MM/DD/YYYY') == moment(date._d).format('MM/DD/YYYY')) {
                this.showCreate = true;
                this.dateChoose = date;
            }
        },
        // eventResize: (event, delta, revertFunc, jsEvent) => {
        //     let confirmBox = confirm('Do you want to update your appointment time?');

        //     if (confirmBox == true) {
        //         this.updateAppointmentTime('', event);
        //     } else {
        //         revertFunc();
        //     }
        // },
        eventDrop: (event, delta, revertFunc) => {
            let today = new Date();
            today.setHours(today.getHours());
            if (event.end._d.getTime() < today.getTime()) {
                revertFunc();
            }
            else {
                let confirmBox = confirm('Do you want to update your appointment time?');
                if (confirmBox == true) {
                    this.calendarOptions.defaultView = $("#gco-calendar").fullCalendar('getView').type;
                    this.updateAppointmentTime('', event);
                } else {
                    revertFunc();
                }
            }
        },
        eventRender: (event, eventElement, view) => {
            if (event.title) {
                if (event.status == 'canceled') {

                    eventElement.css({ backgroundColor: 'rgba(77, 77, 77, .7)', borderColor: '#404040' });
                }
                else if (event.status == 'scheduled') {
                    eventElement.css({ backgroundColor: 'rgba(220,53,70, .7)', borderColor: '#DC3546' });
                }
                else if (event.status == 'confirmed') {
                    eventElement.css({ backgroundColor: 'rgba(58,135,173, .7)', borderColor: '#3a87ad' })
                }
            }
            // eventElement.append('<span class="event-status">'+ event.status +'</span>');
        }
    };

    constructor(public appState: AppState, private _router: Router, private _titleService: Title, private _httpService: HttpServices, private _activeRoute: ActivatedRoute) { }

    public ngOnInit() {
        let url = window.location.href;

        if (url.indexOf('id') != -1 && url.indexOf('token') != -1) {
            let before = url.substr(url.indexOf('?') + 1);

            let p = before.split('&');

            let domain = p[0].split('=');
            let token = p[1].split('=');

            this.domain = domain[1] + ParamsList.domain;
            this.token = token[1];

            localStorage.setItem('checkout-token', JSON.stringify(this.token));
            localStorage.setItem('checkout-domain', JSON.stringify(this.domain));
            $('.top-header').remove();
            $('.main-menu').remove();
            $('.checkout-content').css({
                marginLeft: 0
            });
            // Code get Info Settings

            this.getIsActive();

            let thisComp = this;
            this.timer = setInterval(function () {
                thisComp.reloadData();
            }, 10000);

            $('.tab-btn').each(function () {
                $(this)
                    .on('click', function (e) {
                        let target = $(this).data('target');

                        $('.tab-btn').removeClass('active');
                        $(this).addClass('active');

                        $('.tab-itm').removeClass('active');
                        $('#' + target).addClass('active');
                    });
            });
        } else {
            // this
            //     ._router
            //     .navigate(['/login']);
            if (localStorage.getItem('checkout-token')) {
                this
                    ._router
                    .navigate(['/appointment']);

                this.token = JSON.parse(localStorage.getItem('checkout-token'));

                if (localStorage.getItem('checkout-domain')) {
                    this.domain = JSON.parse(localStorage.getItem('checkout-domain'));

                    let thisComp = this;
                    this.timer = setInterval(function () {
                        thisComp.reloadData();
                    }, 30000);

                }

                let usr_config = JSON.parse(localStorage.getItem('config'));

                if (usr_config['appointment_feature'] != 0) {
                    this.not_available = '';
                    this.getAllAppointment();
                    this.getNotify();
                    $('.tab-btn')
                        .each(function () {
                            $(this)
                                .on('click', function (e) {
                                    let target = $(this).data('target');

                                    $('.tab-btn').removeClass('active');
                                    $(this).addClass('active');

                                    $('.tab-itm').removeClass('active');
                                    $('#' + target).addClass('active');
                                });
                        });
                } else {
                    this.loadCalendar = false;
                    this.not_available = 'Coming soon ! This feature is in development stage. Please contact Fastboy Marketing at 832-968-6668 for more details!';
                }


            }
        }

    }

    ngAfterViewInit() { }

    ngOnDestroy() {
        clearInterval(this.timer);
    }

    /**
     *
     * 1. Get all appointment
     * ------------------------------
     * 2.1 Update appointment time
     * ------------------------------
     * 3.1 Close create modal
     * 3.2 Close detail modal
     * ------------------------------
     * 4.1 Get color
     * 4.2 Convert hex color to rgba
     * ------------------------------
     * 5.1 Reload data
     * 5.2 Accept reload data
     * 5.3 Close reload data alert
     * ------------------------------
     * 6.1 Get notifications
     * 6.2 Reload notifications
     *
     *
   */

    // 1. Get all appointment
    getAllAppointment() {
        if (this.token !== '') {
            let today = new Date();
            // let today = moment().add('days', -1);
            let next = moment().add('days', 20);
            let url = 'http://' + this.domain + '/api/v1/appointment/fetch-all?from=' + moment(today).format('MM/DD/YYYY') + '&to=' + moment(next).format('MM/DD/YYYY');

            this.isLoaded = false;
            this.loadCalendar = true;
            this
                ._httpService
                .getAPI(url, this.token)
                .subscribe(res => {
                    let temp = res.result['appointments'].length
                    this.calendarOptions.event = res.result['appointments'][temp - 1];
                    console.log(this.calendarOptions.event);
                    if (res.result['appointments'].length != 0) {
                        this.oldData = res.result['appointments'];

                        let result = res.result['appointments'];
                        let event = [];
                        for (let i = 0; i < result.length; i++) {
                            if (result[i]['canceled'] != '1') {
                                event.push({
                                    title: result[i]['booker_name'] + ' - ' + result[i]['service']['name'],
                                    start: result[i]['start_time'],
                                    end: result[i]['end_time'],
                                    status: result[i]['status'],
                                    customer: result[i]['customer'],
                                    employee: (result[i]['employee'])
                                        ? result[i]['employee']
                                        : [],
                                    price: result[i]['price_final'],
                                    formatStart: moment(result[i]['start_time']).format('MMM Do YYYY, h:mm:ss a'),
                                    formatEnd: moment(result[i]['end_time']).format('MMM Do YYYY, h:mm:ss a'),
                                    id: result[i]['id'],
                                    service: result[i]['service'],
                                    booker: result[i]['booker_name'],
                                    booker_contact: result[i]['booker_contact'],
                                    end_time_expected: result[i]['end_time_expected'],
                                    price_expected: result[i]['price_expected'],
                                    price_full: result[i]['price_full'],
                                    discount: result[i]['discount'],
                                    note: result[i]['note']
                                });
                            }

                            if (i == result.length - 1) {
                                this.calendarOptions.events = event;
                                console.log(this.calendarOptions.events)
                                this.isLoaded = true;
                                this.loadCalendar = false;
                            }
                        }
                    } else {
                        this.isLoaded = true;
                        this.loadCalendar = false;
                    }
                }, error => {
                    console.log(error);

                    this.isLoaded = true;
                    this.loadCalendar = false;
                }, () => {
                    console.log('');
                });
        }
    }

    // 2.1 Update appointment time
    updateAppointmentTime(event: any, appoint: any) {

        let thisComp = this;
        if (this.token !== '') {
            let url = 'http://' + this.domain + '/api/v1/appointment/update-appointment';

            let urlParam: any = {
                'Appointment[booker_name]': appoint.booker,
                'Appointment[booker_contact]': appoint.booker_contact,
                'Appointment[customer_id]': (appoint.customer)
                    ? appoint.customer.id
                    : '',
                'Appointment[employee_id]': appoint.employee.user_id,
                'Appointment[service_id]': appoint.service.id,
                'Appointment[start_time]': moment(appoint.start).format('YYYY-MM-DD HH:mm:ss'),
                'Appointment[end_time_expected]': moment(appoint.end._d).format('YYYY-MM-DD HH:mm:ss'),
                'Appointment[end_time]': moment(appoint.end._d).format('YYYY-MM-DD HH:mm:ss'),
                'Appointment[price_expected]': appoint.price_expected,
                'Appointment[price_full]': appoint.price_full,
                'Appointment[discount]': appoint.discount,
                'Appointment[price_final]': appoint.price,
                'Appointment[status]': (this.label != '')
                    ? this.label
                    : appoint.status,
                'id': appoint.id
            };


            this.loadCalendar = true;

            this
                ._httpService
                .postAPI(url, this.token, urlParam)
                .subscribe(res => {
                    this.loadCalendar = false;
                    this.getAllAppointment();
                }, error => {
                    console.log(error);
                    this.loadCalendar = false;

                    this.appointStatus.class = 'text-danger';
                    this.appointStatus.text = 'Create failed.';

                    setTimeout(function () {
                        thisComp.appointStatus.class = '';
                        thisComp.appointStatus.text = '';
                    }, 2000);
                }, () => {
                    console.log('');
                });
        }
    }

    // 3.1 Close create modal
    closeFrm(event: any) {
        if (event.isClose == true) {
            this.showCreate = false;
            if (event.reload == true) {
                this.getAllAppointment();
            }
        }
    }
    // 3.2 Close detail modal
    closeEditFrm(event: any) {
        if (event.close == true) {
            this.showDetail = false;
            if (event.reload == true) {
                this.getAllAppointment();
            }
        }
    }

    // 4.1 Get color
    stringToColorCode(str) {
        let res = 0;
        for (var i = 0; i < str.length; i++) {
            res = str.charCodeAt(i) + ((res << 5) - res);
        }

        let hex = ((res >> 24) & 0xFF).toString(16) + ((res >> 16) & 0xFF).toString(16) + ((res >> 8) & 0xFF).toString(16) + (res & 0xFF).toString(16);
        hex += '000000';
        let hexColor = '#' + hex.substring(0, 6);

        return hexColor;
    }
    // 4.2 Convert hex color to rgba
    hexToRgbA(hex) {
        var c;
        if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
            c = hex
                .substring(1)
                .split('');
            if (c.length == 3) {
                c = [
                    c[0],
                    c[0],
                    c[1],
                    c[1],
                    c[2],
                    c[2]
                ];
            }
            c = '0x' + c.join('');
            let res = {
                hex: hex,
                rgba: 'rgba(' + [
                    (c >> 16) & 255,
                    (c >> 8) & 255,
                    c & 255
                ].join(',') + ', .7)'
            };

            return res;
        }
    }

    // 5.1 Reload data
    reloadData() {
        if (this.token !== '') {
            let today = new Date();
            let next = moment().add('days', 20);
            let url = 'http://' + this.domain + '/api/v1/appointment/fetch-all?from=' + moment(today).format('MM/DD/YYYY') + '&to=' + moment(next).format('MM/DD/YYYY');

            this
                ._httpService
                .getAPI(url, this.token)
                .subscribe(res => {
                    if (res.result['appointments'].length != 0) {
                        let result = res.result['appointments'];

                        let thisComp = this;

                        let r = _.differenceWith(result, this.oldData, _.isEqual);

                        if (r.length != 0) {
                            this.showReload = true;
                        }
                    }
                }, error => {
                    console.log(error);

                }, () => {
                    console.log('');
                });
        }
    }
    // 5.2 Accept reload data
    refreshCalendar(event: any) {
        this.showReload = false;
        this.getAllAppointment();
    }
    // 5.3 Close reload data alert
    closeReload(event: any) {
        this.showReload = false;
    }

    // 6.1 Get notifications
    getNotify() {
        if (this.token !== '') {
            let url = 'http://' + this.domain + '/api/v1/appointment/notifications';

            this
                ._httpService
                .getAPI(url, this.token)
                .subscribe(res => {
                    // console.log(res);
                    this.notiList = {
                        list: res,
                        status: true
                    };
                }, error => {
                    this.notiList = {
                        list: [],
                        status: false
                    };
                    console.log(error);

                }, () => {
                    console.log('');
                });
        }
    }
    // 6.2 Reload notifications
    changeNotiList(event: any) {
        console.log(event);
        if (event.reload == true) {
            this.getNotify();
        }
    }
    // 16. Get user info
    getIsActive() {

        if (this.token !== '') {
            let url = 'http://' + this.domain + '/api/v1/user/info';

            this
                ._httpService
                .getAPI(url, this.token)
                .subscribe(res => {
                    console.log('User info', res);
                    if (res.result['settings']) {

                        let setting = res.result['settings'];
                        if (parseInt(setting['appointment_feature']) == 1) {
                            this.getAllAppointment();
                        } else {
                            this.loadCalendar = false;
                            this.not_available = 'Coming soon ! This feature is in development stage. Please contact Fastboy Marketing at 832-968-6668 for more details!';
                        }
                    }
                }, error => {
                    console.log(error);
                }, () => {
                    console.log('Get checkout list successfully.');
                });
        }
    }
    getAppointServices(id: any, appoint: any) {
        if (this.token !== '') {
            let url = 'http://' + this.domain + '/api/v1/appointment/list-appointment-service?appointment_id=' + id;

            this
                ._httpService
                .getAPI(url, this.token)
                .subscribe(res => {
                    if (res.result) {
                        appoint['services_booked'] = res.result['services_booked'];

                        this.appointmentDetail = appoint;
                        this.showDetail = true;
                    }
                }, error => {
                    console.log(error);
                }, () => {
                    console.log('');
                });
        }
    }
}
