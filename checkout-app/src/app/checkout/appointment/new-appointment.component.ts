import { Component, OnInit, ViewEncapsulation, AfterViewInit, Input, Output, OnChanges, SimpleChange, EventEmitter } from '@angular/core';

import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AppState } from '../../app.service';

// Import services
import * as _ from 'lodash';

// Import services
import { HttpServices } from '../../services/http.service';
import { ValidationService } from '../../services/validation.service';

import { Customer } from '../../models/customer';
import * as moment from 'moment';
import '../../loader/datepicker.loader';
import 'bootstrap/js/modal';
import { parse } from 'querystring';
import { ClickOutsideModule } from 'ng-click-outside';

declare var $: any;

@Component({
  selector: 'new-appointment',
  templateUrl: '../../views/appointment/new-appointment.html',
  providers: [HttpServices, ValidationService],
  outputs: ['closeCreateFrm']
})
export class NewAppointment implements OnInit,
  AfterViewInit {

  @Input() isShow: any;
  @Input() dateChoose: any;

  mask: any = [
    '(', /[2-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/
  ];

  token = '';
  domain = '';

  searchFrm: FormGroup;
  customerFrm: FormGroup;
  bookingFrm: FormGroup;

  customerSelected: any;
  serviceSelected: any = [];
  providerSelected: any;

  searchStatus: any = '';
  noService: any = '';

  searchRes: any = [];
  timeAvailable: any = [];
  serviceList: any = [];
  providerList: any = [];
  mainService: any = [];

  bookingShw: boolean = false;
  loading: boolean = false;
  createFail: boolean = false;
  isReadonly: boolean = false;
  showSelectTime: boolean = false;
  showSelectClient: boolean = false;
  showCreateClient: boolean = false;
  noClient: boolean = false;

  totalPrice: any = 0;
  totalDuration: any = 0;

  public closeCreateFrm = new EventEmitter();

  constructor(
    public appState: AppState,
    private _router: Router,
    private _titleService: Title,
    private _httpService: HttpServices,
    private _activeRoute: ActivatedRoute) { }

  public ngOnInit() {
  }

  ngAfterViewInit() {
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    if (this.isShow == true) {
      $('body').addClass('modal-open').append('<div class="modal-backdrop fade in"></div>');
      if (localStorage.getItem('checkout-token')) {
        this
          ._router
          .navigate(['/appointment']);

        this.token = JSON.parse(localStorage.getItem('checkout-token'));

        if (localStorage.getItem('checkout-domain')) {
          this.domain = JSON.parse(localStorage.getItem('checkout-domain'));

          $('#appoint-time').removeClass('err');
          $('#appoint-time').siblings('.err-msg').removeClass('shw');

          this.showCreateClient = false;
          this.showSelectTime = false;
          this.createFail = false;
          this.isReadonly = false;
          this.providerList = [];
          this.serviceSelected = [];
          this.timeAvailable = [];
          this.serviceList = [];
          this.mainService = [];
          this.providerSelected = [];

          this.searchFrm = new FormGroup({
            search: new FormControl('', Validators.compose([ValidationService.NoWhitespaceValidator]))
          });

          delete this.customerFrm;

          this.bookingFrm = new FormGroup({
            service: new FormControl('', Validators.compose([Validators.required])),
            time: new FormControl(moment(this.dateChoose).format('HH:mm'), Validators.compose([Validators.required])),
          });

          this.getService();
        }
      }



    } else {
      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();
    }
  }

  /**
   *
   * 1. Search customer
   * 1.1 Keypress search client
   * ----------------------------------------------
   * 2.1 Choose customer
   * 2.2 Add new customer
   * 2.3 Back to create client
   * 2.4 Back to search
   * ----------------------------------------------
   * 3.1 Get service
   * 3.2 Select service
   * ----------------------------------------------
   * 4. Select provider
   * 5.1 Show time available
   * 5.2 Show time available
   * 5.3 Change date
   * 5.4 Select time
   * 5.5 Conver time to min
   * ----------------------------------------------
   * 6. Close form
   * ----------------------------------------------
   * 7. Submit new appointment
   * ----------------------------------------------
   * 8. Click outside to close client search result
   *
   */

  // 1. Search customer
  searchCustomer(frmData: any) {
    if (this.token !== '') {
      let url = 'http://' + this.domain + '/api/v1/customer/search?q=' + frmData.search;

      this.searchStatus = '';
      this.showSelectClient = false;
      this._httpService.getAPI(url, this.token).subscribe(
        res => {
          if (res.result.customers) {
            if (res.result.customers.length != 0) {
              let result = res.result.customers;
              let tmpResult = [];

              for (let i = 0; i < result.length; i++) {
                let tmp = new Customer();

                tmp.phone = result[i]['phone'];
                tmp.id = result[i]['id'];
                tmp.name = result[i]['full_name'];
                tmp.note = result[i]['note'];

                tmpResult.push(tmp);

                if (i == result.length - 1) {
                  $('.create-client-btn').html('Create new client with ' + '<strong>' + this.searchFrm.value.search + '</strong>');
                  this.searchRes = tmpResult;
                  this.showSelectClient = true;
                }
              }
            } else {
              this.searchRes = [];
              this.showSelectClient = true;
              $('.create-client-btn').html('Create new client with ' + '<strong>' + this.searchFrm.value.search + '</strong>');
            }
          } else {
            this.searchRes = [];
            this.showSelectClient = true;
          }
        }, error => {
          console.log(error);

          this.searchRes = [];
          this.showSelectClient = true;
          this.searchStatus = 'No customer found.';

        }, () => {
          console.log('Get customer list successfully.');
        });
    }
  }
  // 1.1 Keypress search client
  keypressCustomer(frmData: any) {
    if (frmData.search.trim().length >= 3) {
      this.searchCustomer(frmData);
    }
  }

  // 2.1 Choose customer
  chooseCustomer(event: any, id: any) {
    for (let i = 0; i < this.searchRes.length; i++) {
      if (id == this.searchRes[i]['id']) {
        this.customerSelected = this.searchRes[i];
        this.showSelectClient = false;
        this.noClient = false;
        this.showCreateClient = true;
        this.isReadonly = true;

        this.customerFrm = new FormGroup({
          name: new FormControl(this.customerSelected.name, Validators.compose([Validators.required])),
          phone: new FormControl(this.customerSelected.formatPhone, Validators.compose([Validators.required, ValidationService.phoneValid])),
          id: new FormControl(this.customerSelected.id)
        });
      }
    }
  }
  // 2.2 Add new customer
  addCustomer(event: any) {
    this.showCreateClient = true;
    this.showSelectClient = false;
    this.noClient = false;
    this.isReadonly = false;

    let regex = /^\d+$/;
    if (regex.test(this.searchFrm.value.search)) {
      this.customerFrm = new FormGroup({
        name: new FormControl('', Validators.compose([Validators.required])),
        phone: new FormControl(this.searchFrm.value.search, Validators.compose([Validators.required, ValidationService.phoneValid])),
        id: new FormControl('')
      });
    } else {
      this.customerFrm = new FormGroup({
        name: new FormControl(this.searchFrm.value.search, Validators.compose([Validators.required])),
        phone: new FormControl('', Validators.compose([Validators.required, ValidationService.phoneValid])),
        id: new FormControl('')
      });
    }

  }
  // 2.3 Back to create client
  backToCreate(event: any) {
    this.isReadonly = false;

    let regex = /^\d+$/;
    if (regex.test(this.searchFrm.value.search)) {
      this.customerFrm = new FormGroup({
        name: new FormControl('', Validators.compose([Validators.required])),
        phone: new FormControl(this.searchFrm.value.search, Validators.compose([Validators.required, ValidationService.phoneValid])),
        id: new FormControl('')
      });
    } else {
      this.customerFrm = new FormGroup({
        name: new FormControl(this.searchFrm.value.search, Validators.compose([Validators.required])),
        phone: new FormControl('', Validators.compose([Validators.required, ValidationService.phoneValid])),
        id: new FormControl('')
      });
    }
  }
  // 2.4 Back to search
  backToSearch(event: any) {
    this.showCreateClient = false;
    this.isReadonly = false;

    this.searchFrm = new FormGroup({
      search: new FormControl('')
    });
  }

  // 3.1 Get service
  getService() {
    if (this.token !== '') {
      let url = 'http://' + this.domain + '/api/v1/appointment/services';

      this.noService = '';
      this.loading = true;

      this._httpService.getAPI(url, this.token).subscribe(
        res => {
          let thisComp = this;
          setTimeout(function () {
            thisComp.loading = false;
            thisComp.serviceList = [];
            if (res.result.services.length != 0) {
              for (let i = 0; i < res.result.services.length; i++) {
                if (res.result.services[i]['price'] === null) {
                  res.result.services[i]['price'] = 0;
                }

                if (res.result.services[i]['duration'] === null) {
                  res.result.services[i]['duration'] = 0;
                }

                if (i === res.result.services.length - 1) {
                  let list = res.result.services;

                  thisComp.serviceList = list;

                  thisComp.bookingShw = true;

                  $('.frm-ttl h3').text('Make your appointment');
                }
              }

              let maskText = moment(thisComp.dateChoose._d).format('MM/DD/YYYY');

              setTimeout(function () {
                $('.date-mask').text(maskText);
                $('#app')
                let workingHours = ['480, 1020'];
                let defaultTime = moment(thisComp.dateChoose).format('HH:mm');
                let time = workingHours[0].split(',').map(function (item) {
                  return parseInt(item, 10);
                });

                thisComp.createTimeAvailable(time, defaultTime);

                $('#appoint-time').datetimepicker({
                  format: 'MM/DD/YYYY',
                  keepOpen: true,
                  defaultDate: maskText,
                  //daysOfWeekDisabled: [0,6],
                  minDate: new Date()
                });
                console.log(maskText);
                $('.date-gr').removeClass('hide');
              }, 500);
            } else {
              thisComp.bookingShw = true;
              thisComp.noService = 'No service available.';
            }
          }, 200);
        }, error => {
          console.log(error);

          this.bookingShw = true;
          this.loading = false;
          this.noService = 'No service available.';

        }, () => {
          console.log('Get service list successfully.');
        });
    }
  }
  // 3.2 Select service
  selectService(event: any, type: any) {
    let id = parseInt($(event.target).val());
    for (let i = 0; i < this.serviceList.length; i++) {
      if (id == parseInt(this.serviceList[i]['id'])) {
        if (type == 'main') {
          this.mainService = this.serviceList[i];
          if (this.serviceSelected.length == 0) {
            this.totalPrice = parseFloat(this.mainService.price != null ? this.mainService.price : 0);
            this.totalDuration = parseInt(this.mainService.duration != null ? this.mainService.duration : 0);
          }
          else {
            this.totalPrice += parseFloat(this.mainService.price != null ? this.mainService.price : 0);
            this.totalDuration += parseInt(this.mainService.duration != null ? this.mainService.duration : 0);
          }

          // this.totalPrice = _.sumBy(this.serviceSelected, function(n){
          //   return parseFloat(n['price']);
          // });

          // this.totalDuration = _.sumBy(this.serviceSelected, function(n){
          //   return parseInt(n['duration']);
          // });


          this.providerList = this.serviceList[i].employeeProvided;
          this.bookingFrm = new FormGroup({
            service: new FormControl(this.serviceList[i]['id'], Validators.compose([Validators.required])),
            time: new FormControl(moment(this.dateChoose).format('HH:mm'), Validators.compose([Validators.required])),
          });
        } else {
          if (this.serviceList[i]['id'] !== this.mainService['id']) {
            this.serviceSelected.push(this.serviceList[i]);

            this.serviceSelected = _.uniqBy(this.serviceSelected, function (n) {
              return n['id'];
            });

            for (let j = 0; j < this.providerList.length; j++) {
              this.providerList[j]['service_id'] = this.serviceList[i]['id'];
            }

            this.totalPrice += parseFloat(this.serviceList[i]['price'] != null ? this.serviceList[i]['price'] : 0);

            this.totalDuration += parseInt(this.serviceList[i]['duration'] != null ? this.serviceList[i]['duration'] : 0);

            // this.totalPrice += parseFloat(this.mainService.price);
            // this.totalDuration += parseInt(this.mainService.duration);
          }
          $('.additional-service').addClass('hide');
          $('.add-more-serv').closest('tr').removeClass('hide');
        }
      }
    }
  }
  // 4. Select provider
  selectProvider(event: any, service_id: any) {
    if ($(event.target).val() != '') {
      let id = parseInt($(event.target).val());
      let thisComp = this;

      for (let i = 0; i < this.providerList.length; i++) {
        if (id == parseInt(this.providerList[i]['user_id'])) {
          this.providerSelected = this.providerList[i];
        }
      }
    }
  }

  // 5.1 Show time available
  showTimeAvailable(daySlt: any, provider: any) {
    let workingHours = provider['workingHours'];
    let defaultTime = moment(this.dateChoose).format('HH:mm:ss');
    let time = [];

    switch (daySlt) {
      case 'monday':
        time = workingHours['mo'].split(',').map(function (item) {
          return parseInt(item, 10);
        });
        this.createTimeAvailable(time, defaultTime);
        break;
      case 'tuesday':
        time = workingHours['tu'].split(',').map(function (item) {
          return parseInt(item, 10);
        });
        this.createTimeAvailable(time, defaultTime);
        break;
      case 'wednesday':
        time = workingHours['we'].split(',').map(function (item) {
          return parseInt(item, 10);
        });
        this.createTimeAvailable(time, defaultTime);
        break;
      case 'thursday':
        time = workingHours['th'].split(',').map(function (item) {
          return parseInt(item, 10);
        });
        this.createTimeAvailable(time, defaultTime);
        break;
      case 'friday':
        time = workingHours['fr'].split(',').map(function (item) {
          return parseInt(item, 10);
        });
        this.createTimeAvailable(time, defaultTime);
        break;
      case 'saturday':
        time = workingHours['sa'].split(',').map(function (item) {
          return parseInt(item, 10);
        });
        this.createTimeAvailable(time, defaultTime);
        break;
      case 'sunday':
        time = workingHours['su'].split(',').map(function (item) {
          return parseInt(item, 10);
        });
        this.createTimeAvailable(time, defaultTime);
        break;
      default: break;
    }
  }
  // 5.2 Show time available
  createTimeAvailable(time: any, defaultTime: any) {
    this.timeAvailable = [];
    let i = time[0];

    while (i <= time[1]) {
      let m = i % 60;
      let h = (i - m) / 60;
      let am = true;
      let h1 = h;
      if (h >= 13) {
        h1 = h - 12;
        am = false;
      }
      let r = (h < 10 ? '0' : '') + h.toString() + ':' + (m < 10 ? '0' : '') + m.toString();
      let r1 = (h1 < 10 ? '0' : '') + h1.toString() + ':' + (m < 10 ? '0' : '') + m.toString() + ' ' + (am == true ? 'am' : 'pm');
      if (r === defaultTime) {
        this.timeAvailable.push({
          time: r,
          time1: r1,
          selected: true
        });
      } else {
        this.timeAvailable.push({
          time: r,
          time1: r1,
          selected: false
        });
      }

      i += 15;
    }
  }
  // 5.3 Change date
  changeDate(event: any) {
    if ($(event.target).val() != '') {
      $(event.target).removeClass('err');
      $(event.target).siblings('.err-msg').removeClass('shw');

      let c = new Date($(event.target).val());

      let d = moment(this.dateChoose).format('HH:mm:ss');
      let tmp1 = d.split(':');
      let r1 = parseInt(tmp1[0]) * 60 + parseInt(tmp1[1]);

      this.dateChoose = moment(c).add(r1, 'minutes');

      $('.date-mask').text($(event.target).val());

      // let defaultTime = moment(this.dateChoose).format('MM/DD/YYYY hh:mm:ss A');

      // let timeSlt = moment($(event.target).val()).format('dddd');

      // if(this.providerList.length != 0) {
      //   this.timeAvailable = [];
      //   this.showTimeAvailable(timeSlt.toLowerCase(), this.providerSelected);
      // }
    } else {
      $(event.target).addClass('err');
      $(event.target).siblings('.err-msg').addClass('shw');
    }
  }
  // 5.4 Select time
  selectTime(event: any) {
    if ($(event.target).val() != '') {

      let d = moment(this.dateChoose).format('HH:mm:ss');
      let t = $(event.target).val();

      let r1 = this.convertToMin(d);

      let r2 = this.convertToMin(t);

      let timeDiff = r2 - r1;

      if (timeDiff != 0) {
        this.dateChoose = moment(this.dateChoose).add(timeDiff, 'minutes');
      }


    }
  }
  // 5.5 Conver time to min
  convertToMin(time: any) {
    let tmp = time.split(':');

    return parseInt(tmp[0]) * 60 + parseInt(tmp[1]);
  }

  // 6. Close form
  closeForm(event: any) {
    this.isShow = false;
    this.closeCreateFrm.emit({
      isClose: true,
      reload: false
    });

    this.totalPrice = 0;
    this.totalDuration = 0;

    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
  }

  // 7. Submit new appointment
  createAppointment(event: any, bookFrm: any) {
    if (this.customerFrm) {
      this.noClient = false;
      if (this.customerFrm.valid) {
        this.noClient = false;
        if (this.bookingFrm.valid) {
          console.log(this.token);
          console.log($('#appoint-time').val());
          if (this.token !== '' && $('#appoint-time').val() != '' && (this.mainService.length != 0 || this.serviceSelected.length != 0)) {

            let url = 'http://' + this.domain + '/api/v1/appointment/make-appointment';

            let service_id = '';
            let emp_id: any;

            if (this.mainService.length == 0) {
              for (let j = 1; j < this.serviceSelected.length; j++) {
                service_id += this.serviceSelected[j]['id'] + ',';
              }
            } else {
              for (let j = 0; j < this.serviceSelected.length; j++) {
                service_id += this.serviceSelected[j]['id'] + ',';
              }
            }
            let service_id_param;
            if (service_id) {
              service_id_param = service_id.substring(0, service_id.length - 1)
            }
            let urlParam: any = {
              'Appointment[booker_name]': this.customerFrm.value.name,
              'Appointment[booker_contact]': this.customerFrm.value.phone,
              'Appointment[customer_id]': (this.customerFrm.value.id) ? this.customerFrm.value.id : '',
              'Appointment[employee_id]': (this.providerSelected.length != 0) ? this.providerSelected['user_id'] : '',
              'Appointment[service_id]': (this.mainService.length != 0) ? this.mainService['id'] : this.serviceSelected[0]['id'],
              'Appointment[start_time]': moment(this.dateChoose).format('YYYY-MM-DD HH:mm:ss'),
              'Appointment[end_time_expected]': moment(this.dateChoose).add('minutes', this.totalDuration).format('YYYY-MM-DD HH:mm:ss'),
              'Appointment[end_time]': moment(this.dateChoose).add('minutes', this.totalDuration).format('YYYY-MM-DD HH:mm:ss'),
              'Appointment[price_expected]': this.totalPrice,
              'Appointment[price_full]': this.totalPrice,
              'Appointment[discount]': 0,
              'Appointment[price_final]': this.totalPrice,
              'Appointment[status]': 'confirmed',
              'Appointment[note]': ($('#note').val() != '') ? $('#note').val() : '',
              'services_id': service_id_param
            };

            this.loading = true;
            this.createFail = false;

            this._httpService.postAPI(url, this.token, urlParam).subscribe(
              res => {
                let thisComp = this;
                setTimeout(function () {
                  thisComp.loading = false;

                  thisComp.isShow = false;
                  thisComp.closeCreateFrm.emit({
                    isClose: true,
                    reload: true
                  });

                  thisComp.searchFrm = new FormGroup({
                    search: new FormControl('')
                  });

                  delete this.customerFrm;

                  thisComp.bookingFrm = new FormGroup({
                    service: new FormControl('', Validators.compose([Validators.required])),
                    provider: new FormControl('', Validators.compose([Validators.required])),
                    cost: new FormControl('', Validators.compose([Validators.required])),
                    duration: new FormControl('', Validators.compose([Validators.required])),
                    time: new FormControl('', Validators.compose([Validators.required])),
                  });

                  thisComp.showCreateClient = false;
                  thisComp.showSelectTime = false;
                  thisComp.providerList = [];
                  thisComp.timeAvailable = [];
                  thisComp.serviceSelected = [];
                  thisComp.serviceList = [];
                  thisComp.mainService = [];
                  thisComp.providerSelected = [];

                  this.totalDuration = 0;
                  this.totalPrice = 0;
                }, 1500);


              }, error => {
                console.log(error);
                this.loading = false;
                this.createFail = true;
              }, () => {
                console.log('Get history list successfully.');
              });
          }
        } else {
          for (let i in this.bookingFrm.controls) {
            this.bookingFrm.controls[i].markAsTouched();
          }
        }
      } else {
        for (let i in this.customerFrm.controls) {
          this.customerFrm.controls[i].markAsTouched();
        }
      }
    } else {
      this.noClient = true;
    }
  }

  // 8. Click outside to close client search result
  onClickedOutside(event: any) {
    this.showSelectClient = false;
  }

  deleteServiceSelecter(event: any, id: any, type: any) {
    if (type === 'main') {
      this.totalPrice -= parseFloat(this.mainService.price != null ? this.mainService.price : 0);
      this.totalDuration -= parseInt(this.mainService.duration != null ? this.mainService.duration : 0);

      this.mainService = [];
      this.providerList = [];
      this.providerSelected = [];

      if (this.serviceSelected.length == 0) {
        this.totalDuration = 0;
        this.totalPrice = 0;

        this.bookingFrm = new FormGroup({
          service: new FormControl('', Validators.compose([Validators.required])),
          time: new FormControl(moment(this.dateChoose).format('HH:mm'), Validators.compose([Validators.required])),
        });
      } else {
        this.bookingFrm = new FormGroup({
          service: new FormControl(''),
          time: new FormControl(moment(this.dateChoose).format('HH:mm'), Validators.compose([Validators.required])),
        });
      }
    } else {
      let index = _.findIndex(this.serviceSelected, function (n) {
        return parseInt(n['id']) === parseInt(id);
      });

      if (index != -1) {
        this.totalPrice -= parseFloat(this.serviceSelected[index]['price'] != null ? this.serviceSelected[index]['price'] : 0);

        this.totalDuration -= parseInt(this.serviceSelected[index]['duration'] != null ? this.serviceSelected[index]['duration'] : 0);

        _.remove(this.serviceSelected, function (n) {
          return parseInt(n['id']) === parseInt(id);
        });

        if (this.serviceSelected.length == 0 && this.mainService.length == 0) {
          this.totalDuration = 0;
          this.totalPrice = 0;
        }
      }
    }
  }

  addMoreService(event: any) {
    $(event.target).closest('tr').addClass('hide');
    $('.additional-service').removeClass('hide');
  }

  closeMoreService(event: any) {
    $('.additional-service').addClass('hide');
    $('.add-more-serv').closest('tr').removeClass('hide');
  }

  changeTotalVal(event: any, type: any) {
    if (type == 'duration') {
      this.totalDuration = parseInt($(event.target).val());
    } else {
      this.totalPrice = parseFloat($(event.target).val());
    }
  }

  openPicker(event: any) {
    let abc = $('#appoint-time');
    $('#appoint-time').data("DateTimePicker").show();

    $('#appoint-time').on('dp.change', function (e) {
      $('.date-mask').text($('#appoint-time').val());
      $('#appoint-time').data("DateTimePicker").hide();
    });
  }

  onClickedOutsidePicker(event: any) {
    $('#appoint-time').data("DateTimePicker").hide();
  }
}
