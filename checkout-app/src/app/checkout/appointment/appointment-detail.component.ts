import {
    Component,
    OnInit,
    ViewEncapsulation,
    AfterViewInit,
    Input,
    Output,
    OnChanges,
    SimpleChange,
    EventEmitter
} from '@angular/core';

import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AppState } from '../../app.service';

// Import services
import * as _ from 'lodash';

// Import services
import { HttpServices } from '../../services/http.service';
import { ValidationService } from '../../services/validation.service';

import * as moment from 'moment';
import '../../loader/datepicker.loader';
import { duration } from "moment";
import { parse } from 'uglify-js';
declare var $: any;

@Component({
    selector: 'appointment-detail',
    templateUrl: '../../views/appointment/appointment-detail.html',
    providers: [
        HttpServices, ValidationService
    ],
    outputs: ['closeDetailFrm']
})
export class AppointmentDetail implements OnInit, AfterViewInit {
    @Input() isShowModal: any;
    @Input() appointDetail: any;

    mask: any = [
        '(', /[2-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/
    ];

    token = '';
    domain = '';

    public closeDetailFrm = new EventEmitter();

    editFrm: FormGroup;

    loading: boolean = false;
    hideEdit: boolean = true;
    hideView: boolean = false;
    showSelectTime: boolean = false;

    serviceList: any = [];
    providerList: any = [];
    timeAvailable: any = [];
    additionalServ: any = [];
    mainService: any = [];

    dateChoose: any;
    serviceSelected: any = [];
    providerSelected: any = [];

    totalPrice: any = 0;
    totalDuration: any = 0;

    constructor(
        public appState: AppState,
        private _router: Router,
        private _titleService: Title,
        private _httpService: HttpServices,
        private _activeRoute: ActivatedRoute
    ) { }

    public ngOnInit() { }

    ngAfterViewInit() { }

    ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
        if (this.isShowModal == true) {
            this.totalDuration = 0;
            this.totalPrice = 0;
            $('body').addClass('modal-open').append('<div class="modal-backdrop fade in"></div>');
            if (localStorage.getItem('checkout-token')) {
                this._router.navigate(['/appointment']);

                this.token = JSON.parse(localStorage.getItem('checkout-token'));

                if (localStorage.getItem('checkout-domain')) {
                    this.domain = JSON.parse(localStorage.getItem('checkout-domain'));

                    this.dateChoose = this.appointDetail.start._d;

                    this.additionalServ = this.appointDetail.services_booked;
                    if (this.additionalServ.length == 0) {
                        this.totalDuration += parseInt(this.appointDetail['service']['duration']);
                        this.totalPrice += parseInt(this.appointDetail['service']['price']);
                    }
                    else {
                        let tmpArr = [];
                        for (let i = 0; i < this.additionalServ.length; i++) {
                            if (this.additionalServ[i]['service'] !== null) {
                                tmpArr.push(this.additionalServ[i]['service']);
                            }
                            if (this.additionalServ[i]['service']) {
                                let tmp = this.additionalServ[i]['service'];
                                this.totalPrice += parseFloat((tmp['price'] !== null) ? tmp['price'] : 0);
                                this.totalDuration += parseFloat((tmp['duration'] !== null) ? tmp['duration'] : 0);
                            }

                            if (i === this.additionalServ.length - 1) {
                                this.totalDuration += parseInt(this.appointDetail['service']['duration']);
                                this.totalPrice += parseInt(this.appointDetail['service']['price']);
                                this.serviceSelected = tmpArr;
                            }
                        }
                    }
                    console.log(this.appointDetail);

                    this.showSelectTime = false;
                    this.hideView = false;
                    this.hideEdit = true;

                    this.getService();
                }
            }
        } else {
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }
    }

    /**
     *
     * 1.1 Get service
     * 1.2 Select service
     * --------------------------
     * 2.1 Show edit form
     * 2.2 Select provider
     * 2.3 Cancel edit
     * --------------------------
     * 3.1 Show time available
     * 3.2 Create time available
     * 3.3 Change date
     * 3.4 Select time
     * 3.5 Convert time
     * --------------------------
     * 4. Update appointment
     * 4.1 Confirm Appointment
     * --------------------------
     * 5. Close detail modal
     * --------------------------
     * 6.1 Show confirm delete
     * 6.2 Hide confirm delete
     * 6.3 Delete appointment
     *
    */

    // 1.1 Get service
    getService() {
        if (this.token !== '') {
            let url = 'http://' + this.domain + '/api/v1/appointment/services';

            this.loading = true;

            this._httpService.getAPI(url, this.token).subscribe(
                res => {
                    let thisComp = this;

                    setTimeout(function () {
                        thisComp.loading = false;
                        thisComp.serviceList = [];
                        if (res.result.services.length != 0) {
                            let list = res.result.services;
                            let employee = thisComp.appointDetail['employee'];

                            thisComp.mainService = thisComp.appointDetail['service'];
                            if (thisComp.mainService['duration'] == null) {
                                thisComp.mainService['duration'] = '0';
                            }
                            if (thisComp.mainService['price'] === null) {
                                thisComp.mainService['price'] = '0';
                            }

                            thisComp.serviceList = list;

                            // Get provider from service list
                            let index = _.findIndex(thisComp.serviceList, function (n) {
                                return parseInt(n['id']) == parseInt(thisComp.appointDetail.service.id);
                            });

                            if (index != -1 && employee.length != 0) {
                                thisComp.providerList = thisComp.serviceList[index]['employeeProvided'];

                                // Get provider detail from list
                                let providerIndex = _.findIndex(thisComp.providerList, function (n) {
                                    return parseInt(n['user_id']) == parseInt(thisComp.appointDetail.employee['user_id']);
                                });

                                if (providerIndex != -1) {
                                    thisComp.providerSelected = thisComp.providerList[providerIndex];
                                }


                                setTimeout(function () {
                                    let defaultDate = moment(thisComp.appointDetail.start._d).format('MM/DD/YYYY');

                                    let workingHours = ['480, 1020'];
                                    let defaultTime = moment(thisComp.dateChoose).format('HH:mm');
                                    let time = workingHours[0].split(',').map(function (item) {
                                        return parseInt(item, 10);
                                    });

                                    thisComp.createTimeAvailable(time, defaultTime);

                                    $('#edit-appoint-time').datetimepicker({
                                        format: 'MM/DD/YYYY',
                                        keepOpen: true,
                                        defaultDate: defaultDate,
                                        daysOfWeekDisabled: [0, 6],
                                        // minDate: new Date()
                                    });

                                    $('.date-gr').removeClass('hide');
                                }, 500);
                            } else {
                                thisComp.providerList = [];
                                let defaultDate = moment(thisComp.appointDetail.start._d).format('MM/DD/YYYY');
                                let workingHours = ['480, 1020'];
                                let defaultTime = moment(this.dateChoose).format('HH:mm');
                                let time = workingHours[0].split(',').map(function (item) {
                                    return parseInt(item, 10);
                                });

                                thisComp.createTimeAvailable(time, defaultTime);

                                setTimeout(() => {
                                    $('#edit-appoint-time').datetimepicker({
                                        format: 'MM/DD/YYYY',
                                        keepOpen: true,
                                        defaultDate: defaultDate,
                                        daysOfWeekDisabled: [0, 6],
                                        // minDate: new Date()
                                    });

                                    $('.date-gr').removeClass('hide');
                                }, 500);
                            }

                            if (thisComp.appointDetail) {
                                let start = thisComp.convertToMin(moment(thisComp.appointDetail.start).format('HH:mm'));
                                let end = thisComp.convertToMin(moment(thisComp.appointDetail.end).format('HH:mm'));
                                let diffMin = end - start;

                                thisComp.editFrm = new FormGroup({
                                    customer: new FormControl(thisComp.appointDetail.booker),
                                    phone: new FormControl(thisComp.appointDetail.booker_contact),
                                    service: new FormControl(thisComp.appointDetail.service.id, Validators.compose([Validators.required])),
                                    status: new FormControl(thisComp.appointDetail.status, Validators.compose([Validators.required])),
                                    provider: new FormControl((thisComp.appointDetail.employee.length != 0) ? thisComp.appointDetail.employee.user_id : ''),
                                    time: new FormControl(moment(thisComp.appointDetail.start).format('HH:mm'), Validators.compose([Validators.required])),
                                });

                                thisComp.showSelectTime = true;
                            }
                        }
                    }, 200);
                    // console.log(res);
                }, error => {
                    console.log(error);
                    this.loading = true;
                }, () => {
                    console.log('Get service list successfully.');
                });
        }
    }
    // 1.2 Select service
    selectService(event: any, type: any) {
        let id = parseInt($(event.target).val());

        for (let i = 0; i < this.serviceList.length; i++) {
            if (id == parseInt(this.serviceList[i]['id'])) {
                if (this.serviceList[i]['price'] == null) {
                    this.serviceList[i]['price'] = '0';
                }
                if (this.serviceList[i]['duration'] == null) {
                    this.serviceList[i]['duration'] = '0';
                }

                if (type == 'main') {
                    let index = _.findIndex(this.serviceSelected, function (e) {
                        return e['id'] == id;
                    });

                    if (index == -1) {
                        this.mainService = this.serviceList[i];
                        this.providerList = this.serviceList[i].employeeProvided;

                        this.totalDuration = _.sumBy(this.serviceSelected, function (e) {
                            return parseInt(e['duration'] != null ? e['duration'] : 0);
                        });

                        this.totalPrice = _.sumBy(this.serviceSelected, function (e) {
                            return parseFloat(e['price'] != null ? e['price'] : 0);
                        });

                        this.totalDuration += parseInt(this.serviceList[i]['duration'] != null ? this.serviceList[i]['duration'] : 0);
                        this.totalPrice += parseFloat(this.serviceList[i]['price'] != null ? this.serviceList[i]['price'] : 0);
                    }

                    this.editFrm = new FormGroup({
                        customer: new FormControl(this.appointDetail.booker),
                        phone: new FormControl(this.appointDetail.booker_contact),
                        service: new FormControl(this.serviceList[i]['id'], Validators.compose([Validators.required])),
                        status: new FormControl(this.appointDetail.status, Validators.compose([Validators.required])),
                        provider: new FormControl((this.appointDetail.employee.length != 0) ? this.appointDetail.employee.user_id : ''),
                        time: new FormControl(moment(this.appointDetail.start).format('HH:mm'), Validators.compose([Validators.required])),
                    });
                } else {
                    // console.log(this.serviceList[i]['id'], this.mainService['id']);
                    let index = _.findIndex(this.serviceSelected, function (e) {
                        return e['id'] == id;
                    });

                    if (this.serviceList[i]['id'] !== this.mainService['id']) {
                        if (index === -1) {
                            this.serviceSelected.push(this.serviceList[i]);

                            this.serviceSelected = _.uniqBy(this.serviceSelected, function (n) {
                                return n['id'];
                            });

                            this.totalPrice += parseFloat(this.serviceList[i]['price'] != null ? this.serviceList[i]['price'] : 0);
                            this.totalDuration += parseInt(this.serviceList[i]['duration'] != null ? this.serviceList[i]['duration'] : 0);
                        }
                    }
                }
            }
        }
    }

    // 2.1 Show edit form
    showEditFrm(event: any) {
        this.hideView = true;
        this.hideEdit = false;
    }
    // 2.2 Select provider
    selectProvider(event: any) {
        if ($(event.target).val() != '') {
            let id = parseInt($(event.target).val());

            for (let i = 0; i < this.providerList.length; i++) {
                if (id == parseInt(this.providerList[i]['user_id'])) {
                    this.providerSelected = this.providerList[i];
                }

            }
        } else {
            this.showSelectTime = false;
        }
    }
    // 2.3 Cancel edit
    cancelEdit(event: any) {
        this.hideView = false;
        this.hideEdit = true;
    }

    // 3.1 Show time available
    showTimeAvailable(daySlt: any, provider: any) {
        let workingHours = provider;
        let defaultTime = moment(this.appointDetail.start).format('HH:mm');
        let time = [];

        switch (daySlt) {
            case 'monday':
                time = workingHours['mo'].split(',').map(function (item) {
                    return parseInt(item, 10);
                });

                this.createTimeAvailable(time, defaultTime);
                break;
            case 'tuesday':
                time = workingHours['tu'].split(',').map(function (item) {
                    return parseInt(item, 10);
                });

                this.createTimeAvailable(time, defaultTime);
                break;
            case 'wednesday':
                time = workingHours['we'].split(',').map(function (item) {
                    return parseInt(item, 10);
                });

                this.createTimeAvailable(time, defaultTime);
                break;
            case 'thursday':
                time = workingHours['th'].split(',').map(function (item) {
                    return parseInt(item, 10);
                });

                this.createTimeAvailable(time, defaultTime);
                break;
            case 'friday':
                time = workingHours['fr'].split(',').map(function (item) {
                    return parseInt(item, 10);
                });

                this.createTimeAvailable(time, defaultTime);
                break;
            case 'saturday':
                time = workingHours['sa'].split(',').map(function (item) {
                    return parseInt(item, 10);
                });

                this.createTimeAvailable(time, defaultTime);
                break;
            case 'sunday':
                time = workingHours['su'].split(',').map(function (item) {
                    return parseInt(item, 10);
                });

                this.createTimeAvailable(time, defaultTime);
                break;
            default: break;
        }
    }
    // 3.2 Create time available
    createTimeAvailable(time: any, defaultTime: any) {
        this.timeAvailable = [];
        let i = time[0];

        while (i <= time[1]) {
            let m = i % 60;
            let h = (i - m) / 60;
            let am = true;
            let h1 = h;
            if (h >= 13) {
                h1 = h - 12;
                am = false;
            }
            let r = (h < 10 ? '0' : '') + h.toString() + ':' + (m < 10 ? '0' : '') + m.toString();
            let r1 = (h1 < 10 ? '0' : '') + h1.toString() + ':' + (m < 10 ? '0' : '') + m.toString() + ' ' + (am == true ? 'am' : 'pm');
            if (r === defaultTime) {
                this.timeAvailable.push({
                    time: r,
                    time1: r1,
                    selected: true
                });
            } else {
                this.timeAvailable.push({
                    time: r,
                    time1: r1,
                    selected: false
                });
            }

            i += 15;
        }
    }
    // 3.3 Change date
    changeDate(event: any) {
        if ($(event.target).val() != '') {
            $(event.target).removeClass('err');
            $(event.target).siblings('.err-msg').removeClass('shw');

            let c = new Date($(event.target).val());

            let d = moment(this.dateChoose).format('HH:mm');
            let tmp1 = d.split(':');
            let r1 = parseInt(tmp1[0]) * 60 + parseInt(tmp1[1]);

            this.dateChoose = moment(c).add(r1, 'minutes');

            let defaultTime = moment(this.dateChoose).format('MM/DD/YYYY hh:mm A');

            let timeSlt = moment($(event.target).val()).format('dddd');

        } else {
            $(event.target).addClass('err');
            $(event.target).siblings('.err-msg').addClass('shw');
        }
    }
    // 3.4 Select time
    selectTime(event: any) {
        if ($(event.target).val() != '') {
            let d = moment(this.dateChoose).format('HH:mm');
            let t = $(event.target).val();

            let r1 = this.convertToMin(d);

            let r2 = this.convertToMin(t);

            let timeDiff = r2 - r1;

            if (timeDiff != 0) {
                this.dateChoose = moment(this.dateChoose).add(timeDiff, 'minutes');
            }
        }
    }
    // 3.5 Convert time
    convertToMin(time: any) {
        let tmp = time.split(':');

        return parseInt(tmp[0]) * 60 + parseInt(tmp[1]);
    }

    // 4. Update appointment
    updateAppointment(event: any, frmData: any) {

        if (this.editFrm.valid) {
            if (this.token !== '' && $('#edit-appoint-time').val() != '' && (this.mainService.length != 0 || this.serviceSelected.length != 0)) {
                let url = 'http://' + this.domain + '/api/v1/appointment/update-appointment';

                let service_id = '';
                let emp_id: any;

                if (this.mainService.length == 0) {
                    for (let j = 1; j < this.serviceSelected.length; j++) {
                        service_id += this.serviceSelected[j]['id'] + ',';
                    }
                } else {
                    for (let j = 0; j < this.serviceSelected.length; j++) {
                        service_id += this.serviceSelected[j]['id'] + ',';
                    }
                }

                let service_id_param;
                if (service_id) {
                    service_id_param = service_id.substring(0, service_id.length - 1)
                }

                let urlParam: any = {
                    'Appointment[booker_name]': frmData.customer,
                    'Appointment[booker_contact]': frmData.phone,
                    'Appointment[customer_id]': (this.appointDetail.customer) ? this.appointDetail.customer.id : '',
                    'Appointment[employee_id]': (this.providerSelected.length != 0) ? this.providerSelected['user_id'] : '',
                    'Appointment[service_id]': (this.mainService.length != 0) ? this.mainService['id'] : this.serviceSelected[0]['id'],
                    'Appointment[start_time]': moment(this.dateChoose).format('YYYY-MM-DD HH:mm:ss'),
                    'Appointment[end_time_expected]': moment(this.dateChoose).add('minutes', parseInt(this.totalDuration)).format('YYYY-MM-DD HH:mm:ss'),
                    'Appointment[end_time]': moment(this.dateChoose).add('minutes', parseInt(this.totalDuration)).format('YYYY-MM-DD HH:mm:ss'),
                    'Appointment[price_expected]': this.totalPrice,
                    'Appointment[price_full]': this.totalPrice,
                    'Appointment[discount]': 0,
                    'Appointment[price_final]': this.totalPrice,
                    'Appointment[status]': frmData.status,
                    'Appointment[note]': $('#edit-note').val(),
                    'id': this.appointDetail.id,
                    'services_id': service_id_param
                };
                this.loading = true;
                console.log(urlParam);

                this._httpService.postAPI(url, this.token, urlParam).subscribe(
                    res => {
                        let thisComp = this;

                        this.closeDetailFrm.emit({
                            close: true,
                            reload: true
                        });

                        this.isShowModal = false;
                        this.loading = false;

                        this.showSelectTime = false;
                        this.hideView = false;
                        this.hideEdit = true;

                        console.log(res);
                    }, error => {
                        console.log(error);
                        this.loading = false;
                    },
                    () => {
                        console.log('Update appointment successfully.');
                    });
            }
        } else {
            for (let i in this.editFrm.controls) {
                this.editFrm.controls[i].markAsTouched();
            }
        }
        this.totalDuration = 0;
        this.totalPrice = 0;
    }

    //4.1 Confirm Appoitment
    confirmAppointment(event: any) {
        if (this.token !== '') {
            let url = 'http://' + this.domain + '/api/v1/appointment/update-appointment';
            let urlParam: any = {
                'id': this.appointDetail.id,
                'Appointment[status]': "confirmed"
            };
            console.log(this.appointDetail.id);
            this.loading = true;
            this._httpService.postAPI(url, this.token, urlParam).subscribe(
                res => {
                    this.loading = false;
                    this.closeDetailFrm.emit({
                        close: true,
                        reload: true
                    });
                }, error => {
                    console.log(error);
                }, () => {
                    console.log("Confirm successfully");
                }
            )
        }


    }

    // 5. Close detail modal
    closeFrm(event: any) {
        this.isShowModal = false;
        this.closeDetailFrm.emit({
            close: true,
            reload: false
        });
        this.totalPrice = 0;
        this.totalDuration = 0;
    }

    // 6.1 Show confirm delete
    deleteAppoint(event: any) {
        $('.del-confirm').addClass('shw');
    }
    // 6.2 Hide confirm delete
    cancelDelete(event: any) {
        $('.del-confirm').removeClass('shw');
    }
    // 6.3 Delete appointment
    acceptDelete(event: any) {
        if (this.appointDetail) {
            if (this.token !== '') {
                let url = 'http://' + this.domain + '/api/v1/appointment/cancel-appointment';

                let urlParam: any = {
                    'id': this.appointDetail.id,
                    'note': 'Cancel appointment'
                };

                this.loading = true;

                this._httpService.postAPI(url, this.token, urlParam).subscribe(
                    res => {
                        let thisComp = this;

                        setTimeout(function () {
                            thisComp.isShowModal = false;
                            thisComp.closeDetailFrm.emit({
                                close: true,
                                reload: true
                            });
                            $('.del-confirm').removeClass('shw');
                        }, 1000);
                    }, error => {
                        console.log(error);
                        this.loading = false;
                    }, () => {
                        console.log('Get history list successfully.');
                    });

            }
        }
        this.totalPrice = 0;
        this.totalDuration = 0;
    }

    addMoreService(event: any) {
        $(event.target).closest('tr').addClass('hide');
        $('.additional-service').removeClass('hide');
    }

    closeMoreService(event: any) {
        $('.additional-service').addClass('hide');
        $('.add-more-serv').closest('tr').removeClass('hide');
    }
    changeTotalVal(event: any, type: any) {
        if (type == 'duration') {
            this.totalDuration = parseInt($(event.target).val());
        } else {
            this.totalPrice = parseFloat($(event.target).val());
        }
    }

    deleteServiceSelecter(event: any, id: any, type: any) {
        if (type === 'main') {
            this.totalPrice -= parseFloat(this.mainService.price != null ? this.mainService.price : 0);
            this.totalDuration -= parseInt(this.mainService.duration != null ? this.mainService.duration : 0);

            this.mainService = [];
            this.providerList = [];
            this.providerSelected = [];

            if (this.serviceSelected.length == 0) {
                this.totalDuration = 0;
                this.totalPrice = 0;

                this.editFrm = new FormGroup({
                    customer: new FormControl(this.appointDetail.booker),
                    phone: new FormControl(this.appointDetail.booker_contact),
                    service: new FormControl('', Validators.compose([Validators.required])),
                    status: new FormControl(this.appointDetail.status, Validators.compose([Validators.required])),
                    provider: new FormControl((this.appointDetail.employee.length != 0) ? this.appointDetail.employee.user_id : ''),
                    time: new FormControl(moment(this.appointDetail.start).format('HH:mm'), Validators.compose([Validators.required])),
                });
            } else {
                this.editFrm = new FormGroup({
                    customer: new FormControl(this.appointDetail.booker),
                    phone: new FormControl(this.appointDetail.booker_contact),
                    service: new FormControl(''),
                    status: new FormControl(this.appointDetail.status, Validators.compose([Validators.required])),
                    provider: new FormControl((this.appointDetail.employee.length != 0) ? this.appointDetail.employee.user_id : ''),
                    time: new FormControl(moment(this.appointDetail.start).format('HH:mm'), Validators.compose([Validators.required])),
                });
            }
        } else {
            let index = _.findIndex(this.serviceSelected, function (n) {
                return parseInt(n['id']) === parseInt(id);
            });

            if (index != -1) {
                this.totalPrice -= parseFloat(this.serviceSelected[index]['price'] != null ? this.serviceSelected[index]['price'] : 0);

                this.totalDuration -= parseInt(this.serviceSelected[index]['duration'] != null ? this.serviceSelected[index]['duration'] : 0);

                _.remove(this.serviceSelected, function (n) {
                    return parseInt(n['id']) === parseInt(id);
                });

                if (this.serviceSelected.length == 0 && this.mainService.length == 0) {
                    this.totalDuration = 0;
                    this.totalPrice = 0;

                    this.editFrm = new FormGroup({
                        customer: new FormControl(this.appointDetail.booker),
                        phone: new FormControl(this.appointDetail.booker_contact),
                        service: new FormControl('', Validators.compose([Validators.required])),
                        status: new FormControl(this.appointDetail.status, Validators.compose([Validators.required])),
                        provider: new FormControl((this.appointDetail.employee.length != 0) ? this.appointDetail.employee.user_id : ''),
                        time: new FormControl(moment(this.appointDetail.start).format('HH:mm'), Validators.compose([Validators.required])),
                    });
                }
            }
        }
    }
}
