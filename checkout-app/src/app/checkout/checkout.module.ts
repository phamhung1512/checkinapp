import { NgModule }            from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Component
import { CheckoutList }       from './checkout-list/checkout-list.component';
import { HistoryList }       from './history/history.component';
import { Toas } from '../toas/toas.component';
import { Appointment } from './appointment/appointment.component';
import { NewAppointment } from './appointment/new-appointment.component';
import { AppointmentDetail } from './appointment/appointment-detail.component';

// import module
import { CheckoutRoute } from './checkout.route';
import { MasterpageModule } from '../shared/masterpage.module';
import { AvatarModule } from 'ngx-avatar';
import { ClickOutsideModule } from 'ng-click-outside';
import {CalendarComponent} from "angular2-fullcalendar/src/calendar/calendar";
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  imports : [
    CheckoutRoute,
    CommonModule,
    MasterpageModule,
    FormsModule,
    ReactiveFormsModule,
    AvatarModule,
    ClickOutsideModule,
    TextMaskModule
  ],
  declarations: [
    CheckoutList,
    HistoryList,
    Toas,
    Appointment,
    NewAppointment,
    CalendarComponent,
    AppointmentDetail
  ]
})

export class CheckoutModule{ }
