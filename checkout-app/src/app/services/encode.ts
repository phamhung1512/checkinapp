export class ParamConfig {
    
    static paramGen(options: any): string {
        let result: string = '';      
        
        for (let key in options) {
            if(options[key]) {
                result += '&' + key + '=' + options[key];
            }
        }
        result = '?' + result.substring(result.indexOf('&')+1);
        
        return result;
    };

    static encodeURI(str: string): string {
        return encodeURIComponent(str);
    }

    static decodeURI(str: string): string {
        return decodeURIComponent(str);
    }
}