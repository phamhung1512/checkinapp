import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response, Request, RequestMethod } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/catch';
// Import Config Url API

declare var $: any;
@Injectable()
/**
 * 1. GET API
 * 2. POST API
 * 3. PUT API
 * 4. DELETE API
 */

export class HttpServices {
  // Headers all api
  private headersGet: Headers;
  private optionsGet: RequestOptions;
  private headersPost: Headers;
  private optionsPost: RequestOptions;
  private headersGetFilterList: Headers;
  private optionsGetFilterList: RequestOptions;
  private headerConfig: any;
  private headerGetConfig: any;

  userStorage: any;

  constructor(
    private _http: Http,
    private _router: Router
  ) {
    // GET
    this.headersGet = new Headers({
    });
    this.optionsGet = new RequestOptions({ headers: this.headersGet });

    this.headersGetFilterList = new Headers({
    });
    this.optionsGetFilterList = new RequestOptions({ headers: this.headersGetFilterList });

    // POST
    this.headerConfig = {
    };

    this.headerGetConfig = {
    };

    this.headersPost = new Headers({
    });
    this.optionsPost = new RequestOptions({ headers: this.headersPost });

  }

  private extractData(res: Response) {
    let res_json = res.json();
    return res_json || {};
  }

  private handleError(error: any, comp: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg, error); // log to console instead
    console.log('err status', error.status);

    if (error.status === 302) {
      // localStorage.removeItem('userLog');
      // this._router.navigate(['/login']);
    }

    if (error.status === 403) {
      localStorage.clear();
      sessionStorage.clear();
      window.location.href = 'http://' + window.location.host + '/login';
    }

    return Observable.throw(error);
  }

  // request new token
  postAPINoToken(url: string, params: any): Observable<any> {
    this.headerConfig['Content-Type'] = 'application/x-www-form-urlencoded';

    // console.log(this.optionsPost);
    let optionsPost = new RequestOptions({ headers: new Headers(this.headerConfig) });

    let paramStr = this.parseParamToString(params);

    return this._http.post(url, paramStr, optionsPost)
      .catch((e) => this.handleError(e, this));
  }

  // -----------------------------------------------
  // with Token
  // ----------------------------------------------
  /**
   * 2. POST API
   */
  postAPI(url: string, tokenKey: string, params: any): Observable<any> {
    if (tokenKey == '') { return; }

    let paramStr = this.parseParamToString(params);

    // Setup Header Opts

    let header = {
      'Authorization': 'Bearer ' + tokenKey,
      'Content-Type': 'application/x-www-form-urlencoded'
    }
    let optionsPost = new RequestOptions({ headers: new Headers(header) });
    return this._http.post(url, paramStr, optionsPost)
      .catch((e) => this.handleError(e, this));
  }


  /**
   * 2.2 GET API WITH TOKEN NO ACCEPT HEADER
   */
  getAPI(url: string, tokenKey: string): Observable<any> {
    if (tokenKey == '') { return; }
    // console.log(tokenKey);
    this.headerGetConfig['Authorization'] = 'Bearer ' + tokenKey;
    this.headerGetConfig['Content-type'] = 'application/json';
    let optGet = new RequestOptions({ headers: new Headers(this.headerGetConfig) });

    // console.log(optGet);

    return this._http.get(url, optGet)
      .map(this.extractData)
      .catch((e) => this.handleError(e, this));
  }



  /**
   * 5. Parse JSON to Form-Data String
   */
  parseParamToString(param) {
    if (typeof param == 'object') {
      let result: string = '';
      let counting = 0;

      for (var k in param) {
        if (counting != 0) {
          result += '&';
        }
        if (typeof param[k] == 'string') {
          param[k] = param[k].trim();
        }
        result += k + '=' + param[k];
        counting++;
        if (param[k] != '') {

        }
      }
      return result;
    }
    return;
  }
}
