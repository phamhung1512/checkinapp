import { FormGroup } from "@angular/forms";
export class ValidationService {

  static NoWhitespaceValidator(control) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }

  static emailValidator(control) {
    // RFC 2822 compliant regex
    if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
      return null;
    } else {
      return { 'invalidEmailAddress': true };
    }
  }

  static emailValidatorsecondary(control) {
    // RFC 2822 compliant regex
    if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)||(control.value.match(/^\s*$/))) {
      return null;
    } else {
      return { 'invalid_EmailAddress': true };
    }
  }

  static codeValid (control) {
    if (control.value.match(/^[^a-zA-Z]*$/) && control.value.length == 14) {
      return null;
    } else {
      return { 'invalidCode': true };
    }
  }

  static phoneValid (control) {
    let val = control.value.trim();
    console.log(val.length)
    if (control.value.match(/^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/) && (val.length == 14 || val.length == 10)) {
      return null;
    } else {
      return { 'invalidPhone': true };
    }


  }

  static passwordValidator(control) {
    // {6,100}           - Assert password is between 8 and 20 characters
    // (?=.*[0-9])       - Assert a string has at least one number
    if (control.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,20}$/)) {
      return null;
    } else {
      return { 'invalidPassword': true };
    }
  }

  static urlValid(control) {
    // http[s] URL , accept with no http.
    // IP : /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
    if (control.value.match(/(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/)) {
      return null;
    } else {
      return { 'urlValid': true };
    }
  }

  static lengthMaxFifty(control) {
    // http[s] URL , accept with no http.
    // IP : /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
    if (control.value.trim().length <= 50) {
      return null;
    } else {
      return { 'lengthMaxFifty': true };
    }
  }

  static matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {[key: string]: any} => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value == confirmPassword.value) {
        return  null;
      } else {
        return { 'mismatchedPasswords': true  };
      }

    }
  }

  static minLengthValid(control) {
    var val=control.value + "";
    if(control.value<=20 && control.value>=8 && val.match(/^[0-9]*$/) ){

      return null;
    }

    else {
      return { 'pwd_min_len' : true };
    }
  }

  static maxLengthValid(control) {
    var val=control.value + "";
    if(control.value<=20 && control.value>=8 && val.match(/^[0-9]*$/) ){
      return null;
    }
    else{
      return { 'pwd_min_len' : true };

    }
  }

  static pwdHistoryValid(control) {
    var val=control.value + "";
    if(control.value<=99 && control.value>=0 && val.match(/^[0-9]*$/)){
      return null;
    }
    else {
      return { 'pwd_history' : true };

    }
  }

  static maxLoginAttemptsValid(control) {
    var val=control.value + "";
    if(control.value<99 && control.value>=0 && val.match(/^[0-9]*$/) ){
      return null;
    }
    else {
      return { 'max_pwd_attempts' : true };
    }
  }
  //    static numberValid(control) {

  //     if( control.value.match([0-9])){
  //         return null;
  //     }
  //     else {
  //         return  { 'max_pwd_attempts' : true };
  //     }
  // }

  static numberValid(control) {
    //console.log("control value for the numberValid :: "+control.value);
    var val = control.value + "";
    if ((val.match(/^[0-9]{0,10}$/)) || (val.match(/^\s*$/))) {
      return null;
    }
    else {
      return {'max_pwd_attempts': true};
    }
  }


  static pwdExpiryAfterValid(control) {
    var val=control.value + "";
    if(control.value<=99 && control.value>=0 && val.match(/^[0-9]*$/)){
      return null;
    }
    else {
      return { 'pswExpiryAfter' : true };
    }
  }

  static sessionExpiryAfterValid(control) {
    var val=control.value + "";
    if(control.value<=999 && control.value>=0 && val.match(/^[0-9]*$/)){
      return null;
    }
    else {
      return { 'sessionExpiryAfter' : true };
    }
  }
  static unlockafterValid(control) {
    var val=control.value + "";
    if(control.value<=999 && control.value>=0 && val.match(/^[0-9]*$/)){
      return null;
    }
    else {
      return { 'sessionExpiryAfter' : true };
    }
  }
  static addressValidator(control) {
    // RFC 2822 compliant regex
    if (control.value.match(/^\S+.*?\S+$/)) {
      return null;
    } else {
      return { 'invalidAddress': true };
    }
  }
  static nameValidator(control) {
    if(control.value.match(/^[a-zA-Z0-9]+[0-9a-zA-Z\s]*\S$/)){
      return null;
    }
    else {
      return { 'nameValidator' : true };
    }
  }

  static roleandpolicyvalid (control) {
    var val=control.value + "";

    if (val.match(/^[a-zA-Z0-9]*$/) && val.length >=3) {
      return null;
    } else {
      return { 'invalidname': true };
    }
  }
  static descriptionLength(control){
    // IP : /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
    if (control.value.trim().length <= 1000) {
      return null;
    } else {
      return { 'lengthMaxFifty': true };
    }
  }
  static targetnamevalid (control) {
    var val=control.value + "";

    if (val.match(/^[a-zA-Z0-9\.\\\-\/]*$/) && val.length >=3) {
      return null;
    } else {
      return { 'invalidname': true };
    }
  }
  static targetgrpnamevalid (control) {
    var val=control.value + "";

    if (val.match(/^[a-zA-Z0-9]*$/) && val.length >=3) {
      return null;
    } else {
      return { 'invalidname': true };
    }
  }
} // END class​
