/* This file is used for bundle webpack
 * - GET all plugin js */
//require.context('plugins', true, /\.js$/);
//var expr = /^\.\/.*\.js$/;
//require("plugins/" + expr);
var req = require.context("assets/js/plugins/", true, /^(.*\.(js$))[^.]*$/igm);
req.keys().forEach(function(key){
    req(key);
});
