(function() {
    var d;
    window.AmCharts ? d = window.AmCharts : (d = {}, window.AmCharts = d, d.themes = {}, d.maps = {}, d.inheriting = {}, d.charts = [], d.onReadyArray = [], d.useUTC = !1, d.updateRate = 60, d.uid = 0, d.lang = {}, d.translations = {}, d.mapTranslations = {}, d.windows = {}, d.initHandlers = [], d.amString = "am", d.pmString = "pm");
    d.Class = function(a) {
        var b = function() {
            arguments[0] !== d.inheriting && (this.events = {}, this.construct.apply(this, arguments))
        };
        a.inherits ? (b.prototype = new a.inherits(d.inheriting), b.base = a.inherits.prototype, delete a.inherits) :
            (b.prototype.createEvents = function() {
                for (var a = 0; a < arguments.length; a++) this.events[arguments[a]] = []
            }, b.prototype.listenTo = function(a, b, c) {
                this.removeListener(a, b, c);
                a.events[b].push({
                    handler: c,
                    scope: this
                })
            }, b.prototype.addListener = function(a, b, c) {
                this.removeListener(this, a, b);
                a && this.events[a] && this.events[a].push({
                    handler: b,
                    scope: c
                })
            }, b.prototype.removeListener = function(a, b, c) {
                if (a && a.events && (a = a.events[b]))
                    for (b = a.length - 1; 0 <= b; b--) a[b].handler === c && a.splice(b, 1)
            }, b.prototype.fire = function(a) {
                for (var b =
                        this.events[a.type], c = 0; c < b.length; c++) {
                    var d = b[c];
                    d.handler.call(d.scope, a)
                }
            });
        for (var c in a) b.prototype[c] = a[c];
        return b
    };
    d.addChart = function(a) {
        window.requestAnimationFrame ? d.animationRequested || (d.animationRequested = !0, window.requestAnimationFrame(d.update)) : d.updateInt || (d.updateInt = setInterval(function() {
            d.update()
        }, Math.round(1E3 / d.updateRate)));
        d.charts.push(a)
    };
    d.removeChart = function(a) {
        for (var b = d.charts, c = b.length - 1; 0 <= c; c--) b[c] == a && b.splice(c, 1);
        0 === b.length && (d.requestAnimation && (window.cancelAnimationFrame(d.requestAnimation),
            d.animationRequested = !1), d.updateInt && (clearInterval(d.updateInt), d.updateInt = NaN))
    };
    d.isModern = !0;
    d.getIEVersion = function() {
        var a = 0,
            b, c;
        "Microsoft Internet Explorer" == navigator.appName && (b = navigator.userAgent, c = /MSIE ([0-9]{1,}[.0-9]{0,})/, null !== c.exec(b) && (a = parseFloat(RegExp.$1)));
        return a
    };
    d.applyLang = function(a, b) {
        var c = d.translations;
        b.dayNames = d.extend({}, d.dayNames);
        b.shortDayNames = d.extend({}, d.shortDayNames);
        b.monthNames = d.extend({}, d.monthNames);
        b.shortMonthNames = d.extend({}, d.shortMonthNames);
        b.amString = "am";
        b.pmString = "pm";
        c && (c = c[a]) && (d.lang = c, b.langObj = c, c.monthNames && (b.dayNames = d.extend({}, c.dayNames), b.shortDayNames = d.extend({}, c.shortDayNames), b.monthNames = d.extend({}, c.monthNames), b.shortMonthNames = d.extend({}, c.shortMonthNames)), c.am && (b.amString = c.am), c.pm && (b.pmString = c.pm));
        d.amString = b.amString;
        d.pmString = b.pmString
    };
    d.IEversion = d.getIEVersion();
    9 > d.IEversion && 0 < d.IEversion && (d.isModern = !1, d.isIE = !0);
    d.dx = 0;
    d.dy = 0;
    if (document.addEventListener || window.opera) d.isNN = !0, d.isIE = !1, d.dx = .5, d.dy = .5;
    document.attachEvent && (d.isNN = !1, d.isIE = !0, d.isModern || (d.dx = 0, d.dy = 0));
    window.chrome && (d.chrome = !0);
    d.handleMouseUp = function(a) {
        for (var b = d.charts, c = 0; c < b.length; c++) {
            var e = b[c];
            e && e.handleReleaseOutside && e.handleReleaseOutside(a)
        }
    };
    d.handleMouseMove = function(a) {
        for (var b = d.charts, c = 0; c < b.length; c++) {
            var e = b[c];
            e && e.handleMouseMove && e.handleMouseMove(a)
        }
    };
    d.handleWheel = function(a) {
        for (var b = d.charts, c = 0; c < b.length; c++) {
            var e = b[c];
            if (e && e.mouseIsOver) {
                (e.mouseWheelScrollEnabled ||
                    e.mouseWheelZoomEnabled) && e.handleWheel && e.handleWheel(a);
                break
            }
        }
    };
    d.resetMouseOver = function() {
        for (var a = d.charts, b = 0; b < a.length; b++) {
            var c = a[b];
            c && (c.mouseIsOver = !1)
        }
    };
    d.ready = function(a) {
        d.onReadyArray.push(a)
    };
    d.handleLoad = function() {
        d.isReady = !0;
        for (var a = d.onReadyArray, b = 0; b < a.length; b++) {
            var c = a[b];
            isNaN(d.processDelay) ? c() : setTimeout(c, d.processDelay * b)
        }
    };
    d.addInitHandler = function(a, b) {
        d.initHandlers.push({
            method: a,
            types: b
        })
    };
    d.callInitHandler = function(a) {
        var b = d.initHandlers;
        if (d.initHandlers)
            for (var c =
                    0; c < b.length; c++) {
                var e = b[c];
                e.types ? d.isInArray(e.types, a.type) && e.method(a) : e.method(a)
            }
    };
    d.getUniqueId = function() {
        d.uid++;
        return "AmChartsEl-" + d.uid
    };
    d.isNN && (document.addEventListener("mousemove", d.handleMouseMove), document.addEventListener("mouseup", d.handleMouseUp, !0), window.addEventListener("load", d.handleLoad, !0));
    d.isIE && (document.attachEvent("onmousemove", d.handleMouseMove), document.attachEvent("onmouseup", d.handleMouseUp), window.attachEvent("onload", d.handleLoad));
    d.addWheelListeners = function() {
        d.wheelIsListened ||
            (d.isNN && (window.addEventListener("DOMMouseScroll", d.handleWheel, !0), document.addEventListener("mousewheel", d.handleWheel, !0)), d.isIE && document.attachEvent("onmousewheel", d.handleWheel));
        d.wheelIsListened = !0
    };
    d.clear = function() {
        var a = d.charts;
        if (a)
            for (var b = a.length - 1; 0 <= b; b--) a[b].clear();
        d.updateInt && clearInterval(d.updateInt);
        d.requestAnimation && window.cancelAnimationFrame(d.requestAnimation);
        d.charts = [];
        d.isNN && (document.removeEventListener("mousemove", d.handleMouseMove, !0), document.removeEventListener("mouseup",
            d.handleMouseUp, !0), window.removeEventListener("load", d.handleLoad, !0), window.removeEventListener("DOMMouseScroll", d.handleWheel, !0), document.removeEventListener("mousewheel", d.handleWheel, !0));
        d.isIE && (document.detachEvent("onmousemove", d.handleMouseMove), document.detachEvent("onmouseup", d.handleMouseUp), window.detachEvent("onload", d.handleLoad))
    };
    d.makeChart = function(a, b, c) {
        var e = b.type,
            h = b.theme;
        d.isString(h) && (h = d.themes[h], b.theme = h);
        var f;
        switch (e) {
            case "serial":
                f = new d.AmSerialChart(h);
                break;
            case "xy":
                f = new d.AmXYChart(h);
                break;
            case "pie":
                f = new d.AmPieChart(h);
                break;
            case "radar":
                f = new d.AmRadarChart(h);
                break;
            case "gauge":
                f = new d.AmAngularGauge(h);
                break;
            case "funnel":
                f = new d.AmFunnelChart(h);
                break;
            case "map":
                f = new d.AmMap(h);
                break;
            case "stock":
                f = new d.AmStockChart(h);
                break;
            case "gantt":
                f = new d.AmGanttChart(h)
        }
        d.extend(f, b);
        d.isReady ? isNaN(c) ? f.write(a) : setTimeout(function() {
            d.realWrite(f, a)
        }, c) : d.ready(function() {
            isNaN(c) ? f.write(a) : setTimeout(function() {
                d.realWrite(f, a)
            }, c)
        });
        return f
    };
    d.realWrite = function(a, b) {
        a.write(b)
    };
    d.updateCount = 0;
    d.validateAt = Math.round(d.updateRate / 10);
    d.update = function() {
        var a = d.charts;
        d.updateCount++;
        var b = !1;
        d.updateCount == d.validateAt && (b = !0, d.updateCount = 0);
        if (a)
            for (var c = a.length - 1; 0 <= c; c--) a[c].update && a[c].update(), b && (a[c].autoResize ? a[c].validateSize && a[c].validateSize() : a[c].premeasure && a[c].premeasure());
        window.requestAnimationFrame && (d.requestAnimation = window.requestAnimationFrame(d.update))
    };
    "complete" == document.readyState && d.handleLoad()
})();
(function() {
    var d = window.AmCharts;
    d.toBoolean = function(a, b) {
        if (void 0 === a) return b;
        switch (String(a).toLowerCase()) {
            case "true":
            case "yes":
            case "1":
                return !0;
            case "false":
            case "no":
            case "0":
            case null:
                return !1;
            default:
                return !!a
        }
    };
    d.removeFromArray = function(a, b) {
        var c;
        if (void 0 !== b && void 0 !== a)
            for (c = a.length - 1; 0 <= c; c--) a[c] == b && a.splice(c, 1)
    };
    d.getPath = function() {
        var a = document.getElementsByTagName("script");
        if (a)
            for (var b = 0; b < a.length; b++) {
                var c = a[b].src;
                if (-1 !== c.search(/\/(amcharts|ammap)\.js/)) return c.replace(/\/(amcharts|ammap)\.js.*/,
                    "/")
            }
    };
    d.normalizeUrl = function(a) {
        return "" !== a && -1 === a.search(/\/$/) ? a + "/" : a
    };
    d.isAbsolute = function(a) {
        return 0 === a.search(/^http[s]?:|^\//)
    };
    d.isInArray = function(a, b) {
        for (var c = 0; c < a.length; c++)
            if (a[c] == b) return !0;
        return !1
    };
    d.getDecimals = function(a) {
        var b = 0;
        isNaN(a) || (a = String(a), -1 != a.indexOf("e-") ? b = Number(a.split("-")[1]) : -1 != a.indexOf(".") && (b = a.split(".")[1].length));
        return b
    };
    d.wordwrap = function(a, b, c, e) {
        var h, f, g, k;
        a += "";
        if (1 > b) return a;
        h = -1;
        for (a = (k = a.split(/\r\n|\n|\r/)).length; ++h < a; k[h] +=
            g) {
            g = k[h];
            for (k[h] = ""; g.length > b; k[h] += d.trim(g.slice(0, f)) + ((g = g.slice(f)).length ? c : "")) f = 2 == e || (f = g.slice(0, b + 1).match(/\S*(\s)?$/))[1] ? b : f.input.length - f[0].length || 1 == e && b || f.input.length + (f = g.slice(b).match(/^\S*/))[0].length;
            g = d.trim(g)
        }
        return k.join(c)
    };
    d.trim = function(a) {
        return a.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "")
    };
    d.wrappedText = function(a, b, c, e, h, f, g, k) {
        var l = d.text(a, b, c, e, h, f, g);
        if (l) {
            var m = l.getBBox();
            if (m.width > k) {
                var p = "\n";
                d.isModern || (p = "<br>");
                k = Math.floor(k / (m.width /
                    b.length));
                2 < k && (k -= 2);
                b = d.wordwrap(b, k, p, !0);
                l.remove();
                l = d.text(a, b, c, e, h, f, g)
            }
        }
        return l
    };
    d.getStyle = function(a, b) {
        var c = "";
        if (document.defaultView && document.defaultView.getComputedStyle) try {
            c = document.defaultView.getComputedStyle(a, "").getPropertyValue(b)
        } catch (e) {} else a.currentStyle && (b = b.replace(/\-(\w)/g, function(a, b) {
            return b.toUpperCase()
        }), c = a.currentStyle[b]);
        return c
    };
    d.removePx = function(a) {
        if (void 0 !== a) return Number(a.substring(0, a.length - 2))
    };
    d.getURL = function(a, b) {
        if (a)
            if ("_self" !=
                b && b)
                if ("_top" == b && window.top) window.top.location.href = a;
                else if ("_parent" == b && window.parent) window.parent.location.href = a;
        else if ("_blank" == b) window.open(a);
        else {
            var c = document.getElementsByName(b)[0];
            c ? c.src = a : (c = d.windows[b]) ? c.opener && !c.opener.closed ? c.location.href = a : d.windows[b] = window.open(a) : d.windows[b] = window.open(a)
        } else window.location.href = a
    };
    d.ifArray = function(a) {
        return a && "object" == typeof a && 0 < a.length ? !0 : !1
    };
    d.callMethod = function(a, b) {
        var c;
        for (c = 0; c < b.length; c++) {
            var e = b[c];
            if (e) {
                if (e[a]) e[a]();
                var d = e.length;
                if (0 < d) {
                    var f;
                    for (f = 0; f < d; f++) {
                        var g = e[f];
                        if (g && g[a]) g[a]()
                    }
                }
            }
        }
    };
    d.toNumber = function(a) {
        return "number" == typeof a ? a : Number(String(a).replace(/[^0-9\-.]+/g, ""))
    };
    d.toColor = function(a) {
        if ("" !== a && void 0 !== a)
            if (-1 != a.indexOf(",")) {
                a = a.split(",");
                var b;
                for (b = 0; b < a.length; b++) {
                    var c = a[b].substring(a[b].length - 6, a[b].length);
                    a[b] = "#" + c
                }
            } else a = a.substring(a.length - 6, a.length), a = "#" + a;
        return a
    };
    d.toCoordinate = function(a, b, c) {
        var e;
        void 0 !== a && (a = String(a), c && c < b && (b = c), e = Number(a), -1 != a.indexOf("!") &&
            (e = b - Number(a.substr(1))), -1 != a.indexOf("%") && (e = b * Number(a.substr(0, a.length - 1)) / 100));
        return e
    };
    d.fitToBounds = function(a, b, c) {
        a < b && (a = b);
        a > c && (a = c);
        return a
    };
    d.isDefined = function(a) {
        return void 0 === a ? !1 : !0
    };
    d.stripNumbers = function(a) {
        return a.replace(/[0-9]+/g, "")
    };
    d.roundTo = function(a, b) {
        if (0 > b) return a;
        var c = Math.pow(10, b);
        return Math.round(a * c) / c
    };
    d.toFixed = function(a, b) {
        var c = String(Math.round(a * Math.pow(10, b)));
        if (0 < b) {
            var e = c.length;
            if (e < b) {
                var d;
                for (d = 0; d < b - e; d++) c = "0" + c
            }
            e = c.substring(0,
                c.length - b);
            "" === e && (e = 0);
            return e + "." + c.substring(c.length - b, c.length)
        }
        return String(c)
    };
    d.formatDuration = function(a, b, c, e, h, f) {
        var g = d.intervals,
            k = f.decimalSeparator;
        if (a >= g[b].contains) {
            var l = a - Math.floor(a / g[b].contains) * g[b].contains;
            "ss" == b ? (l = d.formatNumber(l, f), 1 == l.split(k)[0].length && (l = "0" + l)) : l = d.roundTo(l, f.precision);
            ("mm" == b || "hh" == b) && 10 > l && (l = "0" + l);
            c = l + "" + e[b] + "" + c;
            a = Math.floor(a / g[b].contains);
            b = g[b].nextInterval;
            return d.formatDuration(a, b, c, e, h, f)
        }
        "ss" == b && (a = d.formatNumber(a,
            f), 1 == a.split(k)[0].length && (a = "0" + a));
        ("mm" == b || "hh" == b) && 10 > a && (a = "0" + a);
        c = a + "" + e[b] + "" + c;
        if (g[h].count > g[b].count)
            for (a = g[b].count; a < g[h].count; a++) b = g[b].nextInterval, "ss" == b || "mm" == b || "hh" == b ? c = "00" + e[b] + "" + c : "DD" == b && (c = "0" + e[b] + "" + c);
        ":" == c.charAt(c.length - 1) && (c = c.substring(0, c.length - 1));
        return c
    };
    d.formatNumber = function(a, b, c, e, h) {
        a = d.roundTo(a, b.precision);
        isNaN(c) && (c = b.precision);
        var f = b.decimalSeparator;
        b = b.thousandsSeparator;
        var g;
        g = 0 > a ? "-" : "";
        a = Math.abs(a);
        var k = String(a),
            l = !1; - 1 !=
            k.indexOf("e") && (l = !0);
        0 <= c && !l && (k = d.toFixed(a, c));
        var m = "";
        if (l) m = k;
        else {
            var k = k.split("."),
                l = String(k[0]),
                p;
            for (p = l.length; 0 <= p; p -= 3) m = p != l.length ? 0 !== p ? l.substring(p - 3, p) + b + m : l.substring(p - 3, p) + m : l.substring(p - 3, p);
            void 0 !== k[1] && (m = m + f + k[1]);
            void 0 !== c && 0 < c && "0" != m && (m = d.addZeroes(m, f, c))
        }
        m = g + m;
        "" === g && !0 === e && 0 !== a && (m = "+" + m);
        !0 === h && (m += "%");
        return m
    };
    d.addZeroes = function(a, b, c) {
        a = a.split(b);
        void 0 === a[1] && 0 < c && (a[1] = "0");
        return a[1].length < c ? (a[1] += "0", d.addZeroes(a[0] + b + a[1], b, c)) : void 0 !==
            a[1] ? a[0] + b + a[1] : a[0]
    };
    d.scientificToNormal = function(a) {
        var b;
        a = String(a).split("e");
        var c;
        if ("-" == a[1].substr(0, 1)) {
            b = "0.";
            for (c = 0; c < Math.abs(Number(a[1])) - 1; c++) b += "0";
            b += a[0].split(".").join("")
        } else {
            var e = 0;
            b = a[0].split(".");
            b[1] && (e = b[1].length);
            b = a[0].split(".").join("");
            for (c = 0; c < Math.abs(Number(a[1])) - e; c++) b += "0"
        }
        return b
    };
    d.toScientific = function(a, b) {
        if (0 === a) return "0";
        var c = Math.floor(Math.log(Math.abs(a)) * Math.LOG10E),
            e = String(e).split(".").join(b);
        return String(e) + "e" + c
    };
    d.randomColor =
        function() {
            return "#" + ("00000" + (16777216 * Math.random() << 0).toString(16)).substr(-6)
        };
    d.hitTest = function(a, b, c) {
        var e = !1,
            h = a.x,
            f = a.x + a.width,
            g = a.y,
            k = a.y + a.height,
            l = d.isInRectangle;
        e || (e = l(h, g, b));
        e || (e = l(h, k, b));
        e || (e = l(f, g, b));
        e || (e = l(f, k, b));
        e || !0 === c || (e = d.hitTest(b, a, !0));
        return e
    };
    d.isInRectangle = function(a, b, c) {
        return a >= c.x - 5 && a <= c.x + c.width + 5 && b >= c.y - 5 && b <= c.y + c.height + 5 ? !0 : !1
    };
    d.isPercents = function(a) {
        if (-1 != String(a).indexOf("%")) return !0
    };
    d.formatValue = function(a, b, c, e, h, f, g, k) {
        if (b) {
            void 0 ===
                h && (h = "");
            var l;
            for (l = 0; l < c.length; l++) {
                var m = c[l],
                    p = b[m];
                void 0 !== p && (p = f ? d.addPrefix(p, k, g, e) : d.formatNumber(p, e), a = a.replace(new RegExp("\\[\\[" + h + "" + m + "\\]\\]", "g"), p))
            }
        }
        return a
    };
    d.formatDataContextValue = function(a, b) {
        if (a) {
            var c = a.match(/\[\[.*?\]\]/g),
                e;
            for (e = 0; e < c.length; e++) {
                var d = c[e],
                    d = d.substr(2, d.length - 4);
                void 0 !== b[d] && (a = a.replace(new RegExp("\\[\\[" + d + "\\]\\]", "g"), b[d]))
            }
        }
        return a
    };
    d.massReplace = function(a, b) {
        for (var c in b)
            if (b.hasOwnProperty(c)) {
                var e = b[c];
                void 0 === e && (e = "");
                a =
                    a.replace(c, e)
            }
        return a
    };
    d.cleanFromEmpty = function(a) {
        return a.replace(/\[\[[^\]]*\]\]/g, "")
    };
    d.addPrefix = function(a, b, c, e, h) {
        var f = d.formatNumber(a, e),
            g = "",
            k, l, m;
        if (0 === a) return "0";
        0 > a && (g = "-");
        a = Math.abs(a);
        if (1 < a)
            for (k = b.length - 1; - 1 < k; k--) {
                if (a >= b[k].number && (l = a / b[k].number, m = Number(e.precision), 1 > m && (m = 1), c = d.roundTo(l, m), m = d.formatNumber(c, {
                        precision: -1,
                        decimalSeparator: e.decimalSeparator,
                        thousandsSeparator: e.thousandsSeparator
                    }), !h || l == c)) {
                    f = g + "" + m + "" + b[k].prefix;
                    break
                }
            } else
                for (k = 0; k < c.length; k++)
                    if (a <=
                        c[k].number) {
                        l = a / c[k].number;
                        m = Math.abs(Math.floor(Math.log(l) * Math.LOG10E));
                        l = d.roundTo(l, m);
                        f = g + "" + l + "" + c[k].prefix;
                        break
                    }
        return f
    };
    d.remove = function(a) {
        a && a.remove()
    };
    d.getEffect = function(a) {
        ">" == a && (a = "easeOutSine");
        "<" == a && (a = "easeInSine");
        "elastic" == a && (a = "easeOutElastic");
        return a
    };
    d.getObjById = function(a, b) {
        var c, e;
        for (e = 0; e < a.length; e++) {
            var d = a[e];
            if (d.id == b) {
                c = d;
                break
            }
        }
        return c
    };
    d.applyTheme = function(a, b, c) {
        b || (b = d.theme);
        try {
            b = JSON.parse(JSON.stringify(b))
        } catch (e) {}
        b && b[c] && d.extend(a,
            b[c])
    };
    d.isString = function(a) {
        return "string" == typeof a ? !0 : !1
    };
    d.extend = function(a, b, c) {
        var e;
        a || (a = {});
        for (e in b) c ? a.hasOwnProperty(e) || (a[e] = b[e]) : a[e] = b[e];
        return a
    };
    d.copyProperties = function(a, b) {
        for (var c in a) a.hasOwnProperty(c) && "events" != c && void 0 !== a[c] && "function" != typeof a[c] && "cname" != c && (b[c] = a[c])
    };
    d.processObject = function(a, b, c, e) {
        if (!1 === a instanceof b && (a = e ? d.extend(new b(c), a) : d.extend(a, new b(c), !0), a.listeners))
            for (var h in a.listeners) b = a.listeners[h], a.addListener(b.event, b.method);
        return a
    };
    d.fixNewLines = function(a) {
        var b = RegExp("\\n", "g");
        a && (a = a.replace(b, "<br />"));
        return a
    };
    d.fixBrakes = function(a) {
        if (d.isModern) {
            var b = RegExp("<br>", "g");
            a && (a = a.replace(b, "\n"))
        } else a = d.fixNewLines(a);
        return a
    };
    d.deleteObject = function(a, b) {
        if (a) {
            if (void 0 === b || null === b) b = 20;
            if (0 !== b)
                if ("[object Array]" === Object.prototype.toString.call(a))
                    for (var c = 0; c < a.length; c++) d.deleteObject(a[c], b - 1), a[c] = null;
                else if (a && !a.tagName) try {
                for (c in a.theme = null, a) a[c] && ("object" == typeof a[c] && d.deleteObject(a[c],
                    b - 1), "function" != typeof a[c] && (a[c] = null))
            } catch (e) {}
        }
    };
    d.bounce = function(a, b, c, e, d) {
        return (b /= d) < 1 / 2.75 ? 7.5625 * e * b * b + c : b < 2 / 2.75 ? e * (7.5625 * (b -= 1.5 / 2.75) * b + .75) + c : b < 2.5 / 2.75 ? e * (7.5625 * (b -= 2.25 / 2.75) * b + .9375) + c : e * (7.5625 * (b -= 2.625 / 2.75) * b + .984375) + c
    };
    d.easeInOutQuad = function(a, b, c, e, d) {
        b /= d / 2;
        if (1 > b) return e / 2 * b * b + c;
        b--;
        return -e / 2 * (b * (b - 2) - 1) + c
    };
    d.easeInSine = function(a, b, c, e, d) {
        return -e * Math.cos(b / d * (Math.PI / 2)) + e + c
    };
    d.easeOutSine = function(a, b, c, e, d) {
        return e * Math.sin(b / d * (Math.PI / 2)) + c
    };
    d.easeOutElastic =
        function(a, b, c, e, d) {
            a = 1.70158;
            var f = 0,
                g = e;
            if (0 === b) return c;
            if (1 == (b /= d)) return c + e;
            f || (f = .3 * d);
            g < Math.abs(e) ? (g = e, a = f / 4) : a = f / (2 * Math.PI) * Math.asin(e / g);
            return g * Math.pow(2, -10 * b) * Math.sin(2 * (b * d - a) * Math.PI / f) + e + c
        };
    d.fixStepE = function(a) {
        a = a.toExponential(0).split("e");
        var b = Number(a[1]);
        9 == Number(a[0]) && b++;
        return d.generateNumber(1, b)
    };
    d.generateNumber = function(a, b) {
        var c = "",
            e;
        e = 0 > b ? Math.abs(b) - 1 : Math.abs(b);
        var d;
        for (d = 0; d < e; d++) c += "0";
        return 0 > b ? Number("0." + c + String(a)) : Number(String(a) + c)
    };
    d.setCN =
        function(a, b, c, e) {
            if (a.addClassNames && b && (b = b.node) && c) {
                var d = b.getAttribute("class");
                a = a.classNamePrefix + "-";
                e && (a = "");
                d ? b.setAttribute("class", d + " " + a + c) : b.setAttribute("class", a + c)
            }
        };
    d.removeCN = function(a, b, c) {
        b && (b = b.node) && c && (b = b.classList) && b.remove(a.classNamePrefix + "-" + c)
    };
    d.parseDefs = function(a, b) {
        for (var c in a) {
            var e = typeof a[c];
            if (0 < a[c].length && "object" == e)
                for (var h = 0; h < a[c].length; h++) e = document.createElementNS(d.SVG_NS, c), b.appendChild(e), d.parseDefs(a[c][h], e);
            else "object" == e ? (e =
                document.createElementNS(d.SVG_NS, c), b.appendChild(e), d.parseDefs(a[c], e)) : b.setAttribute(c, a[c])
        }
    }
})();
(function() {
    var d = window.AmCharts;
    d.AxisBase = d.Class({
        construct: function(a) {
            this.createEvents("clickItem", "rollOverItem", "rollOutItem", "rollOverGuide", "rollOutGuide", "clickGuide");
            this.titleDY = this.y = this.x = this.dy = this.dx = 0;
            this.axisThickness = 1;
            this.axisColor = "#000000";
            this.axisAlpha = 1;
            this.gridCount = this.tickLength = 5;
            this.gridAlpha = .15;
            this.gridThickness = 1;
            this.gridColor = "#000000";
            this.dashLength = 0;
            this.labelFrequency = 1;
            this.showLastLabel = this.showFirstLabel = !0;
            this.fillColor = "#FFFFFF";
            this.fillAlpha =
                0;
            this.labelsEnabled = !0;
            this.labelRotation = 0;
            this.autoGridCount = !0;
            this.offset = 0;
            this.guides = [];
            this.visible = !0;
            this.counter = 0;
            this.guides = [];
            this.ignoreAxisWidth = this.inside = !1;
            this.minHorizontalGap = 75;
            this.minVerticalGap = 35;
            this.titleBold = !0;
            this.minorGridEnabled = !1;
            this.minorGridAlpha = .07;
            this.autoWrap = !1;
            this.titleAlign = "middle";
            this.labelOffset = 0;
            this.bcn = "axis-";
            this.centerLabels = !1;
            this.firstDayOfWeek = 1;
            this.centerLabelOnFullPeriod = this.markPeriodChange = this.boldPeriodBeginning = !0;
            this.periods = [{
                    period: "fff",
                    count: 1
                }, {
                    period: "fff",
                    count: 5
                }, {
                    period: "fff",
                    count: 10
                }, {
                    period: "fff",
                    count: 50
                }, {
                    period: "fff",
                    count: 100
                }, {
                    period: "fff",
                    count: 500
                }, {
                    period: "ss",
                    count: 1
                }, {
                    period: "ss",
                    count: 5
                }, {
                    period: "ss",
                    count: 10
                }, {
                    period: "ss",
                    count: 30
                }, {
                    period: "mm",
                    count: 1
                }, {
                    period: "mm",
                    count: 5
                }, {
                    period: "mm",
                    count: 10
                }, {
                    period: "mm",
                    count: 30
                }, {
                    period: "hh",
                    count: 1
                }, {
                    period: "hh",
                    count: 3
                }, {
                    period: "hh",
                    count: 6
                }, {
                    period: "hh",
                    count: 12
                }, {
                    period: "DD",
                    count: 1
                }, {
                    period: "DD",
                    count: 2
                }, {
                    period: "DD",
                    count: 3
                }, {
                    period: "DD",
                    count: 4
                },
                {
                    period: "DD",
                    count: 5
                }, {
                    period: "WW",
                    count: 1
                }, {
                    period: "MM",
                    count: 1
                }, {
                    period: "MM",
                    count: 2
                }, {
                    period: "MM",
                    count: 3
                }, {
                    period: "MM",
                    count: 6
                }, {
                    period: "YYYY",
                    count: 1
                }, {
                    period: "YYYY",
                    count: 2
                }, {
                    period: "YYYY",
                    count: 5
                }, {
                    period: "YYYY",
                    count: 10
                }, {
                    period: "YYYY",
                    count: 50
                }, {
                    period: "YYYY",
                    count: 100
                }
            ];
            this.dateFormats = [{
                    period: "fff",
                    format: "NN:SS.QQQ"
                }, {
                    period: "ss",
                    format: "JJ:NN:SS"
                }, {
                    period: "mm",
                    format: "JJ:NN"
                }, {
                    period: "hh",
                    format: "JJ:NN"
                }, {
                    period: "DD",
                    format: "MMM DD"
                }, {
                    period: "WW",
                    format: "MMM DD"
                }, {
                    period: "MM",
                    format: "MMM"
                },
                {
                    period: "YYYY",
                    format: "YYYY"
                }
            ];
            this.nextPeriod = {
                fff: "ss",
                ss: "mm",
                mm: "hh",
                hh: "DD",
                DD: "MM",
                MM: "YYYY"
            };
            d.applyTheme(this, a, "AxisBase")
        },
        zoom: function(a, b) {
            this.start = a;
            this.end = b;
            this.dataChanged = !0;
            this.draw()
        },
        fixAxisPosition: function() {
            var a = this.position;
            "H" == this.orientation ? ("left" == a && (a = "bottom"), "right" == a && (a = "top")) : ("bottom" == a && (a = "left"), "top" == a && (a = "right"));
            this.position = a
        },
        init: function() {
            this.createBalloon()
        },
        draw: function() {
            var a = this.chart;
            this.prevBY = this.prevBX = NaN;
            this.allLabels = [];
            this.counter = 0;
            this.destroy();
            this.fixAxisPosition();
            this.setBalloonBounds();
            this.labels = [];
            var b = a.container,
                c = b.set();
            a.gridSet.push(c);
            this.set = c;
            b = b.set();
            a.axesLabelsSet.push(b);
            this.labelsSet = b;
            this.axisLine = new this.axisRenderer(this);
            this.autoGridCount ? ("V" == this.orientation ? (a = this.height / this.minVerticalGap, 3 > a && (a = 3)) : a = this.width / this.minHorizontalGap, this.gridCountR = Math.max(a, 1)) : this.gridCountR = this.gridCount;
            this.axisWidth = this.axisLine.axisWidth;
            this.addTitle()
        },
        setOrientation: function(a) {
            this.orientation =
                a ? "H" : "V"
        },
        addTitle: function() {
            var a = this.title;
            this.titleLabel = null;
            if (a) {
                var b = this.chart,
                    c = this.titleColor;
                void 0 === c && (c = b.color);
                var e = this.titleFontSize;
                isNaN(e) && (e = b.fontSize + 1);
                a = d.text(b.container, a, c, b.fontFamily, e, this.titleAlign, this.titleBold);
                d.setCN(b, a, this.bcn + "title");
                this.titleLabel = a
            }
        },
        positionTitle: function() {
            var a = this.titleLabel;
            if (a) {
                var b, c, e = this.labelsSet,
                    h = {};
                0 < e.length() ? h = e.getBBox() : (h.x = 0, h.y = 0, h.width = this.width, h.height = this.height, d.VML && (h.y += this.y, h.x += this.x));
                e.push(a);
                var e = h.x,
                    f = h.y;
                d.VML && (f -= this.y, e -= this.x);
                var g = h.width,
                    h = h.height,
                    k = this.width,
                    l = this.height,
                    m = 0,
                    p = a.getBBox().height / 2,
                    q = this.inside,
                    n = this.titleAlign;
                switch (this.position) {
                    case "top":
                        b = "left" == n ? -1 : "right" == n ? k : k / 2;
                        c = f - 10 - p;
                        break;
                    case "bottom":
                        b = "left" == n ? -1 : "right" == n ? k : k / 2;
                        c = f + h + 10 + p;
                        break;
                    case "left":
                        b = e - 10 - p;
                        q && (b -= 5);
                        m = -90;
                        c = ("left" == n ? l + 1 : "right" == n ? -1 : l / 2) + this.titleDY;
                        break;
                    case "right":
                        b = e + g + 10 + p, q && (b += 7), c = ("left" == n ? l + 2 : "right" == n ? -2 : l / 2) + this.titleDY, m = -90
                }
                this.marginsChanged ?
                    (a.translate(b, c), this.tx = b, this.ty = c) : a.translate(this.tx, this.ty);
                this.marginsChanged = !1;
                isNaN(this.titleRotation) || (m = this.titleRotation);
                0 !== m && a.rotate(m)
            }
        },
        pushAxisItem: function(a, b) {
            var c = this,
                e = a.graphics();
            0 < e.length() && (b ? c.labelsSet.push(e) : c.set.push(e));
            if (e = a.getLabel()) c.labelsSet.push(e), e.click(function(b) {
                c.handleMouse(b, a, "clickItem")
            }).touchend(function(b) {
                c.handleMouse(b, a, "clickItem")
            }).mouseover(function(b) {
                c.handleMouse(b, a, "rollOverItem")
            }).mouseout(function(b) {
                c.handleMouse(b,
                    a, "rollOutItem")
            })
        },
        handleMouse: function(a, b, c) {
            this.fire({
                type: c,
                value: b.value,
                serialDataItem: b.serialDataItem,
                axis: this,
                target: b.label,
                chart: this.chart,
                event: a
            })
        },
        addGuide: function(a) {
            for (var b = this.guides, c = !1, e = b.length, h = 0; h < b.length; h++) b[h] == a && (c = !0, e = h);
            a = d.processObject(a, d.Guide, this.theme);
            a.id || (a.id = "guideAuto" + e + "_" + (new Date).getTime());
            c || b.push(a)
        },
        removeGuide: function(a) {
            var b = this.guides,
                c;
            for (c = 0; c < b.length; c++) b[c] == a && b.splice(c, 1)
        },
        handleGuideOver: function(a) {
            clearTimeout(this.chart.hoverInt);
            var b = a.graphics.getBBox(),
                c = this.x + b.x + b.width / 2,
                b = this.y + b.y + b.height / 2,
                e = a.fillColor;
            void 0 === e && (e = a.lineColor);
            this.chart.showBalloon(a.balloonText, e, !0, c, b);
            this.fire({
                type: "rollOverGuide",
                guide: a,
                chart: this.chart
            })
        },
        handleGuideOut: function(a) {
            this.chart.hideBalloon();
            this.fire({
                type: "rollOutGuide",
                guide: a,
                chart: this.chart
            })
        },
        handleGuideClick: function(a) {
            this.chart.hideBalloon();
            this.fire({
                type: "clickGuide",
                guide: a,
                chart: this.chart
            })
        },
        addEventListeners: function(a, b) {
            var c = this;
            a.mouseover(function() {
                c.handleGuideOver(b)
            });
            a.mouseup(function() {
                c.handleGuideClick(b)
            });
            a.touchstart(function() {
                c.handleGuideOver(b)
            });
            a.mouseout(function() {
                c.handleGuideOut(b)
            })
        },
        getBBox: function() {
            var a;
            this.labelsSet && (a = this.labelsSet.getBBox());
            a ? d.VML || (a = {
                x: a.x + this.x,
                y: a.y + this.y,
                width: a.width,
                height: a.height
            }) : a = {
                x: 0,
                y: 0,
                width: 0,
                height: 0
            };
            return a
        },
        destroy: function() {
            d.remove(this.set);
            d.remove(this.labelsSet);
            var a = this.axisLine;
            a && d.remove(a.axisSet);
            d.remove(this.grid0)
        },
        chooseMinorFrequency: function(a) {
            for (var b = 10; 0 < b; b--)
                if (a /
                    b == Math.round(a / b)) return a / b
        },
        parseDatesDraw: function() {
            var a, b = this.chart,
                c = this.showFirstLabel,
                e = this.showLastLabel,
                h, f = "",
                g = d.extractPeriod(this.minPeriod),
                k = d.getPeriodDuration(g.period, g.count),
                l, m, p, q, n, t = this.firstDayOfWeek,
                r = this.boldPeriodBeginning;
            a = this.minorGridEnabled;
            var w, z = this.gridAlpha,
                x, u = this.choosePeriod(0),
                A = u.period,
                u = u.count,
                y = d.getPeriodDuration(A, u);
            y < k && (A = g.period, u = g.count, y = k);
            g = A;
            "WW" == g && (g = "DD");
            this.stepWidth = this.getStepWidth(this.timeDifference);
            var B = Math.ceil(this.timeDifference /
                    y) + 5,
                D = l = d.resetDateToMin(new Date(this.startTime - y), A, u, t).getTime();
            if (g == A && 1 == u && this.centerLabelOnFullPeriod || this.autoWrap || this.centerLabels) p = y * this.stepWidth, this.autoWrap && !this.centerLabels && (p = -p);
            this.cellWidth = k * this.stepWidth;
            q = Math.round(l / y);
            k = -1;
            q / 2 == Math.round(q / 2) && (k = -2, l -= y);
            q = this.firstTime;
            var C = 0,
                I = 0;
            a && 1 < u && (w = this.chooseMinorFrequency(u), x = d.getPeriodDuration(A, w), "DD" == A && (x += d.getPeriodDuration("hh")), "fff" == A && (x = 1));
            if (0 < this.gridCountR)
                for (B - 5 - k > this.autoRotateCount &&
                    !isNaN(this.autoRotateAngle) && (this.labelRotationR = this.autoRotateAngle), a = k; a <= B; a++) {
                    n = q + y * (a + Math.floor((D - q) / y)) - C;
                    "DD" == A && (n += 36E5);
                    n = d.resetDateToMin(new Date(n), A, u, t).getTime();
                    "MM" == A && (h = (n - l) / y, 1.5 <= (n - l) / y && (n = n - (h - 1) * y + d.getPeriodDuration("DD", 3), n = d.resetDateToMin(new Date(n), A, 1).getTime(), C += y));
                    h = (n - this.startTime) * this.stepWidth;
                    if ("radar" == b.type) {
                        if (h = this.axisWidth - h, 0 > h || h > this.axisWidth) continue
                    } else this.rotate ? "date" == this.type && "middle" == this.gridPosition && (I = -y * this.stepWidth /
                        2) : "date" == this.type && (h = this.axisWidth - h);
                    f = !1;
                    this.nextPeriod[g] && (f = this.checkPeriodChange(this.nextPeriod[g], 1, n, l, g));
                    l = !1;
                    f && this.markPeriodChange ? (f = this.dateFormatsObject[this.nextPeriod[g]], this.twoLineMode && (f = this.dateFormatsObject[g] + "\n" + f, f = d.fixBrakes(f)), l = !0) : f = this.dateFormatsObject[g];
                    r || (l = !1);
                    this.currentDateFormat = f;
                    f = d.formatDate(new Date(n), f, b);
                    if (a == k && !c || a == B && !e) f = " ";
                    this.labelFunction && (f = this.labelFunction(f, new Date(n), this, A, u, m).toString());
                    this.boldLabels && (l = !0);
                    m = new this.axisItemRenderer(this, h, f, !1, p, I, !1, l);
                    this.pushAxisItem(m);
                    m = l = n;
                    if (!isNaN(w))
                        for (h = 1; h < u; h += w) this.gridAlpha = this.minorGridAlpha, f = n + x * h, f = d.resetDateToMin(new Date(f), A, w, t).getTime(), f = new this.axisItemRenderer(this, (f - this.startTime) * this.stepWidth, void 0, void 0, void 0, void 0, void 0, void 0, void 0, !0), this.pushAxisItem(f);
                    this.gridAlpha = z
                }
        },
        choosePeriod: function(a) {
            var b = d.getPeriodDuration(this.periods[a].period, this.periods[a].count),
                c = this.periods;
            return this.timeDifference < b && 0 <
                a ? c[a - 1] : Math.ceil(this.timeDifference / b) <= this.gridCountR ? c[a] : a + 1 < c.length ? this.choosePeriod(a + 1) : c[a]
        },
        getStepWidth: function(a) {
            var b;
            this.startOnAxis ? (b = this.axisWidth / (a - 1), 1 == a && (b = this.axisWidth)) : b = this.axisWidth / a;
            return b
        },
        timeZoom: function(a, b) {
            this.startTime = a;
            this.endTime = b
        },
        minDuration: function() {
            var a = d.extractPeriod(this.minPeriod);
            return d.getPeriodDuration(a.period, a.count)
        },
        checkPeriodChange: function(a, b, c, e, h) {
            c = new Date(c);
            var f = new Date(e),
                g = this.firstDayOfWeek;
            e = b;
            "DD" == a &&
                (b = 1);
            c = d.resetDateToMin(c, a, b, g).getTime();
            b = d.resetDateToMin(f, a, b, g).getTime();
            return "DD" == a && "hh" != h && c - b < d.getPeriodDuration(a, e) - d.getPeriodDuration("hh", 1) ? !1 : c != b ? !0 : !1
        },
        generateDFObject: function() {
            this.dateFormatsObject = {};
            var a;
            for (a = 0; a < this.dateFormats.length; a++) {
                var b = this.dateFormats[a];
                this.dateFormatsObject[b.period] = b.format
            }
        },
        hideBalloon: function() {
            this.balloon && this.balloon.hide && this.balloon.hide();
            this.prevBY = this.prevBX = NaN
        },
        formatBalloonText: function(a) {
            return a
        },
        showBalloon: function(a,
            b, c, e) {
            var d = this.offset;
            switch (this.position) {
                case "bottom":
                    b = this.height + d;
                    break;
                case "top":
                    b = -d;
                    break;
                case "left":
                    a = -d;
                    break;
                case "right":
                    a = this.width + d
            }
            c || (c = this.currentDateFormat);
            if ("V" == this.orientation) {
                if (0 > b || b > this.height) return;
                if (isNaN(b)) {
                    this.hideBalloon();
                    return
                }
                b = this.adjustBalloonCoordinate(b, e);
                e = this.coordinateToValue(b)
            } else {
                if (0 > a || a > this.width) return;
                if (isNaN(a)) {
                    this.hideBalloon();
                    return
                }
                a = this.adjustBalloonCoordinate(a, e);
                e = this.coordinateToValue(a)
            }
            var f;
            if (d = this.chart.chartCursor) f =
                d.index;
            if (this.balloon && void 0 !== e && this.balloon.enabled) {
                if (this.balloonTextFunction) {
                    if ("date" == this.type || !0 === this.parseDates) e = new Date(e);
                    e = this.balloonTextFunction(e)
                } else this.balloonText ? e = this.formatBalloonText(this.balloonText, f, c) : isNaN(e) || (e = this.formatValue(e, c));
                if (a != this.prevBX || b != this.prevBY) this.balloon.setPosition(a, b), this.prevBX = a, this.prevBY = b, e && this.balloon.showBalloon(e)
            }
        },
        adjustBalloonCoordinate: function(a) {
            return a
        },
        createBalloon: function() {
            var a = this.chart,
                b = a.chartCursor;
            b && (b = b.cursorPosition, "mouse" != b && (this.stickBalloonToCategory = !0), "start" == b && (this.stickBalloonToStart = !0), "ValueAxis" == this.cname && (this.stickBalloonToCategory = !1));
            this.balloon && (this.balloon.destroy && this.balloon.destroy(), d.extend(this.balloon, a.balloon, !0))
        },
        setBalloonBounds: function() {
            var a = this.balloon;
            if (a) {
                var b = this.chart;
                a.cornerRadius = 0;
                a.shadowAlpha = 0;
                a.borderThickness = 1;
                a.borderAlpha = 1;
                a.adjustBorderColor = !1;
                a.showBullet = !1;
                this.balloon = a;
                a.chart = b;
                a.mainSet = b.plotBalloonsSet;
                a.pointerWidth =
                    this.tickLength;
                if (this.parseDates || "date" == this.type) a.pointerWidth = 0;
                a.className = this.id;
                b = "V";
                "V" == this.orientation && (b = "H");
                this.stickBalloonToCategory || (a.animationDuration = 0);
                var c, e, d, f, g = this.inside,
                    k = this.width,
                    l = this.height;
                switch (this.position) {
                    case "bottom":
                        c = 0;
                        e = k;
                        g ? (d = 0, f = l) : (d = l, f = l + 1E3);
                        break;
                    case "top":
                        c = 0;
                        e = k;
                        g ? (d = 0, f = l) : (d = -1E3, f = 0);
                        break;
                    case "left":
                        d = 0;
                        f = l;
                        g ? (c = 0, e = k) : (c = -1E3, e = 0);
                        break;
                    case "right":
                        d = 0, f = l, g ? (c = 0, e = k) : (c = k, e = k + 1E3)
                }
                a.drop || (a.pointerOrientation = b);
                a.setBounds(c,
                    d, e, f)
            }
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.ValueAxis = d.Class({
        inherits: d.AxisBase,
        construct: function(a) {
            this.cname = "ValueAxis";
            this.createEvents("axisChanged", "logarithmicAxisFailed", "axisZoomed", "axisIntZoomed");
            d.ValueAxis.base.construct.call(this, a);
            this.dataChanged = !0;
            this.stackType = "none";
            this.position = "left";
            this.unitPosition = "right";
            this.includeAllValues = this.recalculateToPercents = this.includeHidden = this.includeGuidesInMinMax = this.integersOnly = !1;
            this.durationUnits = {
                DD: "d. ",
                hh: ":",
                mm: ":",
                ss: ""
            };
            this.scrollbar = !1;
            this.baseValue = 0;
            this.radarCategoriesEnabled = !0;
            this.axisFrequency = 1;
            this.gridType = "polygons";
            this.useScientificNotation = !1;
            this.axisTitleOffset = 10;
            this.pointPosition = "axis";
            this.minMaxMultiplier = 1;
            this.logGridLimit = 2;
            this.totalTextOffset = this.treatZeroAs = 0;
            this.minPeriod = "ss";
            this.relativeStart = 0;
            this.relativeEnd = 1;
            d.applyTheme(this, a, this.cname)
        },
        updateData: function() {
            0 >= this.gridCountR && (this.gridCountR = 1);
            this.totals = [];
            this.data = this.chart.chartData;
            var a = this.chart;
            "xy" != a.type &&
                (this.stackGraphs("smoothedLine"), this.stackGraphs("line"), this.stackGraphs("column"), this.stackGraphs("step"));
            this.recalculateToPercents && this.recalculate();
            this.synchronizationMultiplier && this.synchronizeWith ? (d.isString(this.synchronizeWith) && (this.synchronizeWith = a.getValueAxisById(this.synchronizeWith)), this.synchronizeWith && (this.synchronizeWithAxis(this.synchronizeWith), this.foundGraphs = !0)) : (this.foundGraphs = !1, this.getMinMax(), 0 === this.start && this.end == this.data.length - 1 && isNaN(this.minZoom) &&
                isNaN(this.maxZoom) && (this.fullMin = this.min, this.fullMax = this.max, "date" != this.type && this.strictMinMax && (isNaN(this.minimum) || (this.fullMin = this.minimum), isNaN(this.maximum) || (this.fullMax = this.maximum)), this.logarithmic && (this.fullMin = this.logMin, 0 === this.fullMin && (this.fullMin = this.treatZeroAs)), "date" == this.type && (this.minimumDate || (this.fullMin = this.minRR), this.maximumDate || (this.fullMax = this.maxRR), this.strictMinMax && (this.minimumDate && (this.fullMin = this.minimumDate.getTime()), this.maximumDate &&
                    (this.fullMax = this.maximumDate.getTime())))))
        },
        draw: function() {
            d.ValueAxis.base.draw.call(this);
            var a = this.chart,
                b = this.set;
            this.labelRotationR = this.labelRotation;
            d.setCN(a, this.set, "value-axis value-axis-" + this.id);
            d.setCN(a, this.labelsSet, "value-axis value-axis-" + this.id);
            d.setCN(a, this.axisLine.axisSet, "value-axis value-axis-" + this.id);
            var c = this.type;
            "duration" == c && (this.duration = "ss");
            !0 === this.dataChanged && (this.updateData(), this.dataChanged = !1);
            "date" == c && (this.logarithmic = !1, this.min = this.minRR,
                this.max = this.maxRR, this.reversed = !1, this.getDateMinMax());
            if (this.logarithmic) {
                var e = this.treatZeroAs,
                    h = this.getExtremes(0, this.data.length - 1).min;
                !isNaN(this.minimum) && this.minimum < h && (h = this.minimum);
                this.logMin = h;
                this.minReal < h && (this.minReal = h);
                isNaN(this.minReal) && (this.minReal = h);
                0 < e && 0 === h && (this.minReal = h = e);
                if (0 >= h || 0 >= this.minimum) {
                    this.fire({
                        type: "logarithmicAxisFailed",
                        chart: a
                    });
                    return
                }
            }
            this.grid0 = null;
            var f, g, k = a.dx,
                l = a.dy,
                e = !1,
                h = this.logarithmic;
            if (isNaN(this.min) || isNaN(this.max) ||
                !this.foundGraphs || Infinity == this.min || -Infinity == this.max) e = !0;
            else {
                "date" == this.type && this.min == this.max && (this.max += this.minDuration(), this.min -= this.minDuration());
                var m = this.labelFrequency,
                    p = this.showFirstLabel,
                    q = this.showLastLabel,
                    n = 1,
                    t = 0;
                this.minCalc = this.min;
                this.maxCalc = this.max;
                if (this.strictMinMax && (isNaN(this.minimum) || (this.min = this.minimum), isNaN(this.maximum) || (this.max = this.maximum), this.min == this.max)) return;
                isNaN(this.minZoom) || (this.minReal = this.min = this.minZoom);
                isNaN(this.maxZoom) ||
                    (this.max = this.maxZoom);
                if (this.logarithmic) {
                    g = this.fullMin;
                    var r = this.fullMax;
                    isNaN(this.minimum) || (g = this.minimum);
                    isNaN(this.maximum) || (r = this.maximum);
                    var r = Math.log(r) * Math.LOG10E - Math.log(g) * Math.LOG10E,
                        w = Math.log(this.max) / Math.LN10 - Math.log(g) * Math.LOG10E;
                    this.relativeStart = d.roundTo((Math.log(this.minReal) / Math.LN10 - Math.log(g) * Math.LOG10E) / r, 5);
                    this.relativeEnd = d.roundTo(w / r, 5)
                } else this.relativeStart = d.roundTo(d.fitToBounds((this.min - this.fullMin) / (this.fullMax - this.fullMin), 0, 1), 5), this.relativeEnd =
                    d.roundTo(d.fitToBounds((this.max - this.fullMin) / (this.fullMax - this.fullMin), 0, 1), 5);
                var r = Math.round((this.maxCalc - this.minCalc) / this.step) + 1,
                    z;
                !0 === h ? (z = Math.log(this.max) * Math.LOG10E - Math.log(this.minReal) * Math.LOG10E, this.stepWidth = this.axisWidth / z, z > this.logGridLimit && (r = Math.ceil(Math.log(this.max) * Math.LOG10E) + 1, t = Math.round(Math.log(this.minReal) * Math.LOG10E), r > this.gridCountR && (n = Math.ceil(r / this.gridCountR)))) : this.stepWidth = this.axisWidth / (this.max - this.min);
                var x = 0;
                1 > this.step && -1 < this.step &&
                    (x = d.getDecimals(this.step));
                this.integersOnly && (x = 0);
                x > this.maxDecCount && (x = this.maxDecCount);
                w = this.precision;
                isNaN(w) || (x = w);
                isNaN(this.maxZoom) && (this.max = d.roundTo(this.max, this.maxDecCount), this.min = d.roundTo(this.min, this.maxDecCount));
                g = {};
                g.precision = x;
                g.decimalSeparator = a.nf.decimalSeparator;
                g.thousandsSeparator = a.nf.thousandsSeparator;
                this.numberFormatter = g;
                var u;
                this.exponential = !1;
                for (g = t; g < r; g += n) {
                    var A = d.roundTo(this.step * g + this.min, x); - 1 != String(A).indexOf("e") && (this.exponential = !0)
                }
                this.duration && (this.maxInterval = d.getMaxInterval(this.max, this.duration));
                var x = this.step,
                    y, A = this.minorGridAlpha;
                this.minorGridEnabled && (y = this.getMinorGridStep(x, this.stepWidth * x));
                if (this.autoGridCount || 0 !== this.gridCount)
                    if ("date" == c) this.generateDFObject(), this.timeDifference = this.max - this.min, this.maxTime = this.lastTime = this.max, this.startTime = this.firstTime = this.min, this.parseDatesDraw();
                    else
                        for (r >= this.autoRotateCount && !isNaN(this.autoRotateAngle) && (this.labelRotationR = this.autoRotateAngle),
                            c = this.minCalc, h && (r++, c = this.maxCalc - r * x), this.gridCountReal = r, g = this.startCount = t; g < r; g += n)
                            if (t = x * g + c, t = d.roundTo(t, this.maxDecCount + 1), !this.integersOnly || Math.round(t) == t)
                                if (isNaN(w) || Number(d.toFixed(t, w)) == t) {
                                    if (!0 === h)
                                        if (z > this.logGridLimit) t = Math.pow(10, g);
                                        else if (0 >= t && (t = c + x * g + x / 2, 0 >= t)) continue;
                                    u = this.formatValue(t, !1, g);
                                    Math.round(g / m) != g / m && (u = void 0);
                                    if (0 === g && !p || g == r - 1 && !q) u = " ";
                                    f = this.getCoordinate(t);
                                    var B;
                                    this.rotate && this.autoWrap && (B = this.stepWidth * x - 10);
                                    u = new this.axisItemRenderer(this,
                                        f, u, void 0, B, void 0, void 0, this.boldLabels);
                                    this.pushAxisItem(u);
                                    if (t == this.baseValue && "radar" != a.type) {
                                        var D, C, I = this.width,
                                            H = this.height;
                                        "H" == this.orientation ? 0 <= f && f <= I + 1 && (D = [f, f, f + k], C = [H, 0, l]) : 0 <= f && f <= H + 1 && (D = [0, I, I + k], C = [f, f, f + l]);
                                        D && (f = d.fitToBounds(2 * this.gridAlpha, 0, 1), isNaN(this.zeroGridAlpha) || (f = this.zeroGridAlpha), f = d.line(a.container, D, C, this.gridColor, f, 1, this.dashLength), f.translate(this.x, this.y), this.grid0 = f, a.axesSet.push(f), f.toBack(), d.setCN(a, f, this.bcn + "zero-grid-" + this.id),
                                            d.setCN(a, f, this.bcn + "zero-grid"))
                                    }
                                    if (!isNaN(y) && 0 < A && g < r - 1) {
                                        f = x / y;
                                        h && (y = x * (g + n) + this.minCalc, y = d.roundTo(y, this.maxDecCount + 1), z > this.logGridLimit && (y = Math.pow(10, g + n)), f = 9, y = (y - t) / f);
                                        I = this.gridAlpha;
                                        this.gridAlpha = this.minorGridAlpha;
                                        for (H = 1; H < f; H++) {
                                            var Q = this.getCoordinate(t + y * H),
                                                Q = new this.axisItemRenderer(this, Q, "", !1, 0, 0, !1, !1, 0, !0);
                                            this.pushAxisItem(Q)
                                        }
                                        this.gridAlpha = I
                                    }
                                }
                z = this.guides;
                B = z.length;
                if (0 < B) {
                    D = this.fillAlpha;
                    for (g = this.fillAlpha = 0; g < B; g++) C = z[g], k = NaN, y = C.above, isNaN(C.toValue) ||
                        (k = this.getCoordinate(C.toValue), u = new this.axisItemRenderer(this, k, "", !0, NaN, NaN, C), this.pushAxisItem(u, y)), l = NaN, isNaN(C.value) || (l = this.getCoordinate(C.value), u = new this.axisItemRenderer(this, l, C.label, !0, NaN, (k - l) / 2, C), this.pushAxisItem(u, y)), isNaN(k) && (l -= 3, k = l + 3), u && (m = u.label) && this.addEventListeners(m, C), isNaN(k - l) || 0 > l && 0 > k || (k = new this.guideFillRenderer(this, l, k, C), this.pushAxisItem(k, y), y = k.graphics(), C.graphics = y, this.addEventListeners(y, C));
                    this.fillAlpha = D
                }
                u = this.baseValue;
                this.min >
                    this.baseValue && this.max > this.baseValue && (u = this.min);
                this.min < this.baseValue && this.max < this.baseValue && (u = this.max);
                h && u < this.minReal && (u = this.minReal);
                this.baseCoord = this.getCoordinate(u, !0);
                u = {
                    type: "axisChanged",
                    target: this,
                    chart: a
                };
                u.min = h ? this.minReal : this.min;
                u.max = this.max;
                this.fire(u);
                this.axisCreated = !0
            }
            h = this.axisLine.set;
            u = this.labelsSet;
            b.translate(this.x, this.y);
            u.translate(this.x, this.y);
            this.positionTitle();
            "radar" != a.type && h.toFront();
            !this.visible || e ? (b.hide(), h.hide(), u.hide()) :
                (b.show(), h.show(), u.show());
            this.axisY = this.y;
            this.axisX = this.x
        },
        getDateMinMax: function() {
            this.minimumDate && (this.minimumDate instanceof Date || (this.minimumDate = d.getDate(this.minimumDate, this.chart.dataDateFormat, "fff")), this.min = this.minimumDate.getTime());
            this.maximumDate && (this.maximumDate instanceof Date || (this.maximumDate = d.getDate(this.maximumDate, this.chart.dataDateFormat, "fff")), this.max = this.maximumDate.getTime())
        },
        formatValue: function(a, b, c) {
            var e = this.exponential,
                h = this.logarithmic,
                f =
                this.numberFormatter,
                g = this.chart;
            if (f) return !0 === this.logarithmic && (e = -1 != String(a).indexOf("e") ? !0 : !1), this.useScientificNotation && (e = !0), this.usePrefixes && (e = !1), e ? (c = -1 == String(a).indexOf("e") ? a.toExponential(15) : String(a), e = c.split("e"), c = Number(e[0]), e = Number(e[1]), c = d.roundTo(c, 14), b || isNaN(this.precision) || (c = d.roundTo(c, this.precision)), 10 == c && (c = 1, e += 1), c = c + "e" + e, 0 === a && (c = "0"), 1 == a && (c = "1")) : (h && (e = String(a).split("."), e[1] ? (f.precision = e[1].length, 0 > c && (f.precision = Math.abs(c)), b && 1 < a &&
                    (f.precision = 0), b || isNaN(this.precision) || (f.precision = this.precision)) : f.precision = -1), c = this.usePrefixes ? d.addPrefix(a, g.prefixesOfBigNumbers, g.prefixesOfSmallNumbers, f, !b) : d.formatNumber(a, f, f.precision)), this.duration && (b && (f.precision = 0), c = d.formatDuration(a, this.duration, "", this.durationUnits, this.maxInterval, f)), "date" == this.type && (c = d.formatDate(new Date(a), this.currentDateFormat, g)), this.recalculateToPercents ? c += "%" : (b = this.unit) && (c = "left" == this.unitPosition ? b + c : c + b), this.labelFunction &&
                (c = "date" == this.type ? this.labelFunction(c, new Date(a), this).toString() : this.labelFunction(a, c, this).toString()), c
        },
        getMinorGridStep: function(a, b) {
            var c = [5, 4, 2];
            60 > b && c.shift();
            for (var e = Math.floor(Math.log(Math.abs(a)) * Math.LOG10E), d = 0; d < c.length; d++) {
                var f = a / c[d],
                    g = Math.floor(Math.log(Math.abs(f)) * Math.LOG10E);
                if (!(1 < Math.abs(e - g)))
                    if (1 > a) {
                        if (g = Math.pow(10, -g) * f, g == Math.round(g)) return f
                    } else if (f == Math.round(f)) return f
            }
        },
        stackGraphs: function(a) {
            var b = this.stackType;
            "stacked" == b && (b = "regular");
            "line" == b && (b = "none");
            "100% stacked" == b && (b = "100%");
            this.stackType = b;
            var c = [],
                e = [],
                h = [],
                f = [],
                g, k = this.chart.graphs,
                l, m, p, q, n, t = this.baseValue,
                r = !1;
            if ("line" == a || "step" == a || "smoothedLine" == a) r = !0;
            if (r && ("regular" == b || "100%" == b))
                for (q = 0; q < k.length; q++) p = k[q], p.stackGraph = null, p.hidden || (m = p.type, p.chart == this.chart && p.valueAxis == this && a == m && p.stackable && (l && (p.stackGraph = l), l = p));
            p = this.start - 10;
            l = this.end + 10;
            q = this.data.length - 1;
            p = d.fitToBounds(p, 0, q);
            l = d.fitToBounds(l, 0, q);
            for (n = p; n <= l; n++) {
                var w =
                    0;
                for (q = 0; q < k.length; q++)
                    if (p = k[q], p.hidden) p.newStack && (h[n] = NaN, e[n] = NaN);
                    else if (m = p.type, p.chart == this.chart && p.valueAxis == this && a == m && p.stackable)
                    if (m = this.data[n].axes[this.id].graphs[p.id], g = m.values.value, isNaN(g)) p.newStack && (h[n] = NaN, e[n] = NaN);
                    else {
                        var z = d.getDecimals(g);
                        w < z && (w = z);
                        isNaN(f[n]) ? f[n] = Math.abs(g) : f[n] += Math.abs(g);
                        f[n] = d.roundTo(f[n], w);
                        z = p.fillToGraph;
                        r && z && (z = this.data[n].axes[this.id].graphs[z.id]) && (m.values.open = z.values.value);
                        "regular" == b && (r && (isNaN(c[n]) ? (c[n] = g, m.values.close =
                            g, m.values.open = this.baseValue) : (isNaN(g) ? m.values.close = c[n] : m.values.close = g + c[n], m.values.open = c[n], c[n] = m.values.close)), "column" == a && (p.newStack && (h[n] = NaN, e[n] = NaN), m.values.close = g, 0 > g ? (m.values.close = g, isNaN(e[n]) ? m.values.open = t : (m.values.close += e[n], m.values.open = e[n]), e[n] = m.values.close) : (m.values.close = g, isNaN(h[n]) ? m.values.open = t : (m.values.close += h[n], m.values.open = h[n]), h[n] = m.values.close)))
                    }
            }
            for (n = this.start; n <= this.end; n++)
                for (q = 0; q < k.length; q++)(p = k[q], p.hidden) ? p.newStack && (h[n] =
                    NaN, e[n] = NaN) : (m = p.type, p.chart == this.chart && p.valueAxis == this && a == m && p.stackable && (m = this.data[n].axes[this.id].graphs[p.id], g = m.values.value, isNaN(g) || (c = g / f[n] * 100, m.values.percents = c, m.values.total = f[n], p.newStack && (h[n] = NaN, e[n] = NaN), "100%" == b && (isNaN(e[n]) && (e[n] = 0), isNaN(h[n]) && (h[n] = 0), 0 > c ? (m.values.close = d.fitToBounds(c + e[n], -100, 100), m.values.open = e[n], e[n] = m.values.close) : (m.values.close = d.fitToBounds(c + h[n], -100, 100), m.values.open = h[n], h[n] = m.values.close)))))
        },
        recalculate: function() {
            var a =
                this.chart,
                b = a.graphs,
                c;
            for (c = 0; c < b.length; c++) {
                var e = b[c];
                if (e.valueAxis == this) {
                    var h = "value";
                    if ("candlestick" == e.type || "ohlc" == e.type) h = "open";
                    var f, g, k = this.end + 2,
                        k = d.fitToBounds(this.end + 1, 0, this.data.length - 1),
                        l = this.start;
                    0 < l && l--;
                    var m;
                    g = this.start;
                    e.compareFromStart && (g = 0);
                    if (!isNaN(a.startTime) && (m = a.categoryAxis)) {
                        var p = m.minDuration(),
                            p = new Date(a.startTime + p / 2),
                            q = d.resetDateToMin(new Date(a.startTime), m.minPeriod).getTime();
                        d.resetDateToMin(new Date(p), m.minPeriod).getTime() > q && g++
                    }
                    if (m =
                        a.recalculateFromDate) m = d.getDate(m, a.dataDateFormat, "fff"), g = a.getClosestIndex(a.chartData, "time", m.getTime(), !0, 0, a.chartData.length), k = a.chartData.length - 1;
                    for (m = g; m <= k && (g = this.data[m].axes[this.id].graphs[e.id], f = g.values[h], e.recalculateValue && (f = g.dataContext[e.valueField + e.recalculateValue]), isNaN(f)); m++);
                    this.recBaseValue = f;
                    for (h = l; h <= k; h++) {
                        g = this.data[h].axes[this.id].graphs[e.id];
                        g.percents = {};
                        var l = g.values,
                            n;
                        for (n in l) g.percents[n] = "percents" != n ? l[n] / f * 100 - 100 : l[n]
                    }
                }
            }
        },
        getMinMax: function() {
            var a = !1,
                b = this.chart,
                c = b.graphs,
                e;
            for (e = 0; e < c.length; e++) {
                var h = c[e].type;
                ("line" == h || "step" == h || "smoothedLine" == h) && this.expandMinMax && (a = !0)
            }
            a && (0 < this.start && this.start--, this.end < this.data.length - 1 && this.end++);
            "serial" == b.type && (!0 !== b.categoryAxis.parseDates || a || this.end < this.data.length - 1 && this.end++);
            this.includeAllValues && (this.start = 0, this.end = this.data.length - 1);
            a = this.minMaxMultiplier;
            b = this.getExtremes(this.start, this.end);
            this.min = b.min;
            this.max = b.max;
            this.minRR = this.min;
            this.maxRR = this.max;
            a = (this.max - this.min) * (a - 1);
            this.min -= a;
            this.max += a;
            a = this.guides.length;
            if (this.includeGuidesInMinMax && 0 < a)
                for (b = 0; b < a; b++) c = this.guides[b], c.toValue < this.min && (this.min = c.toValue), c.value < this.min && (this.min = c.value), c.toValue > this.max && (this.max = c.toValue), c.value > this.max && (this.max = c.value);
            isNaN(this.minimum) || (this.min = this.minimum);
            isNaN(this.maximum) || (this.max = this.maximum);
            "date" == this.type && this.getDateMinMax();
            this.min > this.max && (a = this.max, this.max = this.min, this.min = a);
            isNaN(this.minZoom) ||
                (this.min = this.minZoom);
            isNaN(this.maxZoom) || (this.max = this.maxZoom);
            this.minCalc = this.min;
            this.maxCalc = this.max;
            this.minReal = this.min;
            this.maxReal = this.max;
            0 === this.min && 0 === this.max && (this.max = 9);
            this.min > this.max && (this.min = this.max - 1);
            a = this.min;
            b = this.max;
            c = this.max - this.min;
            e = 0 === c ? Math.pow(10, Math.floor(Math.log(Math.abs(this.max)) * Math.LOG10E)) / 10 : Math.pow(10, Math.floor(Math.log(Math.abs(c)) * Math.LOG10E)) / 10;
            isNaN(this.maximum) && (this.max = Math.ceil(this.max / e) * e + e);
            isNaN(this.minimum) && (this.min =
                Math.floor(this.min / e) * e - e);
            0 > this.min && 0 <= a && (this.min = 0);
            0 < this.max && 0 >= b && (this.max = 0);
            "100%" == this.stackType && (this.min = 0 > this.min ? -100 : 0, this.max = 0 > this.max ? 0 : 100);
            c = this.max - this.min;
            e = Math.pow(10, Math.floor(Math.log(Math.abs(c)) * Math.LOG10E)) / 10;
            this.step = Math.ceil(c / this.gridCountR / e) * e;
            c = Math.pow(10, Math.floor(Math.log(Math.abs(this.step)) * Math.LOG10E));
            c = d.fixStepE(c);
            e = Math.ceil(this.step / c);
            5 < e && (e = 10);
            5 >= e && 2 < e && (e = 5);
            this.step = Math.ceil(this.step / (c * e)) * c * e;
            isNaN(this.setStep) || (this.step =
                this.setStep);
            1 > c ? (this.maxDecCount = Math.abs(Math.log(Math.abs(c)) * Math.LOG10E), this.maxDecCount = Math.round(this.maxDecCount), this.step = d.roundTo(this.step, this.maxDecCount + 1)) : this.maxDecCount = 0;
            this.min = this.step * Math.floor(this.min / this.step);
            this.max = this.step * Math.ceil(this.max / this.step);
            0 > this.min && 0 <= a && (this.min = 0);
            0 < this.max && 0 >= b && (this.max = 0);
            1 < this.minReal && 1 < this.max - this.minReal && (this.minReal = Math.floor(this.minReal));
            c = Math.pow(10, Math.floor(Math.log(Math.abs(this.minReal)) * Math.LOG10E));
            0 === this.min && (this.minReal = c);
            0 === this.min && 1 < this.minReal && (this.minReal = 1);
            0 < this.min && 0 < this.minReal - this.step && (this.minReal = this.min + this.step < this.minReal ? this.min + this.step : this.min);
            this.logarithmic && (2 < Math.log(b) * Math.LOG10E - Math.log(a) * Math.LOG10E ? (this.minReal = this.min = Math.pow(10, Math.floor(Math.log(Math.abs(a)) * Math.LOG10E)), this.maxReal = this.max = Math.pow(10, Math.ceil(Math.log(Math.abs(b)) * Math.LOG10E))) : (a = Math.pow(10, Math.floor(Math.log(Math.abs(a)) * Math.LOG10E)) / 10, Math.pow(10, Math.floor(Math.log(Math.abs(this.min)) *
                Math.LOG10E)) / 10 < a && (this.minReal = this.min = 10 * a)))
        },
        getExtremes: function(a, b) {
            var c, e, d;
            for (d = a; d <= b; d++) {
                var f = this.data[d].axes[this.id].graphs,
                    g;
                for (g in f)
                    if (f.hasOwnProperty(g)) {
                        var k = this.chart.graphsById[g];
                        if (k.includeInMinMax && (!k.hidden || this.includeHidden)) {
                            isNaN(c) && (c = Infinity);
                            isNaN(e) && (e = -Infinity);
                            this.foundGraphs = !0;
                            k = f[g].values;
                            this.recalculateToPercents && (k = f[g].percents);
                            var l;
                            if (this.minMaxField) l = k[this.minMaxField], l < c && (c = l), l > e && (e = l);
                            else
                                for (var m in k) k.hasOwnProperty(m) &&
                                    "percents" != m && "total" != m && "error" != m && (l = k[m], l < c && (c = l), l > e && (e = l))
                        }
                    }
            }
            return {
                min: c,
                max: e
            }
        },
        zoomOut: function(a) {
            this.maxZoom = this.minZoom = NaN;
            this.zoomToRelativeValues(0, 1, a)
        },
        zoomToRelativeValues: function(a, b, c) {
            if (this.reversed) {
                var e = a;
                a = 1 - b;
                b = 1 - e
            }
            var d = this.fullMax,
                e = this.fullMin,
                f = e + (d - e) * a,
                g = e + (d - e) * b;
            0 <= this.minimum && 0 > f && (f = 0);
            this.logarithmic && (isNaN(this.minimum) || (e = this.minimum), isNaN(this.maximum) || (d = this.maximum), d = Math.log(d) * Math.LOG10E - Math.log(e) * Math.LOG10E, f = Math.pow(10, d * a +
                Math.log(e) * Math.LOG10E), g = Math.pow(10, d * b + Math.log(e) * Math.LOG10E));
            return this.zoomToValues(f, g, c)
        },
        zoomToValues: function(a, b, c) {
            if (b < a) {
                var e = b;
                b = a;
                a = e
            }
            var h = this.fullMax,
                e = this.fullMin;
            this.relativeStart = d.roundTo((a - e) / (h - e), 9);
            this.relativeEnd = d.roundTo((b - e) / (h - e), 9);
            if (this.logarithmic) {
                isNaN(this.minimum) || (e = this.minimum);
                isNaN(this.maximum) || (h = this.maximum);
                var h = Math.log(h) * Math.LOG10E - Math.log(e) * Math.LOG10E,
                    f = Math.log(b) / Math.LN10 - Math.log(e) * Math.LOG10E;
                this.relativeStart = d.roundTo((Math.log(a) /
                    Math.LN10 - Math.log(e) * Math.LOG10E) / h, 9);
                this.relativeEnd = d.roundTo(f / h, 9)
            }
            if (this.minZoom != a || this.maxZoom != b) return this.minZoom = a, this.maxZoom = b, e = {
                    type: "axisZoomed"
                }, e.chart = this.chart, e.valueAxis = this, e.startValue = a, e.endValue = b, e.relativeStart = this.relativeStart, e.relativeEnd = this.relativeEnd, this.prevStartValue == a && this.prevEndValue == b || this.fire(e), this.prevStartValue = a, this.prevEndValue = b, c || (a = {}, d.copyProperties(e, a), a.type = "axisIntZoomed", this.fire(a)), 0 === this.relativeStart && 1 == this.relativeEnd &&
                (this.maxZoom = this.minZoom = NaN), !0
        },
        coordinateToValue: function(a) {
            if (isNaN(a)) return NaN;
            var b = this.axisWidth,
                c = this.stepWidth,
                e = this.reversed,
                d = this.rotate,
                f = this.min,
                g = this.minReal;
            return !0 === this.logarithmic ? Math.pow(10, (d ? !0 === e ? (b - a) / c : a / c : !0 === e ? a / c : (b - a) / c) + Math.log(g) * Math.LOG10E) : !0 === e ? d ? f - (a - b) / c : a / c + f : d ? a / c + f : f - (a - b) / c
        },
        getCoordinate: function(a, b) {
            if (isNaN(a)) return NaN;
            var c = this.rotate,
                e = this.reversed,
                d = this.axisWidth,
                f = this.stepWidth,
                g = this.min,
                k = this.minReal;
            !0 === this.logarithmic ?
                (0 === a && (a = this.treatZeroAs), g = Math.log(a) * Math.LOG10E - Math.log(k) * Math.LOG10E, c = c ? !0 === e ? d - f * g : f * g : !0 === e ? f * g : d - f * g) : c = !0 === e ? c ? d - f * (a - g) : f * (a - g) : c ? f * (a - g) : d - f * (a - g);
            1E7 < Math.abs(c) && (c = c / Math.abs(c) * 1E7);
            b || (c = Math.round(c));
            return c
        },
        synchronizeWithAxis: function(a) {
            this.synchronizeWith = a;
            this.listenTo(this.synchronizeWith, "axisChanged", this.handleSynchronization)
        },
        handleSynchronization: function() {
            if (this.synchronizeWith) {
                d.isString(this.synchronizeWith) && (this.synchronizeWith = this.chart.getValueAxisById(this.synchronizeWith));
                var a = this.synchronizeWith,
                    b = a.min,
                    c = a.max,
                    a = a.step,
                    e = this.synchronizationMultiplier;
                e && (this.min = b * e, this.max = c * e, this.step = a * e, b = Math.abs(Math.log(Math.abs(Math.pow(10, Math.floor(Math.log(Math.abs(this.step)) * Math.LOG10E)))) * Math.LOG10E), this.maxDecCount = b = Math.round(b), this.draw())
            }
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.RecAxis = d.Class({
        construct: function(a) {
            var b = a.chart,
                c = a.axisThickness,
                e = a.axisColor,
                h = a.axisAlpha,
                f = a.offset,
                g = a.dx,
                k = a.dy,
                l = a.x,
                m = a.y,
                p = a.height,
                q = a.width,
                n = b.container;
            "H" == a.orientation ? (e = d.line(n, [0, q], [0, 0], e, h, c), this.axisWidth = a.width, "bottom" == a.position ? (k = c / 2 + f + p + m - 1, c = l) : (k = -c / 2 - f + m + k, c = g + l)) : (this.axisWidth = a.height, "right" == a.position ? (e = d.line(n, [0, 0, -g], [0, p, p - k], e, h, c), k = m + k, c = c / 2 + f + g + q + l - 1) : (e = d.line(n, [0, 0], [0, p], e, h, c), k = m, c = -c / 2 - f + l));
            e.translate(c,
                k);
            c = b.container.set();
            c.push(e);
            b.axesSet.push(c);
            d.setCN(b, e, a.bcn + "line");
            this.axisSet = c;
            this.set = e
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.RecItem = d.Class({
        construct: function(a, b, c, e, h, f, g, k, l, m, p, q) {
            b = Math.round(b);
            var n = a.chart;
            this.value = c;
            void 0 == c && (c = "");
            l || (l = 0);
            void 0 == e && (e = !0);
            var t = n.fontFamily,
                r = a.fontSize;
            void 0 == r && (r = n.fontSize);
            var w = a.color;
            void 0 == w && (w = n.color);
            void 0 !== p && (w = p);
            var z = a.chart.container,
                x = z.set();
            this.set = x;
            var u = a.axisThickness,
                A = a.axisColor,
                y = a.axisAlpha,
                B = a.tickLength,
                D = a.gridAlpha,
                C = a.gridThickness,
                I = a.gridColor,
                H = a.dashLength,
                Q = a.fillColor,
                M = a.fillAlpha,
                P = a.labelsEnabled;
            p = a.labelRotationR;
            var ia = a.counter,
                J = a.inside,
                aa = a.labelOffset,
                ma = a.dx,
                na = a.dy,
                Pa = a.orientation,
                Z = a.position,
                da = a.previousCoord,
                X = a.height,
                xa = a.width,
                ea = a.offset,
                fa, Ba;
            g ? (void 0 !== g.id && (q = n.classNamePrefix + "-guide-" + g.id), P = !0, isNaN(g.tickLength) || (B = g.tickLength), void 0 != g.lineColor && (I = g.lineColor), void 0 != g.color && (w = g.color), isNaN(g.lineAlpha) || (D = g.lineAlpha), isNaN(g.dashLength) || (H = g.dashLength), isNaN(g.lineThickness) || (C = g.lineThickness), !0 === g.inside && (J = !0, 0 < ea && (ea = 0)), isNaN(g.labelRotation) ||
                (p = g.labelRotation), isNaN(g.fontSize) || (r = g.fontSize), g.position && (Z = g.position), void 0 !== g.boldLabel && (k = g.boldLabel), isNaN(g.labelOffset) || (aa = g.labelOffset)) : "" === c && (B = 0);
            m && !isNaN(a.minorTickLength) && (B = a.minorTickLength);
            var ga = "start";
            0 < h && (ga = "middle");
            a.centerLabels && (ga = "middle");
            var V = p * Math.PI / 180,
                Y, Da, G = 0,
                v = 0,
                oa = 0,
                ha = Y = 0,
                Qa = 0;
            "V" == Pa && (p = 0);
            var ca;
            P && "" !== c && (ca = a.autoWrap && 0 === p ? d.wrappedText(z, c, w, t, r, ga, k, Math.abs(h), 0) : d.text(z, c, w, t, r, ga, k), ga = ca.getBBox(), ha = ga.width, Qa = ga.height);
            if ("H" == Pa) {
                if (0 <= b && b <= xa + 1 && (0 < B && 0 < y && b + l <= xa + 1 && (fa = d.line(z, [b + l, b + l], [0, B], A, y, C), x.push(fa)), 0 < D && (Ba = d.line(z, [b, b + ma, b + ma], [X, X + na, na], I, D, C, H), x.push(Ba))), v = 0, G = b, g && 90 == p && J && (G -= r), !1 === e ? (ga = "start", v = "bottom" == Z ? J ? v + B : v - B : J ? v - B : v + B, G += 3, 0 < h && (G += h / 2 - 3, ga = "middle"), 0 < p && (ga = "middle")) : ga = "middle", 1 == ia && 0 < M && !g && !m && da < xa && (e = d.fitToBounds(b, 0, xa), da = d.fitToBounds(da, 0, xa), Y = e - da, 0 < Y && (Da = d.rect(z, Y, a.height, Q, M), Da.translate(e - Y + ma, na), x.push(Da))), "bottom" == Z ? (v += X + r / 2 + ea, J ? (0 < p ? (v =
                        X - ha / 2 * Math.sin(V) - B - 3, a.centerRotatedLabels || (G += ha / 2 * Math.cos(V) - 4 + 2)) : 0 > p ? (v = X + ha * Math.sin(V) - B - 3 + 2, G += -ha * Math.cos(V) - Qa * Math.sin(V) - 4) : v -= B + r + 3 + 3, v -= aa) : (0 < p ? (v = X + ha / 2 * Math.sin(V) + B + 3, a.centerRotatedLabels || (G -= ha / 2 * Math.cos(V))) : 0 > p ? (v = X + B + 3 - ha / 2 * Math.sin(V) + 2, G += ha / 2 * Math.cos(V)) : v += B + u + 3 + 3, v += aa)) : (v += na + r / 2 - ea, G += ma, J ? (0 < p ? (v = ha / 2 * Math.sin(V) + B + 3, a.centerRotatedLabels || (G -= ha / 2 * Math.cos(V))) : v += B + 3, v += aa) : (0 < p ? (v = -(ha / 2) * Math.sin(V) - B - 6, a.centerRotatedLabels || (G += ha / 2 * Math.cos(V))) : v -= B +
                        r + 3 + u + 3, v -= aa)), "bottom" == Z ? Y = (J ? X - B - 1 : X + u - 1) + ea : (oa = ma, Y = (J ? na : na - B - u + 1) - ea), f && (G += f), r = G, 0 < p && (r += ha / 2 * Math.cos(V)), ca && (f = 0, J && (f = ha / 2 * Math.cos(V)), r + f > xa + 2 || 0 > r)) ca.remove(), ca = null
            } else {
                0 <= b && b <= X + 1 && (0 < B && 0 < y && b + l <= X + 1 && (fa = d.line(z, [0, B + 1], [b + l, b + l], A, y, C), x.push(fa)), 0 < D && (Ba = d.line(z, [0, ma, xa + ma], [b, b + na, b + na], I, D, C, H), x.push(Ba)));
                ga = "end";
                if (!0 === J && "left" == Z || !1 === J && "right" == Z) ga = "start";
                v = b - Qa / 2 + 2;
                1 == ia && 0 < M && !g && !m && (e = d.fitToBounds(b, 0, X), da = d.fitToBounds(da, 0, X), V = e - da, Da = d.polygon(z, [0, a.width, a.width, 0], [0, 0, V, V], Q, M), Da.translate(ma, e - V + na), x.push(Da));
                v += r / 2;
                "right" == Z ? (G += ma + xa + ea, v += na, J ? (f || (v -= r / 2 + 3), G = G - (B + 4) - aa) : (G += B + 4 + u, v -= 2, G += aa)) : J ? (G += B + 4 - ea, f || (v -= r / 2 + 3), g && (G += ma, v += na), G += aa) : (G += -B - u - 4 - 2 - ea, v -= 2, G -= aa);
                fa && ("right" == Z ? (oa += ma + ea + xa - 1, Y += na, oa = J ? oa - u : oa + u) : (oa -= ea, J || (oa -= B + u)));
                f && (v += f);
                J = -3;
                "right" == Z && (J += na);
                ca && (v > X + 1 || v < J - r / 10) && (ca.remove(), ca = null)
            }
            fa && (fa.translate(oa, Y), d.setCN(n, fa, a.bcn + "tick"), d.setCN(n, fa, q, !0), g && d.setCN(n, fa, "guide"));
            !1 ===
                a.visible && (fa && fa.remove(), ca && (ca.remove(), ca = null));
            ca && (ca.attr({
                "text-anchor": ga
            }), ca.translate(G, v, NaN, !0), 0 !== p && ca.rotate(-p, a.chart.backgroundColor), a.allLabels.push(ca), this.label = ca, d.setCN(n, ca, a.bcn + "label"), d.setCN(n, ca, q, !0), g && d.setCN(n, ca, "guide"));
            Ba && (d.setCN(n, Ba, a.bcn + "grid"), d.setCN(n, Ba, q, !0), g && d.setCN(n, Ba, "guide"));
            Da && (d.setCN(n, Da, a.bcn + "fill"), d.setCN(n, Da, q, !0));
            m ? Ba && d.setCN(n, Ba, a.bcn + "grid-minor") : (a.counter = 0 === ia ? 1 : 0, a.previousCoord = b);
            0 === this.set.node.childNodes.length &&
                this.set.remove()
        },
        graphics: function() {
            return this.set
        },
        getLabel: function() {
            return this.label
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.RecFill = d.Class({
        construct: function(a, b, c, e) {
            var h = a.dx,
                f = a.dy,
                g = a.orientation,
                k = 0;
            if (c < b) {
                var l = b;
                b = c;
                c = l
            }
            var m = e.fillAlpha;
            isNaN(m) && (m = 0);
            var l = a.chart.container,
                p = e.fillColor;
            "V" == g ? (b = d.fitToBounds(b, 0, a.height), c = d.fitToBounds(c, 0, a.height)) : (b = d.fitToBounds(b, 0, a.width), c = d.fitToBounds(c, 0, a.width));
            c -= b;
            isNaN(c) && (c = 4, k = 2, m = 0);
            0 > c && "object" == typeof p && (p = p.join(",").split(",").reverse());
            "V" == g ? (g = d.rect(l, a.width, c, p, m), g.translate(h, b - k + f)) : (g = d.rect(l,
                c, a.height, p, m), g.translate(b - k + h, f));
            d.setCN(a.chart, g, "guide-fill");
            e.id && d.setCN(a.chart, g, "guide-fill-" + e.id);
            this.set = l.set([g])
        },
        graphics: function() {
            return this.set
        },
        getLabel: function() {}
    })
})();
(function() {
    var d = window.AmCharts;
    d.AmChart = d.Class({
        construct: function(a) {
            this.svgIcons = this.tapToActivate = !0;
            this.theme = a;
            this.classNamePrefix = "amcharts";
            this.addClassNames = !1;
            this.version = "3.21.2";
            d.addChart(this);
            this.createEvents("buildStarted", "dataUpdated", "init", "rendered", "drawn", "failed", "resized", "animationFinished");
            this.height = this.width = "100%";
            this.dataChanged = !0;
            this.chartCreated = !1;
            this.previousWidth = this.previousHeight = 0;
            this.backgroundColor = "#FFFFFF";
            this.borderAlpha = this.backgroundAlpha =
                0;
            this.color = this.borderColor = "#000000";
            this.fontFamily = "Verdana";
            this.fontSize = 11;
            this.usePrefixes = !1;
            this.autoResize = !0;
            this.autoDisplay = !1;
            this.addCodeCredits = this.accessible = !0;
            this.touchStartTime = this.touchClickDuration = 0;
            this.precision = -1;
            this.percentPrecision = 2;
            this.decimalSeparator = ".";
            this.thousandsSeparator = ",";
            this.labels = [];
            this.allLabels = [];
            this.titles = [];
            this.marginRight = this.marginLeft = this.autoMarginOffset = 0;
            this.timeOuts = [];
            this.creditsPosition = "top-left";
            var b = document.createElement("div"),
                c = b.style;
            c.overflow = "hidden";
            c.position = "relative";
            c.textAlign = "left";
            this.chartDiv = b;
            b = document.createElement("div");
            c = b.style;
            c.overflow = "hidden";
            c.position = "relative";
            c.textAlign = "left";
            this.legendDiv = b;
            this.titleHeight = 0;
            this.hideBalloonTime = 150;
            this.handDrawScatter = 2;
            this.cssScale = this.handDrawThickness = 1;
            this.cssAngle = 0;
            this.prefixesOfBigNumbers = [{
                    number: 1E3,
                    prefix: "k"
                }, {
                    number: 1E6,
                    prefix: "M"
                }, {
                    number: 1E9,
                    prefix: "G"
                }, {
                    number: 1E12,
                    prefix: "T"
                }, {
                    number: 1E15,
                    prefix: "P"
                }, {
                    number: 1E18,
                    prefix: "E"
                },
                {
                    number: 1E21,
                    prefix: "Z"
                }, {
                    number: 1E24,
                    prefix: "Y"
                }
            ];
            this.prefixesOfSmallNumbers = [{
                number: 1E-24,
                prefix: "y"
            }, {
                number: 1E-21,
                prefix: "z"
            }, {
                number: 1E-18,
                prefix: "a"
            }, {
                number: 1E-15,
                prefix: "f"
            }, {
                number: 1E-12,
                prefix: "p"
            }, {
                number: 1E-9,
                prefix: "n"
            }, {
                number: 1E-6,
                prefix: "\u03bc"
            }, {
                number: .001,
                prefix: "m"
            }];
            this.panEventsEnabled = !0;
            this.product = "amcharts";
            this.animations = [];
            this.balloon = new d.AmBalloon(this.theme);
            this.balloon.chart = this;
            this.processTimeout = 0;
            this.processCount = 1E3;
            this.animatable = [];
            this.langObj = {};
            d.applyTheme(this, a, "AmChart")
        },
        drawChart: function() {
            0 < this.realWidth && 0 < this.realHeight && (this.drawBackground(), this.redrawLabels(), this.drawTitles(), this.brr(), this.renderFix(), this.chartDiv && (this.boundingRect = this.chartDiv.getBoundingClientRect()))
        },
        makeAccessible: function(a, b, c) {
            this.accessible && a && (c && a.setAttr("role", c), a.setAttr("aria-label", b))
        },
        drawBackground: function() {
            d.remove(this.background);
            var a = this.container,
                b = this.backgroundColor,
                c = this.backgroundAlpha,
                e = this.set;
            d.isModern || 0 !==
                c || (c = .001);
            var h = this.updateWidth();
            this.realWidth = h;
            var f = this.updateHeight();
            this.realHeight = f;
            b = d.polygon(a, [0, h - 1, h - 1, 0], [0, 0, f - 1, f - 1], b, c, 1, this.borderColor, this.borderAlpha);
            d.setCN(this, b, "bg");
            this.background = b;
            e.push(b);
            if (b = this.backgroundImage) a = a.image(b, 0, 0, h, f), d.setCN(this, b, "bg-image"), this.bgImg = a, e.push(a)
        },
        drawTitles: function(a) {
            var b = this.titles;
            this.titleHeight = 0;
            if (d.ifArray(b)) {
                var c = 20,
                    e;
                for (e = 0; e < b.length; e++) {
                    var h = b[e],
                        h = d.processObject(h, d.Title, this.theme);
                    if (!1 !== h.enabled) {
                        var f =
                            h.color;
                        void 0 === f && (f = this.color);
                        var g = h.size;
                        isNaN(g) && (g = this.fontSize + 2);
                        isNaN(h.alpha);
                        var k = this.marginLeft,
                            l = !0;
                        void 0 !== h.bold && (l = h.bold);
                        f = d.wrappedText(this.container, h.text, f, this.fontFamily, g, "middle", l, this.realWidth - 35 - this.marginRight - k);
                        f.translate(k + (this.realWidth - this.marginRight - k) / 2, c);
                        f.node.style.pointerEvents = "none";
                        h.sprite = f;
                        void 0 !== h.tabIndex && f.setAttr("tabindex", h.tabIndex);
                        d.setCN(this, f, "title");
                        h.id && d.setCN(this, f, "title-" + h.id);
                        f.attr({
                            opacity: h.alpha
                        });
                        c += f.getBBox().height +
                            5;
                        a ? f.remove() : this.freeLabelsSet.push(f)
                    }
                }
                this.titleHeight = c - 10
            }
        },
        write: function(a) {
            var b = this;
            if (b.listeners)
                for (var c = 0; c < b.listeners.length; c++) {
                    var e = b.listeners[c];
                    b.addListener(e.event, e.method)
                }
            b.fire({
                type: "buildStarted",
                chart: b
            });
            b.afterWriteTO && clearTimeout(b.afterWriteTO);
            0 < b.processTimeout ? b.afterWriteTO = setTimeout(function() {
                b.afterWrite.call(b, a)
            }, b.processTimeout) : b.afterWrite(a)
        },
        afterWrite: function(a) {
            var b;
            if (b = "object" != typeof a ? document.getElementById(a) : a) {
                for (; b.firstChild;) b.removeChild(b.firstChild);
                this.div = b;
                b.style.overflow = "hidden";
                b.style.textAlign = "left";
                a = this.chartDiv;
                var c = this.legendDiv,
                    e = this.legend,
                    h = c.style,
                    f = a.style;
                this.measure();
                this.previousHeight = this.divRealHeight;
                this.previousWidth = this.divRealWidth;
                var g, k = document.createElement("div");
                g = k.style;
                g.position = "relative";
                this.containerDiv = k;
                k.className = this.classNamePrefix + "-main-div";
                a.className = this.classNamePrefix + "-chart-div";
                b.appendChild(k);
                (b = this.exportConfig) && d.AmExport && !this.AmExport && (this.AmExport = new d.AmExport(this,
                    b));
                this.amExport && d.AmExport && (this.AmExport = d.extend(this.amExport, new d.AmExport(this), !0));
                this.AmExport && this.AmExport.init && this.AmExport.init();
                if (e) {
                    e = this.addLegend(e, e.divId);
                    if (e.enabled) switch (h.left = null, h.top = null, h.right = null, f.left = null, f.right = null, f.top = null, h.position = "relative", f.position = "relative", g.width = "100%", g.height = "100%", e.position) {
                        case "bottom":
                            k.appendChild(a);
                            k.appendChild(c);
                            break;
                        case "top":
                            k.appendChild(c);
                            k.appendChild(a);
                            break;
                        case "absolute":
                            h.position = "absolute";
                            f.position = "absolute";
                            void 0 !== e.left && (h.left = e.left + "px");
                            void 0 !== e.right && (h.right = e.right + "px");
                            void 0 !== e.top && (h.top = e.top + "px");
                            void 0 !== e.bottom && (h.bottom = e.bottom + "px");
                            e.marginLeft = 0;
                            e.marginRight = 0;
                            k.appendChild(a);
                            k.appendChild(c);
                            break;
                        case "right":
                            h.position = "relative";
                            f.position = "absolute";
                            k.appendChild(a);
                            k.appendChild(c);
                            break;
                        case "left":
                            h.position = "absolute";
                            f.position = "relative";
                            k.appendChild(a);
                            k.appendChild(c);
                            break;
                        case "outside":
                            k.appendChild(a)
                    } else k.appendChild(a);
                    this.prevLegendPosition =
                        e.position
                } else k.appendChild(a);
                this.listenersAdded || (this.addListeners(), this.listenersAdded = !0);
                (this.mouseWheelScrollEnabled || this.mouseWheelZoomEnabled) && d.addWheelListeners();
                this.initChart()
            }
        },
        createLabelsSet: function() {
            d.remove(this.labelsSet);
            this.labelsSet = this.container.set();
            this.freeLabelsSet.push(this.labelsSet)
        },
        initChart: function() {
            this.balloon = d.processObject(this.balloon, d.AmBalloon, this.theme);
            window.AmCharts_path && (this.path = window.AmCharts_path);
            void 0 === this.path && (this.path =
                d.getPath());
            void 0 === this.path && (this.path = "amcharts/");
            this.path = d.normalizeUrl(this.path);
            void 0 === this.pathToImages && (this.pathToImages = this.path + "images/");
            this.initHC || (d.callInitHandler(this), this.initHC = !0);
            d.applyLang(this.language, this);
            var a = this.numberFormatter;
            a && (isNaN(a.precision) || (this.precision = a.precision), void 0 !== a.thousandsSeparator && (this.thousandsSeparator = a.thousandsSeparator), void 0 !== a.decimalSeparator && (this.decimalSeparator = a.decimalSeparator));
            (a = this.percentFormatter) &&
            !isNaN(a.precision) && (this.percentPrecision = a.precision);
            this.nf = {
                precision: this.precision,
                thousandsSeparator: this.thousandsSeparator,
                decimalSeparator: this.decimalSeparator
            };
            this.pf = {
                precision: this.percentPrecision,
                thousandsSeparator: this.thousandsSeparator,
                decimalSeparator: this.decimalSeparator
            };
            this.destroy();
            (a = this.container) ? (a.container.innerHTML = "", a.width = this.realWidth, a.height = this.realHeight, a.addDefs(this), this.chartDiv.appendChild(a.container)) : a = new d.AmDraw(this.chartDiv, this.realWidth,
                this.realHeight, this);
            this.container = a;
            this.extension = ".png";
            this.svgIcons && d.SVG && (this.extension = ".svg");
            this.checkDisplay();
            this.checkTransform(this.div);
            a.chart = this;
            d.VML || d.SVG ? (a.handDrawn = this.handDrawn, a.handDrawScatter = this.handDrawScatter, a.handDrawThickness = this.handDrawThickness, d.remove(this.set), this.set = a.set(), d.remove(this.gridSet), this.gridSet = a.set(), d.remove(this.cursorLineSet), this.cursorLineSet = a.set(), d.remove(this.graphsBehindSet), this.graphsBehindSet = a.set(), d.remove(this.bulletBehindSet),
                this.bulletBehindSet = a.set(), d.remove(this.columnSet), this.columnSet = a.set(), d.remove(this.graphsSet), this.graphsSet = a.set(), d.remove(this.trendLinesSet), this.trendLinesSet = a.set(), d.remove(this.axesSet), this.axesSet = a.set(), d.remove(this.cursorSet), this.cursorSet = a.set(), d.remove(this.scrollbarsSet), this.scrollbarsSet = a.set(), d.remove(this.bulletSet), this.bulletSet = a.set(), d.remove(this.freeLabelsSet), this.freeLabelsSet = a.set(), d.remove(this.axesLabelsSet), this.axesLabelsSet = a.set(), d.remove(this.balloonsSet),
                this.balloonsSet = a.set(), d.remove(this.plotBalloonsSet), this.plotBalloonsSet = a.set(), d.remove(this.zoomButtonSet), this.zoomButtonSet = a.set(), d.remove(this.zbSet), this.zbSet = null, d.remove(this.linkSet), this.linkSet = a.set()) : this.fire({
                type: "failed",
                chart: this
            })
        },
        premeasure: function() {
            var a = this.div;
            if (a) {
                try {
                    this.boundingRect = this.chartDiv.getBoundingClientRect()
                } catch (e) {}
                var b = a.offsetWidth,
                    c = a.offsetHeight;
                a.clientHeight && (b = a.clientWidth, c = a.clientHeight);
                if (b != this.mw || c != this.mh) this.mw = b, this.mh =
                    c, this.measure()
            }
        },
        measure: function() {
            var a = this.div;
            if (a) {
                var b = this.chartDiv,
                    c = a.offsetWidth,
                    e = a.offsetHeight,
                    h = this.container;
                a.clientHeight && (c = a.clientWidth, e = a.clientHeight);
                var e = Math.round(e),
                    c = Math.round(c),
                    a = Math.round(d.toCoordinate(this.width, c)),
                    f = Math.round(d.toCoordinate(this.height, e));
                (c != this.previousWidth || e != this.previousHeight) && 0 < a && 0 < f && (b.style.width = a + "px", b.style.height = f + "px", b.style.padding = 0, h && h.setSize(a, f), this.balloon = d.processObject(this.balloon, d.AmBalloon, this.theme));
                this.balloon && this.balloon.setBounds && this.balloon.setBounds(2, 2, a - 2, f);
                this.updateWidth();
                this.balloon.chart = this;
                this.realWidth = a;
                this.realHeight = f;
                this.divRealWidth = c;
                this.divRealHeight = e
            }
        },
        checkDisplay: function() {
            if (this.autoDisplay && this.container) {
                var a = d.rect(this.container, 10, 10),
                    b = a.getBBox();
                0 === b.width && 0 === b.height && (this.divRealHeight = this.divRealWidth = this.realHeight = this.realWidth = 0, this.previousWidth = this.previousHeight = NaN);
                a.remove()
            }
        },
        checkTransform: function(a) {
            if (this.autoTransform &&
                window.getComputedStyle && a) {
                if (a.style) {
                    var b = window.getComputedStyle(a, null);
                    if (b && (b = b.getPropertyValue("-webkit-transform") || b.getPropertyValue("-moz-transform") || b.getPropertyValue("-ms-transform") || b.getPropertyValue("-o-transform") || b.getPropertyValue("transform")) && "none" !== b) {
                        var c = b.split("(")[1].split(")")[0].split(","),
                            b = c[0],
                            c = c[1],
                            b = Math.sqrt(b * b + c * c);
                        isNaN(b) || (this.cssScale *= b)
                    }
                }
                a.parentNode && this.checkTransform(a.parentNode)
            }
        },
        destroy: function() {
            this.chartDiv.innerHTML = "";
            this.clearTimeOuts();
            this.legend && this.legend.destroy()
        },
        clearTimeOuts: function() {
            var a = this.timeOuts;
            if (a) {
                var b;
                for (b = 0; b < a.length; b++) clearTimeout(a[b])
            }
            this.timeOuts = []
        },
        clear: function(a) {
            try {
                document.removeEventListener("touchstart", this.docfn1, !0), document.removeEventListener("touchend", this.docfn2, !0)
            } catch (b) {}
            d.callMethod("clear", [this.chartScrollbar, this.scrollbarV, this.scrollbarH, this.chartCursor]);
            this.chartCursor = this.scrollbarH = this.scrollbarV = this.chartScrollbar = null;
            this.clearTimeOuts();
            this.container &&
                (this.container.remove(this.chartDiv), this.container.remove(this.legendDiv));
            a || d.removeChart(this);
            if (a = this.div)
                for (; a.firstChild;) a.removeChild(a.firstChild);
            this.legend && this.legend.destroy();
            this.AmExport && this.AmExport.clear && this.AmExport.clear()
        },
        setMouseCursor: function(a) {
            "auto" == a && d.isNN && (a = "default");
            this.chartDiv.style.cursor = a;
            this.legendDiv.style.cursor = a
        },
        redrawLabels: function() {
            this.labels = [];
            var a = this.allLabels;
            this.createLabelsSet();
            var b;
            for (b = 0; b < a.length; b++) this.drawLabel(a[b])
        },
        drawLabel: function(a) {
            var b = this;
            if (b.container && !1 !== a.enabled) {
                a = d.processObject(a, d.Label, b.theme);
                var c = a.y,
                    e = a.text,
                    h = a.align,
                    f = a.size,
                    g = a.color,
                    k = a.rotation,
                    l = a.alpha,
                    m = a.bold,
                    p = d.toCoordinate(a.x, b.realWidth),
                    c = d.toCoordinate(c, b.realHeight);
                p || (p = 0);
                c || (c = 0);
                void 0 === g && (g = b.color);
                isNaN(f) && (f = b.fontSize);
                h || (h = "start");
                "left" == h && (h = "start");
                "right" == h && (h = "end");
                "center" == h && (h = "middle", k ? c = b.realHeight - c + c / 2 : p = b.realWidth / 2 - p);
                void 0 === l && (l = 1);
                void 0 === k && (k = 0);
                c += f / 2;
                e = d.text(b.container,
                    e, g, b.fontFamily, f, h, m, l);
                e.translate(p, c);
                void 0 !== a.tabIndex && e.setAttr("tabindex", a.tabIndex);
                d.setCN(b, e, "label");
                a.id && d.setCN(b, e, "label-" + a.id);
                0 !== k && e.rotate(k);
                a.url ? (e.setAttr("cursor", "pointer"), e.click(function() {
                    d.getURL(a.url, b.urlTarget)
                })) : e.node.style.pointerEvents = "none";
                b.labelsSet.push(e);
                b.labels.push(e)
            }
        },
        addLabel: function(a, b, c, e, d, f, g, k, l, m) {
            a = {
                x: a,
                y: b,
                text: c,
                align: e,
                size: d,
                color: f,
                alpha: k,
                rotation: g,
                bold: l,
                url: m,
                enabled: !0
            };
            this.container && this.drawLabel(a);
            this.allLabels.push(a)
        },
        clearLabels: function() {
            var a = this.labels,
                b;
            for (b = a.length - 1; 0 <= b; b--) a[b].remove();
            this.labels = [];
            this.allLabels = []
        },
        updateHeight: function() {
            var a = this.divRealHeight,
                b = this.legend;
            if (b) {
                var c = this.legendDiv.offsetHeight,
                    b = b.position;
                if ("top" == b || "bottom" == b) {
                    a -= c;
                    if (0 > a || isNaN(a)) a = 0;
                    this.chartDiv.style.height = a + "px"
                }
            }
            return a
        },
        updateWidth: function() {
            var a = this.divRealWidth,
                b = this.divRealHeight,
                c = this.legend;
            if (c) {
                var e = this.legendDiv,
                    d = e.offsetWidth;
                isNaN(c.width) || (d = c.width);
                c.ieW && (d = c.ieW);
                var f =
                    e.offsetHeight,
                    e = e.style,
                    g = this.chartDiv.style,
                    k = c.position;
                if (("right" == k || "left" == k) && void 0 === c.divId) {
                    a -= d;
                    if (0 > a || isNaN(a)) a = 0;
                    g.width = a + "px";
                    this.balloon && this.balloon.setBounds && this.balloon.setBounds(2, 2, a - 2, this.realHeight);
                    "left" == k ? (g.left = d + "px", e.left = "0px") : (g.left = "0px", e.left = a + "px");
                    b > f && (e.top = (b - f) / 2 + "px")
                }
            }
            return a
        },
        getTitleHeight: function() {
            this.drawTitles(!0);
            return this.titleHeight
        },
        addTitle: function(a, b, c, e, d) {
            isNaN(b) && (b = this.fontSize + 2);
            a = {
                text: a,
                size: b,
                color: c,
                alpha: e,
                bold: d,
                enabled: !0
            };
            this.titles.push(a);
            return a
        },
        handleWheel: function(a) {
            var b = 0;
            a || (a = window.event);
            a.wheelDelta ? b = a.wheelDelta / 120 : a.detail && (b = -a.detail / 3);
            b && this.handleWheelReal(b, a.shiftKey);
            a.preventDefault && a.preventDefault()
        },
        handleWheelReal: function() {},
        handleDocTouchStart: function() {
            this.handleMouseMove();
            this.tmx = this.mouseX;
            this.tmy = this.mouseY;
            this.touchStartTime = (new Date).getTime()
        },
        handleDocTouchEnd: function() {
            -.5 < this.tmx && this.tmx < this.divRealWidth + 1 && 0 < this.tmy && this.tmy < this.divRealHeight ?
                (this.handleMouseMove(), 4 > Math.abs(this.mouseX - this.tmx) && 4 > Math.abs(this.mouseY - this.tmy) ? (this.tapped = !0, this.panRequired && this.panEventsEnabled && this.chartDiv && (this.chartDiv.style.msTouchAction = "none", this.chartDiv.style.touchAction = "none")) : this.mouseIsOver || this.resetTouchStyle()) : (this.tapped = !1, this.resetTouchStyle())
        },
        resetTouchStyle: function() {
            this.panEventsEnabled && this.chartDiv && (this.chartDiv.style.msTouchAction = "auto", this.chartDiv.style.touchAction = "auto")
        },
        checkTouchDuration: function(a) {
            var b =
                this,
                c = (new Date).getTime();
            if (a)
                if (a.touches) b.isTouchEvent = !0;
                else if (!b.isTouchEvent) return !0;
            if (c - b.touchStartTime > b.touchClickDuration) return !0;
            setTimeout(function() {
                b.resetTouchDuration()
            }, 300)
        },
        resetTouchDuration: function() {
            this.isTouchEvent = !1
        },
        checkTouchMoved: function() {
            if (4 < Math.abs(this.mouseX - this.tmx) || 4 < Math.abs(this.mouseY - this.tmy)) return !0
        },
        addListeners: function() {
            var a = this,
                b = a.chartDiv;
            document.addEventListener ? ("ontouchstart" in document.documentElement && (b.addEventListener("touchstart",
                function(b) {
                    a.handleTouchStart.call(a, b)
                }, !0), b.addEventListener("touchmove", function(b) {
                a.handleMouseMove.call(a, b)
            }, !0), b.addEventListener("touchend", function(b) {
                a.handleTouchEnd.call(a, b)
            }, !0), a.docfn1 = function(b) {
                a.handleDocTouchStart.call(a, b)
            }, a.docfn2 = function(b) {
                a.handleDocTouchEnd.call(a, b)
            }, document.addEventListener("touchstart", a.docfn1, !0), document.addEventListener("touchend", a.docfn2, !0)), b.addEventListener("mousedown", function(b) {
                a.mouseIsOver = !0;
                a.handleMouseMove.call(a, b);
                a.handleMouseDown.call(a,
                    b);
                a.handleDocTouchStart.call(a, b)
            }, !0), b.addEventListener("mouseover", function(b) {
                a.handleMouseOver.call(a, b)
            }, !0), b.addEventListener("mouseout", function(b) {
                a.handleMouseOut.call(a, b)
            }, !0), b.addEventListener("mouseup", function(b) {
                a.handleDocTouchEnd.call(a, b)
            }, !0)) : (b.attachEvent("onmousedown", function(b) {
                a.handleMouseDown.call(a, b)
            }), b.attachEvent("onmouseover", function(b) {
                a.handleMouseOver.call(a, b)
            }), b.attachEvent("onmouseout", function(b) {
                a.handleMouseOut.call(a, b)
            }))
        },
        dispDUpd: function() {
            this.skipEvents ||
                (this.dispatchDataUpdated && (this.dispatchDataUpdated = !1, this.fire({
                    type: "dataUpdated",
                    chart: this
                })), this.chartCreated || (this.chartCreated = !0, this.fire({
                    type: "init",
                    chart: this
                })), this.chartRendered || (this.fire({
                    type: "rendered",
                    chart: this
                }), this.chartRendered = !0), this.fire({
                    type: "drawn",
                    chart: this
                }));
            this.skipEvents = !1
        },
        validateSize: function() {
            var a = this;
            a.premeasure();
            a.checkDisplay();
            a.cssScale = 1;
            a.cssAngle = 0;
            a.checkTransform(a.div);
            if (a.divRealWidth != a.previousWidth || a.divRealHeight != a.previousHeight) {
                var b =
                    a.legend;
                if (0 < a.realWidth && 0 < a.realHeight) {
                    a.sizeChanged = !0;
                    if (b) {
                        a.legendInitTO && clearTimeout(a.legendInitTO);
                        var c = setTimeout(function() {
                            b.invalidateSize()
                        }, 10);
                        a.timeOuts.push(c);
                        a.legendInitTO = c
                    }
                    a.marginsUpdated = !1;
                    clearTimeout(a.initTO);
                    c = setTimeout(function() {
                        a.initChart()
                    }, 10);
                    a.timeOuts.push(c);
                    a.initTO = c
                }
                a.renderFix();
                b && b.renderFix && b.renderFix();
                clearTimeout(a.resizedTO);
                a.resizedTO = setTimeout(function() {
                    a.fire({
                        type: "resized",
                        chart: a
                    })
                }, 10);
                a.previousHeight = a.divRealHeight;
                a.previousWidth =
                    a.divRealWidth
            }
        },
        invalidateSize: function() {
            this.previousHeight = this.previousWidth = NaN;
            this.invalidateSizeReal()
        },
        invalidateSizeReal: function() {
            var a = this;
            a.marginsUpdated = !1;
            clearTimeout(a.validateTO);
            var b = setTimeout(function() {
                a.validateSize()
            }, 5);
            a.timeOuts.push(b);
            a.validateTO = b
        },
        validateData: function(a) {
            this.chartCreated && (this.dataChanged = !0, this.marginsUpdated = !1, this.initChart(a))
        },
        validateNow: function(a, b) {
            this.initTO && clearTimeout(this.initTO);
            a && (this.dataChanged = !0, this.marginsUpdated = !1);
            this.skipEvents = b;
            this.chartRendered = !1;
            var c = this.legend;
            c && c.position != this.prevLegendPosition && (this.previousWidth = this.mw = 0, c.invalidateSize && (c.invalidateSize(), this.validateSize()));
            this.write(this.div)
        },
        showItem: function(a) {
            a.hidden = !1;
            this.initChart()
        },
        hideItem: function(a) {
            a.hidden = !0;
            this.initChart()
        },
        hideBalloon: function() {
            var a = this;
            clearTimeout(a.hoverInt);
            clearTimeout(a.balloonTO);
            a.hoverInt = setTimeout(function() {
                a.hideBalloonReal.call(a)
            }, a.hideBalloonTime)
        },
        cleanChart: function() {},
        hideBalloonReal: function() {
            var a = this.balloon;
            a && a.hide && a.hide()
        },
        showBalloon: function(a, b, c, e, d) {
            var f = this;
            clearTimeout(f.balloonTO);
            clearTimeout(f.hoverInt);
            f.balloonTO = setTimeout(function() {
                f.showBalloonReal.call(f, a, b, c, e, d)
            }, 1)
        },
        showBalloonReal: function(a, b, c, e, d) {
            this.handleMouseMove();
            var f = this.balloon;
            f.enabled && (f.followCursor(!1), f.changeColor(b), !c || f.fixedPosition ? (f.setPosition(e, d), isNaN(e) || isNaN(d) ? f.followCursor(!0) : f.followCursor(!1)) : f.followCursor(!0), a && f.showBalloon(a))
        },
        handleMouseOver: function() {
            this.outTO && clearTimeout(this.outTO);
            d.resetMouseOver();
            this.mouseIsOver = !0
        },
        handleMouseOut: function() {
            var a = this;
            d.resetMouseOver();
            a.outTO && clearTimeout(a.outTO);
            a.outTO = setTimeout(function() {
                a.handleMouseOutReal()
            }, 10)
        },
        handleMouseOutReal: function() {
            this.mouseIsOver = !1
        },
        handleMouseMove: function(a) {
            a || (a = window.event);
            this.mouse2Y = this.mouse2X = NaN;
            var b, c, e, d;
            if (a) {
                if (a.touches) {
                    var f = a.touches.item(1);
                    f && this.panEventsEnabled && this.boundingRect && (e = f.clientX - this.boundingRect.left,
                        d = f.clientY - this.boundingRect.top);
                    a = a.touches.item(0);
                    if (!a) return
                } else this.wasTouched = !1;
                this.boundingRect && a.clientX && (b = a.clientX - this.boundingRect.left, c = a.clientY - this.boundingRect.top);
                isNaN(e) ? this.mouseX = b : (this.mouseX = Math.min(b, e), this.mouse2X = Math.max(b, e));
                isNaN(d) ? this.mouseY = c : (this.mouseY = Math.min(c, d), this.mouse2Y = Math.max(c, d));
                this.autoTransform && (this.mouseX /= this.cssScale, this.mouseY /= this.cssScale)
            }
        },
        handleTouchStart: function(a) {
            this.hideBalloonReal();
            a && (a.touches && this.tapToActivate &&
                !this.tapped || !this.panRequired) || (this.handleMouseMove(a), this.handleMouseDown(a))
        },
        handleTouchEnd: function(a) {
            this.wasTouched = !0;
            this.handleMouseMove(a);
            d.resetMouseOver();
            this.handleReleaseOutside(a)
        },
        handleReleaseOutside: function() {
            this.handleDocTouchEnd.call(this)
        },
        handleMouseDown: function(a) {
            d.resetMouseOver();
            this.mouseIsOver = !0;
            a && a.preventDefault && (this.panEventsEnabled ? a.preventDefault() : a.touches || a.preventDefault())
        },
        addLegend: function(a, b) {
            a = d.processObject(a, d.AmLegend, this.theme);
            a.divId =
                b;
            a.ieW = 0;
            var c;
            c = "object" != typeof b && b ? document.getElementById(b) : b;
            this.legend = a;
            a.chart = this;
            c ? (a.div = c, a.position = "outside", a.autoMargins = !1) : a.div = this.legendDiv;
            return a
        },
        removeLegend: function() {
            this.legend = void 0;
            this.previousWidth = 0;
            this.legendDiv.innerHTML = ""
        },
        handleResize: function() {
            (d.isPercents(this.width) || d.isPercents(this.height)) && this.invalidateSizeReal();
            this.renderFix()
        },
        renderFix: function() {
            if (!d.VML) {
                var a = this.container;
                a && a.renderFix()
            }
        },
        getSVG: function() {
            if (d.hasSVG) return this.container
        },
        animate: function(a, b, c, e, h, f, g) {
            a["an_" + b] && d.removeFromArray(this.animations, a["an_" + b]);
            c = {
                obj: a,
                frame: 0,
                attribute: b,
                from: c,
                to: e,
                time: h,
                effect: f,
                suffix: g
            };
            a["an_" + b] = c;
            this.animations.push(c);
            return c
        },
        setLegendData: function(a) {
            var b = this.legend;
            b && b.setData(a)
        },
        stopAnim: function(a) {
            d.removeFromArray(this.animations, a)
        },
        updateAnimations: function() {
            var a;
            this.container && this.container.update();
            if (this.animations)
                for (a = this.animations.length - 1; 0 <= a; a--) {
                    var b = this.animations[a],
                        c = d.updateRate * b.time,
                        e = b.frame + 1,
                        h = b.obj,
                        f = b.attribute;
                    if (e <= c) {
                        b.frame++;
                        var g = Number(b.from),
                            k = Number(b.to) - g,
                            c = d[b.effect](0, e, g, k, c);
                        0 === k ? (this.animations.splice(a, 1), h.node.style[f] = Number(b.to) + b.suffix) : h.node.style[f] = c + b.suffix
                    } else h.node.style[f] = Number(b.to) + b.suffix, h.animationFinished = !0, this.animations.splice(a, 1)
                }
        },
        update: function() {
            this.updateAnimations();
            var a = this.animatable;
            if (0 < a.length) {
                for (var b = !0, c = a.length - 1; 0 <= c; c--) {
                    var e = a[c];
                    e && (e.animationFinished ? a.splice(c, 1) : b = !1)
                }
                b && (this.fire({
                    type: "animationFinished",
                    chart: this
                }), this.animatable = [])
            }
        },
        inIframe: function() {
            try {
                return window.self !== window.top
            } catch (a) {
                return !0
            }
        },
        brr: function() {
            if (!this.hideCredits) {
                var a = "amcharts.com",
                    b = window.location.hostname.split("."),
                    c;
                2 <= b.length && (c = b[b.length - 2] + "." + b[b.length - 1]);
                this.amLink && (b = this.amLink.parentNode) && b.removeChild(this.amLink);
                b = this.creditsPosition;
                if (c != a || !0 === this.inIframe()) {
                    var a = "http://www." + a,
                        e = c = 0,
                        d = this.realWidth,
                        f = this.realHeight,
                        g = this.type;
                    if ("serial" == g || "xy" == g || "gantt" == g) c = this.marginLeftReal,
                        e = this.marginTopReal, d = c + this.plotAreaWidth, f = e + this.plotAreaHeight;
                    var g = a + "/javascript-charts/",
                        k = "",
                        l = "";
                    "ammap" == this.product && (g = a + "/javascript-maps/", k = "", l = "");
                    a = document.createElement("a");
                    l = document.createTextNode(l);
                    a.setAttribute("href", g);
                    a.setAttribute("title", k);
                    this.urlTarget && a.setAttribute("target", this.urlTarget);
                    a.appendChild(l);
                    this.chartDiv.appendChild(a);
                    this.amLink = a;
                    g = a.style;
                    g.position = "absolute";
                    g.textDecoration = "none";
                    g.color = this.color;
                    g.fontFamily = this.fontFamily;
                    g.fontSize = "11px";
                    g.opacity = .7;
                    g.display = "block";
                    var k = a.offsetWidth,
                        a = a.offsetHeight,
                        l = 5 + c,
                        m = e + 5;
                    "bottom-left" == b && (l = 5 + c, m = f - a - 3);
                    "bottom-right" == b && (l = d - k - 5, m = f - a - 3);
                    "top-right" == b && (l = d - k - 5, m = e + 5);
                    g.left = l + "px";
                    g.top = m + "px"
                }
            }
        }
    });
    d.Slice = d.Class({
        construct: function() {}
    });
    d.SerialDataItem = d.Class({
        construct: function() {}
    });
    d.GraphDataItem = d.Class({
        construct: function() {}
    });
    d.Guide = d.Class({
        construct: function(a) {
            this.cname = "Guide";
            d.applyTheme(this, a, this.cname)
        }
    });
    d.Title = d.Class({
        construct: function(a) {
            this.cname = "Title";
            d.applyTheme(this, a, this.cname)
        }
    });
    d.Label = d.Class({
        construct: function(a) {
            this.cname = "Label";
            d.applyTheme(this, a, this.cname)
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.AmGraph = d.Class({
        construct: function(a) {
            this.cname = "AmGraph";
            this.createEvents("rollOverGraphItem", "rollOutGraphItem", "clickGraphItem", "doubleClickGraphItem", "rightClickGraphItem", "clickGraph", "rollOverGraph", "rollOutGraph");
            this.type = "line";
            this.stackable = !0;
            this.columnCount = 1;
            this.columnIndex = 0;
            this.centerCustomBullets = this.showBalloon = !0;
            this.maxBulletSize = 50;
            this.minBulletSize = 4;
            this.balloonText = "[[value]]";
            this.hidden = this.scrollbar = this.animationPlayed = !1;
            this.pointPosition = "middle";
            this.depthCount = 1;
            this.includeInMinMax = !0;
            this.negativeBase = 0;
            this.visibleInLegend = !0;
            this.showAllValueLabels = !1;
            this.showBulletsAt = this.showBalloonAt = "close";
            this.lineThickness = 1;
            this.dashLength = 0;
            this.connect = !0;
            this.lineAlpha = 1;
            this.bullet = "none";
            this.bulletBorderThickness = 2;
            this.bulletBorderAlpha = 0;
            this.bulletAlpha = 1;
            this.bulletSize = 8;
            this.cornerRadiusTop = this.hideBulletsCount = this.bulletOffset = 0;
            this.cursorBulletAlpha = 1;
            this.gradientOrientation = "vertical";
            this.dy =
                this.dx = 0;
            this.periodValue = "";
            this.clustered = !0;
            this.periodSpan = 1;
            this.accessibleLabel = "[[title]] [[category]] [[value]]";
            this.accessibleSkipText = "Press enter to skip [[title]]";
            this.y = this.x = 0;
            this.switchable = !0;
            this.tcc = this.minDistance = 1;
            this.labelRotation = 0;
            this.labelAnchor = "auto";
            this.labelOffset = 3;
            this.bcn = "graph-";
            this.dateFormat = "MMM DD, YYYY";
            this.noRounding = !0;
            d.applyTheme(this, a, this.cname)
        },
        init: function() {
            this.createBalloon()
        },
        draw: function() {
            var a = this.chart;
            a.isRolledOverBullet = !1;
            var b = a.type;
            if (a.drawGraphs) {
                isNaN(this.precision) || (this.numberFormatter ? this.numberFormatter.precision = this.precision : this.numberFormatter = {
                    precision: this.precision,
                    decimalSeparator: a.decimalSeparator,
                    thousandsSeparator: a.thousandsSeparator
                });
                var c = a.container;
                this.container = c;
                this.destroy();
                var e = c.set();
                this.set = e;
                e.translate(this.x, this.y);
                var h = c.set();
                this.bulletSet = h;
                h.translate(this.x, this.y);
                this.behindColumns ? (a.graphsBehindSet.push(e), a.bulletBehindSet.push(h)) : (a.graphsSet.push(e), a.bulletSet.push(h));
                var f = this.bulletAxis;
                d.isString(f) && (this.bulletAxis = a.getValueAxisById(f));
                c = c.set();
                d.remove(this.columnsSet);
                this.columnsSet = c;
                d.setCN(a, e, "graph-" + this.type);
                d.setCN(a, e, "graph-" + this.id);
                d.setCN(a, h, "graph-" + this.type);
                d.setCN(a, h, "graph-" + this.id);
                this.columnsArray = [];
                this.ownColumns = [];
                this.allBullets = [];
                this.animationArray = [];
                h = this.labelPosition;
                h || (f = this.valueAxis.stackType, h = "top", "column" == this.type && (a.rotate && (h = "right"), "100%" == f || "regular" == f) && (h = "middle"), this.labelPosition = h);
                d.ifArray(this.data) && (a = !1, "xy" == b ? this.xAxis.axisCreated && this.yAxis.axisCreated && (a = !0) : this.valueAxis.axisCreated && (a = !0), !this.hidden && a && this.createGraph());
                e.push(c)
            }
        },
        createGraph: function() {
            var a = this,
                b = a.chart;
            a.startAlpha = b.startAlpha;
            a.seqAn = b.sequencedAnimation;
            a.baseCoord = a.valueAxis.baseCoord;
            void 0 === a.fillAlphas && (a.fillAlphas = 0);
            a.bulletColorR = a.bulletColor;
            void 0 === a.bulletColorR && (a.bulletColorR = a.lineColorR, a.bulletColorNegative = a.negativeLineColor);
            void 0 === a.bulletAlpha && (a.bulletAlpha =
                a.lineAlpha);
            if ("step" == c || d.VML) a.noRounding = !1;
            var c = b.type;
            "gantt" == c && (c = "serial");
            clearTimeout(a.playedTO);
            if (!isNaN(a.valueAxis.min) && !isNaN(a.valueAxis.max)) {
                switch (c) {
                    case "serial":
                        a.categoryAxis && (a.createSerialGraph(), "candlestick" == a.type && 1 > a.valueAxis.minMaxMultiplier && a.positiveClip(a.set));
                        break;
                    case "radar":
                        a.createRadarGraph();
                        break;
                    case "xy":
                        a.createXYGraph()
                }
                a.playedTO = setTimeout(function() {
                    a.setAnimationPlayed.call(a)
                }, 500 * a.chart.startDuration)
            }
        },
        setAnimationPlayed: function() {
            this.animationPlayed = !0
        },
        createXYGraph: function() {
            var a = [],
                b = [],
                c = this.xAxis,
                e = this.yAxis;
            this.pmh = e.height;
            this.pmw = c.width;
            this.pmy = this.pmx = 0;
            var d;
            for (d = this.start; d <= this.end; d++) {
                var f = this.data[d].axes[c.id].graphs[this.id],
                    g = f.values,
                    k = g.x,
                    l = g.y,
                    g = c.getCoordinate(k, this.noRounding),
                    m = e.getCoordinate(l, this.noRounding);
                if (!isNaN(k) && !isNaN(l) && (a.push(g), b.push(m), f.x = g, f.y = m, k = this.createBullet(f, g, m, d), l = this.labelText)) {
                    var l = this.createLabel(f, l),
                        p = 0;
                    k && (p = k.size);
                    this.positionLabel(f, g, m, l, p)
                }
            }
            this.drawLineGraph(a,
                b);
            this.launchAnimation()
        },
        createRadarGraph: function() {
            var a = this.valueAxis.stackType,
                b = [],
                c = [],
                e = [],
                d = [],
                f, g, k, l, m;
            for (m = this.start; m <= this.end; m++) {
                var p = this.data[m].axes[this.valueAxis.id].graphs[this.id],
                    q, n;
                "none" == a || "3d" == a ? q = p.values.value : (q = p.values.close, n = p.values.open);
                if (isNaN(q)) this.connect || (this.drawLineGraph(b, c, e, d), b = [], c = [], e = [], d = []);
                else {
                    var t = this.valueAxis.getCoordinate(q, this.noRounding) - this.height,
                        t = t * this.valueAxis.rMultiplier,
                        r = -360 / (this.end - this.start + 1) * m;
                    "middle" ==
                    this.valueAxis.pointPosition && (r -= 180 / (this.end - this.start + 1));
                    q = t * Math.sin(r / 180 * Math.PI);
                    t *= Math.cos(r / 180 * Math.PI);
                    b.push(q);
                    c.push(t);
                    if (!isNaN(n)) {
                        var w = this.valueAxis.getCoordinate(n, this.noRounding) - this.height,
                            w = w * this.valueAxis.rMultiplier,
                            z = w * Math.sin(r / 180 * Math.PI),
                            r = w * Math.cos(r / 180 * Math.PI);
                        e.push(z);
                        d.push(r);
                        isNaN(k) && (k = z);
                        isNaN(l) && (l = r)
                    }
                    r = this.createBullet(p, q, t, m);
                    p.x = q;
                    p.y = t;
                    if (z = this.labelText) z = this.createLabel(p, z), w = 0, r && (w = r.size), this.positionLabel(p, q, t, z, w);
                    isNaN(f) && (f =
                        q);
                    isNaN(g) && (g = t)
                }
            }
            b.push(f);
            c.push(g);
            isNaN(k) || (e.push(k), d.push(l));
            this.drawLineGraph(b, c, e, d);
            this.launchAnimation()
        },
        positionLabel: function(a, b, c, e, d) {
            if (e) {
                var f = this.chart,
                    g = this.valueAxis,
                    k = "middle",
                    l = !1,
                    m = this.labelPosition,
                    p = e.getBBox(),
                    q = this.chart.rotate,
                    n = a.isNegative,
                    t = this.fontSize;
                void 0 === t && (t = this.chart.fontSize);
                c -= p.height / 2 - t / 2 - 1;
                void 0 !== a.labelIsNegative && (n = a.labelIsNegative);
                switch (m) {
                    case "right":
                        m = q ? n ? "left" : "right" : "right";
                        break;
                    case "top":
                        m = q ? "top" : n ? "bottom" : "top";
                        break;
                    case "bottom":
                        m = q ? "bottom" : n ? "top" : "bottom";
                        break;
                    case "left":
                        m = q ? n ? "right" : "left" : "left"
                }
                var t = a.columnGraphics,
                    r = 0,
                    w = 0;
                t && (r = t.x, w = t.y);
                var z = this.labelOffset;
                switch (m) {
                    case "right":
                        k = "start";
                        b += d / 2 + z;
                        break;
                    case "top":
                        c = g.reversed ? c + (d / 2 + p.height / 2 + z) : c - (d / 2 + p.height / 2 + z);
                        break;
                    case "bottom":
                        c = g.reversed ? c - (d / 2 + p.height / 2 + z) : c + (d / 2 + p.height / 2 + z);
                        break;
                    case "left":
                        k = "end";
                        b -= d / 2 + z;
                        break;
                    case "inside":
                        "column" == this.type && (l = !0, q ? n ? (k = "end", b = r - 3 - z) : (k = "start", b = r + 3 + z) : c = n ? w + 7 + z : w - 10 - z);
                        break;
                    case "middle":
                        "column" == this.type && (l = !0, q ? b -= (b - r) / 2 + z - 3 : c -= (c - w) / 2 + z - 3)
                }
                "auto" != this.labelAnchor && (k = this.labelAnchor);
                e.attr({
                    "text-anchor": k
                });
                this.labelRotation && e.rotate(this.labelRotation);
                e.translate(b, c);
                !this.showAllValueLabels && t && l && (p = e.getBBox(), p.height > a.columnHeight || p.width > a.columnWidth) && (e.remove(), e = null);
                if (e && "radar" != f.type)
                    if (q) {
                        if (0 > c || c > this.height) e.remove(), e = null;
                        !this.showAllValueLabels && e && (0 > b || b > this.width) && (e.remove(), e = null)
                    } else {
                        if (0 > b || b > this.width) e.remove(),
                            e = null;
                        !this.showAllValueLabels && e && (0 > c || c > this.height) && (e.remove(), e = null)
                    }
                e && this.allBullets.push(e);
                return e
            }
        },
        getGradRotation: function() {
            var a = 270;
            "horizontal" == this.gradientOrientation && (a = 0);
            return this.gradientRotation = a
        },
        createSerialGraph: function() {
            this.dashLengthSwitched = this.fillColorsSwitched = this.lineColorSwitched = void 0;
            var a = this.chart,
                b = this.id,
                c = this.index,
                e = this.data,
                h = this.chart.container,
                f = this.valueAxis,
                g = this.type,
                k = this.columnWidthReal,
                l = this.showBulletsAt;
            isNaN(this.columnWidth) ||
                (k = this.columnWidth);
            isNaN(k) && (k = .8);
            var m = this.useNegativeColorIfDown,
                p = this.width,
                q = this.height,
                n = this.y,
                t = this.rotate,
                r = this.columnCount,
                w = d.toCoordinate(this.cornerRadiusTop, k / 2),
                z = this.connect,
                x = [],
                u = [],
                A, y, B, D, C = this.chart.graphs.length,
                I, H = this.dx / this.tcc,
                Q = this.dy / this.tcc,
                M = f.stackType,
                P = this.start,
                ia = this.end,
                J = this.scrollbar,
                aa = "graph-column-";
            J && (aa = "scrollbar-graph-column-");
            var ma = this.categoryAxis,
                na = this.baseCoord,
                Pa = this.negativeBase,
                Z = this.columnIndex,
                da = this.lineThickness,
                X = this.lineAlpha,
                xa = this.lineColorR,
                ea = this.dashLength,
                fa = this.set,
                Ba, ga = this.getGradRotation(),
                V = this.chart.columnSpacing,
                Y = ma.cellWidth,
                Da = (Y * k - r) / r;
            V > Da && (V = Da);
            var G, v, oa, ha = q,
                Qa = p,
                ca = 0,
                tb = 0,
                ub = 0,
                vb = 0,
                gb = 0,
                hb = 0,
                wb = this.fillColorsR,
                Ra = this.negativeFillColors,
                Ja = this.negativeLineColor,
                Ya = this.fillAlphas,
                Za = this.negativeFillAlphas;
            "object" == typeof Ya && (Ya = Ya[0]);
            "object" == typeof Za && (Za = Za[0]);
            var xb = this.noRounding;
            "step" == g && (xb = !1);
            var ib = f.getCoordinate(f.min);
            f.logarithmic && (ib = f.getCoordinate(f.minReal));
            this.minCoord =
                ib;
            this.resetBullet && (this.bullet = "none");
            if (!(J || "line" != g && "smoothedLine" != g && "step" != g || (1 == e.length && "step" != g && "none" == this.bullet && (this.bullet = "round", this.resetBullet = !0), !Ra && void 0 == Ja || m))) {
                var Ua = Pa;
                Ua > f.max && (Ua = f.max);
                Ua < f.min && (Ua = f.min);
                f.logarithmic && (Ua = f.minReal);
                var Ka = f.getCoordinate(Ua),
                    Mb = f.getCoordinate(f.max);
                t ? (ha = q, Qa = Math.abs(Mb - Ka), ub = q, vb = Math.abs(ib - Ka), hb = tb = 0, f.reversed ? (ca = 0, gb = Ka) : (ca = Ka, gb = 0)) : (Qa = p, ha = Math.abs(Mb - Ka), vb = p, ub = Math.abs(ib - Ka), gb = ca = 0, f.reversed ?
                    (hb = n, tb = Ka) : hb = Ka)
            }
            var La = Math.round;
            this.pmx = La(ca);
            this.pmy = La(tb);
            this.pmh = La(ha);
            this.pmw = La(Qa);
            this.nmx = La(gb);
            this.nmy = La(hb);
            this.nmh = La(ub);
            this.nmw = La(vb);
            d.isModern || (this.nmy = this.nmx = 0, this.nmh = this.height);
            this.clustered || (r = 1);
            k = "column" == g ? (Y * k - V * (r - 1)) / r : Y * k;
            1 > k && (k = 1);
            var Nb = this.fixedColumnWidth;
            isNaN(Nb) || (k = Nb);
            var L;
            if ("line" == g || "step" == g || "smoothedLine" == g) {
                if (0 < P) {
                    for (L = P - 1; - 1 < L; L--)
                        if (G = e[L], v = G.axes[f.id].graphs[b], oa = v.values.value, !isNaN(oa)) {
                            P = L;
                            break
                        }
                    if (this.lineColorField)
                        for (L =
                            P; - 1 < L; L--)
                            if (G = e[L], v = G.axes[f.id].graphs[b], v.lineColor) {
                                this.lineColorSwitched = v.lineColor;
                                void 0 === this.bulletColor && (this.bulletColorSwitched = this.lineColorSwitched);
                                break
                            }
                    if (this.fillColorsField)
                        for (L = P; - 1 < L; L--)
                            if (G = e[L], v = G.axes[f.id].graphs[b], v.fillColors) {
                                this.fillColorsSwitched = v.fillColors;
                                break
                            }
                    if (this.dashLengthField)
                        for (L = P; - 1 < L; L--)
                            if (G = e[L], v = G.axes[f.id].graphs[b], !isNaN(v.dashLength)) {
                                this.dashLengthSwitched = v.dashLength;
                                break
                            }
                }
                if (ia < e.length - 1)
                    for (L = ia + 1; L < e.length; L++)
                        if (G = e[L],
                            v = G.axes[f.id].graphs[b], oa = v.values.value, !isNaN(oa)) {
                            ia = L;
                            break
                        }
            }
            ia < e.length - 1 && ia++;
            var T = [],
                U = [],
                Ma = !1;
            if ("line" == g || "step" == g || "smoothedLine" == g)
                if (this.stackable && "regular" == M || "100%" == M || this.fillToGraph) Ma = !0;
            var Ob = this.noStepRisers,
                jb = -1E3,
                kb = -1E3,
                lb = this.minDistance,
                Sa = !0,
                $a = !1;
            for (L = P; L <= ia; L++) {
                G = e[L];
                v = G.axes[f.id].graphs[b];
                v.index = L;
                var ab, Ta = NaN;
                if (m && void 0 == this.openField)
                    for (var yb = L + 1; yb < e.length && (!e[yb] || !(ab = e[L + 1].axes[f.id].graphs[b]) || !ab.values || (Ta = ab.values.value, isNaN(Ta))); yb++);
                var S, R, K, ba, ja = NaN,
                    E = NaN,
                    F = NaN,
                    O = NaN,
                    N = NaN,
                    qa = NaN,
                    ra = NaN,
                    sa = NaN,
                    ta = NaN,
                    ya = NaN,
                    Ea = NaN,
                    ka = NaN,
                    la = NaN,
                    W = NaN,
                    zb = NaN,
                    Ab = NaN,
                    ua = NaN,
                    va = void 0,
                    Na = wb,
                    Va = Ya,
                    Ha = xa,
                    Ca, za, Bb = this.proCandlesticks,
                    mb = this.topRadius,
                    Fa = q - 1,
                    pa = p - 1,
                    bb = this.pattern;
                void 0 != v.pattern && (bb = v.pattern);
                isNaN(v.alpha) || (Va = v.alpha);
                isNaN(v.dashLength) || (ea = v.dashLength);
                var Ia = v.values;
                f.recalculateToPercents && (Ia = v.percents);
                "none" == M && (Z = isNaN(v.columnIndex) ? this.columnIndex : v.columnIndex);
                if (Ia) {
                    W = this.stackable && "none" != M && "3d" !=
                        M ? Ia.close : Ia.value;
                    if ("candlestick" == g || "ohlc" == g) W = Ia.close, Ab = Ia.low, ra = f.getCoordinate(Ab), zb = Ia.high, ta = f.getCoordinate(zb);
                    ua = Ia.open;
                    F = f.getCoordinate(W, xb);
                    isNaN(ua) || (N = f.getCoordinate(ua, xb), m && "regular" != M && "100%" != M && (Ta = ua, ua = N = NaN));
                    m && (void 0 == this.openField ? ab && (ab.isNegative = Ta < W ? !0 : !1, isNaN(Ta) && (v.isNegative = !Sa)) : v.isNegative = Ta > W ? !0 : !1);
                    if (!J) switch (this.showBalloonAt) {
                        case "close":
                            v.y = F;
                            break;
                        case "open":
                            v.y = N;
                            break;
                        case "high":
                            v.y = ta;
                            break;
                        case "low":
                            v.y = ra
                    }
                    var ja = G.x[ma.id],
                        Wa = this.periodSpan - 1;
                    "step" != g || isNaN(G.cellWidth) || (Y = G.cellWidth);
                    var wa = Math.floor(Y / 2) + Math.floor(Wa * Y / 2),
                        Ga = wa,
                        nb = 0;
                    "left" == this.stepDirection && (nb = (2 * Y + Wa * Y) / 2, ja -= nb);
                    "center" == this.stepDirection && (nb = Y / 2, ja -= nb);
                    "start" == this.pointPosition && (ja -= Y / 2 + Math.floor(Wa * Y / 2), wa = 0, Ga = Math.floor(Y) + Math.floor(Wa * Y));
                    "end" == this.pointPosition && (ja += Y / 2 + Math.floor(Wa * Y / 2), wa = Math.floor(Y) + Math.floor(Wa * Y), Ga = 0);
                    if (Ob) {
                        var Cb = this.columnWidth;
                        isNaN(Cb) || (wa *= Cb, Ga *= Cb)
                    }
                    J || (v.x = ja); - 1E5 > ja && (ja = -1E5);
                    ja > p + 1E5 && (ja = p + 1E5);
                    t ? (E = F, O = N, N = F = ja, isNaN(ua) && !this.fillToGraph && (O = na), qa = ra, sa = ta) : (O = E = ja, isNaN(ua) && !this.fillToGraph && (N = na));
                    if (!Bb && W < ua || Bb && W < Ba) v.isNegative = !0, Ra && (Na = Ra), Za && (Va = Za), void 0 != Ja && (Ha = Ja);
                    $a = !1;
                    isNaN(W) || (m ? W > Ta ? (Sa && ($a = !0), Sa = !1) : (Sa || ($a = !0), Sa = !0) : v.isNegative = W < Pa ? !0 : !1, Ba = W);
                    var Pb = !1;
                    J && a.chartScrollbar.ignoreCustomColors && (Pb = !0);
                    Pb || (void 0 != v.color && (Na = v.color), v.fillColors && (Na = v.fillColors));
                    F = d.fitToBounds(F, -3E4, 3E4);
                    switch (g) {
                        case "line":
                            if (isNaN(W)) z ||
                                (this.drawLineGraph(x, u, T, U), x = [], u = [], T = [], U = []);
                            else {
                                if (Math.abs(E - jb) >= lb || Math.abs(F - kb) >= lb) x.push(E), u.push(F), jb = E, kb = F;
                                ya = E;
                                Ea = F;
                                ka = E;
                                la = F;
                                !Ma || isNaN(N) || isNaN(O) || (T.push(O), U.push(N));
                                if ($a || void 0 != v.lineColor && v.lineColor != this.lineColorSwitched || void 0 != v.fillColors && v.fillColors != this.fillColorsSwitched || !isNaN(v.dashLength)) this.drawLineGraph(x, u, T, U), x = [E], u = [F], T = [], U = [], !Ma || isNaN(N) || isNaN(O) || (T.push(O), U.push(N)), m ? (Sa ? (this.lineColorSwitched = xa, this.fillColorsSwitched = wb) : (this.lineColorSwitched =
                                    Ja, this.fillColorsSwitched = Ra), void 0 === this.bulletColor && (this.bulletColorSwitched = xa)) : (this.lineColorSwitched = v.lineColor, this.fillColorsSwitched = v.fillColors, void 0 === this.bulletColor && (this.bulletColorSwitched = this.lineColorSwitched)), this.dashLengthSwitched = v.dashLength;
                                v.gap && (this.drawLineGraph(x, u, T, U), x = [], u = [], T = [], U = [])
                            }
                            break;
                        case "smoothedLine":
                            if (isNaN(W)) z || (this.drawSmoothedGraph(x, u, T, U), x = [], u = [], T = [], U = []);
                            else {
                                if (Math.abs(E - jb) >= lb || Math.abs(F - kb) >= lb) x.push(E), u.push(F), jb = E,
                                    kb = F;
                                ya = E;
                                Ea = F;
                                ka = E;
                                la = F;
                                !Ma || isNaN(N) || isNaN(O) || (T.push(O), U.push(N));
                                void 0 == v.lineColor && void 0 == v.fillColors && isNaN(v.dashLength) || (this.drawSmoothedGraph(x, u, T, U), x = [E], u = [F], T = [], U = [], !Ma || isNaN(N) || isNaN(O) || (T.push(O), U.push(N)), this.lineColorSwitched = v.lineColor, this.fillColorsSwitched = v.fillColors, this.dashLengthSwitched = v.dashLength);
                                v.gap && (this.drawSmoothedGraph(x, u, T, U), x = [], u = [], T = [], U = [])
                            }
                            break;
                        case "step":
                            if (!isNaN(W)) {
                                t ? (isNaN(A) || (x.push(A), u.push(F - wa)), u.push(F - wa), x.push(E),
                                    u.push(F + Ga), x.push(E), !Ma || isNaN(N) || isNaN(O) || (isNaN(B) || (T.push(B), U.push(N - wa)), T.push(O), U.push(N - wa), T.push(O), U.push(N + Ga))) : (isNaN(y) || (u.push(y), x.push(E - wa)), x.push(E - wa), u.push(F), x.push(E + Ga), u.push(F), !Ma || isNaN(N) || isNaN(O) || (isNaN(D) || (T.push(O - wa), U.push(D)), T.push(O - wa), U.push(N), T.push(O + Ga), U.push(N)));
                                A = E;
                                y = F;
                                B = O;
                                D = N;
                                ya = E;
                                Ea = F;
                                ka = E;
                                la = F;
                                if ($a || void 0 != v.lineColor || void 0 != v.fillColors || !isNaN(v.dashLength)) {
                                    var Db = x[x.length - 2],
                                        dc = u[u.length - 2];
                                    x.pop();
                                    u.pop();
                                    T.pop();
                                    U.pop();
                                    this.drawLineGraph(x, u, T, U);
                                    x = [Db];
                                    u = [dc];
                                    T = [];
                                    U = [];
                                    Ma && (T = [Db, Db + wa + Ga], U = [D, D]);
                                    t ? (u.push(F + Ga), x.push(E)) : (x.push(E + Ga), u.push(F));
                                    this.lineColorSwitched = v.lineColor;
                                    this.fillColorsSwitched = v.fillColors;
                                    this.dashLengthSwitched = v.dashLength;
                                    m && (Sa ? (this.lineColorSwitched = xa, this.fillColorsSwitched = wb) : (this.lineColorSwitched = Ja, this.fillColorsSwitched = Ra))
                                }
                                if (Ob || v.gap) A = y = NaN, this.drawLineGraph(x, u, T, U), x = [], u = [], T = [], U = []
                            } else if (!z) {
                                if (1 >= this.periodSpan || 1 < this.periodSpan && E - A > wa + Ga) A = y = NaN;
                                this.drawLineGraph(x, u, T, U);
                                x = [];
                                u = [];
                                T = [];
                                U = []
                            }
                            break;
                        case "column":
                            Ca = Ha;
                            void 0 != v.lineColor && (Ca = v.lineColor);
                            if (!isNaN(W)) {
                                m || (v.isNegative = W < Pa ? !0 : !1);
                                v.isNegative && (Ra && (Na = Ra), void 0 != Ja && (Ca = Ja));
                                var Qb = f.min,
                                    Rb = f.max,
                                    ob = ua;
                                isNaN(ob) && (ob = Pa);
                                if (!(W < Qb && ob < Qb || W > Rb && ob > Rb)) {
                                    var Aa;
                                    if (t) {
                                        "3d" == M ? (R = F - (r / 2 - this.depthCount + 1) * (k + V) + V / 2 + Q * Z, S = O + H * Z, Aa = Z) : (R = Math.floor(F - (r / 2 - Z) * (k + V) + V / 2), S = O, Aa = 0);
                                        K = k;
                                        ya = E;
                                        Ea = R + k / 2;
                                        ka = E;
                                        la = R + k / 2;
                                        R + K > q + Aa * Q && (K = q - R + Aa * Q);
                                        R < Aa * Q && (K += R, R = Aa * Q);
                                        ba = E - O;
                                        var ec = S;
                                        S =
                                            d.fitToBounds(S, 0, p);
                                        ba += ec - S;
                                        ba = d.fitToBounds(ba, -S, p - S + H * Z);
                                        v.labelIsNegative = 0 > ba ? !0 : !1;
                                        0 === ba && 1 / W === 1 / -0 && (v.labelIsNegative = !0);
                                        isNaN(G.percentWidthValue) || (K = this.height * G.percentWidthValue / 100, R = ja - K / 2, Ea = R + K / 2);
                                        K = d.roundTo(K, 2);
                                        ba = d.roundTo(ba, 2);
                                        R < q && 0 < K && (va = new d.Cuboid(h, ba, K, H - a.d3x, Q - a.d3y, Na, Va, da, Ca, X, ga, w, t, ea, bb, mb, aa), v.columnWidth = Math.abs(ba), v.columnHeight = Math.abs(K))
                                    } else {
                                        "3d" == M ? (S = E - (r / 2 - this.depthCount + 1) * (k + V) + V / 2 + H * Z, R = N + Q * Z, Aa = Z) : (S = E - (r / 2 - Z) * (k + V) + V / 2, R = N, Aa = 0);
                                        K = k;
                                        ya = S + k / 2;
                                        Ea = F;
                                        ka = S + k / 2;
                                        la = F;
                                        S + K > p + Aa * H && (K = p - S + Aa * H);
                                        S < Aa * H && (K += S - Aa * H, S = Aa * H);
                                        ba = F - N;
                                        v.labelIsNegative = 0 < ba ? !0 : !1;
                                        0 === ba && 1 / W !== 1 / Math.abs(W) && (v.labelIsNegative = !0);
                                        var fc = R;
                                        R = d.fitToBounds(R, this.dy, q);
                                        ba += fc - R;
                                        ba = d.fitToBounds(ba, -R + Q * Aa, q - R);
                                        isNaN(G.percentWidthValue) || (K = this.width * G.percentWidthValue / 100, S = ja - K / 2, ya = S + K / 2);
                                        K = d.roundTo(K, 2);
                                        ba = d.roundTo(ba, 2);
                                        S < p + Z * H && 0 < K && (this.showOnAxis && (R -= Q / 2), va = new d.Cuboid(h, K, ba, H - a.d3x, Q - a.d3y, Na, Va, da, Ca, this.lineAlpha, ga, w, t, ea, bb, mb, aa), v.columnHeight =
                                            Math.abs(ba), v.columnWidth = Math.abs(K))
                                    }
                                }
                                if (va) {
                                    za = va.set;
                                    d.setCN(a, va.set, "graph-" + this.type);
                                    d.setCN(a, va.set, "graph-" + this.id);
                                    v.className && d.setCN(a, va.set, v.className, !0);
                                    v.columnGraphics = za;
                                    S = d.roundTo(S, 2);
                                    R = d.roundTo(R, 2);
                                    za.translate(S, R);
                                    (v.url || this.showHandOnHover) && za.setAttr("cursor", "pointer");
                                    if (!J) {
                                        "none" == M && (I = t ? (this.end + 1 - L) * C - c : C * L + c);
                                        "3d" == M && (t ? (I = (this.end + 1 - L) * C - c - 1E3 * this.depthCount, ya += H * Z, ka += H * Z, v.y += H * Z) : (I = (C - c) * (L + 1) + 1E3 * this.depthCount, Ea += Q * Z, la += Q * Z, v.y += Q * Z));
                                        if ("regular" == M || "100%" == M) I = t ? 0 < Ia.value ? (this.end + 1 - L) * C + c : (this.end + 1 - L) * C - c : 0 < Ia.value ? C * L + c : C * L - c;
                                        this.columnsArray.push({
                                            column: va,
                                            depth: I
                                        });
                                        v.x = t ? R + K / 2 : S + K / 2;
                                        this.ownColumns.push(va);
                                        this.animateColumns(va, L, E, O, F, N);
                                        this.addListeners(za, v);
                                        void 0 !== this.tabIndex && za.setAttr("tabindex", this.tabIndex)
                                    }
                                    this.columnsSet.push(za)
                                }
                            }
                            break;
                        case "candlestick":
                            if (!isNaN(ua) && !isNaN(W)) {
                                var Xa, cb;
                                Ca = Ha;
                                void 0 != v.lineColor && (Ca = v.lineColor);
                                ya = E;
                                la = Ea = F;
                                ka = E;
                                if (t) {
                                    "open" == l && (ka = O);
                                    "high" == l && (ka = sa);
                                    "low" == l && (ka = qa);
                                    E = d.fitToBounds(E, 0, pa);
                                    O = d.fitToBounds(O, 0, pa);
                                    qa = d.fitToBounds(qa, 0, pa);
                                    sa = d.fitToBounds(sa, 0, pa);
                                    if (0 === E && 0 === O && 0 === qa && 0 === sa) continue;
                                    if (E == pa && O == pa && qa == pa && sa == pa) continue;
                                    R = F - k / 2;
                                    S = O;
                                    K = k;
                                    R + K > q && (K = q - R);
                                    0 > R && (K += R, R = 0);
                                    if (R < q && 0 < K) {
                                        var Eb, Fb;
                                        W > ua ? (Eb = [E, sa], Fb = [O, qa]) : (Eb = [O, sa], Fb = [E, qa]);
                                        !isNaN(sa) && !isNaN(qa) && F < q && 0 < F && (Xa = d.line(h, Eb, [F, F], Ca, X, da), cb = d.line(h, Fb, [F, F], Ca, X, da));
                                        ba = E - O;
                                        va = new d.Cuboid(h, ba, K, H, Q, Na, Ya, da, Ca, X, ga, w, t, ea, bb, mb, aa)
                                    }
                                } else {
                                    "open" == l && (la =
                                        N);
                                    "high" == l && (la = ta);
                                    "low" == l && (la = ra);
                                    F = d.fitToBounds(F, 0, Fa);
                                    N = d.fitToBounds(N, 0, Fa);
                                    ra = d.fitToBounds(ra, 0, Fa);
                                    ta = d.fitToBounds(ta, 0, Fa);
                                    if (0 === F && 0 === N && 0 === ra && 0 === ta) continue;
                                    if (F == Fa && N == Fa && ra == Fa && ta == Fa) continue;
                                    S = E - k / 2;
                                    R = N + da / 2;
                                    K = k;
                                    S + K > p && (K = p - S);
                                    0 > S && (K += S, S = 0);
                                    ba = F - N;
                                    if (S < p && 0 < K) {
                                        Bb && W >= ua && (Va = 0);
                                        var va = new d.Cuboid(h, K, ba, H, Q, Na, Va, da, Ca, X, ga, w, t, ea, bb, mb, aa),
                                            Gb, Hb;
                                        W > ua ? (Gb = [F, ta], Hb = [N, ra]) : (Gb = [N, ta], Hb = [F, ra]);
                                        !isNaN(ta) && !isNaN(ra) && E < p && 0 < E && (Xa = d.line(h, [E, E], Gb, Ca, X, da), cb = d.line(h, [E, E], Hb, Ca, X, da), d.setCN(a, Xa, this.bcn + "line-high"), v.className && d.setCN(a, Xa, v.className, !0), d.setCN(a, cb, this.bcn + "line-low"), v.className && d.setCN(a, cb, v.className, !0))
                                    }
                                }
                                va && (za = va.set, v.columnGraphics = za, fa.push(za), za.translate(S, R - da / 2), (v.url || this.showHandOnHover) && za.setAttr("cursor", "pointer"), Xa && (fa.push(Xa), fa.push(cb)), J || (v.x = t ? R + K / 2 : S + K / 2, this.animateColumns(va, L, E, O, F, N), this.addListeners(za, v), void 0 !== this.tabIndex && za.setAttr("tabindex", this.tabIndex)))
                            }
                            break;
                        case "ohlc":
                            if (!(isNaN(ua) ||
                                    isNaN(zb) || isNaN(Ab) || isNaN(W))) {
                                var Sb = h.set();
                                fa.push(Sb);
                                W < ua && (v.isNegative = !0, void 0 != Ja && (Ha = Ja));
                                void 0 != v.lineColor && (Ha = v.lineColor);
                                var pb, qb, rb;
                                if (t) {
                                    la = F;
                                    ka = E;
                                    "open" == l && (ka = O);
                                    "high" == l && (ka = sa);
                                    "low" == l && (ka = qa);
                                    qa = d.fitToBounds(qa, 0, pa);
                                    sa = d.fitToBounds(sa, 0, pa);
                                    if (0 === E && 0 === O && 0 === qa && 0 === sa) continue;
                                    if (E == pa && O == pa && qa == pa && sa == pa) continue;
                                    var Ib = F - k / 2,
                                        Ib = d.fitToBounds(Ib, 0, q),
                                        Tb = d.fitToBounds(F, 0, q),
                                        Jb = F + k / 2,
                                        Jb = d.fitToBounds(Jb, 0, q);
                                    0 <= O && O <= pa && (qb = d.line(h, [O, O], [Ib, Tb], Ha, X,
                                        da, ea));
                                    0 < F && F < q && (pb = d.line(h, [qa, sa], [F, F], Ha, X, da, ea));
                                    0 <= E && E <= pa && (rb = d.line(h, [E, E], [Tb, Jb], Ha, X, da, ea))
                                } else {
                                    la = F;
                                    "open" == l && (la = N);
                                    "high" == l && (la = ta);
                                    "low" == l && (la = ra);
                                    var ka = E,
                                        ra = d.fitToBounds(ra, 0, Fa),
                                        ta = d.fitToBounds(ta, 0, Fa),
                                        Kb = E - k / 2,
                                        Kb = d.fitToBounds(Kb, 0, p),
                                        Ub = d.fitToBounds(E, 0, p),
                                        Lb = E + k / 2,
                                        Lb = d.fitToBounds(Lb, 0, p);
                                    0 <= N && N <= Fa && (qb = d.line(h, [Kb, Ub], [N, N], Ha, X, da, ea));
                                    0 < E && E < p && (pb = d.line(h, [E, E], [ra, ta], Ha, X, da, ea));
                                    0 <= F && F <= Fa && (rb = d.line(h, [Ub, Lb], [F, F], Ha, X, da, ea))
                                }
                                fa.push(qb);
                                fa.push(pb);
                                fa.push(rb);
                                d.setCN(a, qb, this.bcn + "stroke-open");
                                d.setCN(a, rb, this.bcn + "stroke-close");
                                d.setCN(a, pb, this.bcn + "stroke");
                                v.className && d.setCN(a, Sb, v.className, !0);
                                ya = E;
                                Ea = F
                            }
                    }
                    if (!J && !isNaN(W)) {
                        var Vb = this.hideBulletsCount;
                        if (this.end - this.start <= Vb || 0 === Vb) {
                            var Wb = this.createBullet(v, ka, la, L),
                                Xb = this.labelText;
                            if (Xb && !isNaN(ya) && !isNaN(ya)) {
                                var gc = this.createLabel(v, Xb),
                                    Yb = 0;
                                Wb && (Yb = Wb.size);
                                this.positionLabel(v, ya, Ea, gc, Yb)
                            }
                            if ("regular" == M || "100%" == M) {
                                var Zb = f.totalText;
                                if (Zb) {
                                    var Oa = this.createLabel(v,
                                        Zb, f.totalTextColor);
                                    d.setCN(a, Oa, this.bcn + "label-total");
                                    this.allBullets.push(Oa);
                                    if (Oa) {
                                        var $b = Oa.getBBox(),
                                            ac = $b.width,
                                            bc = $b.height,
                                            db, eb, sb = f.totalTextOffset,
                                            cc = f.totals[L];
                                        cc && cc.remove();
                                        var fb = 0;
                                        "column" != g && (fb = this.bulletSize);
                                        t ? (eb = Ea, db = 0 > W ? E - ac / 2 - 2 - fb - sb : E + ac / 2 + 3 + fb + sb) : (db = ya, eb = 0 > W ? F + bc / 2 + fb + sb : F - bc / 2 - 3 - fb - sb);
                                        Oa.translate(db, eb);
                                        f.totals[L] = Oa;
                                        t ? (0 > eb || eb > q) && Oa.remove() : (0 > db || db > p) && Oa.remove()
                                    }
                                }
                            }
                        }
                    }
                }
            }
            this.lastDataItem = v;
            if ("line" == g || "step" == g || "smoothedLine" == g) "smoothedLine" == g ?
                this.drawSmoothedGraph(x, u, T, U) : this.drawLineGraph(x, u, T, U), J || this.launchAnimation();
            this.bulletsHidden && this.hideBullets();
            this.customBulletsHidden && this.hideCustomBullets()
        },
        animateColumns: function(a, b) {
            var c = this,
                e = c.chart.startDuration;
            0 < e && !c.animationPlayed && (c.seqAn ? (a.set.hide(), c.animationArray.push(a), e = setTimeout(function() {
                c.animate.call(c)
            }, e / (c.end - c.start + 1) * (b - c.start) * 1E3), c.timeOuts.push(e)) : c.animate(a), c.chart.animatable.push(a))
        },
        createLabel: function(a, b, c) {
            var e = this.chart,
                h = a.labelColor;
            h || (h = this.color);
            h || (h = e.color);
            c && (h = c);
            c = this.fontSize;
            void 0 === c && (this.fontSize = c = e.fontSize);
            var f = this.labelFunction;
            b = e.formatString(b, a);
            b = d.cleanFromEmpty(b);
            f && (b = f(a, b));
            if (void 0 !== b && "" !== b) return a = d.text(this.container, b, h, e.fontFamily, c), a.node.style.pointerEvents = "none", d.setCN(e, a, this.bcn + "label"), this.bulletSet.push(a), a
        },
        positiveClip: function(a) {
            a.clipRect(this.pmx, this.pmy, this.pmw, this.pmh)
        },
        negativeClip: function(a) {
            a.clipRect(this.nmx, this.nmy, this.nmw, this.nmh)
        },
        drawLineGraph: function(a, b, c, e) {
            var h = this;
            if (1 < a.length) {
                var f = h.noRounding,
                    g = h.set,
                    k = h.chart,
                    l = h.container,
                    m = l.set(),
                    p = l.set();
                g.push(p);
                g.push(m);
                var q = h.lineAlpha,
                    n = h.lineThickness,
                    g = h.fillAlphas,
                    t = h.lineColorR,
                    r = h.negativeLineAlpha;
                isNaN(r) && (r = q);
                var w = h.lineColorSwitched;
                w && (t = w);
                var w = h.fillColorsR,
                    z = h.fillColorsSwitched;
                z && (w = z);
                var x = h.dashLength;
                (z = h.dashLengthSwitched) && (x = z);
                var z = h.negativeLineColor,
                    u = h.negativeFillColors,
                    A = h.negativeFillAlphas,
                    y = h.baseCoord;
                0 !== h.negativeBase && (y = h.valueAxis.getCoordinate(h.negativeBase,
                    f), y > h.height && (y = h.height), 0 > y && (y = 0));
                q = d.line(l, a, b, t, q, n, x, !1, !0, f);
                q.node.setAttribute("stroke-linejoin", "round");
                d.setCN(k, q, h.bcn + "stroke");
                m.push(q);
                m.click(function(a) {
                    h.handleGraphEvent(a, "clickGraph")
                }).mouseover(function(a) {
                    h.handleGraphEvent(a, "rollOverGraph")
                }).mouseout(function(a) {
                    h.handleGraphEvent(a, "rollOutGraph")
                }).touchmove(function(a) {
                    h.chart.handleMouseMove(a)
                }).touchend(function(a) {
                    h.chart.handleTouchEnd(a)
                });
                void 0 === z || h.useNegativeColorIfDown || (n = d.line(l, a, b, z, r, n, x, !1, !0, f), n.node.setAttribute("stroke-linejoin", "round"), d.setCN(k, n, h.bcn + "stroke"), d.setCN(k, n, h.bcn + "stroke-negative"), p.push(n));
                if (0 < g || 0 < A)
                    if (n = a.join(";").split(";"), r = b.join(";").split(";"), q = k.type, "serial" == q || "radar" == q ? 0 < c.length ? (c.reverse(), e.reverse(), n = a.concat(c), r = b.concat(e)) : "radar" == q ? (r.push(0), n.push(0)) : h.rotate ? (r.push(r[r.length - 1]), n.push(y), r.push(r[0]), n.push(y), r.push(r[0]), n.push(n[0])) : (n.push(n[n.length - 1]), r.push(y), n.push(n[0]), r.push(y), n.push(a[0]), r.push(r[0])) : "xy" ==
                        q && (b = h.fillToAxis) && (d.isString(b) && (b = k.getValueAxisById(b)), "H" == b.orientation ? (y = "top" == b.position ? 0 : b.height, n.push(n[n.length - 1]), r.push(y), n.push(n[0]), r.push(y), n.push(a[0]), r.push(r[0])) : (y = "left" == b.position ? 0 : b.width, r.push(r[r.length - 1]), n.push(y), r.push(r[0]), n.push(y), r.push(r[0]), n.push(n[0]))), a = h.gradientRotation, 0 < g && (b = d.polygon(l, n, r, w, g, 1, "#000", 0, a, f), b.pattern(h.pattern, NaN, k.path), d.setCN(k, b, h.bcn + "fill"), m.push(b)), u || void 0 !== z) isNaN(A) && (A = g), u || (u = z), f = d.polygon(l, n, r,
                        u, A, 1, "#000", 0, a, f), d.setCN(k, f, h.bcn + "fill"), d.setCN(k, f, h.bcn + "fill-negative"), f.pattern(h.pattern, NaN, k.path), p.push(f), p.click(function(a) {
                        h.handleGraphEvent(a, "clickGraph")
                    }).mouseover(function(a) {
                        h.handleGraphEvent(a, "rollOverGraph")
                    }).mouseout(function(a) {
                        h.handleGraphEvent(a, "rollOutGraph")
                    }).touchmove(function(a) {
                        h.chart.handleMouseMove(a)
                    }).touchend(function(a) {
                        h.chart.handleTouchEnd(a)
                    });
                h.applyMask(p, m)
            }
        },
        applyMask: function(a, b) {
            var c = a.length();
            "serial" != this.chart.type || this.scrollbar ||
                (this.positiveClip(b), 0 < c && this.negativeClip(a))
        },
        drawSmoothedGraph: function(a, b, c, e) {
            if (1 < a.length) {
                var h = this.set,
                    f = this.chart,
                    g = this.container,
                    k = g.set(),
                    l = g.set();
                h.push(l);
                h.push(k);
                var m = this.lineAlpha,
                    p = this.lineThickness,
                    h = this.dashLength,
                    q = this.fillAlphas,
                    n = this.lineColorR,
                    t = this.fillColorsR,
                    r = this.negativeLineColor,
                    w = this.negativeFillColors,
                    z = this.negativeFillAlphas,
                    x = this.baseCoord,
                    u = this.lineColorSwitched;
                u && (n = u);
                (u = this.fillColorsSwitched) && (t = u);
                var A = this.negativeLineAlpha;
                isNaN(A) &&
                    (A = m);
                u = this.getGradRotation();
                m = new d.Bezier(g, a, b, n, m, p, t, 0, h, void 0, u);
                d.setCN(f, m, this.bcn + "stroke");
                k.push(m.path);
                void 0 !== r && (p = new d.Bezier(g, a, b, r, A, p, t, 0, h, void 0, u), d.setCN(f, p, this.bcn + "stroke"), d.setCN(f, p, this.bcn + "stroke-negative"), l.push(p.path));
                0 < q && (p = a.join(";").split(";"), m = b.join(";").split(";"), n = "", 0 < c.length ? (c.push("M"), e.push("M"), c.reverse(), e.reverse(), p = a.concat(c), m = b.concat(e)) : (this.rotate ? (n += " L" + x + "," + b[b.length - 1], n += " L" + x + "," + b[0]) : (n += " L" + a[a.length - 1] + "," +
                    x, n += " L" + a[0] + "," + x), n += " L" + a[0] + "," + b[0]), a = new d.Bezier(g, p, m, NaN, 0, 0, t, q, h, n, u), d.setCN(f, a, this.bcn + "fill"), a.path.pattern(this.pattern, NaN, f.path), k.push(a.path), w || void 0 !== r) && (z || (z = q), w || (w = r), g = new d.Bezier(g, p, m, NaN, 0, 0, w, z, h, n, u), g.path.pattern(this.pattern, NaN, f.path), d.setCN(f, g, this.bcn + "fill"), d.setCN(f, g, this.bcn + "fill-negative"), l.push(g.path));
                this.applyMask(l, k)
            }
        },
        launchAnimation: function() {
            var a = this,
                b = a.chart.startDuration;
            if (0 < b && !a.animationPlayed) {
                var c = a.set,
                    e = a.bulletSet;
                d.VML || (c.attr({
                    opacity: a.startAlpha
                }), e.attr({
                    opacity: a.startAlpha
                }));
                c.hide();
                e.hide();
                a.seqAn ? (b = setTimeout(function() {
                    a.animateGraphs.call(a)
                }, a.index * b * 1E3), a.timeOuts.push(b)) : a.animateGraphs()
            }
        },
        animateGraphs: function() {
            var a = this.chart,
                b = this.set,
                c = this.bulletSet,
                e = this.x,
                d = this.y;
            b.show();
            c.show();
            var f = a.startDuration,
                g = a.startEffect;
            b && (this.rotate ? (b.translate(-1E3, d), c.translate(-1E3, d)) : (b.translate(e, -1E3), c.translate(e, -1E3)), b.animate({
                opacity: 1,
                translate: e + "," + d
            }, f, g), c.animate({
                opacity: 1,
                translate: e + "," + d
            }, f, g), a.animatable.push(b))
        },
        animate: function(a) {
            var b = this.chart,
                c = this.animationArray;
            !a && 0 < c.length && (a = c[0], c.shift());
            c = d[d.getEffect(b.startEffect)];
            b = b.startDuration;
            a && (this.rotate ? a.animateWidth(b, c) : a.animateHeight(b, c), a.set.show())
        },
        legendKeyColor: function() {
            var a = this.legendColor,
                b = this.lineAlpha;
            void 0 === a && (a = this.lineColorR, 0 === b && (b = this.fillColorsR) && (a = "object" == typeof b ? b[0] : b));
            return a
        },
        legendKeyAlpha: function() {
            var a = this.legendAlpha;
            void 0 === a && (a = this.lineAlpha,
                this.fillAlphas > a && (a = this.fillAlphas), 0 === a && (a = this.bulletAlpha), 0 === a && (a = 1));
            return a
        },
        createBullet: function(a, b, c) {
            if (!isNaN(b) && !isNaN(c) && ("none" != this.bullet || this.customBullet || a.bullet || a.customBullet)) {
                var e = this.chart,
                    h = this.container,
                    f = this.bulletOffset,
                    g = this.bulletSize;
                isNaN(a.bulletSize) || (g = a.bulletSize);
                var k = a.values.value,
                    l = this.maxValue,
                    m = this.minValue,
                    p = this.maxBulletSize,
                    q = this.minBulletSize;
                isNaN(l) || (isNaN(k) || (g = (k - m) / (l - m) * (p - q) + q), m == l && (g = p));
                l = g;
                this.bulletAxis && (g = a.values.error,
                    isNaN(g) || (k = g), g = this.bulletAxis.stepWidth * k);
                g < this.minBulletSize && (g = this.minBulletSize);
                this.rotate ? b = a.isNegative ? b - f : b + f : c = a.isNegative ? c + f : c - f;
                q = this.bulletColorR;
                a.lineColor && void 0 === this.bulletColor && (this.bulletColorSwitched = a.lineColor);
                this.bulletColorSwitched && (q = this.bulletColorSwitched);
                a.isNegative && void 0 !== this.bulletColorNegative && (q = this.bulletColorNegative);
                void 0 !== a.color && (q = a.color);
                var n;
                "xy" == e.type && this.valueField && (n = this.pattern, a.pattern && (n = a.pattern));
                f = this.bullet;
                a.bullet && (f = a.bullet);
                var k = this.bulletBorderThickness,
                    m = this.bulletBorderColorR,
                    p = this.bulletBorderAlpha,
                    t = this.bulletAlpha;
                m || (m = q);
                this.useLineColorForBulletBorder && (m = this.lineColorR, a.isNegative && this.negativeLineColor && (m = this.negativeLineColor), this.lineColorSwitched && (m = this.lineColorSwitched));
                var r = a.alpha;
                isNaN(r) || (t = r);
                n = d.bullet(h, f, g, q, t, k, m, p, l, 0, n, e.path);
                l = this.customBullet;
                a.customBullet && (l = a.customBullet);
                l && (n && n.remove(), "function" == typeof l ? (l = new l, l.chart = e, a.bulletConfig &&
                    (l.availableSpace = c, l.graph = this, l.graphDataItem = a, l.bulletY = c, a.bulletConfig.minCoord = this.minCoord - c, l.bulletConfig = a.bulletConfig), l.write(h), n && l.showBullet && l.set.push(n), a.customBulletGraphics = l.cset, n = l.set) : (n = h.set(), l = h.image(l, 0, 0, g, g), n.push(l), this.centerCustomBullets && l.translate(-g / 2, -g / 2)));
                if (n) {
                    (a.url || this.showHandOnHover) && n.setAttr("cursor", "pointer");
                    if ("serial" == e.type || "gantt" == e.type)
                        if (-.5 > b || b > this.width || c < -g / 2 || c > this.height) n.remove(), n = null;
                    n && (this.bulletSet.push(n),
                        n.translate(b, c), this.addListeners(n, a), this.allBullets.push(n));
                    a.bx = b;
                    a.by = c;
                    d.setCN(e, n, this.bcn + "bullet");
                    a.className && d.setCN(e, n, a.className, !0)
                }
                if (n) {
                    n.size = g || 0;
                    if (e = this.bulletHitAreaSize) h = d.circle(h, e, "#FFFFFF", .001, 0), h.translate(b, c), a.hitBullet = h, this.bulletSet.push(h), this.addListeners(h, a);
                    a.bulletGraphics = n;
                    void 0 !== this.tabIndex && n.setAttr("tabindex", this.tabIndex)
                } else n = {
                    size: 0
                };
                n.graphDataItem = a;
                return n
            }
        },
        showBullets: function() {
            var a = this.allBullets,
                b;
            this.bulletsHidden = !1;
            for (b =
                0; b < a.length; b++) a[b].show()
        },
        hideBullets: function() {
            var a = this.allBullets,
                b;
            this.bulletsHidden = !0;
            for (b = 0; b < a.length; b++) a[b].hide()
        },
        showCustomBullets: function() {
            var a = this.allBullets,
                b;
            this.customBulletsHidden = !1;
            for (b = 0; b < a.length; b++) {
                var c = a[b].graphDataItem;
                c && c.customBulletGraphics && c.customBulletGraphics.show()
            }
        },
        hideCustomBullets: function() {
            var a = this.allBullets,
                b;
            this.customBulletsHidden = !0;
            for (b = 0; b < a.length; b++) {
                var c = a[b].graphDataItem;
                c && c.customBulletGraphics && c.customBulletGraphics.hide()
            }
        },
        addListeners: function(a, b) {
            var c = this;
            a.mouseover(function(a) {
                c.handleRollOver(b, a)
            }).mouseout(function(a) {
                c.handleRollOut(b, a)
            }).touchend(function(a) {
                c.handleRollOver(b, a);
                c.chart.panEventsEnabled && c.handleClick(b, a)
            }).touchstart(function(a) {
                c.handleRollOver(b, a)
            }).click(function(a) {
                c.handleClick(b, a)
            }).dblclick(function(a) {
                c.handleDoubleClick(b, a)
            }).contextmenu(function(a) {
                c.handleRightClick(b, a)
            });
            var e = c.chart;
            if (e.accessible && c.accessibleLabel) {
                var d = e.formatString(c.accessibleLabel, b);
                e.makeAccessible(a,
                    d)
            }
        },
        handleRollOver: function(a, b) {
            this.handleGraphEvent(b, "rollOverGraph");
            if (a) {
                var c = this.chart;
                a.bulletConfig && (c.isRolledOverBullet = !0);
                var e = {
                    type: "rollOverGraphItem",
                    item: a,
                    index: a.index,
                    graph: this,
                    target: this,
                    chart: this.chart,
                    event: b
                };
                this.fire(e);
                c.fire(e);
                clearTimeout(c.hoverInt);
                (c = c.chartCursor) && c.valueBalloonsEnabled || this.showGraphBalloon(a, "V", !0)
            }
        },
        handleRollOut: function(a, b) {
            var c = this.chart;
            if (a) {
                var e = {
                    type: "rollOutGraphItem",
                    item: a,
                    index: a.index,
                    graph: this,
                    target: this,
                    chart: this.chart,
                    event: b
                };
                this.fire(e);
                c.fire(e);
                c.isRolledOverBullet = !1
            }
            this.handleGraphEvent(b, "rollOutGraph");
            (c = c.chartCursor) && c.valueBalloonsEnabled || this.hideBalloon()
        },
        handleClick: function(a, b) {
            if (!this.chart.checkTouchMoved() && this.chart.checkTouchDuration(b)) {
                if (a) {
                    var c = {
                        type: "clickGraphItem",
                        item: a,
                        index: a.index,
                        graph: this,
                        target: this,
                        chart: this.chart,
                        event: b
                    };
                    this.fire(c);
                    this.chart.fire(c);
                    d.getURL(a.url, this.urlTarget)
                }
                this.handleGraphEvent(b, "clickGraph")
            }
        },
        handleGraphEvent: function(a, b) {
            var c = {
                type: b,
                graph: this,
                target: this,
                chart: this.chart,
                event: a
            };
            this.fire(c);
            this.chart.fire(c)
        },
        handleRightClick: function(a, b) {
            if (a) {
                var c = {
                    type: "rightClickGraphItem",
                    item: a,
                    index: a.index,
                    graph: this,
                    target: this,
                    chart: this.chart,
                    event: b
                };
                this.fire(c);
                this.chart.fire(c)
            }
        },
        handleDoubleClick: function(a, b) {
            if (a) {
                var c = {
                    type: "doubleClickGraphItem",
                    item: a,
                    index: a.index,
                    graph: this,
                    target: this,
                    chart: this.chart,
                    event: b
                };
                this.fire(c);
                this.chart.fire(c)
            }
        },
        zoom: function(a, b) {
            this.start = a;
            this.end = b;
            this.draw()
        },
        changeOpacity: function(a) {
            var b =
                this.set;
            b && b.setAttr("opacity", a);
            if (b = this.ownColumns) {
                var c;
                for (c = 0; c < b.length; c++) {
                    var e = b[c].set;
                    e && e.setAttr("opacity", a)
                }
            }(b = this.bulletSet) && b.setAttr("opacity", a)
        },
        destroy: function() {
            d.remove(this.set);
            d.remove(this.bulletSet);
            var a = this.timeOuts;
            if (a) {
                var b;
                for (b = 0; b < a.length; b++) clearTimeout(a[b])
            }
            this.timeOuts = []
        },
        createBalloon: function() {
            var a = this.chart;
            this.balloon ? this.balloon.destroy && this.balloon.destroy() : this.balloon = {};
            var b = this.balloon;
            d.extend(b, a.balloon, !0);
            b.chart = a;
            b.mainSet =
                a.plotBalloonsSet;
            b.className = this.id
        },
        hideBalloon: function() {
            var a = this,
                b = a.chart;
            b.chartCursor ? b.chartCursor.valueBalloonsEnabled || b.hideBalloon() : b.hideBalloon();
            clearTimeout(a.hoverInt);
            a.hoverInt = setTimeout(function() {
                a.hideBalloonReal.call(a)
            }, b.hideBalloonTime)
        },
        hideBalloonReal: function() {
            this.balloon && this.balloon.hide();
            this.fixBulletSize()
        },
        fixBulletSize: function() {
            if (d.isModern) {
                var a = this.resizedDItem;
                if (a) {
                    var b = a.bulletGraphics;
                    if (b && !b.doNotScale) {
                        b.translate(a.bx, a.by, 1);
                        var c = this.bulletAlpha;
                        isNaN(a.alpha) || (c = a.alpha);
                        b.setAttr("fill-opacity", c);
                        b.setAttr("stroke-opacity", this.bulletBorderAlpha)
                    }
                }
                this.resizedDItem = null
            }
        },
        showGraphBalloon: function(a, b, c, e, h) {
            if (a) {
                var f = this.chart,
                    g = this.balloon,
                    k = 0,
                    l = 0,
                    m = f.chartCursor,
                    p = !0;
                m ? m.valueBalloonsEnabled || (g = f.balloon, k = this.x, l = this.y, p = !1) : (g = f.balloon, k = this.x, l = this.y, p = !1);
                clearTimeout(this.hoverInt);
                if (f.chartCursor && (this.currentDataItem = a, "serial" == f.type && f.isRolledOverBullet && f.chartCursor.valueBalloonsEnabled)) {
                    this.hideBalloonReal();
                    return
                }
                this.resizeBullet(a, e, h);
                if (g && g.enabled && this.showBalloon && !this.hidden) {
                    var m = f.formatString(this.balloonText, a, !0),
                        q = this.balloonFunction;
                    q && (m = q(a, a.graph));
                    m && (m = d.cleanFromEmpty(m));
                    m && "" !== m ? (e = f.getBalloonColor(this, a), g.drop || (g.pointerOrientation = b), b = a.x, h = a.y, f.rotate && (b = a.y, h = a.x), b += k, h += l, isNaN(b) || isNaN(h) ? this.hideBalloonReal() : (a = this.width, q = this.height, p && g.setBounds(k, l, a + k, q + l), g.changeColor(e), g.setPosition(b, h), g.fixPrevious(), g.fixedPosition && (c = !1), !c && "radar" != f.type &&
                        (b < k || b > a + k || h < l - .5 || h > q + l) ? (g.showBalloon(m), g.hide(0)) : (g.followCursor(c), g.showBalloon(m)))) : (this.hideBalloonReal(), g.hide(), this.resizeBullet(a, e, h))
                } else this.hideBalloonReal()
            }
        },
        resizeBullet: function(a, b, c) {
            this.fixBulletSize();
            if (a && d.isModern && (1 != b || !isNaN(c))) {
                var e = a.bulletGraphics;
                e && !e.doNotScale && (e.translate(a.bx, a.by, b), isNaN(c) || (e.setAttr("fill-opacity", c), e.setAttr("stroke-opacity", c)), this.resizedDItem = a)
            }
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.ChartCursor = d.Class({
        construct: function(a) {
            this.cname = "ChartCursor";
            this.createEvents("changed", "zoomed", "onHideCursor", "onShowCursor", "draw", "selected", "moved", "panning", "zoomStarted");
            this.enabled = !0;
            this.cursorAlpha = 1;
            this.selectionAlpha = .2;
            this.cursorColor = "#CC0000";
            this.categoryBalloonAlpha = 1;
            this.color = "#FFFFFF";
            this.type = "cursor";
            this.zoomed = !1;
            this.zoomable = !0;
            this.pan = !1;
            this.categoryBalloonDateFormat = "MMM DD, YYYY";
            this.categoryBalloonText = "[[category]]";
            this.categoryBalloonEnabled = this.valueBalloonsEnabled = !0;
            this.rolledOver = !1;
            this.cursorPosition = "middle";
            this.bulletsEnabled = this.skipZoomDispatch = !1;
            this.bulletSize = 8;
            this.selectWithoutZooming = this.oneBalloonOnly = !1;
            this.graphBulletSize = 1.7;
            this.animationDuration = .3;
            this.zooming = !1;
            this.adjustment = 0;
            this.avoidBalloonOverlapping = !0;
            this.leaveCursor = !1;
            this.leaveAfterTouch = !0;
            this.valueZoomable = !1;
            this.balloonPointerOrientation = "horizontal";
            this.hLineEnabled = this.vLineEnabled = !0;
            this.vZoomEnabled =
                this.hZoomEnabled = !1;
            d.applyTheme(this, a, this.cname)
        },
        draw: function() {
            this.destroy();
            var a = this.chart;
            a.panRequired = !0;
            var b = a.container;
            this.rotate = a.rotate;
            this.container = b;
            this.prevLineHeight = this.prevLineWidth = NaN;
            b = b.set();
            b.translate(this.x, this.y);
            this.set = b;
            a.cursorSet.push(b);
            this.createElements();
            d.isString(this.limitToGraph) && (this.limitToGraph = d.getObjById(a.graphs, this.limitToGraph), this.fullWidth = !1, this.cursorPosition = "middle");
            this.pointer = this.balloonPointerOrientation.substr(0, 1).toUpperCase();
            this.isHidden = !1;
            this.hideLines();
            this.valueLineAxis || (this.valueLineAxis = a.valueAxes[0])
        },
        createElements: function() {
            var a = this,
                b = a.chart,
                c = b.dx,
                e = b.dy,
                h = a.width,
                f = a.height,
                g, k, l = a.cursorAlpha,
                m = a.valueLineAlpha;
            a.rotate ? (g = m, k = l) : (k = m, g = l);
            "xy" == b.type && (k = l, void 0 !== m && (k = m), g = l);
            a.vvLine = d.line(a.container, [c, 0, 0], [e, 0, f], a.cursorColor, g, 1);
            d.setCN(b, a.vvLine, "cursor-line");
            d.setCN(b, a.vvLine, "cursor-line-vertical");
            a.hhLine = d.line(a.container, [0, h, h + c], [0, 0, e], a.cursorColor, k, 1);
            d.setCN(b, a.hhLine,
                "cursor-line");
            d.setCN(b, a.hhLine, "cursor-line-horizontal");
            a.vLine = a.rotate ? a.vvLine : a.hhLine;
            a.set.push(a.vvLine);
            a.set.push(a.hhLine);
            a.set.node.style.pointerEvents = "none";
            a.fullLines = a.container.set();
            b = b.cursorLineSet;
            b.push(a.fullLines);
            b.translate(a.x, a.y);
            b.clipRect(-1, -1, h + 2, f + 2);
            void 0 !== a.tabIndex && (b.setAttr("tabindex", a.tabIndex), b.keyup(function(b) {
                a.handleKeys(b)
            }).focus(function(b) {
                a.showCursor()
            }).blur(function(b) {
                a.hideCursor()
            }));
            a.set.clipRect(0, 0, h, f)
        },
        handleKeys: function(a) {
            var b =
                this.prevIndex,
                c = this.chart;
            if (c) {
                var e = c.chartData;
                e && (isNaN(b) && (b = e.length - 1), 37 != a.keyCode && 40 != a.keyCode || b--, 39 != a.keyCode && 38 != a.keyCode || b++, b = d.fitToBounds(b, c.startIndex, c.endIndex), (a = this.chart.chartData[b]) && this.setPosition(a.x.categoryAxis), this.prevIndex = b)
            }
        },
        update: function() {
            var a = this.chart;
            if (a) {
                var b = a.mouseX - this.x,
                    c = a.mouseY - this.y;
                this.mouseX = b;
                this.mouseY = c;
                this.mouse2X = a.mouse2X - this.x;
                this.mouse2Y = a.mouse2Y - this.y;
                var e;
                if (a.chartData && 0 < a.chartData.length) {
                    this.mouseIsOver() ?
                        (this.hideGraphBalloons = !1, this.rolledOver = e = !0, this.updateDrawing(), this.vvLine && isNaN(this.fx) && (a.rotate || !this.limitToGraph) && this.vvLine.translate(b, 0), !this.hhLine || !isNaN(this.fy) || a.rotate && this.limitToGraph || this.hhLine.translate(0, c), isNaN(this.mouse2X) ? this.dispatchMovedEvent(b, c) : e = !1) : this.forceShow || this.hideCursor();
                    if (this.zooming) {
                        if (!isNaN(this.mouse2X)) {
                            isNaN(this.mouse2X0) || this.dispatchPanEvent();
                            return
                        }
                        if (this.pan) {
                            this.dispatchPanEvent();
                            return
                        }(this.hZoomEnabled || this.vZoomEnabled) &&
                        this.zooming && this.updateSelection()
                    }
                    e && this.showCursor()
                }
            }
        },
        updateDrawing: function() {
            this.drawing && this.chart.setMouseCursor("crosshair");
            if (this.drawingNow && (d.remove(this.drawingLine), 1 < Math.abs(this.drawStartX - this.mouseX) || 1 < Math.abs(this.drawStartY - this.mouseY))) {
                var a = this.chart,
                    b = a.marginTop,
                    a = a.marginLeft;
                this.drawingLine = d.line(this.container, [this.drawStartX + a, this.mouseX + a], [this.drawStartY + b, this.mouseY + b], this.cursorColor, 1, 1)
            }
        },
        fixWidth: function(a) {
            if (this.fullWidth && this.prevLineWidth !=
                a) {
                var b = this.vvLine,
                    c = 0;
                b && (b.remove(), c = b.x);
                b = this.container.set();
                b.translate(c, 0);
                c = d.rect(this.container, a, this.height, this.cursorColor, this.cursorAlpha, this.cursorAlpha, this.cursorColor);
                d.setCN(this.chart, c, "cursor-fill");
                c.translate(-a / 2 - 1, 0);
                b.push(c);
                this.vvLine = b;
                this.fullLines.push(b);
                this.prevLineWidth = a
            }
        },
        fixHeight: function(a) {
            if (this.fullWidth && this.prevLineHeight != a) {
                var b = this.hhLine,
                    c = 0;
                b && (b.remove(), c = b.y);
                b = this.container.set();
                b.translate(0, c);
                c = d.rect(this.container, this.width,
                    a, this.cursorColor, this.cursorAlpha);
                c.translate(0, -a / 2);
                b.push(c);
                this.fullLines.push(b);
                this.hhLine = b;
                this.prevLineHeight = a
            }
        },
        fixVLine: function(a, b) {
            if (!isNaN(a)) {
                if (isNaN(this.prevLineX)) {
                    var c = 0,
                        e = this.mouseX;
                    if (this.limitToGraph) {
                        var d = this.chart.categoryAxis;
                        d && (this.chart.rotate || (c = "bottom" == d.position ? this.height : -this.height), e = a)
                    }
                    this.vvLine.translate(e, c)
                } else this.prevLineX != a && this.vvLine.translate(this.prevLineX, this.prevLineY);
                this.fx = a;
                this.prevLineX != a && (c = this.animationDuration,
                    this.zooming && (c = 0), this.vvLine.stop(), this.vvLine.animate({
                        translate: a + "," + b
                    }, c, "easeOutSine"), this.prevLineX = a, this.prevLineY = b)
            }
        },
        fixHLine: function(a, b) {
            if (!isNaN(a)) {
                if (isNaN(this.prevLineY)) {
                    var c = 0,
                        e = this.mouseY;
                    if (this.limitToGraph) {
                        var d = this.chart.categoryAxis;
                        d && (this.chart.rotate && (c = "right" == d.position ? this.width : -this.width), e = a)
                    }
                    this.hhLine.translate(c, e)
                } else this.prevLineY != a && this.hhLine.translate(this.prevLineX, this.prevLineY);
                this.fy = a;
                this.prevLineY != a && (c = this.animationDuration,
                    this.zooming && (c = 0), this.hhLine.stop(), this.hhLine.animate({
                        translate: b + "," + a
                    }, c, "easeOutSine"), this.prevLineY = a, this.prevLineX = b)
            }
        },
        hideCursor: function(a) {
            this.forceShow = !1;
            this.chart.wasTouched && this.leaveAfterTouch || this.isHidden || this.leaveCursor || (this.hideCursorReal(), a ? this.chart.handleCursorHide() : this.fire({
                target: this,
                chart: this.chart,
                type: "onHideCursor"
            }), this.chart.setMouseCursor("auto"))
        },
        hideCursorReal: function() {
            this.hideLines();
            this.isHidden = !0;
            this.index = this.prevLineY = this.prevLineX =
                this.mouseY0 = this.mouseX0 = this.fy = this.fx = NaN
        },
        hideLines: function() {
            this.vvLine && this.vvLine.hide();
            this.hhLine && this.hhLine.hide();
            this.fullLines && this.fullLines.hide();
            this.isHidden = !0;
            this.chart.handleCursorHide()
        },
        showCursor: function(a) {
            !this.drawing && this.enabled && (this.vLineEnabled && this.vvLine && this.vvLine.show(), this.hLineEnabled && this.hhLine && this.hhLine.show(), this.isHidden = !1, this.updateFullLine(), a || this.fire({
                target: this,
                chart: this.chart,
                type: "onShowCursor"
            }), this.pan && this.chart.setMouseCursor("move"))
        },
        updateFullLine: function() {
            this.zooming && this.fullWidth && this.selection && (this.rotate ? 0 < this.selection.height && this.hhLine.hide() : 0 < this.selection.width && this.vvLine.hide())
        },
        updateSelection: function() {
            if (!this.pan && this.enabled) {
                var a = this.mouseX,
                    b = this.mouseY;
                isNaN(this.fx) || (a = this.fx);
                isNaN(this.fy) || (b = this.fy);
                this.clearSelection();
                var c = this.mouseX0,
                    e = this.mouseY0,
                    h = this.width,
                    f = this.height,
                    a = d.fitToBounds(a, 0, h),
                    b = d.fitToBounds(b, 0, f),
                    g;
                a < c && (g = a, a = c, c = g);
                b < e && (g = b, b = e, e = g);
                this.hZoomEnabled ?
                    h = a - c : c = 0;
                this.vZoomEnabled ? f = b - e : e = 0;
                isNaN(this.mouse2X) && 0 < Math.abs(h) && 0 < Math.abs(f) && (a = this.chart, b = d.rect(this.container, h, f, this.cursorColor, this.selectionAlpha), d.setCN(a, b, "cursor-selection"), b.width = h, b.height = f, b.translate(c, e), this.set.push(b), this.selection = b);
                this.updateFullLine()
            }
        },
        mouseIsOver: function() {
            var a = this.mouseX,
                b = this.mouseY;
            if (this.justReleased) return this.justReleased = !1, !0;
            if (this.mouseIsDown) return !0;
            if (!this.chart.mouseIsOver) return this.handleMouseOut(), !1;
            if (0 < a &&
                a < this.width && 0 < b && b < this.height) return !0;
            this.handleMouseOut()
        },
        fixPosition: function() {
            this.prevY = this.prevX = NaN
        },
        handleMouseDown: function() {
            this.update();
            if (this.mouseIsOver())
                if (this.mouseIsDown = !0, this.mouseX0 = this.mouseX, this.mouseY0 = this.mouseY, this.mouse2X0 = this.mouse2X, this.mouse2Y0 = this.mouse2Y, this.drawing) this.drawStartY = this.mouseY, this.drawStartX = this.mouseX, this.drawingNow = !0;
                else if (this.dispatchMovedEvent(this.mouseX, this.mouseY), !this.pan && isNaN(this.mouse2X0) && (isNaN(this.fx) || (this.mouseX0 =
                    this.fx), isNaN(this.fy) || (this.mouseY0 = this.fy)), this.hZoomEnabled || this.vZoomEnabled) {
                this.zooming = !0;
                var a = {
                    chart: this.chart,
                    target: this,
                    type: "zoomStarted"
                };
                a.x = this.mouseX / this.width;
                a.y = this.mouseY / this.height;
                this.index0 = a.index = this.index;
                this.timestamp0 = this.timestamp;
                this.fire(a)
            }
        },
        registerInitialMouse: function() {},
        handleReleaseOutside: function() {
            this.mouseIsDown = !1;
            if (this.drawingNow) {
                this.drawingNow = !1;
                d.remove(this.drawingLine);
                var a = this.drawStartX,
                    b = this.drawStartY,
                    c = this.mouseX,
                    e = this.mouseY,
                    h = this.chart;
                (2 < Math.abs(a - c) || 2 < Math.abs(b - e)) && this.fire({
                    type: "draw",
                    target: this,
                    chart: h,
                    initialX: a,
                    initialY: b,
                    finalX: c,
                    finalY: e
                })
            }
            this.zooming && (this.zooming = !1, this.selectWithoutZooming ? this.dispatchZoomEvent("selected") : (this.hZoomEnabled || this.vZoomEnabled) && this.dispatchZoomEvent("zoomed"), this.rolledOver && this.dispatchMovedEvent(this.mouseX, this.mouseY));
            this.mouse2Y0 = this.mouse2X0 = this.mouseY0 = this.mouseX0 = NaN
        },
        dispatchZoomEvent: function(a) {
            if (!this.pan) {
                var b = this.selection;
                if (b && 3 < Math.abs(b.width) &&
                    3 < Math.abs(b.height)) {
                    var c = Math.min(this.index, this.index0),
                        e = Math.max(this.index, this.index0),
                        d = c,
                        f = e,
                        g = this.chart,
                        k = g.chartData,
                        l = g.categoryAxis;
                    l && l.parseDates && !l.equalSpacing && (d = k[c] ? k[c].time : Math.min(this.timestamp0, this.timestamp), f = k[e] ? g.getEndTime(k[e].time) : Math.max(this.timestamp0, this.timestamp));
                    var b = {
                            type: a,
                            chart: this.chart,
                            target: this,
                            end: f,
                            start: d,
                            startIndex: c,
                            endIndex: e,
                            selectionHeight: b.height,
                            selectionWidth: b.width,
                            selectionY: b.y,
                            selectionX: b.x
                        },
                        m;
                    this.hZoomEnabled && 4 < Math.abs(this.mouseX0 -
                        this.mouseX) && (b.startX = this.mouseX0 / this.width, b.endX = this.mouseX / this.width, m = !0);
                    this.vZoomEnabled && 4 < Math.abs(this.mouseY0 - this.mouseY) && (b.startY = 1 - this.mouseY0 / this.height, b.endY = 1 - this.mouseY / this.height, m = !0);
                    m && (this.prevY = this.prevX = NaN, this.fire(b), "selected" != a && this.clearSelection());
                    this.hideCursor()
                }
            }
        },
        dispatchMovedEvent: function(a, b, c, e) {
            a = Math.round(a);
            b = Math.round(b);
            if (!this.isHidden && (a != this.prevX || b != this.prevY || "changed" == c)) {
                c || (c = "moved");
                var d = this.fx,
                    f = this.fy;
                isNaN(d) &&
                    (d = a);
                isNaN(f) && (f = b);
                var g = !1;
                this.zooming && this.pan && (g = !0);
                g = {
                    hidden: this.isHidden,
                    type: c,
                    chart: this.chart,
                    target: this,
                    x: a,
                    y: b,
                    finalX: d,
                    finalY: f,
                    zooming: this.zooming,
                    panning: g,
                    mostCloseGraph: this.mostCloseGraph,
                    index: this.index,
                    skip: e,
                    hideBalloons: this.hideGraphBalloons
                };
                this.prevIndex = this.index;
                this.rotate ? (g.position = b, g.finalPosition = f) : (g.position = a, g.finalPosition = d);
                this.prevX = a;
                this.prevY = b;
                e ? this.chart.handleCursorMove(g) : (this.fire(g), "changed" == c && this.chart.fire(g))
            }
        },
        dispatchPanEvent: function() {
            if (this.mouseIsDown) {
                var a =
                    d.roundTo((this.mouseX - this.mouseX0) / this.width, 3),
                    b = d.roundTo((this.mouseY - this.mouseY0) / this.height, 3),
                    c = d.roundTo((this.mouse2X - this.mouse2X0) / this.width, 3),
                    e = d.roundTo((this.mouse2Y - this.mouse2Y0) / this.height, 2),
                    h = !1;
                0 !== Math.abs(a) && 0 !== Math.abs(b) && (h = !0);
                if (this.prevDeltaX == a || this.prevDeltaY == b) h = !1;
                isNaN(c) || isNaN(e) || (0 !== Math.abs(c) && 0 !== Math.abs(e) && (h = !0), this.prevDelta2X != c && this.prevDelta2Y != e) || (h = !1);
                h && (this.hideLines(), this.fire({
                    type: "panning",
                    chart: this.chart,
                    target: this,
                    deltaX: a,
                    deltaY: b,
                    delta2X: c,
                    delta2Y: e,
                    index: this.index
                }), this.prevDeltaX = a, this.prevDeltaY = b, this.prevDelta2X = c, this.prevDelta2Y = e)
            }
        },
        clearSelection: function() {
            var a = this.selection;
            a && (a.width = 0, a.height = 0, a.remove())
        },
        destroy: function() {
            this.clear();
            d.remove(this.selection);
            this.selection = null;
            clearTimeout(this.syncTO);
            d.remove(this.set)
        },
        clear: function() {},
        setTimestamp: function(a) {
            this.timestamp = a
        },
        setIndex: function(a, b) {
            a != this.index && (this.index = a, b || this.isHidden || this.dispatchMovedEvent(this.mouseX,
                this.mouseY, "changed"))
        },
        handleMouseOut: function() {
            this.enabled && this.rolledOver && (this.leaveCursor || this.setIndex(void 0), this.forceShow = !1, this.hideCursor(), this.rolledOver = !1)
        },
        showCursorAt: function(a) {
            var b = this.chart.categoryAxis;
            b && this.setPosition(b.categoryToCoordinate(a), a)
        },
        setPosition: function(a, b) {
            var c = this.chart,
                e = c.categoryAxis;
            if (e) {
                var d, f;
                void 0 === b && (b = e.coordinateToValue(a));
                e.showBalloonAt(b, a);
                this.forceShow = !0;
                e.stickBalloonToCategory ? c.rotate ? this.fixHLine(a, 0) : this.fixVLine(a,
                    0) : (this.showCursor(), c.rotate ? this.hhLine.translate(0, a) : this.vvLine.translate(a, 0));
                c.rotate ? d = a : f = a;
                c.rotate ? (this.vvLine && this.vvLine.hide(), this.hhLine && this.hhLine.show()) : (this.hhLine && this.hhLine.hide(), this.vvLine && this.vvLine.show());
                this.updateFullLine();
                this.isHidden = !1;
                this.dispatchMovedEvent(f, d, "moved", !0)
            }
        },
        enableDrawing: function(a) {
            this.enabled = !a;
            this.hideCursor();
            this.drawing = a
        },
        syncWithCursor: function(a, b) {
            clearTimeout(this.syncTO);
            a && (a.isHidden ? this.hideCursor(!0) : this.syncWithCursorReal(a,
                b))
        },
        isZooming: function(a) {
            this.zooming = a
        },
        syncWithCursorReal: function(a, b) {
            var c = a.vvLine,
                e = a.hhLine;
            this.index = a.index;
            this.forceShow = !0;
            this.zooming && this.pan || this.showCursor(!0);
            this.hideGraphBalloons = b;
            this.justReleased = a.justReleased;
            this.zooming = a.zooming;
            this.index0 = a.index0;
            this.mouseX0 = a.mouseX0;
            this.mouseY0 = a.mouseY0;
            this.mouse2X0 = a.mouse2X0;
            this.mouse2Y0 = a.mouse2Y0;
            this.timestamp0 = a.timestamp0;
            this.prevDeltaX = a.prevDeltaX;
            this.prevDeltaY = a.prevDeltaY;
            this.prevDelta2X = a.prevDelta2X;
            this.prevDelta2Y =
                a.prevDelta2Y;
            this.fx = a.fx;
            this.fy = a.fy;
            a.zooming && this.updateSelection();
            var d = a.mouseX,
                f = a.mouseY;
            this.rotate ? (d = NaN, this.vvLine && this.vvLine.hide(), this.hhLine && e && (isNaN(a.fy) ? this.hhLine.translate(0, a.mouseY) : this.fixHLine(a.fy, 0))) : (f = NaN, this.hhLine && this.hhLine.hide(), this.vvLine && c && (isNaN(a.fx) ? this.vvLine.translate(a.mouseX, 0) : this.fixVLine(a.fx, 0)));
            this.dispatchMovedEvent(d, f, "moved", !0)
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.SimpleChartScrollbar = d.Class({
        construct: function(a) {
            this.createEvents("zoomed", "zoomStarted", "zoomEnded");
            this.backgroundColor = "#D4D4D4";
            this.backgroundAlpha = 1;
            this.selectedBackgroundColor = "#EFEFEF";
            this.scrollDuration = this.selectedBackgroundAlpha = 1;
            this.resizeEnabled = !0;
            this.hideResizeGrips = !1;
            this.scrollbarHeight = 20;
            this.updateOnReleaseOnly = !1;
            9 > document.documentMode && (this.updateOnReleaseOnly = !0);
            this.dragIconHeight = this.dragIconWidth = 35;
            this.dragIcon = "dragIconRoundBig";
            this.dragCursorHover = "cursor: move; cursor: grab; cursor: -moz-grab; cursor: -webkit-grab;";
            this.dragCursorDown = "cursor: move; cursor: grab; cursor: -moz-grabbing; cursor: -webkit-grabbing;";
            this.vResizeCursor = "ns-resize";
            this.hResizeCursor = "ew-resize";
            this.enabled = !0;
            this.percentStart = this.offset = 0;
            this.percentEnd = 1;
            d.applyTheme(this, a, "SimpleChartScrollbar")
        },
        getPercents: function() {
            var a = this.getDBox(),
                b = a.x,
                c = a.y,
                e = a.width,
                a = a.height;
            this.rotate ? (b = 1 - c / this.height, c = 1 - (c + a) / this.height) : (c = b / this.width,
                b = (b + e) / this.width);
            this.percentStart = c;
            this.percentEnd = b
        },
        draw: function() {
            var a = this;
            a.destroy();
            if (a.enabled) {
                var b = a.chart.container,
                    c = a.rotate,
                    e = a.chart;
                e.panRequired = !0;
                var h = b.set();
                a.set = h;
                c ? d.setCN(e, h, "scrollbar-vertical") : d.setCN(e, h, "scrollbar-horizontal");
                e.scrollbarsSet.push(h);
                var f, g;
                c ? (f = a.scrollbarHeight, g = e.plotAreaHeight) : (g = a.scrollbarHeight, f = e.plotAreaWidth);
                a.width = f;
                if ((a.height = g) && f) {
                    var k = d.rect(b, f, g, a.backgroundColor, a.backgroundAlpha, 1, a.backgroundColor, a.backgroundAlpha);
                    d.setCN(e, k, "scrollbar-bg");
                    a.bg = k;
                    h.push(k);
                    k = d.rect(b, f, g, "#000", .005);
                    h.push(k);
                    a.invisibleBg = k;
                    k.click(function() {
                        a.handleBgClick()
                    }).mouseover(function() {
                        a.handleMouseOver()
                    }).mouseout(function() {
                        a.handleMouseOut()
                    }).touchend(function() {
                        a.handleBgClick()
                    });
                    k = d.rect(b, f, g, a.selectedBackgroundColor, a.selectedBackgroundAlpha);
                    d.setCN(e, k, "scrollbar-bg-selected");
                    a.selectedBG = k;
                    h.push(k);
                    f = d.rect(b, f, g, "#000", .005);
                    a.dragger = f;
                    h.push(f);
                    f.mousedown(function(b) {
                        a.handleDragStart(b)
                    }).mouseup(function() {
                        a.handleDragStop()
                    }).mouseover(function() {
                        a.handleDraggerOver()
                    }).mouseout(function() {
                        a.handleMouseOut()
                    }).touchstart(function(b) {
                        a.handleDragStart(b)
                    }).touchend(function() {
                        a.handleDragStop()
                    });
                    g = e.pathToImages;
                    var l, k = a.dragIcon.replace(/\.[a-z]*$/i, "");
                    d.isAbsolute(k) && (g = "");
                    c ? (l = g + k + "H" + e.extension, g = a.dragIconWidth, c = a.dragIconHeight) : (l = g + k + e.extension, c = a.dragIconWidth, g = a.dragIconHeight);
                    k = b.image(l, 0, 0, c, g);
                    d.setCN(e, k, "scrollbar-grip-left");
                    l = b.image(l, 0, 0, c, g);
                    d.setCN(e, l, "scrollbar-grip-right");
                    var m = 10,
                        p = 20;
                    e.panEventsEnabled && (m = 25, p = a.scrollbarHeight);
                    var q = d.rect(b, m, p, "#000", .005),
                        n = d.rect(b, m, p, "#000", .005);
                    n.translate(-(m - c) / 2, -(p - g) / 2);
                    q.translate(-(m - c) / 2, -(p - g) / 2);
                    c = b.set([k, n]);
                    b = b.set([l, q]);
                    a.iconLeft = c;
                    h.push(a.iconLeft);
                    a.iconRight = b;
                    h.push(b);
                    a.updateGripCursor(!1);
                    e.makeAccessible(c, a.accessibleLabel);
                    e.makeAccessible(b, a.accessibleLabel);
                    e.makeAccessible(f, a.accessibleLabel);
                    c.setAttr("role", "menuitem");
                    b.setAttr("role", "menuitem");
                    f.setAttr("role", "menuitem");
                    void 0 !== a.tabIndex && (c.setAttr("tabindex", a.tabIndex), c.keyup(function(b) {
                        a.handleKeys(b, 1, 0)
                    }));
                    void 0 !== a.tabIndex && (f.setAttr("tabindex", a.tabIndex), f.keyup(function(b) {
                        a.handleKeys(b, 1, 1)
                    }));
                    void 0 !== a.tabIndex && (b.setAttr("tabindex", a.tabIndex), b.keyup(function(b) {
                        a.handleKeys(b, 0, 1)
                    }));
                    c.mousedown(function() {
                        a.leftDragStart()
                    }).mouseup(function() {
                        a.leftDragStop()
                    }).mouseover(function() {
                        a.iconRollOver()
                    }).mouseout(function() {
                        a.iconRollOut()
                    }).touchstart(function() {
                        a.leftDragStart()
                    }).touchend(function() {
                        a.leftDragStop()
                    });
                    b.mousedown(function() {
                        a.rightDragStart()
                    }).mouseup(function() {
                        a.rightDragStop()
                    }).mouseover(function() {
                        a.iconRollOver()
                    }).mouseout(function() {
                        a.iconRollOut()
                    }).touchstart(function() {
                        a.rightDragStart()
                    }).touchend(function() {
                        a.rightDragStop()
                    });
                    d.ifArray(e.chartData) ? h.show() : h.hide();
                    a.hideDragIcons();
                    a.clipDragger(!1)
                }
                h.translate(a.x, a.y);
                h.node.style.msTouchAction = "none";
                h.node.style.touchAction = "none"
            }
        },
        handleKeys: function(a, b, c) {
            this.getPercents();
            var e = this.percentStart,
                d = this.percentEnd;
            if (this.rotate) var f = d,
                d = e,
                e = f;
            if (37 == a.keyCode || 40 == a.keyCode) e -= .02 * b, d -= .02 * c;
            if (39 == a.keyCode || 38 == a.keyCode) e += .02 * b, d += .02 * c;
            this.rotate && (a = d, d = e, e = a);
            isNaN(d) || isNaN(e) || this.percentZoom(e, d, !0)
        },
        updateScrollbarSize: function(a, b) {
            if (!isNaN(a) &&
                !isNaN(b)) {
                a = Math.round(a);
                b = Math.round(b);
                var c = this.dragger,
                    e, d, f, g, k;
                this.rotate ? (e = 0, d = a, f = this.width + 1, g = b - a, c.setAttr("height", b - a), c.setAttr("y", d)) : (e = a, d = 0, f = b - a, g = this.height + 1, k = b - a, c.setAttr("x", e), c.setAttr("width", k));
                this.clipAndUpdate(e, d, f, g)
            }
        },
        update: function() {
            var a, b = !1,
                c, e, d = this.x,
                f = this.y,
                g = this.dragger,
                k = this.getDBox();
            if (k) {
                c = k.x + d;
                e = k.y + f;
                var l = k.width,
                    k = k.height,
                    m = this.rotate,
                    p = this.chart,
                    q = this.width,
                    n = this.height,
                    t = p.mouseX,
                    p = p.mouseY;
                a = this.initialMouse;
                this.forceClip &&
                    this.clipDragger(!0);
                if (this.dragging) {
                    var r = this.initialCoord;
                    m ? (a = r + (p - a), 0 > a && (a = 0), r = n - k, a > r && (a = r), g.setAttr("y", a)) : (a = r + (t - a), 0 > a && (a = 0), r = q - l, a > r && (a = r), g.setAttr("x", a));
                    this.clipDragger(!0)
                }
                if (this.resizingRight) {
                    if (m)
                        if (a = p - e, !isNaN(this.maxHeight) && a > this.maxHeight && (a = this.maxHeight), a + e > n + f && (a = n - e + f), 0 > a) this.resizingRight = !1, b = this.resizingLeft = !0;
                        else {
                            if (0 === a || isNaN(a)) a = .1;
                            g.setAttr("height", a)
                        }
                    else if (a = t - c, !isNaN(this.maxWidth) && a > this.maxWidth && (a = this.maxWidth), a + c > q + d && (a =
                            q - c + d), 0 > a) this.resizingRight = !1, b = this.resizingLeft = !0;
                    else {
                        if (0 === a || isNaN(a)) a = .1;
                        g.setAttr("width", a)
                    }
                    this.clipDragger(!0)
                }
                if (this.resizingLeft) {
                    if (m)
                        if (c = e, e = p, e < f && (e = f), isNaN(e) && (e = f), e > n + f && (e = n + f), a = !0 === b ? c - e : k + c - e, !isNaN(this.maxHeight) && a > this.maxHeight && (a = this.maxHeight, e = c), 0 > a) this.resizingRight = !0, this.resizingLeft = !1, g.setAttr("y", c + k - f);
                        else {
                            if (0 === a || isNaN(a)) a = .1;
                            g.setAttr("y", e - f);
                            g.setAttr("height", a)
                        }
                    else if (e = t, e < d && (e = d), isNaN(e) && (e = d), e > q + d && (e = q + d), a = !0 === b ? c - e : l + c - e, !isNaN(this.maxWidth) && a > this.maxWidth && (a = this.maxWidth, e = c), 0 > a) this.resizingRight = !0, this.resizingLeft = !1, g.setAttr("x", c + l - d);
                    else {
                        if (0 === a || isNaN(a)) a = .1;
                        g.setAttr("x", e - d);
                        g.setAttr("width", a)
                    }
                    this.clipDragger(!0)
                }
            }
        },
        stopForceClip: function() {
            this.animating = this.forceClip = !1
        },
        clipDragger: function(a) {
            var b = this.getDBox();
            if (b) {
                var c = b.x,
                    e = b.y,
                    d = b.width,
                    b = b.height,
                    f = !1;
                if (this.rotate) {
                    if (c = 0, d = this.width + 1, this.clipY != e || this.clipH != b) f = !0
                } else if (e = 0, b = this.height + 1, this.clipX != c || this.clipW !=
                    d) f = !0;
                f && (this.clipAndUpdate(c, e, d, b), a && (this.updateOnReleaseOnly || this.dispatchScrollbarEvent()))
            }
        },
        maskGraphs: function() {},
        clipAndUpdate: function(a, b, c, d) {
            this.clipX = a;
            this.clipY = b;
            this.clipW = c;
            this.clipH = d;
            this.selectedBG.setAttr("width", c);
            this.selectedBG.setAttr("height", d);
            this.selectedBG.translate(a, b);
            this.updateDragIconPositions();
            this.maskGraphs(a, b, c, d)
        },
        dispatchScrollbarEvent: function() {
            if (this.skipEvent) this.skipEvent = !1;
            else {
                var a = this.chart;
                a.hideBalloon();
                var b = this.getDBox(),
                    c =
                    b.x,
                    d = b.y,
                    h = b.width,
                    b = b.height;
                this.getPercents();
                this.rotate ? (c = d, h = this.height / b) : h = this.width / h;
                this.fire({
                    type: "zoomed",
                    position: c,
                    chart: a,
                    target: this,
                    multiplier: h,
                    relativeStart: this.percentStart,
                    relativeEnd: this.percentEnd
                })
            }
        },
        updateDragIconPositions: function() {
            var a = this.getDBox(),
                b = a.x,
                c = a.y,
                d = this.iconLeft,
                h = this.iconRight,
                f, g, k = this.scrollbarHeight;
            this.rotate ? (f = this.dragIconWidth, g = this.dragIconHeight, d.translate((k - g) / 2, c - f / 2), h.translate((k - g) / 2, c + a.height - f / 2)) : (f = this.dragIconHeight,
                g = this.dragIconWidth, d.translate(b - g / 2, (k - f) / 2), h.translate(b - g / 2 + a.width, (k - f) / 2))
        },
        showDragIcons: function() {
            this.resizeEnabled && (this.iconLeft.show(), this.iconRight.show())
        },
        hideDragIcons: function() {
            if (!this.resizingLeft && !this.resizingRight && !this.dragging) {
                if (this.hideResizeGrips || !this.resizeEnabled) this.iconLeft.hide(), this.iconRight.hide();
                this.removeCursors()
            }
        },
        removeCursors: function() {
            this.chart.setMouseCursor("auto")
        },
        fireZoomEvent: function(a) {
            this.fire({
                type: a,
                chart: this.chart,
                target: this
            })
        },
        percentZoom: function(a, b, c) {
            a = d.fitToBounds(a, 0, b);
            b = d.fitToBounds(b, a, 1);
            if (this.dragger && this.enabled) {
                this.dragger.stop();
                isNaN(a) && (a = 0);
                isNaN(b) && (b = 1);
                var e, h;
                this.rotate ? (e = this.height, b = e - e * b, h = e - e * a) : (e = this.width, h = e * b, b = e * a);
                this.updateScrollbarSize(b, h);
                this.clipDragger(!1);
                this.getPercents();
                c && this.dispatchScrollbarEvent()
            }
        },
        destroy: function() {
            this.clear();
            d.remove(this.set);
            d.remove(this.iconRight);
            d.remove(this.iconLeft)
        },
        clear: function() {},
        handleDragStart: function() {
            if (this.enabled) {
                this.fireZoomEvent("zoomStarted");
                var a = this.chart;
                this.dragger.stop();
                this.removeCursors();
                d.isModern && (this.dragger.node.style.cssText = this.dragCursorDown);
                this.dragging = !0;
                var b = this.getDBox();
                this.rotate ? (this.initialCoord = b.y, this.initialMouse = a.mouseY) : (this.initialCoord = b.x, this.initialMouse = a.mouseX)
            }
        },
        handleDragStop: function() {
            this.updateOnReleaseOnly && (this.update(), this.skipEvent = !1, this.dispatchScrollbarEvent());
            this.dragging = !1;
            this.mouseIsOver && this.removeCursors();
            d.isModern && (this.dragger.node.style.cssText = this.dragCursorHover);
            this.update();
            this.fireZoomEvent("zoomEnded")
        },
        handleDraggerOver: function() {
            this.handleMouseOver();
            d.isModern && (this.dragger.node.style.cssText = this.dragCursorHover)
        },
        leftDragStart: function() {
            this.fireZoomEvent("zoomStarted");
            this.dragger.stop();
            this.resizingLeft = !0;
            this.updateGripCursor(!0)
        },
        updateGripCursor: function(a) {
            d.isModern && (a = this.rotate ? a ? this.vResizeCursorDown : this.vResizeCursorHover : a ? this.hResizeCursorDown : this.hResizeCursorHover) && (this.iconRight && (this.iconRight.node.style.cssText =
                a), this.iconLeft && (this.iconLeft.node.style.cssText = a))
        },
        leftDragStop: function() {
            this.resizingLeft && (this.resizingLeft = !1, this.mouseIsOver || this.removeCursors(), this.updateOnRelease(), this.fireZoomEvent("zoomEnded"));
            this.updateGripCursor(!1)
        },
        rightDragStart: function() {
            this.fireZoomEvent("zoomStarted");
            this.dragger.stop();
            this.resizingRight = !0;
            this.updateGripCursor(!0)
        },
        rightDragStop: function() {
            this.resizingRight && (this.resizingRight = !1, this.mouseIsOver || this.removeCursors(), this.updateOnRelease(),
                this.fireZoomEvent("zoomEnded"));
            this.updateGripCursor(!1)
        },
        iconRollOut: function() {
            this.removeCursors()
        },
        iconRollOver: function() {
            this.rotate ? this.vResizeCursor && this.chart.setMouseCursor(this.vResizeCursor) : this.hResizeCursor && this.chart.setMouseCursor(this.hResizeCursor);
            this.handleMouseOver()
        },
        getDBox: function() {
            if (this.dragger) return this.dragger.getBBox()
        },
        handleBgClick: function() {
            var a = this;
            if (!a.resizingRight && !a.resizingLeft) {
                a.zooming = !0;
                var b, c, e = a.scrollDuration,
                    h = a.dragger;
                b = a.getDBox();
                var f = b.height,
                    g = b.width;
                c = a.chart;
                var k = a.y,
                    l = a.x,
                    m = a.rotate;
                m ? (b = "y", c = c.mouseY - f / 2 - k, c = d.fitToBounds(c, 0, a.height - f)) : (b = "x", c = c.mouseX - g / 2 - l, c = d.fitToBounds(c, 0, a.width - g));
                a.updateOnReleaseOnly ? (a.skipEvent = !1, h.setAttr(b, c), a.dispatchScrollbarEvent(), a.clipDragger()) : (a.animating = !0, c = Math.round(c), m ? h.animate({
                    y: c
                }, e, ">") : h.animate({
                    x: c
                }, e, ">"), a.forceClip = !0, clearTimeout(a.forceTO), a.forceTO = setTimeout(function() {
                    a.stopForceClip.call(a)
                }, 5E3 * e))
            }
        },
        updateOnRelease: function() {
            this.updateOnReleaseOnly &&
                (this.update(), this.skipEvent = !1, this.dispatchScrollbarEvent())
        },
        handleReleaseOutside: function() {
            if (this.set) {
                if (this.resizingLeft || this.resizingRight || this.dragging) this.dragging = this.resizingRight = this.resizingLeft = !1, this.updateOnRelease(), this.removeCursors();
                this.animating = this.mouseIsOver = !1;
                this.hideDragIcons();
                this.update()
            }
        },
        handleMouseOver: function() {
            this.mouseIsOver = !0;
            this.showDragIcons()
        },
        handleMouseOut: function() {
            this.mouseIsOver = !1;
            this.hideDragIcons();
            this.removeCursors()
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.ChartScrollbar = d.Class({
        inherits: d.SimpleChartScrollbar,
        construct: function(a) {
            this.cname = "ChartScrollbar";
            d.ChartScrollbar.base.construct.call(this, a);
            this.graphLineColor = "#BBBBBB";
            this.graphLineAlpha = 0;
            this.graphFillColor = "#BBBBBB";
            this.graphFillAlpha = 1;
            this.selectedGraphLineColor = "#888888";
            this.selectedGraphLineAlpha = 0;
            this.selectedGraphFillColor = "#888888";
            this.selectedGraphFillAlpha = 1;
            this.gridCount = 0;
            this.gridColor = "#FFFFFF";
            this.gridAlpha = .7;
            this.skipEvent =
                this.autoGridCount = !1;
            this.color = "#FFFFFF";
            this.scrollbarCreated = !1;
            this.oppositeAxis = !0;
            this.accessibleLabel = "Zoom chart using cursor arrows";
            d.applyTheme(this, a, this.cname)
        },
        init: function() {
            var a = this.categoryAxis,
                b = this.chart,
                c = this.gridAxis;
            a || ("CategoryAxis" == this.gridAxis.cname ? (this.catScrollbar = !0, a = new d.CategoryAxis, a.id = "scrollbar") : (a = new d.ValueAxis, a.data = b.chartData, a.id = c.id, a.type = c.type, a.maximumDate = c.maximumDate, a.minimumDate = c.minimumDate, a.minPeriod = c.minPeriod, a.minMaxField =
                c.minMaxField), this.categoryAxis = a);
            a.chart = b;
            var e = b.categoryAxis;
            e && (a.firstDayOfWeek = e.firstDayOfWeek);
            a.dateFormats = c.dateFormats;
            a.markPeriodChange = c.markPeriodChange;
            a.boldPeriodBeginning = c.boldPeriodBeginning;
            a.labelFunction = c.labelFunction;
            a.axisItemRenderer = d.RecItem;
            a.axisRenderer = d.RecAxis;
            a.guideFillRenderer = d.RecFill;
            a.inside = !0;
            a.fontSize = this.fontSize;
            a.tickLength = 0;
            a.axisAlpha = 0;
            d.isString(this.graph) && (this.graph = d.getObjById(b.graphs, this.graph));
            (a = this.graph) && this.catScrollbar &&
                (c = this.valueAxis, c || (this.valueAxis = c = new d.ValueAxis, c.visible = !1, c.scrollbar = !0, c.axisItemRenderer = d.RecItem, c.axisRenderer = d.RecAxis, c.guideFillRenderer = d.RecFill, c.labelsEnabled = !1, c.chart = b), b = this.unselectedGraph, b || (b = new d.AmGraph, b.scrollbar = !0, this.unselectedGraph = b, b.negativeBase = a.negativeBase, b.noStepRisers = a.noStepRisers), b = this.selectedGraph, b || (b = new d.AmGraph, b.scrollbar = !0, this.selectedGraph = b, b.negativeBase = a.negativeBase, b.noStepRisers = a.noStepRisers));
            this.scrollbarCreated = !0
        },
        draw: function() {
            var a = this;
            d.ChartScrollbar.base.draw.call(a);
            if (a.enabled) {
                a.scrollbarCreated || a.init();
                var b = a.chart,
                    c = b.chartData,
                    e = a.categoryAxis,
                    h = a.rotate,
                    f = a.x,
                    g = a.y,
                    k = a.width,
                    l = a.height,
                    m = a.gridAxis,
                    p = a.set;
                e.setOrientation(!h);
                e.parseDates = m.parseDates;
                "ValueAxis" == a.categoryAxis.cname && (e.rotate = !h);
                e.equalSpacing = m.equalSpacing;
                e.minPeriod = m.minPeriod;
                e.startOnAxis = m.startOnAxis;
                e.width = k - 1;
                e.height = l;
                e.gridCount = a.gridCount;
                e.gridColor = a.gridColor;
                e.gridAlpha = a.gridAlpha;
                e.color = a.color;
                e.tickLength = 0;
                e.axisAlpha = 0;
                e.autoGridCount = a.autoGridCount;
                e.parseDates && !e.equalSpacing && e.timeZoom(b.firstTime, b.lastTime);
                e.minimum = a.gridAxis.fullMin;
                e.maximum = a.gridAxis.fullMax;
                e.strictMinMax = !0;
                e.zoom(0, c.length - 1);
                if ((m = a.graph) && a.catScrollbar) {
                    var q = a.valueAxis,
                        n = m.valueAxis;
                    q.id = n.id;
                    q.rotate = h;
                    q.setOrientation(h);
                    q.width = k;
                    q.height = l;
                    q.dataProvider = c;
                    q.reversed = n.reversed;
                    q.logarithmic = n.logarithmic;
                    q.gridAlpha = 0;
                    q.axisAlpha = 0;
                    p.push(q.set);
                    h ? (q.y = g, q.x = 0) : (q.x = f, q.y = 0);
                    var f = Infinity,
                        g = -Infinity,
                        t;
                    for (t = 0; t < c.length; t++) {
                        var r = c[t].axes[n.id].graphs[m.id].values,
                            w;
                        for (w in r)
                            if (r.hasOwnProperty(w) && "percents" != w && "total" != w) {
                                var z = r[w];
                                z < f && (f = z);
                                z > g && (g = z)
                            }
                    }
                    Infinity != f && (q.minimum = f); - Infinity != g && (q.maximum = g + .1 * (g - f));
                    f == g && (--q.minimum, q.maximum += 1);
                    void 0 !== a.minimum && (q.minimum = a.minimum);
                    void 0 !== a.maximum && (q.maximum = a.maximum);
                    q.zoom(0, c.length - 1);
                    w = a.unselectedGraph;
                    w.id = m.id;
                    w.bcn = "scrollbar-graph-";
                    w.rotate = h;
                    w.chart = b;
                    w.data = c;
                    w.valueAxis = q;
                    w.chart = m.chart;
                    w.categoryAxis =
                        a.categoryAxis;
                    w.periodSpan = m.periodSpan;
                    w.valueField = m.valueField;
                    w.openField = m.openField;
                    w.closeField = m.closeField;
                    w.highField = m.highField;
                    w.lowField = m.lowField;
                    w.lineAlpha = a.graphLineAlpha;
                    w.lineColorR = a.graphLineColor;
                    w.fillAlphas = a.graphFillAlpha;
                    w.fillColorsR = a.graphFillColor;
                    w.connect = m.connect;
                    w.hidden = m.hidden;
                    w.width = k;
                    w.height = l;
                    w.pointPosition = m.pointPosition;
                    w.stepDirection = m.stepDirection;
                    w.periodSpan = m.periodSpan;
                    n = a.selectedGraph;
                    n.id = m.id;
                    n.bcn = w.bcn + "selected-";
                    n.rotate = h;
                    n.chart =
                        b;
                    n.data = c;
                    n.valueAxis = q;
                    n.chart = m.chart;
                    n.categoryAxis = e;
                    n.periodSpan = m.periodSpan;
                    n.valueField = m.valueField;
                    n.openField = m.openField;
                    n.closeField = m.closeField;
                    n.highField = m.highField;
                    n.lowField = m.lowField;
                    n.lineAlpha = a.selectedGraphLineAlpha;
                    n.lineColorR = a.selectedGraphLineColor;
                    n.fillAlphas = a.selectedGraphFillAlpha;
                    n.fillColorsR = a.selectedGraphFillColor;
                    n.connect = m.connect;
                    n.hidden = m.hidden;
                    n.width = k;
                    n.height = l;
                    n.pointPosition = m.pointPosition;
                    n.stepDirection = m.stepDirection;
                    n.periodSpan = m.periodSpan;
                    b = a.graphType;
                    b || (b = m.type);
                    w.type = b;
                    n.type = b;
                    c = c.length - 1;
                    w.zoom(0, c);
                    n.zoom(0, c);
                    n.set.click(function() {
                        a.handleBackgroundClick()
                    }).mouseover(function() {
                        a.handleMouseOver()
                    }).mouseout(function() {
                        a.handleMouseOut()
                    });
                    w.set.click(function() {
                        a.handleBackgroundClick()
                    }).mouseover(function() {
                        a.handleMouseOver()
                    }).mouseout(function() {
                        a.handleMouseOut()
                    });
                    p.push(w.set);
                    p.push(n.set)
                }
                p.push(e.set);
                p.push(e.labelsSet);
                a.bg.toBack();
                a.invisibleBg.toFront();
                a.dragger.toFront();
                a.iconLeft.toFront();
                a.iconRight.toFront()
            }
        },
        timeZoom: function(a, b, c) {
            this.startTime = a;
            this.endTime = b;
            this.timeDifference = b - a;
            this.skipEvent = !d.toBoolean(c);
            this.zoomScrollbar();
            this.dispatchScrollbarEvent()
        },
        zoom: function(a, b) {
            this.start = a;
            this.end = b;
            this.skipEvent = !0;
            this.zoomScrollbar()
        },
        dispatchScrollbarEvent: function() {
            if (this.categoryAxis && "ValueAxis" == this.categoryAxis.cname) d.ChartScrollbar.base.dispatchScrollbarEvent.call(this);
            else if (this.skipEvent) this.skipEvent = !1;
            else {
                var a = this.chart.chartData,
                    b, c, e = this.dragger.getBBox();
                b = e.x;
                var h = e.y,
                    f = e.width,
                    e = e.height,
                    g = this.chart;
                this.rotate ? (b = h, c = e) : c = f;
                f = {
                    type: "zoomed",
                    target: this
                };
                f.chart = g;
                var k = this.categoryAxis,
                    l = this.stepWidth,
                    h = g.minSelectedTime,
                    e = g.maxSelectedTime,
                    m = !1;
                if (k.parseDates && !k.equalSpacing) {
                    if (a = g.lastTime, g = g.firstTime, k = Math.round(b / l) + g, b = this.dragging ? k + this.timeDifference : Math.round((b + c) / l) + g, k > b && (k = b), 0 < h && b - k < h && (b = Math.round(k + (b - k) / 2), m = Math.round(h / 2), k = b - m, b += m, m = !0), 0 < e && b - k > e && (b = Math.round(k + (b - k) / 2), m = Math.round(e / 2), k = b - m, b += m, m = !0), b > a && (b =
                            a), b - h < k && (k = b - h), k < g && (k = g), k + h > b && (b = k + h), k != this.startTime || b != this.endTime) this.startTime = k, this.endTime = b, f.start = k, f.end = b, f.startDate = new Date(k), f.endDate = new Date(b), this.fire(f)
                } else {
                    k.startOnAxis || (b += l / 2);
                    c -= this.stepWidth / 2;
                    h = k.xToIndex(b);
                    b = k.xToIndex(b + c);
                    if (h != this.start || this.end != b) k.startOnAxis && (this.resizingRight && h == b && b++, this.resizingLeft && h == b && (0 < h ? h-- : b = 1)), this.start = h, this.end = this.dragging ? this.start + this.difference : b, f.start = this.start, f.end = this.end, k.parseDates && (a[this.start] &&
                        (f.startDate = new Date(a[this.start].time)), a[this.end] && (f.endDate = new Date(a[this.end].time))), this.fire(f);
                    this.percentStart = h;
                    this.percentEnd = b
                }
                m && this.zoomScrollbar(!0)
            }
        },
        zoomScrollbar: function(a) {
            if ((!(this.dragging || this.resizingLeft || this.resizingRight || this.animating) || a) && this.dragger && this.enabled) {
                var b, c, d = this.chart;
                a = d.chartData;
                var h = this.categoryAxis;
                h.parseDates && !h.equalSpacing ? (a = h.stepWidth, c = d.firstTime, b = a * (this.startTime - c), c = a * (this.endTime - c)) : (a[this.start] && (b = a[this.start].x[h.id]),
                    a[this.end] && (c = a[this.end].x[h.id]), a = h.stepWidth, h.startOnAxis || (d = a / 2, b -= d, c += d));
                this.stepWidth = a;
                isNaN(b) || isNaN(c) || this.updateScrollbarSize(b, c)
            }
        },
        maskGraphs: function(a, b, c, d) {
            var h = this.selectedGraph;
            h && h.set.clipRect(a, b, c, d)
        },
        handleDragStart: function() {
            d.ChartScrollbar.base.handleDragStart.call(this);
            this.difference = this.end - this.start;
            this.timeDifference = this.endTime - this.startTime;
            0 > this.timeDifference && (this.timeDifference = 0)
        },
        handleBackgroundClick: function() {
            d.ChartScrollbar.base.handleBackgroundClick.call(this);
            this.dragging || (this.difference = this.end - this.start, this.timeDifference = this.endTime - this.startTime, 0 > this.timeDifference && (this.timeDifference = 0))
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.AmBalloon = d.Class({
        construct: function(a) {
            this.cname = "AmBalloon";
            this.enabled = !0;
            this.fillColor = "#FFFFFF";
            this.fillAlpha = .8;
            this.borderThickness = 2;
            this.borderColor = "#FFFFFF";
            this.borderAlpha = 1;
            this.cornerRadius = 0;
            this.maxWidth = 220;
            this.horizontalPadding = 8;
            this.verticalPadding = 4;
            this.pointerWidth = 6;
            this.pointerOrientation = "V";
            this.color = "#000000";
            this.adjustBorderColor = !0;
            this.show = this.follow = this.showBullet = !1;
            this.bulletSize = 3;
            this.shadowAlpha = .4;
            this.shadowColor =
                "#000000";
            this.fadeOutDuration = this.animationDuration = .3;
            this.fixedPosition = !0;
            this.offsetY = 6;
            this.offsetX = 1;
            this.textAlign = "center";
            this.disableMouseEvents = !0;
            this.deltaSignX = this.deltaSignY = 1;
            d.isModern || (this.offsetY *= 1.5);
            this.sdy = this.sdx = 0;
            d.applyTheme(this, a, this.cname)
        },
        draw: function() {
            var a = this.pointToX,
                b = this.pointToY;
            d.isModern || (this.drop = !1);
            var c = this.chart;
            d.VML && (this.fadeOutDuration = 0);
            this.xAnim && c.stopAnim(this.xAnim);
            this.yAnim && c.stopAnim(this.yAnim);
            this.sdy = this.sdx = 0;
            if (!isNaN(a)) {
                var e =
                    this.follow,
                    h = c.container,
                    f = this.set;
                d.remove(f);
                this.removeDiv();
                f = h.set();
                f.node.style.pointerEvents = "none";
                this.set = f;
                this.mainSet ? (this.mainSet.push(this.set), this.sdx = this.mainSet.x, this.sdy = this.mainSet.y) : c.balloonsSet.push(f);
                if (this.show) {
                    var g = this.l,
                        k = this.t,
                        l = this.r,
                        m = this.b,
                        p = this.balloonColor,
                        q = this.fillColor,
                        n = this.borderColor,
                        t = q;
                    void 0 != p && (this.adjustBorderColor ? t = n = p : q = p);
                    var r = this.horizontalPadding,
                        w = this.verticalPadding,
                        z = this.pointerWidth,
                        x = this.pointerOrientation,
                        u = this.cornerRadius,
                        A = c.fontFamily,
                        y = this.fontSize;
                    void 0 == y && (y = c.fontSize);
                    var p = document.createElement("div"),
                        B = c.classNamePrefix;
                    p.className = B + "-balloon-div";
                    this.className && (p.className = p.className + " " + B + "-balloon-div-" + this.className);
                    B = p.style;
                    this.disableMouseEvents && (B.pointerEvents = "none");
                    B.position = "absolute";
                    var D = this.minWidth,
                        C = document.createElement("div");
                    p.appendChild(C);
                    var I = C.style;
                    isNaN(D) || (I.minWidth = D - 2 * r + "px");
                    I.textAlign = this.textAlign;
                    I.maxWidth = this.maxWidth + "px";
                    I.fontSize = y + "px";
                    I.color =
                        this.color;
                    I.fontFamily = A;
                    C.innerHTML = this.text;
                    c.chartDiv.appendChild(p);
                    this.textDiv = p;
                    var I = p.offsetWidth,
                        H = p.offsetHeight;
                    p.clientHeight && (I = p.clientWidth, H = p.clientHeight);
                    A = H + 2 * w;
                    C = I + 2 * r;
                    !isNaN(D) && C < D && (C = D);
                    window.opera && (A += 2);
                    var Q = !1,
                        y = this.offsetY;
                    c.handDrawn && (y += c.handDrawScatter + 2);
                    "H" != x ? (D = a - C / 2, b < k + A + 10 && "down" != x ? (Q = !0, e && (b += y), y = b + z, this.deltaSignY = -1) : (e && (b -= y), y = b - A - z, this.deltaSignY = 1)) : (2 * z > A && (z = A / 2), y = b - A / 2, a < g + (l - g) / 2 ? (D = a + z, this.deltaSignX = -1) : (D = a - C - z, this.deltaSignX =
                        1));
                    y + A >= m && (y = m - A);
                    y < k && (y = k);
                    D < g && (D = g);
                    D + C > l && (D = l - C);
                    var k = y + w,
                        m = D + r,
                        M = this.shadowAlpha,
                        P = this.shadowColor,
                        r = this.borderThickness,
                        ia = this.bulletSize,
                        J, w = this.fillAlpha,
                        aa = this.borderAlpha;
                    this.showBullet && (J = d.circle(h, ia, t, w), f.push(J));
                    this.drop ? (g = C / 1.6, l = 0, "V" == x && (x = "down"), "H" == x && (x = "left"), "down" == x && (D = a + 1, y = b - g - g / 3), "up" == x && (l = 180, D = a + 1, y = b + g + g / 3), "left" == x && (l = 270, D = a + g + g / 3 + 2, y = b), "right" == x && (l = 90, D = a - g - g / 3 + 2, y = b), k = y - H / 2 + 1, m = D - I / 2 - 1, q = d.drop(h, g, l, q, w, r, n, aa)) : 0 < u || 0 === z ? (0 < M && (a =
                        d.rect(h, C, A, q, 0, r + 1, P, M, u), d.isModern ? a.translate(1, 1) : a.translate(4, 4), f.push(a)), q = d.rect(h, C, A, q, w, r, n, aa, u)) : (t = [], u = [], "H" != x ? (g = a - D, g > C - z && (g = C - z), g < z && (g = z), t = [0, g - z, a - D, g + z, C, C, 0, 0], u = Q ? [0, 0, b - y, 0, 0, A, A, 0] : [A, A, b - y, A, A, 0, 0, A]) : (x = b - y, x > A - z && (x = A - z), x < z && (x = z), u = [0, x - z, b - y, x + z, A, A, 0, 0], t = a < g + (l - g) / 2 ? [0, 0, D < a ? 0 : a - D, 0, 0, C, C, 0] : [C, C, D + C > a ? C : a - D, C, C, 0, 0, C]), 0 < M && (a = d.polygon(h, t, u, q, 0, r, P, M), a.translate(1, 1), f.push(a)), q = d.polygon(h, t, u, q, w, r, n, aa));
                    this.bg = q;
                    f.push(q);
                    q.toFront();
                    d.setCN(c, q,
                        "balloon-bg");
                    this.className && d.setCN(c, q, "balloon-bg-" + this.className);
                    h = 1 * this.deltaSignX;
                    m += this.sdx;
                    k += this.sdy;
                    B.left = m + "px";
                    B.top = k + "px";
                    f.translate(D - h, y, 1, !0);
                    q = q.getBBox();
                    this.bottom = y + A + 1;
                    this.yPos = q.y + y;
                    J && J.translate(this.pointToX - D + h, b - y);
                    b = this.animationDuration;
                    0 < this.animationDuration && !e && !isNaN(this.prevX) && (f.translate(this.prevX, this.prevY, NaN, !0), f.animate({
                        translate: D - h + "," + y
                    }, b, "easeOutSine"), p && (B.left = this.prevTX + "px", B.top = this.prevTY + "px", this.xAnim = c.animate({
                            node: p
                        },
                        "left", this.prevTX, m, b, "easeOutSine", "px"), this.yAnim = c.animate({
                        node: p
                    }, "top", this.prevTY, k, b, "easeOutSine", "px")));
                    this.prevX = D - h;
                    this.prevY = y;
                    this.prevTX = m;
                    this.prevTY = k
                }
            }
        },
        fixPrevious: function() {
            this.rPrevX = this.prevX;
            this.rPrevY = this.prevY;
            this.rPrevTX = this.prevTX;
            this.rPrevTY = this.prevTY
        },
        restorePrevious: function() {
            this.prevX = this.rPrevX;
            this.prevY = this.rPrevY;
            this.prevTX = this.rPrevTX;
            this.prevTY = this.rPrevTY
        },
        followMouse: function() {
            if (this.follow && this.show) {
                var a = this.chart.mouseX - this.offsetX *
                    this.deltaSignX - this.sdx,
                    b = this.chart.mouseY - this.sdy;
                this.pointToX = a;
                this.pointToY = b;
                if (a != this.previousX || b != this.previousY)
                    if (this.previousX = a, this.previousY = b, 0 === this.cornerRadius) this.draw();
                    else {
                        var c = this.set;
                        if (c) {
                            var d = c.getBBox(),
                                a = a - d.width / 2,
                                h = b - d.height - 10;
                            a < this.l && (a = this.l);
                            a > this.r - d.width && (a = this.r - d.width);
                            h < this.t && (h = b + 10);
                            c.translate(a, h);
                            b = this.textDiv.style;
                            b.left = a + this.horizontalPadding + "px";
                            b.top = h + this.verticalPadding + "px"
                        }
                    }
            }
        },
        changeColor: function(a) {
            this.balloonColor =
                a
        },
        setBounds: function(a, b, c, d) {
            this.l = a;
            this.t = b;
            this.r = c;
            this.b = d;
            this.destroyTO && clearTimeout(this.destroyTO)
        },
        showBalloon: function(a) {
            if (this.text != a || this.positionChanged) this.text = a, this.isHiding = !1, this.show = !0, this.destroyTO && clearTimeout(this.destroyTO), a = this.chart, this.fadeAnim1 && a.stopAnim(this.fadeAnim1), this.fadeAnim2 && a.stopAnim(this.fadeAnim2), this.draw(), this.positionChanged = !1
        },
        hide: function(a) {
            var b = this;
            b.text = void 0;
            isNaN(a) && (a = b.fadeOutDuration);
            var c = b.chart;
            if (0 < a && !b.isHiding) {
                b.isHiding = !0;
                b.destroyTO && clearTimeout(b.destroyTO);
                b.destroyTO = setTimeout(function() {
                    b.destroy.call(b)
                }, 1E3 * a);
                b.follow = !1;
                b.show = !1;
                var d = b.set;
                d && (d.setAttr("opacity", b.fillAlpha), b.fadeAnim1 = d.animate({
                    opacity: 0
                }, a, "easeInSine"));
                b.textDiv && (b.fadeAnim2 = c.animate({
                    node: b.textDiv
                }, "opacity", 1, 0, a, "easeInSine", ""))
            } else b.show = !1, b.follow = !1, b.destroy()
        },
        setPosition: function(a, b) {
            if (a != this.pointToX || b != this.pointToY) this.previousX = this.pointToX, this.previousY = this.pointToY, this.pointToX = a, this.pointToY =
                b, this.positionChanged = !0
        },
        followCursor: function(a) {
            var b = this;
            b.follow = a;
            clearInterval(b.interval);
            var c = b.chart.mouseX - b.sdx,
                d = b.chart.mouseY - b.sdy;
            !isNaN(c) && a && (b.pointToX = c - b.offsetX * b.deltaSignX, b.pointToY = d, b.followMouse(), b.interval = setInterval(function() {
                b.followMouse.call(b)
            }, 40))
        },
        removeDiv: function() {
            if (this.textDiv) {
                var a = this.textDiv.parentNode;
                a && a.removeChild(this.textDiv)
            }
        },
        destroy: function() {
            clearInterval(this.interval);
            d.remove(this.set);
            this.removeDiv();
            this.set = null
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.AmCoordinateChart = d.Class({
        inherits: d.AmChart,
        construct: function(a) {
            d.AmCoordinateChart.base.construct.call(this, a);
            this.theme = a;
            this.createEvents("rollOverGraphItem", "rollOutGraphItem", "clickGraphItem", "doubleClickGraphItem", "rightClickGraphItem", "clickGraph", "rollOverGraph", "rollOutGraph");
            this.startAlpha = 1;
            this.startDuration = 0;
            this.startEffect = "elastic";
            this.sequencedAnimation = !0;
            this.colors = "#FF6600 #FCD202 #B0DE09 #0D8ECF #2A0CD0 #CD0D74 #CC0000 #00CC00 #0000CC #DDDDDD #999999 #333333 #990000".split(" ");
            this.balloonDateFormat = "MMM DD, YYYY";
            this.valueAxes = [];
            this.graphs = [];
            this.guides = [];
            this.gridAboveGraphs = !1;
            d.applyTheme(this, a, "AmCoordinateChart")
        },
        initChart: function() {
            d.AmCoordinateChart.base.initChart.call(this);
            this.drawGraphs = !0;
            var a = this.categoryAxis;
            a && (this.categoryAxis = d.processObject(a, d.CategoryAxis, this.theme));
            this.processValueAxes();
            this.createValueAxes();
            this.processGraphs();
            this.processGuides();
            d.VML && (this.startAlpha = 1);
            this.setLegendData(this.graphs);
            this.gridAboveGraphs && (this.gridSet.toFront(),
                this.bulletSet.toFront(), this.balloonsSet.toFront())
        },
        createValueAxes: function() {
            if (0 === this.valueAxes.length) {
                var a = new d.ValueAxis;
                this.addValueAxis(a)
            }
        },
        parseData: function() {
            this.processValueAxes();
            this.processGraphs()
        },
        parseSerialData: function(a) {
            this.chartData = [];
            if (a)
                if (0 < this.processTimeout) {
                    1 > this.processCount && (this.processCount = 1);
                    var b = a.length / this.processCount;
                    this.parseCount = Math.ceil(b) - 1;
                    for (var c = 0; c < b; c++) this.delayParseSerialData(a, c)
                } else this.parseCount = 0, this.parsePartSerialData(a,
                    0, a.length, 0);
            else this.onDataUpdated()
        },
        delayParseSerialData: function(a, b) {
            var c = this,
                d = c.processCount;
            setTimeout(function() {
                c.parsePartSerialData.call(c, a, b * d, (b + 1) * d, b)
            }, c.processTimeout)
        },
        parsePartSerialData: function(a, b, c, e) {
            c > a.length && (c = a.length);
            var h = this.graphs,
                f = {},
                g = this.seriesIdField;
            g || (g = this.categoryField);
            var k = !1,
                l, m = this.categoryAxis,
                p, q, n;
            m && (k = m.parseDates, p = m.forceShowField, n = m.classNameField, q = m.labelColorField, l = m.categoryFunction);
            var t, r, w = {},
                z;
            k && (t = d.extractPeriod(m.minPeriod),
                r = t.period, t = t.count, z = d.getPeriodDuration(r, t));
            var x = {};
            this.lookupTable = x;
            var u, A = this.dataDateFormat,
                y = {};
            for (u = b; u < c; u++) {
                var B = {},
                    D = a[u];
                b = D[this.categoryField];
                B.dataContext = D;
                B.category = l ? l(b, D, m) : String(b);
                p && (B.forceShow = D[p]);
                n && (B.className = D[n]);
                q && (B.labelColor = D[q]);
                x[D[g]] = B;
                if (k && (m.categoryFunction ? b = m.categoryFunction(b, D, m) : (!A || b instanceof Date || (b = b.toString() + " |"), b = d.getDate(b, A, m.minPeriod)), b = d.resetDateToMin(b, r, t, m.firstDayOfWeek), B.category = b, B.time = b.getTime(), isNaN(B.time))) continue;
                var C = this.valueAxes;
                B.axes = {};
                B.x = {};
                var I;
                for (I = 0; I < C.length; I++) {
                    var H = C[I].id;
                    B.axes[H] = {};
                    B.axes[H].graphs = {};
                    var Q;
                    for (Q = 0; Q < h.length; Q++) {
                        b = h[Q];
                        var M = b.id,
                            P = 1.1;
                        isNaN(b.gapPeriod) || (P = b.gapPeriod);
                        var ia = b.periodValue;
                        if (b.valueAxis.id == H) {
                            B.axes[H].graphs[M] = {};
                            var J = {};
                            J.index = u;
                            var aa = D;
                            b.dataProvider && (aa = f);
                            J.values = this.processValues(aa, b, ia);
                            if (!b.connect || b.forceGap && !isNaN(b.gapPeriod))
                                if (y && y[M] && 0 < P && B.time - w[M] >= z * P && (y[M].gap = !0), b.forceGap) {
                                    var P = 0,
                                        ma;
                                    for (ma in J.values) P++;
                                    0 < P && (w[M] = B.time, y[M] = J)
                                } else w[M] = B.time, y[M] = J;
                            this.processFields(b, J, aa);
                            J.category = B.category;
                            J.serialDataItem = B;
                            J.graph = b;
                            B.axes[H].graphs[M] = J
                        }
                    }
                }
                this.chartData[u] = B
            }
            if (this.parseCount == e) {
                for (a = 0; a < h.length; a++) b = h[a], b.dataProvider && this.parseGraphData(b);
                this.dataChanged = !1;
                this.dispatchDataUpdated = !0;
                this.onDataUpdated()
            }
        },
        processValues: function(a, b, c) {
            var e = {},
                h, f = !1;
            "candlestick" != b.type && "ohlc" != b.type || "" === c || (f = !0);
            for (var g = "value error open close low high".split(" "), k = 0; k < g.length; k++) {
                var l =
                    g[k];
                "value" != l && "error" != l && f && (c = l.charAt(0).toUpperCase() + l.slice(1));
                var m = a[b[l + "Field"] + c];
                null !== m && (h = Number(m), isNaN(h) || (e[l] = h), "date" == b.valueAxis.type && void 0 !== m && (h = d.getDate(m, b.chart.dataDateFormat), e[l] = h.getTime()))
            }
            return e
        },
        parseGraphData: function(a) {
            var b = a.dataProvider,
                c = a.seriesIdField;
            c || (c = this.seriesIdField);
            c || (c = this.categoryField);
            var d;
            for (d = 0; d < b.length; d++) {
                var h = b[d],
                    f = this.lookupTable[String(h[c])],
                    g = a.valueAxis.id;
                f && (g = f.axes[g].graphs[a.id], g.serialDataItem = f,
                    g.values = this.processValues(h, a, a.periodValue), this.processFields(a, g, h))
            }
        },
        addValueAxis: function(a) {
            a.chart = this;
            this.valueAxes.push(a);
            this.validateData()
        },
        removeValueAxesAndGraphs: function() {
            var a = this.valueAxes,
                b;
            for (b = a.length - 1; - 1 < b; b--) this.removeValueAxis(a[b])
        },
        removeValueAxis: function(a) {
            var b = this.graphs,
                c;
            for (c = b.length - 1; 0 <= c; c--) {
                var d = b[c];
                d && d.valueAxis == a && this.removeGraph(d)
            }
            b = this.valueAxes;
            for (c = b.length - 1; 0 <= c; c--) b[c] == a && b.splice(c, 1);
            this.validateData()
        },
        addGraph: function(a) {
            this.graphs.push(a);
            this.chooseGraphColor(a, this.graphs.length - 1);
            this.validateData()
        },
        removeGraph: function(a) {
            var b = this.graphs,
                c;
            for (c = b.length - 1; 0 <= c; c--) b[c] == a && (b.splice(c, 1), a.destroy());
            this.validateData()
        },
        handleValueAxisZoom: function() {},
        processValueAxes: function() {
            var a = this.valueAxes,
                b;
            for (b = 0; b < a.length; b++) {
                var c = a[b],
                    c = d.processObject(c, d.ValueAxis, this.theme);
                a[b] = c;
                c.chart = this;
                c.init();
                this.listenTo(c, "axisIntZoomed", this.handleValueAxisZoom);
                c.id || (c.id = "valueAxisAuto" + b + "_" + (new Date).getTime());
                void 0 === c.usePrefixes && (c.usePrefixes = this.usePrefixes)
            }
        },
        processGuides: function() {
            var a = this.guides,
                b = this.categoryAxis;
            if (a)
                for (var c = 0; c < a.length; c++) {
                    var e = a[c];
                    (void 0 !== e.category || void 0 !== e.date) && b && b.addGuide(e);
                    e.id || (e.id = "guideAuto" + c + "_" + (new Date).getTime());
                    var h = e.valueAxis;
                    h ? (d.isString(h) && (h = this.getValueAxisById(h)), h ? h.addGuide(e) : this.valueAxes[0].addGuide(e)) : isNaN(e.value) || this.valueAxes[0].addGuide(e)
                }
        },
        processGraphs: function() {
            var a = this.graphs,
                b;
            this.graphsById = {};
            for (b =
                0; b < a.length; b++) {
                var c = a[b],
                    c = d.processObject(c, d.AmGraph, this.theme);
                a[b] = c;
                this.chooseGraphColor(c, b);
                c.chart = this;
                c.init();
                d.isString(c.valueAxis) && (c.valueAxis = this.getValueAxisById(c.valueAxis));
                c.valueAxis || (c.valueAxis = this.valueAxes[0]);
                c.id || (c.id = "graphAuto" + b + "_" + (new Date).getTime());
                this.graphsById[c.id] = c
            }
        },
        formatString: function(a, b, c) {
            var e = b.graph,
                h = e.valueAxis;
            h.duration && b.values.value && (h = d.formatDuration(b.values.value, h.duration, "", h.durationUnits, h.maxInterval, h.numberFormatter),
                a = a.split("[[value]]").join(h));
            a = d.massReplace(a, {
                "[[title]]": e.title,
                "[[description]]": b.description
            });
            a = c ? d.fixNewLines(a) : d.fixBrakes(a);
            return a = d.cleanFromEmpty(a)
        },
        getBalloonColor: function(a, b, c) {
            var e = a.lineColor,
                h = a.balloonColor;
            c && (h = e);
            c = a.fillColorsR;
            "object" == typeof c ? e = c[0] : void 0 !== c && (e = c);
            b.isNegative && (c = a.negativeLineColor, a = a.negativeFillColors, "object" == typeof a ? c = a[0] : void 0 !== a && (c = a), void 0 !== c && (e = c));
            void 0 !== b.color && (e = b.color);
            void 0 !== b.lineColor && (e = b.lineColor);
            b = b.fillColors;
            void 0 !== b && (e = b, d.ifArray(b) && (e = b[0]));
            void 0 === h && (h = e);
            return h
        },
        getGraphById: function(a) {
            return d.getObjById(this.graphs, a)
        },
        getValueAxisById: function(a) {
            return d.getObjById(this.valueAxes, a)
        },
        processFields: function(a, b, c) {
            if (a.itemColors) {
                var e = a.itemColors,
                    h = b.index;
                b.color = h < e.length ? e[h] : d.randomColor()
            }
            e = "lineColor color alpha fillColors description bullet customBullet bulletSize bulletConfig url labelColor dashLength pattern gap className columnIndex".split(" ");
            for (h = 0; h < e.length; h++) {
                var f =
                    e[h],
                    g = a[f + "Field"];
                g && (g = c[g], d.isDefined(g) && (b[f] = g))
            }
            b.dataContext = c
        },
        chooseGraphColor: function(a, b) {
            if (a.lineColor) a.lineColorR = a.lineColor;
            else {
                var c;
                c = this.colors.length > b ? this.colors[b] : a.lineColorR ? a.lineColorR : d.randomColor();
                a.lineColorR = c
            }
            a.fillColorsR = a.fillColors ? a.fillColors : a.lineColorR;
            a.bulletBorderColorR = a.bulletBorderColor ? a.bulletBorderColor : a.useLineColorForBulletBorder ? a.lineColorR : a.bulletColor;
            a.bulletColorR = a.bulletColor ? a.bulletColor : a.lineColorR;
            if (c = this.patterns) a.pattern =
                c[b]
        },
        handleLegendEvent: function(a) {
            var b = a.type;
            if (a = a.dataItem) {
                var c = a.hidden,
                    d = a.showBalloon;
                switch (b) {
                    case "clickMarker":
                        this.textClickEnabled && (d ? this.hideGraphsBalloon(a) : this.showGraphsBalloon(a));
                        break;
                    case "clickLabel":
                        d ? this.hideGraphsBalloon(a) : this.showGraphsBalloon(a);
                        break;
                    case "rollOverItem":
                        c || this.highlightGraph(a);
                        break;
                    case "rollOutItem":
                        c || this.unhighlightGraph();
                        break;
                    case "hideItem":
                        this.hideGraph(a);
                        break;
                    case "showItem":
                        this.showGraph(a)
                }
            }
        },
        highlightGraph: function(a) {
            var b =
                this.graphs;
            if (b) {
                var c, d = .2;
                this.legend && (d = this.legend.rollOverGraphAlpha);
                if (1 != d)
                    for (c = 0; c < b.length; c++) {
                        var h = b[c];
                        h != a && h.changeOpacity(d)
                    }
            }
        },
        unhighlightGraph: function() {
            var a;
            this.legend && (a = this.legend.rollOverGraphAlpha);
            if (1 != a) {
                a = this.graphs;
                var b;
                for (b = 0; b < a.length; b++) a[b].changeOpacity(1)
            }
        },
        showGraph: function(a) {
            a.switchable && (a.hidden = !1, this.dataChanged = !0, "xy" != this.type && (this.marginsUpdated = !1), this.chartCreated && this.initChart())
        },
        hideGraph: function(a) {
            a.switchable && (this.dataChanged = !0, "xy" != this.type && (this.marginsUpdated = !1), a.hidden = !0, this.chartCreated && this.initChart())
        },
        hideGraphsBalloon: function(a) {
            a.showBalloon = !1;
            this.updateLegend()
        },
        showGraphsBalloon: function(a) {
            a.showBalloon = !0;
            this.updateLegend()
        },
        updateLegend: function() {
            this.legend && this.legend.invalidateSize()
        },
        resetAnimation: function() {
            var a = this.graphs;
            if (a) {
                var b;
                for (b = 0; b < a.length; b++) a[b].animationPlayed = !1
            }
        },
        animateAgain: function() {
            this.resetAnimation();
            this.validateNow()
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.TrendLine = d.Class({
        construct: function(a) {
            this.cname = "TrendLine";
            this.createEvents("click", "rollOver", "rollOut");
            this.isProtected = !1;
            this.dashLength = 0;
            this.lineColor = "#00CC00";
            this.lineThickness = this.lineAlpha = 1;
            d.applyTheme(this, a, this.cname)
        },
        draw: function() {
            var a = this;
            a.destroy();
            var b = a.chart,
                c = b.container,
                e, h, f, g, k = a.categoryAxis,
                l = a.initialDate,
                m = a.initialCategory,
                p = a.finalDate,
                q = a.finalCategory,
                n = a.valueAxis,
                t = a.valueAxisX,
                r = a.initialXValue,
                w = a.finalXValue,
                z = a.initialValue,
                x = a.finalValue,
                u = n.recalculateToPercents,
                A = b.dataDateFormat;
            k && (l && (l = d.getDate(l, A, "fff"), a.initialDate = l, e = k.dateToCoordinate(l)), m && (e = k.categoryToCoordinate(m)), p && (p = d.getDate(p, A, "fff"), a.finalDate = p, h = k.dateToCoordinate(p)), q && (h = k.categoryToCoordinate(q)));
            t && !u && (isNaN(r) || (e = t.getCoordinate(r)), isNaN(w) || (h = t.getCoordinate(w)));
            n && !u && (isNaN(z) || (f = n.getCoordinate(z)), isNaN(x) || (g = n.getCoordinate(x)));
            if (!(isNaN(e) || isNaN(h) || isNaN(f) || isNaN(f))) {
                b.rotate ? (k = [f, g], g = [e,
                    h
                ]) : (k = [e, h], g = [f, g]);
                l = a.lineColor;
                f = d.line(c, k, g, l, a.lineAlpha, a.lineThickness, a.dashLength);
                e = k;
                h = g;
                q = k[1] - k[0];
                n = g[1] - g[0];
                0 === q && (q = .01);
                0 === n && (n = .01);
                m = q / Math.abs(q);
                p = n / Math.abs(n);
                n = 90 * Math.PI / 180 - Math.asin(q / (q * n / Math.abs(q * n) * Math.sqrt(Math.pow(q, 2) + Math.pow(n, 2))));
                q = Math.abs(5 * Math.cos(n));
                n = Math.abs(5 * Math.sin(n));
                e.push(k[1] - m * n, k[0] - m * n);
                h.push(g[1] + p * q, g[0] + p * q);
                g = d.polygon(c, e, h, l, .005, 0);
                c = c.set([g, f]);
                c.translate(b.marginLeftReal, b.marginTopReal);
                b.trendLinesSet.push(c);
                d.setCN(b,
                    f, "trend-line");
                d.setCN(b, f, "trend-line-" + a.id);
                a.line = f;
                a.set = c;
                if (f = a.initialImage) f = d.processObject(f, d.Image, a.theme), f.chart = b, f.draw(), f.translate(e[0] + f.offsetX, h[0] + f.offsetY), c.push(f.set);
                if (f = a.finalImage) f = d.processObject(f, d.Image, a.theme), f.chart = b, f.draw(), f.translate(e[1] + f.offsetX, h[1] + f.offsetY), c.push(f.set);
                g.mouseup(function() {
                    a.handleLineClick()
                }).mouseover(function() {
                    a.handleLineOver()
                }).mouseout(function() {
                    a.handleLineOut()
                });
                g.touchend && g.touchend(function() {
                    a.handleLineClick()
                });
                c.clipRect(0, 0, b.plotAreaWidth, b.plotAreaHeight)
            }
        },
        handleLineClick: function() {
            this.fire({
                type: "click",
                trendLine: this,
                chart: this.chart
            })
        },
        handleLineOver: function() {
            var a = this.rollOverColor;
            void 0 !== a && this.line.attr({
                stroke: a
            });
            this.balloonText && (clearTimeout(this.chart.hoverInt), a = this.line.getBBox(), this.chart.showBalloon(this.balloonText, this.lineColor, !0, this.x + a.x + a.width / 2, this.y + a.y + a.height / 2));
            this.fire({
                type: "rollOver",
                trendLine: this,
                chart: this.chart
            })
        },
        handleLineOut: function() {
            this.line.attr({
                stroke: this.lineColor
            });
            this.balloonText && this.chart.hideBalloon();
            this.fire({
                type: "rollOut",
                trendLine: this,
                chart: this.chart
            })
        },
        destroy: function() {
            d.remove(this.set)
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.Image = d.Class({
        construct: function(a) {
            this.cname = "Image";
            this.height = this.width = 20;
            this.rotation = this.offsetY = this.offsetX = 0;
            this.balloonColor = this.color = "#000000";
            this.opacity = 1;
            d.applyTheme(this, a, this.cname)
        },
        draw: function() {
            var a = this;
            a.set && a.set.remove();
            var b = a.chart.container;
            a.set = b.set();
            var c, d;
            a.url ? (c = b.image(a.url, 0, 0, a.width, a.height), d = 1) : a.svgPath && (c = b.path(a.svgPath), c.setAttr("fill", a.color), c.setAttr("stroke", a.outlineColor), b = c.getBBox(), d =
                Math.min(a.width / b.width, a.height / b.height));
            c && (c.setAttr("opacity", a.opacity), a.set.rotate(a.rotation), c.translate(-a.width / 2, -a.height / 2, d), a.balloonText && c.mouseover(function() {
                a.chart.showBalloon(a.balloonText, a.balloonColor, !0)
            }).mouseout(function() {
                a.chart.hideBalloon()
            }).touchend(function() {
                a.chart.hideBalloon()
            }).touchstart(function() {
                a.chart.showBalloon(a.balloonText, a.balloonColor, !0)
            }), a.set.push(c))
        },
        translate: function(a, b) {
            this.set && this.set.translate(a, b)
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.circle = function(a, b, c, e, h, f, g, k, l) {
        0 >= b && (b = .001);
        if (void 0 == h || 0 === h) h = .01;
        void 0 === f && (f = "#000000");
        void 0 === g && (g = 0);
        e = {
            fill: c,
            stroke: f,
            "fill-opacity": e,
            "stroke-width": h,
            "stroke-opacity": g
        };
        a = isNaN(l) ? a.circle(0, 0, b).attr(e) : a.ellipse(0, 0, b, l).attr(e);
        k && a.gradient("radialGradient", [c, d.adjustLuminosity(c, -.6)]);
        return a
    };
    d.text = function(a, b, c, e, h, f, g, k) {
        f || (f = "middle");
        "right" == f && (f = "end");
        "left" == f && (f = "start");
        isNaN(k) && (k = 1);
        void 0 !== b && (b = String(b), d.isIE &&
            !d.isModern && (b = b.replace("&amp;", "&"), b = b.replace("&", "&amp;")));
        c = {
            fill: c,
            "font-family": e,
            "font-size": h + "px",
            opacity: k
        };
        !0 === g && (c["font-weight"] = "bold");
        c["text-anchor"] = f;
        return a.text(b, c)
    };
    d.polygon = function(a, b, c, e, h, f, g, k, l, m, p) {
        isNaN(f) && (f = .01);
        isNaN(k) && (k = h);
        var q = e,
            n = !1;
        "object" == typeof q && 1 < q.length && (n = !0, q = q[0]);
        void 0 === g && (g = q);
        h = {
            fill: q,
            stroke: g,
            "fill-opacity": h,
            "stroke-width": f,
            "stroke-opacity": k
        };
        void 0 !== p && 0 < p && (h["stroke-dasharray"] = p);
        p = d.dx;
        f = d.dy;
        a.handDrawn && (c = d.makeHD(b,
            c, a.handDrawScatter), b = c[0], c = c[1]);
        g = Math.round;
        m && (b[t] = d.roundTo(b[t], 5), c[t] = d.roundTo(c[t], 5), g = Number);
        k = "M" + (g(b[0]) + p) + "," + (g(c[0]) + f);
        for (var t = 1; t < b.length; t++) m && (b[t] = d.roundTo(b[t], 5), c[t] = d.roundTo(c[t], 5)), k += " L" + (g(b[t]) + p) + "," + (g(c[t]) + f);
        a = a.path(k + " Z").attr(h);
        n && a.gradient("linearGradient", e, l);
        return a
    };
    d.rect = function(a, b, c, e, h, f, g, k, l, m, p) {
        if (isNaN(b) || isNaN(c)) return a.set();
        isNaN(f) && (f = 0);
        void 0 === l && (l = 0);
        void 0 === m && (m = 270);
        isNaN(h) && (h = 0);
        var q = e,
            n = !1;
        "object" == typeof q &&
            (q = q[0], n = !0);
        void 0 === g && (g = q);
        void 0 === k && (k = h);
        b = Math.round(b);
        c = Math.round(c);
        var t = 0,
            r = 0;
        0 > b && (b = Math.abs(b), t = -b);
        0 > c && (c = Math.abs(c), r = -c);
        t += d.dx;
        r += d.dy;
        h = {
            fill: q,
            stroke: g,
            "fill-opacity": h,
            "stroke-opacity": k
        };
        void 0 !== p && 0 < p && (h["stroke-dasharray"] = p);
        a = a.rect(t, r, b, c, l, f).attr(h);
        n && a.gradient("linearGradient", e, m);
        return a
    };
    d.bullet = function(a, b, c, e, h, f, g, k, l, m, p, q, n) {
        var t;
        "circle" == b && (b = "round");
        switch (b) {
            case "round":
                t = d.circle(a, c / 2, e, h, f, g, k);
                break;
            case "square":
                t = d.polygon(a, [-c /
                    2, c / 2, c / 2, -c / 2
                ], [c / 2, c / 2, -c / 2, -c / 2], e, h, f, g, k, m - 180, void 0, n);
                break;
            case "rectangle":
                t = d.polygon(a, [-c, c, c, -c], [c / 2, c / 2, -c / 2, -c / 2], e, h, f, g, k, m - 180, void 0, n);
                break;
            case "diamond":
                t = d.polygon(a, [-c / 2, 0, c / 2, 0], [0, -c / 2, 0, c / 2], e, h, f, g, k);
                break;
            case "triangleUp":
                t = d.triangle(a, c, 0, e, h, f, g, k);
                break;
            case "triangleDown":
                t = d.triangle(a, c, 180, e, h, f, g, k);
                break;
            case "triangleLeft":
                t = d.triangle(a, c, 270, e, h, f, g, k);
                break;
            case "triangleRight":
                t = d.triangle(a, c, 90, e, h, f, g, k);
                break;
            case "bubble":
                t = d.circle(a, c / 2, e, h,
                    f, g, k, !0);
                break;
            case "line":
                t = d.line(a, [-c / 2, c / 2], [0, 0], e, h, f, g, k);
                break;
            case "yError":
                t = a.set();
                t.push(d.line(a, [0, 0], [-c / 2, c / 2], e, h, f));
                t.push(d.line(a, [-l, l], [-c / 2, -c / 2], e, h, f));
                t.push(d.line(a, [-l, l], [c / 2, c / 2], e, h, f));
                break;
            case "xError":
                t = a.set(), t.push(d.line(a, [-c / 2, c / 2], [0, 0], e, h, f)), t.push(d.line(a, [-c / 2, -c / 2], [-l, l], e, h, f)), t.push(d.line(a, [c / 2, c / 2], [-l, l], e, h, f))
        }
        t && t.pattern(p, NaN, q);
        return t
    };
    d.triangle = function(a, b, c, d, h, f, g, k) {
        if (void 0 === f || 0 === f) f = 1;
        void 0 === g && (g = "#000");
        void 0 ===
            k && (k = 0);
        d = {
            fill: d,
            stroke: g,
            "fill-opacity": h,
            "stroke-width": f,
            "stroke-opacity": k
        };
        b /= 2;
        var l;
        0 === c && (l = " M" + -b + "," + b + " L0," + -b + " L" + b + "," + b + " Z");
        180 == c && (l = " M" + -b + "," + -b + " L0," + b + " L" + b + "," + -b + " Z");
        90 == c && (l = " M" + -b + "," + -b + " L" + b + ",0 L" + -b + "," + b + " Z");
        270 == c && (l = " M" + -b + ",0 L" + b + "," + b + " L" + b + "," + -b + " Z");
        return a.path(l).attr(d)
    };
    d.line = function(a, b, c, e, h, f, g, k, l, m, p) {
        if (a.handDrawn && !p) return d.handDrawnLine(a, b, c, e, h, f, g, k, l, m, p);
        f = {
            fill: "none",
            "stroke-width": f
        };
        void 0 !== g && 0 < g && (f["stroke-dasharray"] =
            g);
        isNaN(h) || (f["stroke-opacity"] = h);
        e && (f.stroke = e);
        e = Math.round;
        m && (e = Number, b[0] = d.roundTo(b[0], 5), c[0] = d.roundTo(c[0], 5));
        m = d.dx;
        h = d.dy;
        g = "M" + (e(b[0]) + m) + "," + (e(c[0]) + h);
        for (k = 1; k < b.length; k++) b[k] = d.roundTo(b[k], 5), c[k] = d.roundTo(c[k], 5), g += " L" + (e(b[k]) + m) + "," + (e(c[k]) + h);
        if (d.VML) return a.path(g, void 0, !0).attr(f);
        l && (g += " M0,0 L0,0");
        return a.path(g).attr(f)
    };
    d.makeHD = function(a, b, c) {
        for (var d = [], h = [], f = 1; f < a.length; f++)
            for (var g = Number(a[f - 1]), k = Number(b[f - 1]), l = Number(a[f]), m = Number(b[f]),
                    p = Math.round(Math.sqrt(Math.pow(l - g, 2) + Math.pow(m - k, 2)) / 50) + 1, l = (l - g) / p, m = (m - k) / p, q = 0; q <= p; q++) {
                var n = k + q * m + Math.random() * c;
                d.push(g + q * l + Math.random() * c);
                h.push(n)
            }
        return [d, h]
    };
    d.handDrawnLine = function(a, b, c, e, h, f, g, k, l, m) {
        var p, q = a.set();
        for (p = 1; p < b.length; p++)
            for (var n = [b[p - 1], b[p]], t = [c[p - 1], c[p]], t = d.makeHD(n, t, a.handDrawScatter), n = t[0], t = t[1], r = 1; r < n.length; r++) q.push(d.line(a, [n[r - 1], n[r]], [t[r - 1], t[r]], e, h, f + Math.random() * a.handDrawThickness - a.handDrawThickness / 2, g, k, l, m, !0));
        return q
    };
    d.doNothing =
        function(a) {
            return a
        };
    d.drop = function(a, b, c, d, h, f, g, k) {
        var l = 1 / 180 * Math.PI,
            m = c - 20,
            p = Math.sin(m * l) * b,
            q = Math.cos(m * l) * b,
            n = Math.sin((m + 40) * l) * b,
            t = Math.cos((m + 40) * l) * b,
            r = .8 * b,
            w = -b / 3,
            z = b / 3;
        0 === c && (w = -w, z = 0);
        180 == c && (z = 0);
        90 == c && (w = 0);
        270 == c && (w = 0, z = -z);
        c = {
            fill: d,
            stroke: g,
            "stroke-width": f,
            "stroke-opacity": k,
            "fill-opacity": h
        };
        b = "M" + p + "," + q + " A" + b + "," + b + ",0,1,1," + n + "," + t + (" A" + r + "," + r + ",0,0,0," + (Math.sin((m + 20) * l) * b + z) + "," + (Math.cos((m + 20) * l) * b + w));
        b += " A" + r + "," + r + ",0,0,0," + p + "," + q;
        return a.path(b, void 0,
            void 0, "1000,1000").attr(c)
    };
    d.wedge = function(a, b, c, e, h, f, g, k, l, m, p, q, n, t) {
        var r = Math.round;
        f = r(f);
        g = r(g);
        k = r(k);
        var w = r(g / f * k),
            z = d.VML,
            x = 359.5 + f / 100;
        359.94 < x && (x = 359.94);
        h >= x && (h = x);
        var u = 1 / 180 * Math.PI,
            x = b + Math.sin(e * u) * k,
            A = c - Math.cos(e * u) * w,
            y = b + Math.sin(e * u) * f,
            B = c - Math.cos(e * u) * g,
            D = b + Math.sin((e + h) * u) * f,
            C = c - Math.cos((e + h) * u) * g,
            I = b + Math.sin((e + h) * u) * k,
            u = c - Math.cos((e + h) * u) * w,
            H = {
                fill: d.adjustLuminosity(m.fill, -.2),
                "stroke-opacity": 0,
                "fill-opacity": m["fill-opacity"]
            },
            Q = 0;
        180 < Math.abs(h) && (Q = 1);
        e = a.set();
        var M;
        z && (x = r(10 * x), y = r(10 * y), D = r(10 * D), I = r(10 * I), A = r(10 * A), B = r(10 * B), C = r(10 * C), u = r(10 * u), b = r(10 * b), l = r(10 * l), c = r(10 * c), f *= 10, g *= 10, k *= 10, w *= 10, 1 > Math.abs(h) && 1 >= Math.abs(D - y) && 1 >= Math.abs(C - B) && (M = !0));
        h = "";
        var P;
        q && (H["fill-opacity"] = 0, H["stroke-opacity"] = m["stroke-opacity"] / 2, H.stroke = m.stroke);
        if (0 < l) {
            P = " M" + x + "," + (A + l) + " L" + y + "," + (B + l);
            z ? (M || (P += " A" + (b - f) + "," + (l + c - g) + "," + (b + f) + "," + (l + c + g) + "," + y + "," + (B + l) + "," + D + "," + (C + l)), P += " L" + I + "," + (u + l), 0 < k && (M || (P += " B" + (b - k) + "," + (l + c - w) + "," + (b + k) + "," +
                (l + c + w) + "," + I + "," + (l + u) + "," + x + "," + (l + A)))) : (P += " A" + f + "," + g + ",0," + Q + ",1," + D + "," + (C + l) + " L" + I + "," + (u + l), 0 < k && (P += " A" + k + "," + w + ",0," + Q + ",0," + x + "," + (A + l)));
            P += " Z";
            var ia = l;
            z && (ia /= 10);
            for (var J = 0; J < ia; J += 10) {
                var aa = a.path(P, void 0, void 0, "1000,1000").attr(H);
                e.push(aa);
                aa.translate(0, -J)
            }
            P = a.path(" M" + x + "," + A + " L" + x + "," + (A + l) + " L" + y + "," + (B + l) + " L" + y + "," + B + " L" + x + "," + A + " Z", void 0, void 0, "1000,1000").attr(H);
            l = a.path(" M" + D + "," + C + " L" + D + "," + (C + l) + " L" + I + "," + (u + l) + " L" + I + "," + u + " L" + D + "," + C + " Z", void 0,
                void 0, "1000,1000").attr(H);
            e.push(P);
            e.push(l)
        }
        z ? (M || (h = " A" + r(b - f) + "," + r(c - g) + "," + r(b + f) + "," + r(c + g) + "," + r(y) + "," + r(B) + "," + r(D) + "," + r(C)), g = " M" + r(x) + "," + r(A) + " L" + r(y) + "," + r(B) + h + " L" + r(I) + "," + r(u)) : g = " M" + x + "," + A + " L" + y + "," + B + (" A" + f + "," + g + ",0," + Q + ",1," + D + "," + C) + " L" + I + "," + u;
        0 < k && (z ? M || (g += " B" + (b - k) + "," + (c - w) + "," + (b + k) + "," + (c + w) + "," + I + "," + u + "," + x + "," + A) : g += " A" + k + "," + w + ",0," + Q + ",0," + x + "," + A);
        a.handDrawn && (k = d.line(a, [x, y], [A, B], m.stroke, m.thickness * Math.random() * a.handDrawThickness, m["stroke-opacity"]),
            e.push(k));
        a = a.path(g + " Z", void 0, void 0, "1000,1000").attr(m);
        if (p) {
            k = [];
            for (w = 0; w < p.length; w++) k.push(d.adjustLuminosity(m.fill, p[w]));
            "radial" != t || d.isModern || (k = []);
            0 < k.length && a.gradient(t + "Gradient", k)
        }
        d.isModern && "radial" == t && a.grad && (a.grad.setAttribute("gradientUnits", "userSpaceOnUse"), a.grad.setAttribute("r", f), a.grad.setAttribute("cx", b), a.grad.setAttribute("cy", c));
        a.pattern(q, NaN, n);
        e.wedge = a;
        e.push(a);
        return e
    };
    d.rgb2hex = function(a) {
        return (a = a.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i)) &&
            4 === a.length ? "#" + ("0" + parseInt(a[1], 10).toString(16)).slice(-2) + ("0" + parseInt(a[2], 10).toString(16)).slice(-2) + ("0" + parseInt(a[3], 10).toString(16)).slice(-2) : ""
    };
    d.adjustLuminosity = function(a, b) {
        a && -1 != a.indexOf("rgb") && (a = d.rgb2hex(a));
        a = String(a).replace(/[^0-9a-f]/gi, "");
        6 > a.length && (a = String(a[0]) + String(a[0]) + String(a[1]) + String(a[1]) + String(a[2]) + String(a[2]));
        b = b || 0;
        var c = "#",
            e, h;
        for (h = 0; 3 > h; h++) e = parseInt(a.substr(2 * h, 2), 16), e = Math.round(Math.min(Math.max(0, e + e * b), 255)).toString(16), c += ("00" +
            e).substr(e.length);
        return c
    }
})();
(function() {
    var d = window.AmCharts;
    d.Bezier = d.Class({
        construct: function(a, b, c, e, h, f, g, k, l, m, p) {
            var q = a.chart,
                n = d.bezierX,
                t = d.bezierY;
            isNaN(q.bezierX) || (n = q.bezierX);
            isNaN(q.bezierY) || (t = q.bezierY);
            isNaN(n) && (q.rotate ? (n = 20, t = 4) : (t = 20, n = 4));
            var r, w;
            "object" == typeof g && 1 < g.length && (w = !0, r = g, g = g[0]);
            "object" == typeof k && (k = k[0]);
            0 === k && (g = "none");
            f = {
                fill: g,
                "fill-opacity": k,
                "stroke-width": f
            };
            void 0 !== l && 0 < l && (f["stroke-dasharray"] = l);
            isNaN(h) || (f["stroke-opacity"] = h);
            e && (f.stroke = e);
            e = "M" + Math.round(b[0]) +
                "," + Math.round(c[0]) + " ";
            h = [];
            for (l = 0; l < b.length; l++) isNaN(b[l]) || isNaN(c[l]) ? (e += this.drawSegment(h, n, t), l < b.length - 1 && (e += "L" + b[l + 1] + "," + c[l + 1] + " "), h = []) : h.push({
                x: Number(b[l]),
                y: Number(c[l])
            });
            e += this.drawSegment(h, n, t);
            m ? e += m : d.VML || (e += "M0,0 L0,0");
            this.path = a.path(e).attr(f);
            this.node = this.path.node;
            w && this.path.gradient("linearGradient", r, p)
        },
        drawSegment: function(a, b, c) {
            var d = "";
            if (2 < a.length)
                for (var h = 0; h < a.length - 1; h++) {
                    var f = [],
                        g = a[h - 1],
                        k = a[h],
                        l = a[h + 1],
                        m = a[h + 2];
                    0 === h ? (f.push({
                            x: k.x,
                            y: k.y
                        }),
                        f.push({
                            x: k.x,
                            y: k.y
                        }), f.push({
                            x: l.x,
                            y: l.y
                        }), f.push({
                            x: m.x,
                            y: m.y
                        })) : h >= a.length - 2 ? (f.push({
                        x: g.x,
                        y: g.y
                    }), f.push({
                        x: k.x,
                        y: k.y
                    }), f.push({
                        x: l.x,
                        y: l.y
                    }), f.push({
                        x: l.x,
                        y: l.y
                    })) : (f.push({
                        x: g.x,
                        y: g.y
                    }), f.push({
                        x: k.x,
                        y: k.y
                    }), f.push({
                        x: l.x,
                        y: l.y
                    }), f.push({
                        x: m.x,
                        y: m.y
                    }));
                    g = [];
                    k = Math.round;
                    g.push({
                        x: k(f[1].x),
                        y: k(f[1].y)
                    });
                    g.push({
                        x: k((-f[0].x + b * f[1].x + f[2].x) / b),
                        y: k((-f[0].y + c * f[1].y + f[2].y) / c)
                    });
                    g.push({
                        x: k((f[1].x + b * f[2].x - f[3].x) / b),
                        y: k((f[1].y + c * f[2].y - f[3].y) / c)
                    });
                    g.push({
                        x: k(f[2].x),
                        y: k(f[2].y)
                    });
                    d += "C" +
                        g[1].x + "," + g[1].y + "," + g[2].x + "," + g[2].y + "," + g[3].x + "," + g[3].y + " "
                } else 1 < a.length && (d += "L" + a[1].x + "," + a[1].y);
            return d
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.AmDraw = d.Class({
        construct: function(a, b, c, e) {
            d.SVG_NS = "http://www.w3.org/2000/svg";
            d.SVG_XLINK = "http://www.w3.org/1999/xlink";
            d.hasSVG = !!document.createElementNS && !!document.createElementNS(d.SVG_NS, "svg").createSVGRect;
            1 > b && (b = 10);
            1 > c && (c = 10);
            this.div = a;
            this.width = b;
            this.height = c;
            this.rBin = document.createElement("div");
            d.hasSVG ? (d.SVG = !0, b = this.createSvgElement("svg"), a.appendChild(b), this.container = b, this.addDefs(e), this.R = new d.SVGRenderer(this)) : d.isIE && d.VMLRenderer &&
                (d.VML = !0, d.vmlStyleSheet || (document.namespaces.add("amvml", "urn:schemas-microsoft-com:vml"), 31 > document.styleSheets.length ? (b = document.createStyleSheet(), b.addRule(".amvml", "behavior:url(#default#VML); display:inline-block; antialias:true"), d.vmlStyleSheet = b) : document.styleSheets[0].addRule(".amvml", "behavior:url(#default#VML); display:inline-block; antialias:true")), this.container = a, this.R = new d.VMLRenderer(this, e), this.R.disableSelection(a))
        },
        createSvgElement: function(a) {
            return document.createElementNS(d.SVG_NS,
                a)
        },
        circle: function(a, b, c, e) {
            var h = new d.AmDObject("circle", this);
            h.attr({
                r: c,
                cx: a,
                cy: b
            });
            this.addToContainer(h.node, e);
            return h
        },
        ellipse: function(a, b, c, e, h) {
            var f = new d.AmDObject("ellipse", this);
            f.attr({
                rx: c,
                ry: e,
                cx: a,
                cy: b
            });
            this.addToContainer(f.node, h);
            return f
        },
        setSize: function(a, b) {
            0 < a && 0 < b && (this.container.style.width = a + "px", this.container.style.height = b + "px")
        },
        rect: function(a, b, c, e, h, f, g) {
            var k = new d.AmDObject("rect", this);
            d.VML && (h = Math.round(100 * h / Math.min(c, e)), c += 2 * f, e += 2 * f, k.bw = f, k.node.style.marginLeft = -f, k.node.style.marginTop = -f);
            1 > c && (c = 1);
            1 > e && (e = 1);
            k.attr({
                x: a,
                y: b,
                width: c,
                height: e,
                rx: h,
                ry: h,
                "stroke-width": f
            });
            this.addToContainer(k.node, g);
            return k
        },
        image: function(a, b, c, e, h, f) {
            var g = new d.AmDObject("image", this);
            g.attr({
                x: b,
                y: c,
                width: e,
                height: h
            });
            this.R.path(g, a);
            this.addToContainer(g.node, f);
            return g
        },
        addToContainer: function(a, b) {
            b || (b = this.container);
            b.appendChild(a)
        },
        text: function(a, b, c) {
            return this.R.text(a, b, c)
        },
        path: function(a, b, c, e) {
            var h = new d.AmDObject("path", this);
            e || (e = "100,100");
            h.attr({
                cs: e
            });
            c ? h.attr({
                dd: a
            }) : h.attr({
                d: a
            });
            this.addToContainer(h.node, b);
            return h
        },
        set: function(a) {
            return this.R.set(a)
        },
        remove: function(a) {
            if (a) {
                var b = this.rBin;
                b.appendChild(a);
                b.innerHTML = ""
            }
        },
        renderFix: function() {
            var a = this.container,
                b = a.style;
            b.top = "0px";
            b.left = "0px";
            try {
                var c = a.getBoundingClientRect(),
                    d = c.left - Math.round(c.left),
                    h = c.top - Math.round(c.top);
                d && (b.left = d + "px");
                h && (b.top = h + "px")
            } catch (f) {}
        },
        update: function() {
            this.R.update()
        },
        addDefs: function(a) {
            if (d.hasSVG) {
                var b = this.createSvgElement("desc"),
                    c = this.container;
                c.setAttribute("version", "1.1");
                c.style.position = "absolute";
                this.setSize(this.width, this.height);
                if (a.accessibleTitle) {
                    var e = this.createSvgElement("text");
                    c.appendChild(e);
                    e.innerHTML = a.accessibleTitle;
                    e.style.opacity = 0
                }
                d.rtl && (c.setAttribute("direction", "rtl"), c.style.left = "auto", c.style.right = "0px");
                a && (a.addCodeCredits && b.appendChild(document.createTextNode("JavaScript chart by amCharts " + a.version)), c.appendChild(b), a.defs && (b = this.createSvgElement("defs"), c.appendChild(b), d.parseDefs(a.defs,
                    b), this.defs = b))
            }
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.AmDObject = d.Class({
        construct: function(a, b) {
            this.D = b;
            this.R = b.R;
            this.node = this.R.create(this, a);
            this.y = this.x = 0;
            this.scale = 1
        },
        attr: function(a) {
            this.R.attr(this, a);
            return this
        },
        getAttr: function(a) {
            return this.node.getAttribute(a)
        },
        setAttr: function(a, b) {
            this.R.setAttr(this, a, b);
            return this
        },
        clipRect: function(a, b, c, d) {
            this.R.clipRect(this, a, b, c, d)
        },
        translate: function(a, b, c, d) {
            d || (a = Math.round(a), b = Math.round(b));
            this.R.move(this, a, b, c);
            this.x = a;
            this.y = b;
            this.scale =
                c;
            this.angle && this.rotate(this.angle)
        },
        rotate: function(a, b) {
            this.R.rotate(this, a, b);
            this.angle = a
        },
        animate: function(a, b, c) {
            for (var e in a)
                if (a.hasOwnProperty(e)) {
                    var h = e,
                        f = a[e];
                    c = d.getEffect(c);
                    this.R.animate(this, h, f, b, c)
                }
        },
        push: function(a) {
            if (a) {
                var b = this.node;
                b.appendChild(a.node);
                var c = a.clipPath;
                c && b.appendChild(c);
                (a = a.grad) && b.appendChild(a)
            }
        },
        text: function(a) {
            this.R.setText(this, a)
        },
        remove: function() {
            this.stop();
            this.R.remove(this)
        },
        clear: function() {
            var a = this.node;
            if (a.hasChildNodes())
                for (; 1 <=
                    a.childNodes.length;) a.removeChild(a.firstChild)
        },
        hide: function() {
            this.setAttr("visibility", "hidden")
        },
        show: function() {
            this.setAttr("visibility", "visible")
        },
        getBBox: function() {
            return this.R.getBBox(this)
        },
        toFront: function() {
            var a = this.node;
            if (a) {
                this.prevNextNode = a.nextSibling;
                var b = a.parentNode;
                b && b.appendChild(a)
            }
        },
        toPrevious: function() {
            var a = this.node;
            a && this.prevNextNode && (a = a.parentNode) && a.insertBefore(this.prevNextNode, null)
        },
        toBack: function() {
            var a = this.node;
            if (a) {
                this.prevNextNode = a.nextSibling;
                var b = a.parentNode;
                if (b) {
                    var c = b.firstChild;
                    c && b.insertBefore(a, c)
                }
            }
        },
        mouseover: function(a) {
            this.R.addListener(this, "mouseover", a);
            return this
        },
        mouseout: function(a) {
            this.R.addListener(this, "mouseout", a);
            return this
        },
        click: function(a) {
            this.R.addListener(this, "click", a);
            return this
        },
        dblclick: function(a) {
            this.R.addListener(this, "dblclick", a);
            return this
        },
        mousedown: function(a) {
            this.R.addListener(this, "mousedown", a);
            return this
        },
        mouseup: function(a) {
            this.R.addListener(this, "mouseup", a);
            return this
        },
        touchmove: function(a) {
            this.R.addListener(this,
                "touchmove", a);
            return this
        },
        touchstart: function(a) {
            this.R.addListener(this, "touchstart", a);
            return this
        },
        touchend: function(a) {
            this.R.addListener(this, "touchend", a);
            return this
        },
        keyup: function(a) {
            this.R.addListener(this, "keyup", a);
            return this
        },
        focus: function(a) {
            this.R.addListener(this, "focus", a);
            return this
        },
        blur: function(a) {
            this.R.addListener(this, "blur", a);
            return this
        },
        contextmenu: function(a) {
            this.node.addEventListener ? this.node.addEventListener("contextmenu", a, !0) : this.R.addListener(this, "contextmenu",
                a);
            return this
        },
        stop: function() {
            d.removeFromArray(this.R.animations, this.an_translate);
            d.removeFromArray(this.R.animations, this.an_y);
            d.removeFromArray(this.R.animations, this.an_x)
        },
        length: function() {
            return this.node.childNodes.length
        },
        gradient: function(a, b, c) {
            this.R.gradient(this, a, b, c)
        },
        pattern: function(a, b, c) {
            a && this.R.pattern(this, a, b, c)
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.VMLRenderer = d.Class({
        construct: function(a, b) {
            this.chart = b;
            this.D = a;
            this.cNames = {
                circle: "oval",
                ellipse: "oval",
                rect: "roundrect",
                path: "shape"
            };
            this.styleMap = {
                x: "left",
                y: "top",
                width: "width",
                height: "height",
                "font-family": "fontFamily",
                "font-size": "fontSize",
                visibility: "visibility"
            }
        },
        create: function(a, b) {
            var c;
            if ("group" == b) c = document.createElement("div"), a.type = "div";
            else if ("text" == b) c = document.createElement("div"), a.type = "text";
            else if ("image" == b) c = document.createElement("img"),
                a.type = "image";
            else {
                a.type = "shape";
                a.shapeType = this.cNames[b];
                c = document.createElement("amvml:" + this.cNames[b]);
                var d = document.createElement("amvml:stroke");
                c.appendChild(d);
                a.stroke = d;
                var h = document.createElement("amvml:fill");
                c.appendChild(h);
                a.fill = h;
                h.className = "amvml";
                d.className = "amvml";
                c.className = "amvml"
            }
            c.style.position = "absolute";
            c.style.top = 0;
            c.style.left = 0;
            return c
        },
        path: function(a, b) {
            a.node.setAttribute("src", b)
        },
        setAttr: function(a, b, c) {
            if (void 0 !== c) {
                var e;
                8 === document.documentMode &&
                    (e = !0);
                var h = a.node,
                    f = a.type,
                    g = h.style;
                "r" == b && (g.width = 2 * c, g.height = 2 * c);
                "oval" == a.shapeType && ("rx" == b && (g.width = 2 * c), "ry" == b && (g.height = 2 * c));
                "roundrect" == a.shapeType && ("width" != b && "height" != b || --c);
                "cursor" == b && (g.cursor = c);
                "cx" == b && (g.left = c - d.removePx(g.width) / 2);
                "cy" == b && (g.top = c - d.removePx(g.height) / 2);
                var k = this.styleMap[b];
                "width" == k && 0 > c && (c = 0);
                void 0 !== k && (g[k] = c);
                "text" == f && ("text-anchor" == b && (a.anchor = c, k = h.clientWidth, "end" == c && (g.marginLeft = -k + "px"), "middle" == c && (g.marginLeft = -(k / 2) +
                    "px", g.textAlign = "center"), "start" == c && (g.marginLeft = "0px")), "fill" == b && (g.color = c), "font-weight" == b && (g.fontWeight = c));
                if (g = a.children)
                    for (k = 0; k < g.length; k++) g[k].setAttr(b, c);
                if ("shape" == f) {
                    "cs" == b && (h.style.width = "100px", h.style.height = "100px", h.setAttribute("coordsize", c));
                    "d" == b && h.setAttribute("path", this.svgPathToVml(c));
                    "dd" == b && h.setAttribute("path", c);
                    f = a.stroke;
                    a = a.fill;
                    "stroke" == b && (e ? f.color = c : f.setAttribute("color", c));
                    "stroke-width" == b && (e ? f.weight = c : f.setAttribute("weight", c));
                    "stroke-opacity" ==
                    b && (e ? f.opacity = c : f.setAttribute("opacity", c));
                    "stroke-dasharray" == b && (g = "solid", 0 < c && 3 > c && (g = "dot"), 3 <= c && 6 >= c && (g = "dash"), 6 < c && (g = "longdash"), e ? f.dashstyle = g : f.setAttribute("dashstyle", g));
                    if ("fill-opacity" == b || "opacity" == b) 0 === c ? e ? a.on = !1 : a.setAttribute("on", !1) : e ? a.opacity = c : a.setAttribute("opacity", c);
                    "fill" == b && (e ? a.color = c : a.setAttribute("color", c));
                    "rx" == b && (e ? h.arcSize = c + "%" : h.setAttribute("arcsize", c + "%"))
                }
            }
        },
        attr: function(a, b) {
            for (var c in b) b.hasOwnProperty(c) && this.setAttr(a, c, b[c])
        },
        text: function(a, b, c) {
            var e = new d.AmDObject("text", this.D),
                h = e.node;
            h.style.whiteSpace = "pre";
            h.innerHTML = a;
            this.D.addToContainer(h, c);
            this.attr(e, b);
            return e
        },
        getBBox: function(a) {
            return this.getBox(a.node)
        },
        getBox: function(a) {
            var b = a.offsetLeft,
                c = a.offsetTop,
                d = a.offsetWidth,
                h = a.offsetHeight,
                f;
            if (a.hasChildNodes()) {
                var g, k, l;
                for (l = 0; l < a.childNodes.length; l++) {
                    f = this.getBox(a.childNodes[l]);
                    var m = f.x;
                    isNaN(m) || (isNaN(g) ? g = m : m < g && (g = m));
                    var p = f.y;
                    isNaN(p) || (isNaN(k) ? k = p : p < k && (k = p));
                    m = f.width + m;
                    isNaN(m) ||
                        (d = Math.max(d, m));
                    f = f.height + p;
                    isNaN(f) || (h = Math.max(h, f))
                }
                0 > g && (b += g);
                0 > k && (c += k)
            }
            return {
                x: b,
                y: c,
                width: d,
                height: h
            }
        },
        setText: function(a, b) {
            var c = a.node;
            c && (c.innerHTML = b);
            this.setAttr(a, "text-anchor", a.anchor)
        },
        addListener: function(a, b, c) {
            a.node["on" + b] = c
        },
        move: function(a, b, c) {
            var e = a.node,
                h = e.style;
            "text" == a.type && (c -= d.removePx(h.fontSize) / 2 - 1);
            "oval" == a.shapeType && (b -= d.removePx(h.width) / 2, c -= d.removePx(h.height) / 2);
            a = a.bw;
            isNaN(a) || (b -= a, c -= a);
            isNaN(b) || isNaN(c) || (e.style.left = b + "px", e.style.top =
                c + "px")
        },
        svgPathToVml: function(a) {
            var b = a.split(" ");
            a = "";
            var c, d = Math.round,
                h;
            for (h = 0; h < b.length; h++) {
                var f = b[h],
                    g = f.substring(0, 1),
                    f = f.substring(1),
                    k = f.split(","),
                    l = d(k[0]) + "," + d(k[1]);
                "M" == g && (a += " m " + l);
                "L" == g && (a += " l " + l);
                "Z" == g && (a += " x e");
                if ("Q" == g) {
                    var m = c.length,
                        p = c[m - 1],
                        q = k[0],
                        n = k[1],
                        l = k[2],
                        t = k[3];
                    c = d(c[m - 2] / 3 + 2 / 3 * q);
                    p = d(p / 3 + 2 / 3 * n);
                    q = d(2 / 3 * q + l / 3);
                    n = d(2 / 3 * n + t / 3);
                    a += " c " + c + "," + p + "," + q + "," + n + "," + l + "," + t
                }
                "C" == g && (a += " c " + k[0] + "," + k[1] + "," + k[2] + "," + k[3] + "," + k[4] + "," + k[5]);
                "A" == g && (a += " wa " +
                    f);
                "B" == g && (a += " at " + f);
                c = k
            }
            return a
        },
        animate: function(a, b, c, d, h) {
            var f = a.node,
                g = this.chart;
            a.animationFinished = !1;
            if ("translate" == b) {
                b = c.split(",");
                c = b[1];
                var k = f.offsetTop;
                g.animate(a, "left", f.offsetLeft, b[0], d, h, "px");
                g.animate(a, "top", k, c, d, h, "px")
            }
        },
        clipRect: function(a, b, c, d, h) {
            a = a.node;
            0 === b && 0 === c ? (a.style.width = d + "px", a.style.height = h + "px", a.style.overflow = "hidden") : a.style.clip = "rect(" + c + "px " + (b + d) + "px " + (c + h) + "px " + b + "px)"
        },
        rotate: function(a, b, c) {
            if (0 !== Number(b)) {
                var e = a.node;
                a = e.style;
                c || (c = this.getBGColor(e.parentNode));
                a.backgroundColor = c;
                a.paddingLeft = 1;
                c = b * Math.PI / 180;
                var h = Math.cos(c),
                    f = Math.sin(c),
                    g = d.removePx(a.left),
                    k = d.removePx(a.top),
                    l = e.offsetWidth,
                    e = e.offsetHeight;
                b /= Math.abs(b);
                a.left = g + l / 2 - l / 2 * Math.cos(c) - b * e / 2 * Math.sin(c) + 3;
                a.top = k - b * l / 2 * Math.sin(c) + b * e / 2 * Math.sin(c);
                a.cssText = a.cssText + "; filter:progid:DXImageTransform.Microsoft.Matrix(M11='" + h + "', M12='" + -f + "', M21='" + f + "', M22='" + h + "', sizingmethod='auto expand');"
            }
        },
        getBGColor: function(a) {
            var b = "#FFFFFF";
            if (a.style) {
                var c =
                    a.style.backgroundColor;
                "" !== c ? b = c : a.parentNode && (b = this.getBGColor(a.parentNode))
            }
            return b
        },
        set: function(a) {
            var b = new d.AmDObject("group", this.D);
            this.D.container.appendChild(b.node);
            if (a) {
                var c;
                for (c = 0; c < a.length; c++) b.push(a[c])
            }
            return b
        },
        gradient: function(a, b, c, d) {
            var h = "";
            "radialGradient" == b && (b = "gradientradial", c.reverse());
            "linearGradient" == b && (b = "gradient");
            var f;
            for (f = 0; f < c.length; f++) h += Math.round(100 * f / (c.length - 1)) + "% " + c[f], f < c.length - 1 && (h += ",");
            a = a.fill;
            90 == d ? d = 0 : 270 == d ? d = 180 : 180 ==
                d ? d = 90 : 0 === d && (d = 270);
            8 === document.documentMode ? (a.type = b, a.angle = d) : (a.setAttribute("type", b), a.setAttribute("angle", d));
            h && (a.colors.value = h)
        },
        remove: function(a) {
            a.clipPath && this.D.remove(a.clipPath);
            this.D.remove(a.node)
        },
        disableSelection: function(a) {
            a.onselectstart = function() {
                return !1
            };
            a.style.cursor = "default"
        },
        pattern: function(a, b, c, e) {
            c = a.node;
            a = a.fill;
            var h = "none";
            b.color && (h = b.color);
            c.fillColor = h;
            b = b.url;
            d.isAbsolute(b) || (b = e + b);
            8 === document.documentMode ? (a.type = "tile", a.src = b) : (a.setAttribute("type",
                "tile"), a.setAttribute("src", b))
        },
        update: function() {}
    })
})();
(function() {
    var d = window.AmCharts;
    d.SVGRenderer = d.Class({
        construct: function(a) {
            this.D = a;
            this.animations = []
        },
        create: function(a, b) {
            return document.createElementNS(d.SVG_NS, b)
        },
        attr: function(a, b) {
            for (var c in b) b.hasOwnProperty(c) && this.setAttr(a, c, b[c])
        },
        setAttr: function(a, b, c) {
            void 0 !== c && a.node.setAttribute(b, c)
        },
        animate: function(a, b, c, e, h) {
            a.animationFinished = !1;
            var f = a.node;
            a["an_" + b] && d.removeFromArray(this.animations, a["an_" + b]);
            "translate" == b ? (f = (f = f.getAttribute("transform")) ? String(f).substring(10,
                f.length - 1) : "0,0", f = f.split(", ").join(" "), f = f.split(" ").join(","), 0 === f && (f = "0,0")) : f = Number(f.getAttribute(b));
            c = {
                obj: a,
                frame: 0,
                attribute: b,
                from: f,
                to: c,
                time: e,
                effect: h
            };
            this.animations.push(c);
            a["an_" + b] = c
        },
        update: function() {
            var a, b = this.animations;
            for (a = b.length - 1; 0 <= a; a--) {
                var c = b[a],
                    e = c.time * d.updateRate,
                    h = c.frame + 1,
                    f = c.obj,
                    g = c.attribute,
                    k, l, m;
                if (h <= e) {
                    c.frame++;
                    if ("translate" == g) {
                        k = c.from.split(",");
                        g = Number(k[0]);
                        k = Number(k[1]);
                        isNaN(k) && (k = 0);
                        l = c.to.split(",");
                        m = Number(l[0]);
                        l = Number(l[1]);
                        m = 0 === m - g ? m : Math.round(d[c.effect](0, h, g, m - g, e));
                        c = 0 === l - k ? l : Math.round(d[c.effect](0, h, k, l - k, e));
                        g = "transform";
                        if (isNaN(m) || isNaN(c)) continue;
                        c = "translate(" + m + "," + c + ")"
                    } else l = Number(c.from), k = Number(c.to), m = k - l, c = d[c.effect](0, h, l, m, e), isNaN(c) && (c = k), 0 === m && this.animations.splice(a, 1);
                    this.setAttr(f, g, c)
                } else "translate" == g ? (l = c.to.split(","), m = Number(l[0]), l = Number(l[1]), f.translate(m, l)) : (k = Number(c.to), this.setAttr(f, g, k)), f.animationFinished = !0, this.animations.splice(a, 1)
            }
        },
        getBBox: function(a) {
            if (a =
                a.node) try {
                return a.getBBox()
            } catch (b) {}
            return {
                width: 0,
                height: 0,
                x: 0,
                y: 0
            }
        },
        path: function(a, b) {
            a.node.setAttributeNS(d.SVG_XLINK, "xlink:href", b)
        },
        clipRect: function(a, b, c, e, h) {
            var f = a.node,
                g = a.clipPath;
            g && this.D.remove(g);
            var k = f.parentNode;
            k && (f = document.createElementNS(d.SVG_NS, "clipPath"), g = d.getUniqueId(), f.setAttribute("id", g), this.D.rect(b, c, e, h, 0, 0, f), k.appendChild(f), b = "#", d.baseHref && !d.isIE && (b = this.removeTarget(window.location.href) + b), this.setAttr(a, "clip-path", "url(" + b + g + ")"), this.clipPathC++,
                a.clipPath = f)
        },
        text: function(a, b, c) {
            var e = new d.AmDObject("text", this.D);
            a = String(a).split("\n");
            var h = d.removePx(b["font-size"]),
                f;
            for (f = 0; f < a.length; f++) {
                var g = this.create(null, "tspan");
                g.appendChild(document.createTextNode(a[f]));
                g.setAttribute("y", (h + 2) * f + Math.round(h / 2));
                g.setAttribute("x", 0);
                e.node.appendChild(g)
            }
            e.node.setAttribute("y", Math.round(h / 2));
            this.attr(e, b);
            this.D.addToContainer(e.node, c);
            return e
        },
        setText: function(a, b) {
            var c = a.node;
            c && (c.removeChild(c.firstChild), c.appendChild(document.createTextNode(b)))
        },
        move: function(a, b, c, d) {
            isNaN(b) && (b = 0);
            isNaN(c) && (c = 0);
            b = "translate(" + b + "," + c + ")";
            d && (b = b + " scale(" + d + ")");
            this.setAttr(a, "transform", b)
        },
        rotate: function(a, b) {
            var c = a.node.getAttribute("transform"),
                d = "rotate(" + b + ")";
            c && (d = c + " " + d);
            this.setAttr(a, "transform", d)
        },
        set: function(a) {
            var b = new d.AmDObject("g", this.D);
            this.D.container.appendChild(b.node);
            if (a) {
                var c;
                for (c = 0; c < a.length; c++) b.push(a[c])
            }
            return b
        },
        addListener: function(a, b, c) {
            a.node["on" + b] = c
        },
        gradient: function(a, b, c, e) {
            var h = a.node,
                f = a.grad;
            f && this.D.remove(f);
            b = document.createElementNS(d.SVG_NS, b);
            f = d.getUniqueId();
            b.setAttribute("id", f);
            if (!isNaN(e)) {
                var g = 0,
                    k = 0,
                    l = 0,
                    m = 0;
                90 == e ? l = 100 : 270 == e ? m = 100 : 180 == e ? g = 100 : 0 === e && (k = 100);
                b.setAttribute("x1", g + "%");
                b.setAttribute("x2", k + "%");
                b.setAttribute("y1", l + "%");
                b.setAttribute("y2", m + "%")
            }
            for (e = 0; e < c.length; e++) g = document.createElementNS(d.SVG_NS, "stop"), k = 100 * e / (c.length - 1), 0 === e && (k = 0), g.setAttribute("offset", k + "%"), g.setAttribute("stop-color", c[e]), b.appendChild(g);
            h.parentNode.appendChild(b);
            c = "#";
            d.baseHref && !d.isIE && (c = this.removeTarget(window.location.href) + c);
            h.setAttribute("fill", "url(" + c + f + ")");
            a.grad = b
        },
        removeTarget: function(a) {
            return a.split("#")[0]
        },
        pattern: function(a, b, c, e) {
            var h = a.node;
            isNaN(c) && (c = 1);
            var f = a.patternNode;
            f && this.D.remove(f);
            var f = document.createElementNS(d.SVG_NS, "pattern"),
                g = d.getUniqueId(),
                k = b;
            b.url && (k = b.url);
            d.isAbsolute(k) || -1 != k.indexOf("data:image") || (k = e + k);
            e = Number(b.width);
            isNaN(e) && (e = 4);
            var l = Number(b.height);
            isNaN(l) && (l = 4);
            e /= c;
            l /= c;
            c = b.x;
            isNaN(c) &&
                (c = 0);
            var m = -Math.random() * Number(b.randomX);
            isNaN(m) || (c = m);
            m = b.y;
            isNaN(m) && (m = 0);
            var p = -Math.random() * Number(b.randomY);
            isNaN(p) || (m = p);
            f.setAttribute("id", g);
            f.setAttribute("width", e);
            f.setAttribute("height", l);
            f.setAttribute("patternUnits", "userSpaceOnUse");
            f.setAttribute("xlink:href", k);
            b.color && (p = document.createElementNS(d.SVG_NS, "rect"), p.setAttributeNS(null, "height", e), p.setAttributeNS(null, "width", l), p.setAttributeNS(null, "fill", b.color), f.appendChild(p));
            this.D.image(k, 0, 0, e, l, f).translate(c,
                m);
            k = "#";
            d.baseHref && !d.isIE && (k = this.removeTarget(window.location.href) + k);
            h.setAttribute("fill", "url(" + k + g + ")");
            a.patternNode = f;
            h.parentNode.appendChild(f)
        },
        remove: function(a) {
            a.clipPath && this.D.remove(a.clipPath);
            a.grad && this.D.remove(a.grad);
            a.patternNode && this.D.remove(a.patternNode);
            this.D.remove(a.node)
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.AmLegend = d.Class({
        construct: function(a) {
            this.enabled = !0;
            this.cname = "AmLegend";
            this.createEvents("rollOverMarker", "rollOverItem", "rollOutMarker", "rollOutItem", "showItem", "hideItem", "clickMarker", "clickLabel");
            this.position = "bottom";
            this.borderColor = this.color = "#000000";
            this.borderAlpha = 0;
            this.markerLabelGap = 5;
            this.verticalGap = 10;
            this.align = "left";
            this.horizontalGap = 0;
            this.spacing = 10;
            this.markerDisabledColor = "#AAB3B3";
            this.markerType = "square";
            this.markerSize = 16;
            this.markerBorderThickness =
                this.markerBorderAlpha = 1;
            this.marginBottom = this.marginTop = 0;
            this.marginLeft = this.marginRight = 20;
            this.autoMargins = !0;
            this.valueWidth = 50;
            this.switchable = !0;
            this.switchType = "x";
            this.switchColor = "#FFFFFF";
            this.rollOverColor = "#CC0000";
            this.reversedOrder = !1;
            this.labelText = "[[title]]";
            this.valueText = "[[value]]";
            this.accessibleLabel = "[[title]]";
            this.useMarkerColorForLabels = !1;
            this.rollOverGraphAlpha = 1;
            this.textClickEnabled = !1;
            this.equalWidths = !0;
            this.backgroundColor = "#FFFFFF";
            this.backgroundAlpha = 0;
            this.useGraphSettings = !1;
            this.showEntries = !0;
            this.labelDx = 0;
            d.applyTheme(this, a, this.cname)
        },
        setData: function(a) {
            this.legendData = a;
            this.invalidateSize()
        },
        invalidateSize: function() {
            this.destroy();
            this.entries = [];
            this.valueLabels = [];
            var a = this.legendData;
            this.enabled && (d.ifArray(a) || d.ifArray(this.data)) && this.drawLegend()
        },
        drawLegend: function() {
            var a = this.chart,
                b = this.position,
                c = this.width,
                e = a.divRealWidth,
                h = a.divRealHeight,
                f = this.div,
                g = this.legendData;
            this.data && (g = this.combineLegend ? this.legendData.concat(this.data) :
                this.data);
            isNaN(this.fontSize) && (this.fontSize = a.fontSize);
            this.maxColumnsReal = this.maxColumns;
            if ("right" == b || "left" == b) this.maxColumnsReal = 1, this.autoMargins && (this.marginLeft = this.marginRight = 10);
            else if (this.autoMargins) {
                this.marginRight = a.marginRight;
                this.marginLeft = a.marginLeft;
                var k = a.autoMarginOffset;
                "bottom" == b ? (this.marginBottom = k, this.marginTop = 0) : (this.marginTop = k, this.marginBottom = 0)
            }
            c = void 0 !== c ? d.toCoordinate(c, e) : "right" != b && "left" != b ? a.realWidth : 0 < this.ieW ? this.ieW : a.realWidth;
            "outside" ==
            b ? (c = f.offsetWidth, h = f.offsetHeight, f.clientHeight && (c = f.clientWidth, h = f.clientHeight)) : (isNaN(c) || (f.style.width = c + "px"), f.className = "amChartsLegend " + a.classNamePrefix + "-legend-div");
            this.divWidth = c;
            (b = this.container) ? (b.container.innerHTML = "", f.appendChild(b.container), b.width = c, b.height = h, b.setSize(c, h), b.addDefs(a)) : b = new d.AmDraw(f, c, h, a);
            this.container = b;
            this.lx = 0;
            this.ly = 8;
            h = this.markerSize;
            h > this.fontSize && (this.ly = h / 2 - 1);
            0 < h && (this.lx += h + this.markerLabelGap);
            this.titleWidth = 0;
            if (h = this.title) h =
                d.text(this.container, h, this.color, a.fontFamily, this.fontSize, "start", !0), d.setCN(a, h, "legend-title"), h.translate(this.marginLeft, this.marginTop + this.verticalGap + this.ly + 1), a = h.getBBox(), this.titleWidth = a.width + 15, this.titleHeight = a.height + 6;
            this.index = this.maxLabelWidth = 0;
            if (this.showEntries) {
                for (a = 0; a < g.length; a++) this.createEntry(g[a]);
                for (a = this.index = 0; a < g.length; a++) this.createValue(g[a])
            }
            this.arrangeEntries();
            this.updateValues()
        },
        arrangeEntries: function() {
            var a = this.position,
                b = this.marginLeft +
                this.titleWidth,
                c = this.marginRight,
                e = this.marginTop,
                h = this.marginBottom,
                f = this.horizontalGap,
                g = this.div,
                k = this.divWidth,
                l = this.maxColumnsReal,
                m = this.verticalGap,
                p = this.spacing,
                q = k - c - b,
                n = 0,
                t = 0,
                r = this.container;
            this.set && this.set.remove();
            var w = r.set();
            this.set = w;
            var z = r.set();
            w.push(z);
            var x = this.entries,
                u, A;
            for (A = 0; A < x.length; A++) {
                u = x[A].getBBox();
                var y = u.width;
                y > n && (n = y);
                u = u.height;
                u > t && (t = u)
            }
            var y = t = 0,
                B = f,
                D = 0,
                C = 0;
            for (A = 0; A < x.length; A++) {
                var I = x[A];
                this.reversedOrder && (I = x[x.length - A - 1]);
                u = I.getBBox();
                var H;
                this.equalWidths ? H = y * (n + p + this.markerLabelGap) : (H = B, B = B + u.width + f + p);
                H + u.width > q && 0 < A && 0 !== y && (t++, H = y = 0, B = H + u.width + f + p, D = D + C + m, C = 0);
                u.height > C && (C = u.height);
                I.translate(H, D);
                y++;
                !isNaN(l) && y >= l && (y = 0, t++, D = D + C + m, B = f, C = 0);
                z.push(I)
            }
            u = z.getBBox();
            l = u.height + 2 * m - 1;
            "left" == a || "right" == a ? (p = u.width + 2 * f, k = p + b + c, g.style.width = k + "px", this.ieW = k) : p = k - b - c - 1;
            c = d.polygon(this.container, [0, p, p, 0], [0, 0, l, l], this.backgroundColor, this.backgroundAlpha, 1, this.borderColor, this.borderAlpha);
            d.setCN(this.chart,
                c, "legend-bg");
            w.push(c);
            w.translate(b, e);
            c.toBack();
            b = f;
            if ("top" == a || "bottom" == a || "absolute" == a || "outside" == a) "center" == this.align ? b = f + (p - u.width) / 2 : "right" == this.align && (b = f + p - u.width);
            z.translate(b, m + 1);
            this.titleHeight > l && (l = this.titleHeight);
            e = l + e + h + 1;
            0 > e && (e = 0);
            "absolute" != a && "outside" != a && e > this.chart.divRealHeight && (g.style.top = "0px");
            g.style.height = Math.round(e) + "px";
            r.setSize(this.divWidth, e)
        },
        createEntry: function(a) {
            if (!1 !== a.visibleInLegend && !a.hideFromLegend) {
                var b = this,
                    c = b.chart,
                    e = b.useGraphSettings,
                    h = a.markerType;
                h && (e = !1);
                a.legendEntryWidth = b.markerSize;
                h || (h = b.markerType);
                var f = a.color,
                    g = a.alpha;
                a.legendKeyColor && (f = a.legendKeyColor());
                a.legendKeyAlpha && (g = a.legendKeyAlpha());
                var k;
                !0 === a.hidden && (k = f = b.markerDisabledColor);
                var l = a.pattern,
                    m, p = a.customMarker;
                p || (p = b.customMarker);
                var q = b.container,
                    n = b.markerSize,
                    t = 0,
                    r = 0,
                    w = n / 2;
                if (e) {
                    e = a.type;
                    b.switchType = void 0;
                    if ("line" == e || "step" == e || "smoothedLine" == e || "ohlc" == e) m = q.set(), a.hidden || (f = a.lineColorR, k = a.bulletBorderColorR), t = d.line(q, [0, 2 *
                        n
                    ], [n / 2, n / 2], f, a.lineAlpha, a.lineThickness, a.dashLength), d.setCN(c, t, "graph-stroke"), m.push(t), a.bullet && (a.hidden || (f = a.bulletColorR), t = d.bullet(q, a.bullet, a.bulletSize, f, a.bulletAlpha, a.bulletBorderThickness, k, a.bulletBorderAlpha)) && (d.setCN(c, t, "graph-bullet"), t.translate(n + 1, n / 2), m.push(t)), w = 0, t = n, r = n / 3;
                    else {
                        a.getGradRotation && (m = a.getGradRotation(), 0 === m && (m = 180));
                        t = a.fillColorsR;
                        !0 === a.hidden && (t = f);
                        if (m = b.createMarker("rectangle", t, a.fillAlphas, a.lineThickness, f, a.lineAlpha, m, l, a.dashLength)) w =
                            n, m.translate(w, n / 2);
                        t = n
                    }
                    d.setCN(c, m, "graph-" + e);
                    d.setCN(c, m, "graph-" + a.id)
                } else if (p) m = q.image(p, 0, 0, n, n);
                else {
                    var z;
                    isNaN(b.gradientRotation) || (z = 180 + b.gradientRotation);
                    (m = b.createMarker(h, f, g, void 0, void 0, void 0, z, l)) && m.translate(n / 2, n / 2)
                }
                d.setCN(c, m, "legend-marker");
                b.addListeners(m, a);
                q = q.set([m]);
                b.switchable && a.switchable && q.setAttr("cursor", "pointer");
                void 0 !== a.id && d.setCN(c, q, "legend-item-" + a.id);
                d.setCN(c, q, a.className, !0);
                k = b.switchType;
                var x;
                k && "none" != k && 0 < n && ("x" == k ? (x = b.createX(),
                    x.translate(n / 2, n / 2)) : x = b.createV(), x.dItem = a, !0 !== a.hidden ? "x" == k ? x.hide() : x.show() : "x" != k && x.hide(), b.switchable || x.hide(), b.addListeners(x, a), a.legendSwitch = x, q.push(x), d.setCN(c, x, "legend-switch"));
                k = b.color;
                a.showBalloon && b.textClickEnabled && void 0 !== b.selectedColor && (k = b.selectedColor);
                b.useMarkerColorForLabels && !l && (k = f);
                !0 === a.hidden && (k = b.markerDisabledColor);
                f = d.massReplace(b.labelText, {
                    "[[title]]": a.title
                });
                void 0 !== b.tabIndex && (q.setAttr("tabindex", b.tabIndex), q.setAttr("role", "menuitem"),
                    q.keyup(function(c) {
                        13 == c.keyCode && b.clickMarker(a, c)
                    }));
                c.accessible && b.accessibleLabel && (l = d.massReplace(b.accessibleLabel, {
                    "[[title]]": a.title
                }), c.makeAccessible(q, l));
                l = b.fontSize;
                m && (n <= l && (n = n / 2 + b.ly - l / 2 + (l + 2 - n) / 2 - r, m.translate(w, n), x && x.translate(x.x, n)), a.legendEntryWidth = m.getBBox().width);
                var u;
                f && (f = d.fixBrakes(f), a.legendTextReal = f, u = b.labelWidth, u = isNaN(u) ? d.text(b.container, f, k, c.fontFamily, l, "start") : d.wrappedText(b.container, f, k, c.fontFamily, l, "start", !1, u, 0), d.setCN(c, u, "legend-label"),
                    u.translate(b.lx + t, b.ly), q.push(u), b.labelDx = t, c = u.getBBox().width, b.maxLabelWidth < c && (b.maxLabelWidth = c));
                b.entries[b.index] = q;
                a.legendEntry = b.entries[b.index];
                a.legendMarker = m;
                a.legendLabel = u;
                b.index++
            }
        },
        addListeners: function(a, b) {
            var c = this;
            a && a.mouseover(function(a) {
                c.rollOverMarker(b, a)
            }).mouseout(function(a) {
                c.rollOutMarker(b, a)
            }).click(function(a) {
                c.clickMarker(b, a)
            })
        },
        rollOverMarker: function(a, b) {
            this.switchable && this.dispatch("rollOverMarker", a, b);
            this.dispatch("rollOverItem", a, b)
        },
        rollOutMarker: function(a,
            b) {
            this.switchable && this.dispatch("rollOutMarker", a, b);
            this.dispatch("rollOutItem", a, b)
        },
        clickMarker: function(a, b) {
            this.switchable && (!0 === a.hidden ? this.dispatch("showItem", a, b) : this.dispatch("hideItem", a, b));
            this.dispatch("clickMarker", a, b)
        },
        rollOverLabel: function(a, b) {
            a.hidden || this.textClickEnabled && a.legendLabel && a.legendLabel.attr({
                fill: this.rollOverColor
            });
            this.dispatch("rollOverItem", a, b)
        },
        rollOutLabel: function(a, b) {
            if (!a.hidden && this.textClickEnabled && a.legendLabel) {
                var c = this.color;
                void 0 !==
                    this.selectedColor && a.showBalloon && (c = this.selectedColor);
                this.useMarkerColorForLabels && (c = a.lineColor, void 0 === c && (c = a.color));
                a.legendLabel.attr({
                    fill: c
                })
            }
            this.dispatch("rollOutItem", a, b)
        },
        clickLabel: function(a, b) {
            this.textClickEnabled ? a.hidden || this.dispatch("clickLabel", a, b) : this.switchable && (!0 === a.hidden ? this.dispatch("showItem", a, b) : this.dispatch("hideItem", a, b))
        },
        dispatch: function(a, b, c) {
            a = {
                type: a,
                dataItem: b,
                target: this,
                event: c,
                chart: this.chart
            };
            this.chart && this.chart.handleLegendEvent(a);
            this.fire(a)
        },
        createValue: function(a) {
            var b = this,
                c = b.fontSize,
                e = b.chart;
            if (!1 !== a.visibleInLegend && !a.hideFromLegend) {
                var h = b.maxLabelWidth;
                b.forceWidth && (h = b.labelWidth);
                b.equalWidths || (b.valueAlign = "left");
                "left" == b.valueAlign && a.legendLabel && (h = a.legendLabel.getBBox().width);
                var f = h;
                if (b.valueText && 0 < b.valueWidth) {
                    var g = b.color;
                    b.useMarkerColorForValues && (g = a.color, a.legendKeyColor && (g = a.legendKeyColor()));
                    !0 === a.hidden && (g = b.markerDisabledColor);
                    var k = b.valueText,
                        h = h + b.lx + b.labelDx + b.markerLabelGap +
                        b.valueWidth,
                        l = "end";
                    "left" == b.valueAlign && (h -= b.valueWidth, l = "start");
                    g = d.text(b.container, k, g, b.chart.fontFamily, c, l);
                    d.setCN(e, g, "legend-value");
                    g.translate(h, b.ly);
                    b.entries[b.index].push(g);
                    f += b.valueWidth + 2 * b.markerLabelGap;
                    g.dItem = a;
                    b.valueLabels.push(g)
                }
                b.index++;
                e = b.markerSize;
                e < c + 7 && (e = c + 7, d.VML && (e += 3));
                c = b.container.rect(a.legendEntryWidth, 0, f, e, 0, 0).attr({
                    stroke: "none",
                    fill: "#fff",
                    "fill-opacity": .005
                });
                c.dItem = a;
                b.entries[b.index - 1].push(c);
                c.mouseover(function(c) {
                    b.rollOverLabel(a, c)
                }).mouseout(function(c) {
                    b.rollOutLabel(a,
                        c)
                }).click(function(c) {
                    b.clickLabel(a, c)
                })
            }
        },
        createV: function() {
            var a = this.markerSize;
            return d.polygon(this.container, [a / 5, a / 2, a - a / 5, a / 2], [a / 3, a - a / 5, a / 5, a / 1.7], this.switchColor)
        },
        createX: function() {
            var a = (this.markerSize - 4) / 2,
                b = {
                    stroke: this.switchColor,
                    "stroke-width": 3
                },
                c = this.container,
                e = d.line(c, [-a, a], [-a, a]).attr(b),
                a = d.line(c, [-a, a], [a, -a]).attr(b);
            return this.container.set([e, a])
        },
        createMarker: function(a, b, c, e, h, f, g, k, l) {
            var m = this.markerSize,
                p = this.container;
            h || (h = this.markerBorderColor);
            h ||
                (h = b);
            isNaN(e) && (e = this.markerBorderThickness);
            isNaN(f) && (f = this.markerBorderAlpha);
            return d.bullet(p, a, m, b, c, e, h, f, m, g, k, this.chart.path, l)
        },
        validateNow: function() {
            this.invalidateSize()
        },
        updateValues: function() {
            var a = this.valueLabels,
                b = this.chart,
                c, e = this.data;
            if (a)
                for (c = 0; c < a.length; c++) {
                    var h = a[c],
                        f = h.dItem;
                    f.periodDataItem = void 0;
                    f.periodPercentDataItem = void 0;
                    var g = " ";
                    if (e) f.value ? h.text(f.value) : h.text("");
                    else {
                        var k = null;
                        if (void 0 !== f.type) {
                            var k = f.currentDataItem,
                                l = this.periodValueText;
                            f.legendPeriodValueText &&
                                (l = f.legendPeriodValueText);
                            f.legendPeriodValueTextR && (l = f.legendPeriodValueTextR);
                            k ? (g = this.valueText, f.legendValueText && (g = f.legendValueText), g = b.formatString(g, k)) : l && b.formatPeriodString && (l = d.massReplace(l, {
                                "[[title]]": f.title
                            }), g = b.formatPeriodString(l, f))
                        } else g = b.formatString(this.valueText, f);
                        l = f;
                        k && (l = k);
                        var m = this.valueFunction;
                        m && (g = m(l, g, b.periodDataItem));
                        var p;
                        this.useMarkerColorForLabels && !k && f.lastDataItem && (k = f.lastDataItem);
                        k ? p = b.getBalloonColor(f, k) : f.legendKeyColor && (p = f.legendKeyColor());
                        f.legendColorFunction && (p = f.legendColorFunction(l, g, f.periodDataItem, f.periodPercentDataItem));
                        h.text(g);
                        if (!f.pattern && (this.useMarkerColorForValues && h.setAttr("fill", p), this.useMarkerColorForLabels)) {
                            if (h = f.legendMarker) h.setAttr("fill", p), h.setAttr("stroke", p);
                            (f = f.legendLabel) && f.setAttr("fill", p)
                        }
                    }
                }
        },
        renderFix: function() {
            if (!d.VML && this.enabled) {
                var a = this.container;
                a && a.renderFix()
            }
        },
        destroy: function() {
            this.div.innerHTML = "";
            d.remove(this.set)
        }
    })
})();
(function() {
    var d = window.AmCharts;
    d.formatMilliseconds = function(a, b) {
        if (-1 != a.indexOf("fff")) {
            var c = b.getMilliseconds(),
                d = String(c);
            10 > c && (d = "00" + c);
            10 <= c && 100 > c && (d = "0" + c);
            a = a.replace(/fff/g, d)
        }
        return a
    };
    d.extractPeriod = function(a) {
        var b = d.stripNumbers(a),
            c = 1;
        b != a && (c = Number(a.slice(0, a.indexOf(b))));
        return {
            period: b,
            count: c
        }
    };
    d.getDate = function(a, b, c) {
        return a instanceof Date ? d.newDate(a, c) : b && isNaN(a) ? d.stringToDate(a, b) : new Date(a)
    };
    d.daysInMonth = function(a) {
        return (new Date(a.getYear(), a.getMonth() +
            1, 0)).getDate()
    };
    d.newDate = function(a, b) {
        return b && -1 == b.indexOf("fff") ? new Date(a) : new Date(a.getFullYear(), a.getMonth(), a.getDate(), a.getHours(), a.getMinutes(), a.getSeconds(), a.getMilliseconds())
    };
    d.resetDateToMin = function(a, b, c, e) {
        void 0 === e && (e = 1);
        var h, f, g, k, l, m, p;
        d.useUTC ? (h = a.getUTCFullYear(), f = a.getUTCMonth(), g = a.getUTCDate(), k = a.getUTCHours(), l = a.getUTCMinutes(), m = a.getUTCSeconds(), p = a.getUTCMilliseconds(), a = a.getUTCDay()) : (h = a.getFullYear(), f = a.getMonth(), g = a.getDate(), k = a.getHours(), l =
            a.getMinutes(), m = a.getSeconds(), p = a.getMilliseconds(), a = a.getDay());
        switch (b) {
            case "YYYY":
                h = Math.floor(h / c) * c;
                f = 0;
                g = 1;
                p = m = l = k = 0;
                break;
            case "MM":
                f = Math.floor(f / c) * c;
                g = 1;
                p = m = l = k = 0;
                break;
            case "WW":
                g = a >= e ? g - a + e : g - (7 + a) + e;
                p = m = l = k = 0;
                break;
            case "DD":
                p = m = l = k = 0;
                break;
            case "hh":
                k = Math.floor(k / c) * c;
                p = m = l = 0;
                break;
            case "mm":
                l = Math.floor(l / c) * c;
                p = m = 0;
                break;
            case "ss":
                m = Math.floor(m / c) * c;
                p = 0;
                break;
            case "fff":
                p = Math.floor(p / c) * c
        }
        d.useUTC ? (a = new Date, a.setUTCFullYear(h, f, g), a.setUTCHours(k, l, m, p)) : a = new Date(h, f, g,
            k, l, m, p);
        return a
    };
    d.getPeriodDuration = function(a, b) {
        void 0 === b && (b = 1);
        var c;
        switch (a) {
            case "YYYY":
                c = 316224E5;
                break;
            case "MM":
                c = 26784E5;
                break;
            case "WW":
                c = 6048E5;
                break;
            case "DD":
                c = 864E5;
                break;
            case "hh":
                c = 36E5;
                break;
            case "mm":
                c = 6E4;
                break;
            case "ss":
                c = 1E3;
                break;
            case "fff":
                c = 1
        }
        return c * b
    };
    d.intervals = {
        s: {
            nextInterval: "ss",
            contains: 1E3
        },
        ss: {
            nextInterval: "mm",
            contains: 60,
            count: 0
        },
        mm: {
            nextInterval: "hh",
            contains: 60,
            count: 1
        },
        hh: {
            nextInterval: "DD",
            contains: 24,
            count: 2
        },
        DD: {
            nextInterval: "",
            contains: Infinity,
            count: 3
        }
    };
    d.getMaxInterval = function(a, b) {
        var c = d.intervals;
        return a >= c[b].contains ? (a = Math.round(a / c[b].contains), b = c[b].nextInterval, d.getMaxInterval(a, b)) : "ss" == b ? c[b].nextInterval : b
    };
    d.dayNames = "Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" ");
    d.shortDayNames = "Sun Mon Tue Wed Thu Fri Sat".split(" ");
    d.monthNames = "January February March April May June July August September October November December".split(" ");
    d.shortMonthNames = "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" ");
    d.getWeekNumber = function(a) {
        a = new Date(a);
        a.setHours(0, 0, 0);
        a.setDate(a.getDate() + 4 - (a.getDay() || 7));
        var b = new Date(a.getFullYear(), 0, 1);
        return Math.ceil(((a - b) / 864E5 + 1) / 7)
    };
    d.stringToDate = function(a, b) {
        var c = {},
            e = [{
                    pattern: "YYYY",
                    period: "year"
                }, {
                    pattern: "YY",
                    period: "year"
                }, {
                    pattern: "MM",
                    period: "month"
                }, {
                    pattern: "M",
                    period: "month"
                }, {
                    pattern: "DD",
                    period: "date"
                }, {
                    pattern: "D",
                    period: "date"
                }, {
                    pattern: "JJ",
                    period: "hours"
                }, {
                    pattern: "J",
                    period: "hours"
                }, {
                    pattern: "HH",
                    period: "hours"
                }, {
                    pattern: "H",
                    period: "hours"
                },
                {
                    pattern: "KK",
                    period: "hours"
                }, {
                    pattern: "K",
                    period: "hours"
                }, {
                    pattern: "LL",
                    period: "hours"
                }, {
                    pattern: "L",
                    period: "hours"
                }, {
                    pattern: "NN",
                    period: "minutes"
                }, {
                    pattern: "N",
                    period: "minutes"
                }, {
                    pattern: "SS",
                    period: "seconds"
                }, {
                    pattern: "S",
                    period: "seconds"
                }, {
                    pattern: "QQQ",
                    period: "milliseconds"
                }, {
                    pattern: "QQ",
                    period: "milliseconds"
                }, {
                    pattern: "Q",
                    period: "milliseconds"
                }
            ],
            h = !0,
            f = b.indexOf("AA"); - 1 != f && (a.substr(f, 2), "pm" == a.toLowerCase && (h = !1));
        var f = b,
            g, k, l;
        for (l = 0; l < e.length; l++) k = e[l].period, c[k] = 0, "date" == k && (c[k] =
            1);
        for (l = 0; l < e.length; l++)
            if (g = e[l].pattern, k = e[l].period, -1 != b.indexOf(g)) {
                var m = d.getFromDateString(g, a, f);
                b = b.replace(g, "");
                if ("KK" == g || "K" == g || "LL" == g || "L" == g) h || (m += 12);
                c[k] = m
            }
        d.useUTC ? (e = new Date, e.setUTCFullYear(c.year, c.month, c.date), e.setUTCHours(c.hours, c.minutes, c.seconds, c.milliseconds)) : e = new Date(c.year, c.month, c.date, c.hours, c.minutes, c.seconds, c.milliseconds);
        return e
    };
    d.getFromDateString = function(a, b, c) {
        if (void 0 !== b) return c = c.indexOf(a), b = String(b), b = b.substr(c, a.length), "0" == b.charAt(0) &&
            (b = b.substr(1, b.length - 1)), b = Number(b), isNaN(b) && (b = 0), -1 != a.indexOf("M") && b--, b
    };
    d.formatDate = function(a, b, c) {
        c || (c = d);
        var e, h, f, g, k, l, m, p, q = d.getWeekNumber(a);
        d.useUTC ? (e = a.getUTCFullYear(), h = a.getUTCMonth(), f = a.getUTCDate(), g = a.getUTCDay(), k = a.getUTCHours(), l = a.getUTCMinutes(), m = a.getUTCSeconds(), p = a.getUTCMilliseconds()) : (e = a.getFullYear(), h = a.getMonth(), f = a.getDate(), g = a.getDay(), k = a.getHours(), l = a.getMinutes(), m = a.getSeconds(), p = a.getMilliseconds());
        var n = String(e).substr(2, 2),
            t = "0" + g;
        b = b.replace(/W/g,
            q);
        q = k;
        24 == q && (q = 0);
        var r = q;
        10 > r && (r = "0" + r);
        b = b.replace(/JJ/g, r);
        b = b.replace(/J/g, q);
        r = k;
        0 === r && (r = 24, -1 != b.indexOf("H") && (f--, 0 === f && (e = new Date(a), e.setDate(e.getDate() - 1), h = e.getMonth(), f = e.getDate(), e = e.getFullYear())));
        a = h + 1;
        9 > h && (a = "0" + a);
        q = f;
        10 > f && (q = "0" + f);
        var w = r;
        10 > w && (w = "0" + w);
        b = b.replace(/HH/g, w);
        b = b.replace(/H/g, r);
        r = k;
        11 < r && (r -= 12);
        w = r;
        10 > w && (w = "0" + w);
        b = b.replace(/KK/g, w);
        b = b.replace(/K/g, r);
        r = k;
        0 === r && (r = 12);
        12 < r && (r -= 12);
        w = r;
        10 > w && (w = "0" + w);
        b = b.replace(/LL/g, w);
        b = b.replace(/L/g, r);
        r = l;
        10 > r && (r = "0" + r);
        b = b.replace(/NN/g, r);
        b = b.replace(/N/g, l);
        l = m;
        10 > l && (l = "0" + l);
        b = b.replace(/SS/g, l);
        b = b.replace(/S/g, m);
        m = p;
        10 > m ? m = "00" + m : 100 > m && (m = "0" + m);
        l = p;
        10 > l && (l = "00" + l);
        b = b.replace(/A/g, "@A@");
        b = b.replace(/QQQ/g, m);
        b = b.replace(/QQ/g, l);
        b = b.replace(/Q/g, p);
        b = b.replace(/YYYY/g, "@IIII@");
        b = b.replace(/YY/g, "@II@");
        b = b.replace(/MMMM/g, "@XXXX@");
        b = b.replace(/MMM/g, "@XXX@");
        b = b.replace(/MM/g, "@XX@");
        b = b.replace(/M/g, "@X@");
        b = b.replace(/DD/g, "@RR@");
        b = b.replace(/D/g, "@R@");
        b = b.replace(/EEEE/g, "@PPPP@");
        b = b.replace(/EEE/g, "@PPP@");
        b = b.replace(/EE/g, "@PP@");
        b = b.replace(/E/g, "@P@");
        b = b.replace(/@IIII@/g, e);
        b = b.replace(/@II@/g, n);
        b = b.replace(/@XXXX@/g, c.monthNames[h]);
        b = b.replace(/@XXX@/g, c.shortMonthNames[h]);
        b = b.replace(/@XX@/g, a);
        b = b.replace(/@X@/g, h + 1);
        b = b.replace(/@RR@/g, q);
        b = b.replace(/@R@/g, f);
        b = b.replace(/@PPPP@/g, c.dayNames[g]);
        b = b.replace(/@PPP@/g, c.shortDayNames[g]);
        b = b.replace(/@PP@/g, t);
        b = b.replace(/@P@/g, g);
        return b = 12 > k ? b.replace(/@A@/g, c.amString) : b.replace(/@A@/g, c.pmString)
    };
    d.changeDate =
        function(a, b, c, e, h) {
            if (d.useUTC) return d.changeUTCDate(a, b, c, e, h);
            var f = -1;
            void 0 === e && (e = !0);
            void 0 === h && (h = !1);
            !0 === e && (f = 1);
            switch (b) {
                case "YYYY":
                    a.setFullYear(a.getFullYear() + c * f);
                    e || h || a.setDate(a.getDate() + 1);
                    break;
                case "MM":
                    b = a.getMonth();
                    a.setMonth(a.getMonth() + c * f);
                    a.getMonth() > b + c * f && a.setDate(a.getDate() - 1);
                    e || h || a.setDate(a.getDate() + 1);
                    break;
                case "DD":
                    a.setDate(a.getDate() + c * f);
                    break;
                case "WW":
                    a.setDate(a.getDate() + c * f * 7);
                    break;
                case "hh":
                    a.setHours(a.getHours() + c * f);
                    break;
                case "mm":
                    a.setMinutes(a.getMinutes() +
                        c * f);
                    break;
                case "ss":
                    a.setSeconds(a.getSeconds() + c * f);
                    break;
                case "fff":
                    a.setMilliseconds(a.getMilliseconds() + c * f)
            }
            return a
        };
    d.changeUTCDate = function(a, b, c, d, h) {
        var f = -1;
        void 0 === d && (d = !0);
        void 0 === h && (h = !1);
        !0 === d && (f = 1);
        switch (b) {
            case "YYYY":
                a.setUTCFullYear(a.getUTCFullYear() + c * f);
                d || h || a.setUTCDate(a.getUTCDate() + 1);
                break;
            case "MM":
                b = a.getUTCMonth();
                a.setUTCMonth(a.getUTCMonth() + c * f);
                a.getUTCMonth() > b + c * f && a.setUTCDate(a.getUTCDate() - 1);
                d || h || a.setUTCDate(a.getUTCDate() + 1);
                break;
            case "DD":
                a.setUTCDate(a.getUTCDate() +
                    c * f);
                break;
            case "WW":
                a.setUTCDate(a.getUTCDate() + c * f * 7);
                break;
            case "hh":
                a.setUTCHours(a.getUTCHours() + c * f);
                break;
            case "mm":
                a.setUTCMinutes(a.getUTCMinutes() + c * f);
                break;
            case "ss":
                a.setUTCSeconds(a.getUTCSeconds() + c * f);
                break;
            case "fff":
                a.setUTCMilliseconds(a.getUTCMilliseconds() + c * f)
        }
        return a
    }
})();
//! moment.js
//! version : 2.18.1
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com

;(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    global.moment = factory()
}(this, (function () { 'use strict';

var hookCallback;

function hooks () {
    return hookCallback.apply(null, arguments);
}

// This is done to register the method called with moment()
// without creating circular dependencies.
function setHookCallback (callback) {
    hookCallback = callback;
}

function isArray(input) {
    return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';
}

function isObject(input) {
    // IE8 will treat undefined and null as object if it wasn't for
    // input != null
    return input != null && Object.prototype.toString.call(input) === '[object Object]';
}

function isObjectEmpty(obj) {
    var k;
    for (k in obj) {
        // even if its not own property I'd still call it non-empty
        return false;
    }
    return true;
}

function isUndefined(input) {
    return input === void 0;
}

function isNumber(input) {
    return typeof input === 'number' || Object.prototype.toString.call(input) === '[object Number]';
}

function isDate(input) {
    return input instanceof Date || Object.prototype.toString.call(input) === '[object Date]';
}

function map(arr, fn) {
    var res = [], i;
    for (i = 0; i < arr.length; ++i) {
        res.push(fn(arr[i], i));
    }
    return res;
}

function hasOwnProp(a, b) {
    return Object.prototype.hasOwnProperty.call(a, b);
}

function extend(a, b) {
    for (var i in b) {
        if (hasOwnProp(b, i)) {
            a[i] = b[i];
        }
    }

    if (hasOwnProp(b, 'toString')) {
        a.toString = b.toString;
    }

    if (hasOwnProp(b, 'valueOf')) {
        a.valueOf = b.valueOf;
    }

    return a;
}

function createUTC (input, format, locale, strict) {
    return createLocalOrUTC(input, format, locale, strict, true).utc();
}

function defaultParsingFlags() {
    // We need to deep clone this object.
    return {
        empty           : false,
        unusedTokens    : [],
        unusedInput     : [],
        overflow        : -2,
        charsLeftOver   : 0,
        nullInput       : false,
        invalidMonth    : null,
        invalidFormat   : false,
        userInvalidated : false,
        iso             : false,
        parsedDateParts : [],
        meridiem        : null,
        rfc2822         : false,
        weekdayMismatch : false
    };
}

function getParsingFlags(m) {
    if (m._pf == null) {
        m._pf = defaultParsingFlags();
    }
    return m._pf;
}

var some;
if (Array.prototype.some) {
    some = Array.prototype.some;
} else {
    some = function (fun) {
        var t = Object(this);
        var len = t.length >>> 0;

        for (var i = 0; i < len; i++) {
            if (i in t && fun.call(this, t[i], i, t)) {
                return true;
            }
        }

        return false;
    };
}

var some$1 = some;

function isValid(m) {
    if (m._isValid == null) {
        var flags = getParsingFlags(m);
        var parsedParts = some$1.call(flags.parsedDateParts, function (i) {
            return i != null;
        });
        var isNowValid = !isNaN(m._d.getTime()) &&
            flags.overflow < 0 &&
            !flags.empty &&
            !flags.invalidMonth &&
            !flags.invalidWeekday &&
            !flags.nullInput &&
            !flags.invalidFormat &&
            !flags.userInvalidated &&
            (!flags.meridiem || (flags.meridiem && parsedParts));

        if (m._strict) {
            isNowValid = isNowValid &&
                flags.charsLeftOver === 0 &&
                flags.unusedTokens.length === 0 &&
                flags.bigHour === undefined;
        }

        if (Object.isFrozen == null || !Object.isFrozen(m)) {
            m._isValid = isNowValid;
        }
        else {
            return isNowValid;
        }
    }
    return m._isValid;
}

function createInvalid (flags) {
    var m = createUTC(NaN);
    if (flags != null) {
        extend(getParsingFlags(m), flags);
    }
    else {
        getParsingFlags(m).userInvalidated = true;
    }

    return m;
}

// Plugins that add properties should also add the key here (null value),
// so we can properly clone ourselves.
var momentProperties = hooks.momentProperties = [];

function copyConfig(to, from) {
    var i, prop, val;

    if (!isUndefined(from._isAMomentObject)) {
        to._isAMomentObject = from._isAMomentObject;
    }
    if (!isUndefined(from._i)) {
        to._i = from._i;
    }
    if (!isUndefined(from._f)) {
        to._f = from._f;
    }
    if (!isUndefined(from._l)) {
        to._l = from._l;
    }
    if (!isUndefined(from._strict)) {
        to._strict = from._strict;
    }
    if (!isUndefined(from._tzm)) {
        to._tzm = from._tzm;
    }
    if (!isUndefined(from._isUTC)) {
        to._isUTC = from._isUTC;
    }
    if (!isUndefined(from._offset)) {
        to._offset = from._offset;
    }
    if (!isUndefined(from._pf)) {
        to._pf = getParsingFlags(from);
    }
    if (!isUndefined(from._locale)) {
        to._locale = from._locale;
    }

    if (momentProperties.length > 0) {
        for (i = 0; i < momentProperties.length; i++) {
            prop = momentProperties[i];
            val = from[prop];
            if (!isUndefined(val)) {
                to[prop] = val;
            }
        }
    }

    return to;
}

var updateInProgress = false;

// Moment prototype object
function Moment(config) {
    copyConfig(this, config);
    this._d = new Date(config._d != null ? config._d.getTime() : NaN);
    if (!this.isValid()) {
        this._d = new Date(NaN);
    }
    // Prevent infinite loop in case updateOffset creates new moment
    // objects.
    if (updateInProgress === false) {
        updateInProgress = true;
        hooks.updateOffset(this);
        updateInProgress = false;
    }
}

function isMoment (obj) {
    return obj instanceof Moment || (obj != null && obj._isAMomentObject != null);
}

function absFloor (number) {
    if (number < 0) {
        // -0 -> 0
        return Math.ceil(number) || 0;
    } else {
        return Math.floor(number);
    }
}

function toInt(argumentForCoercion) {
    var coercedNumber = +argumentForCoercion,
        value = 0;

    if (coercedNumber !== 0 && isFinite(coercedNumber)) {
        value = absFloor(coercedNumber);
    }

    return value;
}

// compare two arrays, return the number of differences
function compareArrays(array1, array2, dontConvert) {
    var len = Math.min(array1.length, array2.length),
        lengthDiff = Math.abs(array1.length - array2.length),
        diffs = 0,
        i;
    for (i = 0; i < len; i++) {
        if ((dontConvert && array1[i] !== array2[i]) ||
            (!dontConvert && toInt(array1[i]) !== toInt(array2[i]))) {
            diffs++;
        }
    }
    return diffs + lengthDiff;
}

function warn(msg) {
    if (hooks.suppressDeprecationWarnings === false &&
            (typeof console !==  'undefined') && console.warn) {
        console.warn('Deprecation warning: ' + msg);
    }
}

function deprecate(msg, fn) {
    var firstTime = true;

    return extend(function () {
        if (hooks.deprecationHandler != null) {
            hooks.deprecationHandler(null, msg);
        }
        if (firstTime) {
            var args = [];
            var arg;
            for (var i = 0; i < arguments.length; i++) {
                arg = '';
                if (typeof arguments[i] === 'object') {
                    arg += '\n[' + i + '] ';
                    for (var key in arguments[0]) {
                        arg += key + ': ' + arguments[0][key] + ', ';
                    }
                    arg = arg.slice(0, -2); // Remove trailing comma and space
                } else {
                    arg = arguments[i];
                }
                args.push(arg);
            }
            warn(msg + '\nArguments: ' + Array.prototype.slice.call(args).join('') + '\n' + (new Error()).stack);
            firstTime = false;
        }
        return fn.apply(this, arguments);
    }, fn);
}

var deprecations = {};

function deprecateSimple(name, msg) {
    if (hooks.deprecationHandler != null) {
        hooks.deprecationHandler(name, msg);
    }
    if (!deprecations[name]) {
        warn(msg);
        deprecations[name] = true;
    }
}

hooks.suppressDeprecationWarnings = false;
hooks.deprecationHandler = null;

function isFunction(input) {
    return input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';
}

function set (config) {
    var prop, i;
    for (i in config) {
        prop = config[i];
        if (isFunction(prop)) {
            this[i] = prop;
        } else {
            this['_' + i] = prop;
        }
    }
    this._config = config;
    // Lenient ordinal parsing accepts just a number in addition to
    // number + (possibly) stuff coming from _dayOfMonthOrdinalParse.
    // TODO: Remove "ordinalParse" fallback in next major release.
    this._dayOfMonthOrdinalParseLenient = new RegExp(
        (this._dayOfMonthOrdinalParse.source || this._ordinalParse.source) +
            '|' + (/\d{1,2}/).source);
}

function mergeConfigs(parentConfig, childConfig) {
    var res = extend({}, parentConfig), prop;
    for (prop in childConfig) {
        if (hasOwnProp(childConfig, prop)) {
            if (isObject(parentConfig[prop]) && isObject(childConfig[prop])) {
                res[prop] = {};
                extend(res[prop], parentConfig[prop]);
                extend(res[prop], childConfig[prop]);
            } else if (childConfig[prop] != null) {
                res[prop] = childConfig[prop];
            } else {
                delete res[prop];
            }
        }
    }
    for (prop in parentConfig) {
        if (hasOwnProp(parentConfig, prop) &&
                !hasOwnProp(childConfig, prop) &&
                isObject(parentConfig[prop])) {
            // make sure changes to properties don't modify parent config
            res[prop] = extend({}, res[prop]);
        }
    }
    return res;
}

function Locale(config) {
    if (config != null) {
        this.set(config);
    }
}

var keys;

if (Object.keys) {
    keys = Object.keys;
} else {
    keys = function (obj) {
        var i, res = [];
        for (i in obj) {
            if (hasOwnProp(obj, i)) {
                res.push(i);
            }
        }
        return res;
    };
}

var keys$1 = keys;

var defaultCalendar = {
    sameDay : '[Today at] LT',
    nextDay : '[Tomorrow at] LT',
    nextWeek : 'dddd [at] LT',
    lastDay : '[Yesterday at] LT',
    lastWeek : '[Last] dddd [at] LT',
    sameElse : 'L'
};

function calendar (key, mom, now) {
    var output = this._calendar[key] || this._calendar['sameElse'];
    return isFunction(output) ? output.call(mom, now) : output;
}

var defaultLongDateFormat = {
    LTS  : 'h:mm:ss A',
    LT   : 'h:mm A',
    L    : 'MM/DD/YYYY',
    LL   : 'MMMM D, YYYY',
    LLL  : 'MMMM D, YYYY h:mm A',
    LLLL : 'dddd, MMMM D, YYYY h:mm A'
};

function longDateFormat (key) {
    var format = this._longDateFormat[key],
        formatUpper = this._longDateFormat[key.toUpperCase()];

    if (format || !formatUpper) {
        return format;
    }

    this._longDateFormat[key] = formatUpper.replace(/MMMM|MM|DD|dddd/g, function (val) {
        return val.slice(1);
    });

    return this._longDateFormat[key];
}

var defaultInvalidDate = 'Invalid date';

function invalidDate () {
    return this._invalidDate;
}

var defaultOrdinal = '%d';
var defaultDayOfMonthOrdinalParse = /\d{1,2}/;

function ordinal (number) {
    return this._ordinal.replace('%d', number);
}

var defaultRelativeTime = {
    future : 'in %s',
    past   : '%s ago',
    s  : 'a few seconds',
    ss : '%d seconds',
    m  : 'a minute',
    mm : '%d minutes',
    h  : 'an hour',
    hh : '%d hours',
    d  : 'a day',
    dd : '%d days',
    M  : 'a month',
    MM : '%d months',
    y  : 'a year',
    yy : '%d years'
};

function relativeTime (number, withoutSuffix, string, isFuture) {
    var output = this._relativeTime[string];
    return (isFunction(output)) ?
        output(number, withoutSuffix, string, isFuture) :
        output.replace(/%d/i, number);
}

function pastFuture (diff, output) {
    var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
    return isFunction(format) ? format(output) : format.replace(/%s/i, output);
}

var aliases = {};

function addUnitAlias (unit, shorthand) {
    var lowerCase = unit.toLowerCase();
    aliases[lowerCase] = aliases[lowerCase + 's'] = aliases[shorthand] = unit;
}

function normalizeUnits(units) {
    return typeof units === 'string' ? aliases[units] || aliases[units.toLowerCase()] : undefined;
}

function normalizeObjectUnits(inputObject) {
    var normalizedInput = {},
        normalizedProp,
        prop;

    for (prop in inputObject) {
        if (hasOwnProp(inputObject, prop)) {
            normalizedProp = normalizeUnits(prop);
            if (normalizedProp) {
                normalizedInput[normalizedProp] = inputObject[prop];
            }
        }
    }

    return normalizedInput;
}

var priorities = {};

function addUnitPriority(unit, priority) {
    priorities[unit] = priority;
}

function getPrioritizedUnits(unitsObj) {
    var units = [];
    for (var u in unitsObj) {
        units.push({unit: u, priority: priorities[u]});
    }
    units.sort(function (a, b) {
        return a.priority - b.priority;
    });
    return units;
}

function makeGetSet (unit, keepTime) {
    return function (value) {
        if (value != null) {
            set$1(this, unit, value);
            hooks.updateOffset(this, keepTime);
            return this;
        } else {
            return get(this, unit);
        }
    };
}

function get (mom, unit) {
    return mom.isValid() ?
        mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]() : NaN;
}

function set$1 (mom, unit, value) {
    if (mom.isValid()) {
        mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
    }
}

// MOMENTS

function stringGet (units) {
    units = normalizeUnits(units);
    if (isFunction(this[units])) {
        return this[units]();
    }
    return this;
}


function stringSet (units, value) {
    if (typeof units === 'object') {
        units = normalizeObjectUnits(units);
        var prioritized = getPrioritizedUnits(units);
        for (var i = 0; i < prioritized.length; i++) {
            this[prioritized[i].unit](units[prioritized[i].unit]);
        }
    } else {
        units = normalizeUnits(units);
        if (isFunction(this[units])) {
            return this[units](value);
        }
    }
    return this;
}

function zeroFill(number, targetLength, forceSign) {
    var absNumber = '' + Math.abs(number),
        zerosToFill = targetLength - absNumber.length,
        sign = number >= 0;
    return (sign ? (forceSign ? '+' : '') : '-') +
        Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
}

var formattingTokens = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g;

var localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g;

var formatFunctions = {};

var formatTokenFunctions = {};

// token:    'M'
// padded:   ['MM', 2]
// ordinal:  'Mo'
// callback: function () { this.month() + 1 }
function addFormatToken (token, padded, ordinal, callback) {
    var func = callback;
    if (typeof callback === 'string') {
        func = function () {
            return this[callback]();
        };
    }
    if (token) {
        formatTokenFunctions[token] = func;
    }
    if (padded) {
        formatTokenFunctions[padded[0]] = function () {
            return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
        };
    }
    if (ordinal) {
        formatTokenFunctions[ordinal] = function () {
            return this.localeData().ordinal(func.apply(this, arguments), token);
        };
    }
}

function removeFormattingTokens(input) {
    if (input.match(/\[[\s\S]/)) {
        return input.replace(/^\[|\]$/g, '');
    }
    return input.replace(/\\/g, '');
}

function makeFormatFunction(format) {
    var array = format.match(formattingTokens), i, length;

    for (i = 0, length = array.length; i < length; i++) {
        if (formatTokenFunctions[array[i]]) {
            array[i] = formatTokenFunctions[array[i]];
        } else {
            array[i] = removeFormattingTokens(array[i]);
        }
    }

    return function (mom) {
        var output = '', i;
        for (i = 0; i < length; i++) {
            output += isFunction(array[i]) ? array[i].call(mom, format) : array[i];
        }
        return output;
    };
}

// format date using native date object
function formatMoment(m, format) {
    if (!m.isValid()) {
        return m.localeData().invalidDate();
    }

    format = expandFormat(format, m.localeData());
    formatFunctions[format] = formatFunctions[format] || makeFormatFunction(format);

    return formatFunctions[format](m);
}

function expandFormat(format, locale) {
    var i = 5;

    function replaceLongDateFormatTokens(input) {
        return locale.longDateFormat(input) || input;
    }

    localFormattingTokens.lastIndex = 0;
    while (i >= 0 && localFormattingTokens.test(format)) {
        format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
        localFormattingTokens.lastIndex = 0;
        i -= 1;
    }

    return format;
}

var match1         = /\d/;            //       0 - 9
var match2         = /\d\d/;          //      00 - 99
var match3         = /\d{3}/;         //     000 - 999
var match4         = /\d{4}/;         //    0000 - 9999
var match6         = /[+-]?\d{6}/;    // -999999 - 999999
var match1to2      = /\d\d?/;         //       0 - 99
var match3to4      = /\d\d\d\d?/;     //     999 - 9999
var match5to6      = /\d\d\d\d\d\d?/; //   99999 - 999999
var match1to3      = /\d{1,3}/;       //       0 - 999
var match1to4      = /\d{1,4}/;       //       0 - 9999
var match1to6      = /[+-]?\d{1,6}/;  // -999999 - 999999

var matchUnsigned  = /\d+/;           //       0 - inf
var matchSigned    = /[+-]?\d+/;      //    -inf - inf

var matchOffset    = /Z|[+-]\d\d:?\d\d/gi; // +00:00 -00:00 +0000 -0000 or Z
var matchShortOffset = /Z|[+-]\d\d(?::?\d\d)?/gi; // +00 -00 +00:00 -00:00 +0000 -0000 or Z

var matchTimestamp = /[+-]?\d+(\.\d{1,3})?/; // 123456789 123456789.123

// any word (or two) characters or numbers including two/three word month in arabic.
// includes scottish gaelic two word and hyphenated months
var matchWord = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i;


var regexes = {};

function addRegexToken (token, regex, strictRegex) {
    regexes[token] = isFunction(regex) ? regex : function (isStrict, localeData) {
        return (isStrict && strictRegex) ? strictRegex : regex;
    };
}

function getParseRegexForToken (token, config) {
    if (!hasOwnProp(regexes, token)) {
        return new RegExp(unescapeFormat(token));
    }

    return regexes[token](config._strict, config._locale);
}

// Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
function unescapeFormat(s) {
    return regexEscape(s.replace('\\', '').replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (matched, p1, p2, p3, p4) {
        return p1 || p2 || p3 || p4;
    }));
}

function regexEscape(s) {
    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

var tokens = {};

function addParseToken (token, callback) {
    var i, func = callback;
    if (typeof token === 'string') {
        token = [token];
    }
    if (isNumber(callback)) {
        func = function (input, array) {
            array[callback] = toInt(input);
        };
    }
    for (i = 0; i < token.length; i++) {
        tokens[token[i]] = func;
    }
}

function addWeekParseToken (token, callback) {
    addParseToken(token, function (input, array, config, token) {
        config._w = config._w || {};
        callback(input, config._w, config, token);
    });
}

function addTimeToArrayFromToken(token, input, config) {
    if (input != null && hasOwnProp(tokens, token)) {
        tokens[token](input, config._a, config, token);
    }
}

var YEAR = 0;
var MONTH = 1;
var DATE = 2;
var HOUR = 3;
var MINUTE = 4;
var SECOND = 5;
var MILLISECOND = 6;
var WEEK = 7;
var WEEKDAY = 8;

var indexOf;

if (Array.prototype.indexOf) {
    indexOf = Array.prototype.indexOf;
} else {
    indexOf = function (o) {
        // I know
        var i;
        for (i = 0; i < this.length; ++i) {
            if (this[i] === o) {
                return i;
            }
        }
        return -1;
    };
}

var indexOf$1 = indexOf;

function daysInMonth(year, month) {
    return new Date(Date.UTC(year, month + 1, 0)).getUTCDate();
}

// FORMATTING

addFormatToken('M', ['MM', 2], 'Mo', function () {
    return this.month() + 1;
});

addFormatToken('MMM', 0, 0, function (format) {
    return this.localeData().monthsShort(this, format);
});

addFormatToken('MMMM', 0, 0, function (format) {
    return this.localeData().months(this, format);
});

// ALIASES

addUnitAlias('month', 'M');

// PRIORITY

addUnitPriority('month', 8);

// PARSING

addRegexToken('M',    match1to2);
addRegexToken('MM',   match1to2, match2);
addRegexToken('MMM',  function (isStrict, locale) {
    return locale.monthsShortRegex(isStrict);
});
addRegexToken('MMMM', function (isStrict, locale) {
    return locale.monthsRegex(isStrict);
});

addParseToken(['M', 'MM'], function (input, array) {
    array[MONTH] = toInt(input) - 1;
});

addParseToken(['MMM', 'MMMM'], function (input, array, config, token) {
    var month = config._locale.monthsParse(input, token, config._strict);
    // if we didn't find a month name, mark the date as invalid.
    if (month != null) {
        array[MONTH] = month;
    } else {
        getParsingFlags(config).invalidMonth = input;
    }
});

// LOCALES

var MONTHS_IN_FORMAT = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/;
var defaultLocaleMonths = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');
function localeMonths (m, format) {
    if (!m) {
        return isArray(this._months) ? this._months :
            this._months['standalone'];
    }
    return isArray(this._months) ? this._months[m.month()] :
        this._months[(this._months.isFormat || MONTHS_IN_FORMAT).test(format) ? 'format' : 'standalone'][m.month()];
}

var defaultLocaleMonthsShort = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');
function localeMonthsShort (m, format) {
    if (!m) {
        return isArray(this._monthsShort) ? this._monthsShort :
            this._monthsShort['standalone'];
    }
    return isArray(this._monthsShort) ? this._monthsShort[m.month()] :
        this._monthsShort[MONTHS_IN_FORMAT.test(format) ? 'format' : 'standalone'][m.month()];
}

function handleStrictParse(monthName, format, strict) {
    var i, ii, mom, llc = monthName.toLocaleLowerCase();
    if (!this._monthsParse) {
        // this is not used
        this._monthsParse = [];
        this._longMonthsParse = [];
        this._shortMonthsParse = [];
        for (i = 0; i < 12; ++i) {
            mom = createUTC([2000, i]);
            this._shortMonthsParse[i] = this.monthsShort(mom, '').toLocaleLowerCase();
            this._longMonthsParse[i] = this.months(mom, '').toLocaleLowerCase();
        }
    }

    if (strict) {
        if (format === 'MMM') {
            ii = indexOf$1.call(this._shortMonthsParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf$1.call(this._longMonthsParse, llc);
            return ii !== -1 ? ii : null;
        }
    } else {
        if (format === 'MMM') {
            ii = indexOf$1.call(this._shortMonthsParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._longMonthsParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf$1.call(this._longMonthsParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._shortMonthsParse, llc);
            return ii !== -1 ? ii : null;
        }
    }
}

function localeMonthsParse (monthName, format, strict) {
    var i, mom, regex;

    if (this._monthsParseExact) {
        return handleStrictParse.call(this, monthName, format, strict);
    }

    if (!this._monthsParse) {
        this._monthsParse = [];
        this._longMonthsParse = [];
        this._shortMonthsParse = [];
    }

    // TODO: add sorting
    // Sorting makes sure if one month (or abbr) is a prefix of another
    // see sorting in computeMonthsParse
    for (i = 0; i < 12; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, i]);
        if (strict && !this._longMonthsParse[i]) {
            this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
            this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
        }
        if (!strict && !this._monthsParse[i]) {
            regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
            this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
        }
        // test the regex
        if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
            return i;
        } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
            return i;
        } else if (!strict && this._monthsParse[i].test(monthName)) {
            return i;
        }
    }
}

// MOMENTS

function setMonth (mom, value) {
    var dayOfMonth;

    if (!mom.isValid()) {
        // No op
        return mom;
    }

    if (typeof value === 'string') {
        if (/^\d+$/.test(value)) {
            value = toInt(value);
        } else {
            value = mom.localeData().monthsParse(value);
            // TODO: Another silent failure?
            if (!isNumber(value)) {
                return mom;
            }
        }
    }

    dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));
    mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);
    return mom;
}

function getSetMonth (value) {
    if (value != null) {
        setMonth(this, value);
        hooks.updateOffset(this, true);
        return this;
    } else {
        return get(this, 'Month');
    }
}

function getDaysInMonth () {
    return daysInMonth(this.year(), this.month());
}

var defaultMonthsShortRegex = matchWord;
function monthsShortRegex (isStrict) {
    if (this._monthsParseExact) {
        if (!hasOwnProp(this, '_monthsRegex')) {
            computeMonthsParse.call(this);
        }
        if (isStrict) {
            return this._monthsShortStrictRegex;
        } else {
            return this._monthsShortRegex;
        }
    } else {
        if (!hasOwnProp(this, '_monthsShortRegex')) {
            this._monthsShortRegex = defaultMonthsShortRegex;
        }
        return this._monthsShortStrictRegex && isStrict ?
            this._monthsShortStrictRegex : this._monthsShortRegex;
    }
}

var defaultMonthsRegex = matchWord;
function monthsRegex (isStrict) {
    if (this._monthsParseExact) {
        if (!hasOwnProp(this, '_monthsRegex')) {
            computeMonthsParse.call(this);
        }
        if (isStrict) {
            return this._monthsStrictRegex;
        } else {
            return this._monthsRegex;
        }
    } else {
        if (!hasOwnProp(this, '_monthsRegex')) {
            this._monthsRegex = defaultMonthsRegex;
        }
        return this._monthsStrictRegex && isStrict ?
            this._monthsStrictRegex : this._monthsRegex;
    }
}

function computeMonthsParse () {
    function cmpLenRev(a, b) {
        return b.length - a.length;
    }

    var shortPieces = [], longPieces = [], mixedPieces = [],
        i, mom;
    for (i = 0; i < 12; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, i]);
        shortPieces.push(this.monthsShort(mom, ''));
        longPieces.push(this.months(mom, ''));
        mixedPieces.push(this.months(mom, ''));
        mixedPieces.push(this.monthsShort(mom, ''));
    }
    // Sorting makes sure if one month (or abbr) is a prefix of another it
    // will match the longer piece.
    shortPieces.sort(cmpLenRev);
    longPieces.sort(cmpLenRev);
    mixedPieces.sort(cmpLenRev);
    for (i = 0; i < 12; i++) {
        shortPieces[i] = regexEscape(shortPieces[i]);
        longPieces[i] = regexEscape(longPieces[i]);
    }
    for (i = 0; i < 24; i++) {
        mixedPieces[i] = regexEscape(mixedPieces[i]);
    }

    this._monthsRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
    this._monthsShortRegex = this._monthsRegex;
    this._monthsStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
    this._monthsShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
}

// FORMATTING

addFormatToken('Y', 0, 0, function () {
    var y = this.year();
    return y <= 9999 ? '' + y : '+' + y;
});

addFormatToken(0, ['YY', 2], 0, function () {
    return this.year() % 100;
});

addFormatToken(0, ['YYYY',   4],       0, 'year');
addFormatToken(0, ['YYYYY',  5],       0, 'year');
addFormatToken(0, ['YYYYYY', 6, true], 0, 'year');

// ALIASES

addUnitAlias('year', 'y');

// PRIORITIES

addUnitPriority('year', 1);

// PARSING

addRegexToken('Y',      matchSigned);
addRegexToken('YY',     match1to2, match2);
addRegexToken('YYYY',   match1to4, match4);
addRegexToken('YYYYY',  match1to6, match6);
addRegexToken('YYYYYY', match1to6, match6);

addParseToken(['YYYYY', 'YYYYYY'], YEAR);
addParseToken('YYYY', function (input, array) {
    array[YEAR] = input.length === 2 ? hooks.parseTwoDigitYear(input) : toInt(input);
});
addParseToken('YY', function (input, array) {
    array[YEAR] = hooks.parseTwoDigitYear(input);
});
addParseToken('Y', function (input, array) {
    array[YEAR] = parseInt(input, 10);
});

// HELPERS

function daysInYear(year) {
    return isLeapYear(year) ? 366 : 365;
}

function isLeapYear(year) {
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
}

// HOOKS

hooks.parseTwoDigitYear = function (input) {
    return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
};

// MOMENTS

var getSetYear = makeGetSet('FullYear', true);

function getIsLeapYear () {
    return isLeapYear(this.year());
}

function createDate (y, m, d, h, M, s, ms) {
    // can't just apply() to create a date:
    // https://stackoverflow.com/q/181348
    var date = new Date(y, m, d, h, M, s, ms);

    // the date constructor remaps years 0-99 to 1900-1999
    if (y < 100 && y >= 0 && isFinite(date.getFullYear())) {
        date.setFullYear(y);
    }
    return date;
}

function createUTCDate (y) {
    var date = new Date(Date.UTC.apply(null, arguments));

    // the Date.UTC function remaps years 0-99 to 1900-1999
    if (y < 100 && y >= 0 && isFinite(date.getUTCFullYear())) {
        date.setUTCFullYear(y);
    }
    return date;
}

// start-of-first-week - start-of-year
function firstWeekOffset(year, dow, doy) {
    var // first-week day -- which january is always in the first week (4 for iso, 1 for other)
        fwd = 7 + dow - doy,
        // first-week day local weekday -- which local weekday is fwd
        fwdlw = (7 + createUTCDate(year, 0, fwd).getUTCDay() - dow) % 7;

    return -fwdlw + fwd - 1;
}

// https://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
function dayOfYearFromWeeks(year, week, weekday, dow, doy) {
    var localWeekday = (7 + weekday - dow) % 7,
        weekOffset = firstWeekOffset(year, dow, doy),
        dayOfYear = 1 + 7 * (week - 1) + localWeekday + weekOffset,
        resYear, resDayOfYear;

    if (dayOfYear <= 0) {
        resYear = year - 1;
        resDayOfYear = daysInYear(resYear) + dayOfYear;
    } else if (dayOfYear > daysInYear(year)) {
        resYear = year + 1;
        resDayOfYear = dayOfYear - daysInYear(year);
    } else {
        resYear = year;
        resDayOfYear = dayOfYear;
    }

    return {
        year: resYear,
        dayOfYear: resDayOfYear
    };
}

function weekOfYear(mom, dow, doy) {
    var weekOffset = firstWeekOffset(mom.year(), dow, doy),
        week = Math.floor((mom.dayOfYear() - weekOffset - 1) / 7) + 1,
        resWeek, resYear;

    if (week < 1) {
        resYear = mom.year() - 1;
        resWeek = week + weeksInYear(resYear, dow, doy);
    } else if (week > weeksInYear(mom.year(), dow, doy)) {
        resWeek = week - weeksInYear(mom.year(), dow, doy);
        resYear = mom.year() + 1;
    } else {
        resYear = mom.year();
        resWeek = week;
    }

    return {
        week: resWeek,
        year: resYear
    };
}

function weeksInYear(year, dow, doy) {
    var weekOffset = firstWeekOffset(year, dow, doy),
        weekOffsetNext = firstWeekOffset(year + 1, dow, doy);
    return (daysInYear(year) - weekOffset + weekOffsetNext) / 7;
}

// FORMATTING

addFormatToken('w', ['ww', 2], 'wo', 'week');
addFormatToken('W', ['WW', 2], 'Wo', 'isoWeek');

// ALIASES

addUnitAlias('week', 'w');
addUnitAlias('isoWeek', 'W');

// PRIORITIES

addUnitPriority('week', 5);
addUnitPriority('isoWeek', 5);

// PARSING

addRegexToken('w',  match1to2);
addRegexToken('ww', match1to2, match2);
addRegexToken('W',  match1to2);
addRegexToken('WW', match1to2, match2);

addWeekParseToken(['w', 'ww', 'W', 'WW'], function (input, week, config, token) {
    week[token.substr(0, 1)] = toInt(input);
});

// HELPERS

// LOCALES

function localeWeek (mom) {
    return weekOfYear(mom, this._week.dow, this._week.doy).week;
}

var defaultLocaleWeek = {
    dow : 0, // Sunday is the first day of the week.
    doy : 6  // The week that contains Jan 1st is the first week of the year.
};

function localeFirstDayOfWeek () {
    return this._week.dow;
}

function localeFirstDayOfYear () {
    return this._week.doy;
}

// MOMENTS

function getSetWeek (input) {
    var week = this.localeData().week(this);
    return input == null ? week : this.add((input - week) * 7, 'd');
}

function getSetISOWeek (input) {
    var week = weekOfYear(this, 1, 4).week;
    return input == null ? week : this.add((input - week) * 7, 'd');
}

// FORMATTING

addFormatToken('d', 0, 'do', 'day');

addFormatToken('dd', 0, 0, function (format) {
    return this.localeData().weekdaysMin(this, format);
});

addFormatToken('ddd', 0, 0, function (format) {
    return this.localeData().weekdaysShort(this, format);
});

addFormatToken('dddd', 0, 0, function (format) {
    return this.localeData().weekdays(this, format);
});

addFormatToken('e', 0, 0, 'weekday');
addFormatToken('E', 0, 0, 'isoWeekday');

// ALIASES

addUnitAlias('day', 'd');
addUnitAlias('weekday', 'e');
addUnitAlias('isoWeekday', 'E');

// PRIORITY
addUnitPriority('day', 11);
addUnitPriority('weekday', 11);
addUnitPriority('isoWeekday', 11);

// PARSING

addRegexToken('d',    match1to2);
addRegexToken('e',    match1to2);
addRegexToken('E',    match1to2);
addRegexToken('dd',   function (isStrict, locale) {
    return locale.weekdaysMinRegex(isStrict);
});
addRegexToken('ddd',   function (isStrict, locale) {
    return locale.weekdaysShortRegex(isStrict);
});
addRegexToken('dddd',   function (isStrict, locale) {
    return locale.weekdaysRegex(isStrict);
});

addWeekParseToken(['dd', 'ddd', 'dddd'], function (input, week, config, token) {
    var weekday = config._locale.weekdaysParse(input, token, config._strict);
    // if we didn't get a weekday name, mark the date as invalid
    if (weekday != null) {
        week.d = weekday;
    } else {
        getParsingFlags(config).invalidWeekday = input;
    }
});

addWeekParseToken(['d', 'e', 'E'], function (input, week, config, token) {
    week[token] = toInt(input);
});

// HELPERS

function parseWeekday(input, locale) {
    if (typeof input !== 'string') {
        return input;
    }

    if (!isNaN(input)) {
        return parseInt(input, 10);
    }

    input = locale.weekdaysParse(input);
    if (typeof input === 'number') {
        return input;
    }

    return null;
}

function parseIsoWeekday(input, locale) {
    if (typeof input === 'string') {
        return locale.weekdaysParse(input) % 7 || 7;
    }
    return isNaN(input) ? null : input;
}

// LOCALES

var defaultLocaleWeekdays = 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_');
function localeWeekdays (m, format) {
    if (!m) {
        return isArray(this._weekdays) ? this._weekdays :
            this._weekdays['standalone'];
    }
    return isArray(this._weekdays) ? this._weekdays[m.day()] :
        this._weekdays[this._weekdays.isFormat.test(format) ? 'format' : 'standalone'][m.day()];
}

var defaultLocaleWeekdaysShort = 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_');
function localeWeekdaysShort (m) {
    return (m) ? this._weekdaysShort[m.day()] : this._weekdaysShort;
}

var defaultLocaleWeekdaysMin = 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_');
function localeWeekdaysMin (m) {
    return (m) ? this._weekdaysMin[m.day()] : this._weekdaysMin;
}

function handleStrictParse$1(weekdayName, format, strict) {
    var i, ii, mom, llc = weekdayName.toLocaleLowerCase();
    if (!this._weekdaysParse) {
        this._weekdaysParse = [];
        this._shortWeekdaysParse = [];
        this._minWeekdaysParse = [];

        for (i = 0; i < 7; ++i) {
            mom = createUTC([2000, 1]).day(i);
            this._minWeekdaysParse[i] = this.weekdaysMin(mom, '').toLocaleLowerCase();
            this._shortWeekdaysParse[i] = this.weekdaysShort(mom, '').toLocaleLowerCase();
            this._weekdaysParse[i] = this.weekdays(mom, '').toLocaleLowerCase();
        }
    }

    if (strict) {
        if (format === 'dddd') {
            ii = indexOf$1.call(this._weekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else if (format === 'ddd') {
            ii = indexOf$1.call(this._shortWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf$1.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        }
    } else {
        if (format === 'dddd') {
            ii = indexOf$1.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._shortWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else if (format === 'ddd') {
            ii = indexOf$1.call(this._shortWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf$1.call(this._minWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._shortWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        }
    }
}

function localeWeekdaysParse (weekdayName, format, strict) {
    var i, mom, regex;

    if (this._weekdaysParseExact) {
        return handleStrictParse$1.call(this, weekdayName, format, strict);
    }

    if (!this._weekdaysParse) {
        this._weekdaysParse = [];
        this._minWeekdaysParse = [];
        this._shortWeekdaysParse = [];
        this._fullWeekdaysParse = [];
    }

    for (i = 0; i < 7; i++) {
        // make the regex if we don't have it already

        mom = createUTC([2000, 1]).day(i);
        if (strict && !this._fullWeekdaysParse[i]) {
            this._fullWeekdaysParse[i] = new RegExp('^' + this.weekdays(mom, '').replace('.', '\.?') + '$', 'i');
            this._shortWeekdaysParse[i] = new RegExp('^' + this.weekdaysShort(mom, '').replace('.', '\.?') + '$', 'i');
            this._minWeekdaysParse[i] = new RegExp('^' + this.weekdaysMin(mom, '').replace('.', '\.?') + '$', 'i');
        }
        if (!this._weekdaysParse[i]) {
            regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
            this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
        }
        // test the regex
        if (strict && format === 'dddd' && this._fullWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (strict && format === 'ddd' && this._shortWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (strict && format === 'dd' && this._minWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (!strict && this._weekdaysParse[i].test(weekdayName)) {
            return i;
        }
    }
}

// MOMENTS

function getSetDayOfWeek (input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
    if (input != null) {
        input = parseWeekday(input, this.localeData());
        return this.add(input - day, 'd');
    } else {
        return day;
    }
}

function getSetLocaleDayOfWeek (input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
    return input == null ? weekday : this.add(input - weekday, 'd');
}

function getSetISODayOfWeek (input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }

    // behaves the same as moment#day except
    // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
    // as a setter, sunday should belong to the previous week.

    if (input != null) {
        var weekday = parseIsoWeekday(input, this.localeData());
        return this.day(this.day() % 7 ? weekday : weekday - 7);
    } else {
        return this.day() || 7;
    }
}

var defaultWeekdaysRegex = matchWord;
function weekdaysRegex (isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysStrictRegex;
        } else {
            return this._weekdaysRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            this._weekdaysRegex = defaultWeekdaysRegex;
        }
        return this._weekdaysStrictRegex && isStrict ?
            this._weekdaysStrictRegex : this._weekdaysRegex;
    }
}

var defaultWeekdaysShortRegex = matchWord;
function weekdaysShortRegex (isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysShortStrictRegex;
        } else {
            return this._weekdaysShortRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysShortRegex')) {
            this._weekdaysShortRegex = defaultWeekdaysShortRegex;
        }
        return this._weekdaysShortStrictRegex && isStrict ?
            this._weekdaysShortStrictRegex : this._weekdaysShortRegex;
    }
}

var defaultWeekdaysMinRegex = matchWord;
function weekdaysMinRegex (isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysMinStrictRegex;
        } else {
            return this._weekdaysMinRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysMinRegex')) {
            this._weekdaysMinRegex = defaultWeekdaysMinRegex;
        }
        return this._weekdaysMinStrictRegex && isStrict ?
            this._weekdaysMinStrictRegex : this._weekdaysMinRegex;
    }
}


function computeWeekdaysParse () {
    function cmpLenRev(a, b) {
        return b.length - a.length;
    }

    var minPieces = [], shortPieces = [], longPieces = [], mixedPieces = [],
        i, mom, minp, shortp, longp;
    for (i = 0; i < 7; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, 1]).day(i);
        minp = this.weekdaysMin(mom, '');
        shortp = this.weekdaysShort(mom, '');
        longp = this.weekdays(mom, '');
        minPieces.push(minp);
        shortPieces.push(shortp);
        longPieces.push(longp);
        mixedPieces.push(minp);
        mixedPieces.push(shortp);
        mixedPieces.push(longp);
    }
    // Sorting makes sure if one weekday (or abbr) is a prefix of another it
    // will match the longer piece.
    minPieces.sort(cmpLenRev);
    shortPieces.sort(cmpLenRev);
    longPieces.sort(cmpLenRev);
    mixedPieces.sort(cmpLenRev);
    for (i = 0; i < 7; i++) {
        shortPieces[i] = regexEscape(shortPieces[i]);
        longPieces[i] = regexEscape(longPieces[i]);
        mixedPieces[i] = regexEscape(mixedPieces[i]);
    }

    this._weekdaysRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
    this._weekdaysShortRegex = this._weekdaysRegex;
    this._weekdaysMinRegex = this._weekdaysRegex;

    this._weekdaysStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
    this._weekdaysShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
    this._weekdaysMinStrictRegex = new RegExp('^(' + minPieces.join('|') + ')', 'i');
}

// FORMATTING

function hFormat() {
    return this.hours() % 12 || 12;
}

function kFormat() {
    return this.hours() || 24;
}

addFormatToken('H', ['HH', 2], 0, 'hour');
addFormatToken('h', ['hh', 2], 0, hFormat);
addFormatToken('k', ['kk', 2], 0, kFormat);

addFormatToken('hmm', 0, 0, function () {
    return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2);
});

addFormatToken('hmmss', 0, 0, function () {
    return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2) +
        zeroFill(this.seconds(), 2);
});

addFormatToken('Hmm', 0, 0, function () {
    return '' + this.hours() + zeroFill(this.minutes(), 2);
});

addFormatToken('Hmmss', 0, 0, function () {
    return '' + this.hours() + zeroFill(this.minutes(), 2) +
        zeroFill(this.seconds(), 2);
});

function meridiem (token, lowercase) {
    addFormatToken(token, 0, 0, function () {
        return this.localeData().meridiem(this.hours(), this.minutes(), lowercase);
    });
}

meridiem('a', true);
meridiem('A', false);

// ALIASES

addUnitAlias('hour', 'h');

// PRIORITY
addUnitPriority('hour', 13);

// PARSING

function matchMeridiem (isStrict, locale) {
    return locale._meridiemParse;
}

addRegexToken('a',  matchMeridiem);
addRegexToken('A',  matchMeridiem);
addRegexToken('H',  match1to2);
addRegexToken('h',  match1to2);
addRegexToken('k',  match1to2);
addRegexToken('HH', match1to2, match2);
addRegexToken('hh', match1to2, match2);
addRegexToken('kk', match1to2, match2);

addRegexToken('hmm', match3to4);
addRegexToken('hmmss', match5to6);
addRegexToken('Hmm', match3to4);
addRegexToken('Hmmss', match5to6);

addParseToken(['H', 'HH'], HOUR);
addParseToken(['k', 'kk'], function (input, array, config) {
    var kInput = toInt(input);
    array[HOUR] = kInput === 24 ? 0 : kInput;
});
addParseToken(['a', 'A'], function (input, array, config) {
    config._isPm = config._locale.isPM(input);
    config._meridiem = input;
});
addParseToken(['h', 'hh'], function (input, array, config) {
    array[HOUR] = toInt(input);
    getParsingFlags(config).bigHour = true;
});
addParseToken('hmm', function (input, array, config) {
    var pos = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos));
    array[MINUTE] = toInt(input.substr(pos));
    getParsingFlags(config).bigHour = true;
});
addParseToken('hmmss', function (input, array, config) {
    var pos1 = input.length - 4;
    var pos2 = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos1));
    array[MINUTE] = toInt(input.substr(pos1, 2));
    array[SECOND] = toInt(input.substr(pos2));
    getParsingFlags(config).bigHour = true;
});
addParseToken('Hmm', function (input, array, config) {
    var pos = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos));
    array[MINUTE] = toInt(input.substr(pos));
});
addParseToken('Hmmss', function (input, array, config) {
    var pos1 = input.length - 4;
    var pos2 = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos1));
    array[MINUTE] = toInt(input.substr(pos1, 2));
    array[SECOND] = toInt(input.substr(pos2));
});

// LOCALES

function localeIsPM (input) {
    // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
    // Using charAt should be more compatible.
    return ((input + '').toLowerCase().charAt(0) === 'p');
}

var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i;
function localeMeridiem (hours, minutes, isLower) {
    if (hours > 11) {
        return isLower ? 'pm' : 'PM';
    } else {
        return isLower ? 'am' : 'AM';
    }
}


// MOMENTS

// Setting the hour should keep the time, because the user explicitly
// specified which hour he wants. So trying to maintain the same hour (in
// a new timezone) makes sense. Adding/subtracting hours does not follow
// this rule.
var getSetHour = makeGetSet('Hours', true);

// months
// week
// weekdays
// meridiem
var baseConfig = {
    calendar: defaultCalendar,
    longDateFormat: defaultLongDateFormat,
    invalidDate: defaultInvalidDate,
    ordinal: defaultOrdinal,
    dayOfMonthOrdinalParse: defaultDayOfMonthOrdinalParse,
    relativeTime: defaultRelativeTime,

    months: defaultLocaleMonths,
    monthsShort: defaultLocaleMonthsShort,

    week: defaultLocaleWeek,

    weekdays: defaultLocaleWeekdays,
    weekdaysMin: defaultLocaleWeekdaysMin,
    weekdaysShort: defaultLocaleWeekdaysShort,

    meridiemParse: defaultLocaleMeridiemParse
};

// internal storage for locale config files
var locales = {};
var localeFamilies = {};
var globalLocale;

function normalizeLocale(key) {
    return key ? key.toLowerCase().replace('_', '-') : key;
}

// pick the locale from the array
// try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
// substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
function chooseLocale(names) {
    var i = 0, j, next, locale, split;

    while (i < names.length) {
        split = normalizeLocale(names[i]).split('-');
        j = split.length;
        next = normalizeLocale(names[i + 1]);
        next = next ? next.split('-') : null;
        while (j > 0) {
            locale = loadLocale(split.slice(0, j).join('-'));
            if (locale) {
                return locale;
            }
            if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
                //the next array item is better than a shallower substring of this one
                break;
            }
            j--;
        }
        i++;
    }
    return null;
}

function loadLocale(name) {
    var oldLocale = null;
    // TODO: Find a better way to register and load all the locales in Node
    if (!locales[name] && (typeof module !== 'undefined') &&
            module && module.exports) {
        try {
            oldLocale = globalLocale._abbr;
            require('./locale/' + name);
            // because defineLocale currently also sets the global locale, we
            // want to undo that for lazy loaded locales
            getSetGlobalLocale(oldLocale);
        } catch (e) { }
    }
    return locales[name];
}

// This function will load locale and then set the global locale.  If
// no arguments are passed in, it will simply return the current global
// locale key.
function getSetGlobalLocale (key, values) {
    var data;
    if (key) {
        if (isUndefined(values)) {
            data = getLocale(key);
        }
        else {
            data = defineLocale(key, values);
        }

        if (data) {
            // moment.duration._locale = moment._locale = data;
            globalLocale = data;
        }
    }

    return globalLocale._abbr;
}

function defineLocale (name, config) {
    if (config !== null) {
        var parentConfig = baseConfig;
        config.abbr = name;
        if (locales[name] != null) {
            deprecateSimple('defineLocaleOverride',
                    'use moment.updateLocale(localeName, config) to change ' +
                    'an existing locale. moment.defineLocale(localeName, ' +
                    'config) should only be used for creating a new locale ' +
                    'See http://momentjs.com/guides/#/warnings/define-locale/ for more info.');
            parentConfig = locales[name]._config;
        } else if (config.parentLocale != null) {
            if (locales[config.parentLocale] != null) {
                parentConfig = locales[config.parentLocale]._config;
            } else {
                if (!localeFamilies[config.parentLocale]) {
                    localeFamilies[config.parentLocale] = [];
                }
                localeFamilies[config.parentLocale].push({
                    name: name,
                    config: config
                });
                return null;
            }
        }
        locales[name] = new Locale(mergeConfigs(parentConfig, config));

        if (localeFamilies[name]) {
            localeFamilies[name].forEach(function (x) {
                defineLocale(x.name, x.config);
            });
        }

        // backwards compat for now: also set the locale
        // make sure we set the locale AFTER all child locales have been
        // created, so we won't end up with the child locale set.
        getSetGlobalLocale(name);


        return locales[name];
    } else {
        // useful for testing
        delete locales[name];
        return null;
    }
}

function updateLocale(name, config) {
    if (config != null) {
        var locale, parentConfig = baseConfig;
        // MERGE
        if (locales[name] != null) {
            parentConfig = locales[name]._config;
        }
        config = mergeConfigs(parentConfig, config);
        locale = new Locale(config);
        locale.parentLocale = locales[name];
        locales[name] = locale;

        // backwards compat for now: also set the locale
        getSetGlobalLocale(name);
    } else {
        // pass null for config to unupdate, useful for tests
        if (locales[name] != null) {
            if (locales[name].parentLocale != null) {
                locales[name] = locales[name].parentLocale;
            } else if (locales[name] != null) {
                delete locales[name];
            }
        }
    }
    return locales[name];
}

// returns locale data
function getLocale (key) {
    var locale;

    if (key && key._locale && key._locale._abbr) {
        key = key._locale._abbr;
    }

    if (!key) {
        return globalLocale;
    }

    if (!isArray(key)) {
        //short-circuit everything else
        locale = loadLocale(key);
        if (locale) {
            return locale;
        }
        key = [key];
    }

    return chooseLocale(key);
}

function listLocales() {
    return keys$1(locales);
}

function checkOverflow (m) {
    var overflow;
    var a = m._a;

    if (a && getParsingFlags(m).overflow === -2) {
        overflow =
            a[MONTH]       < 0 || a[MONTH]       > 11  ? MONTH :
            a[DATE]        < 1 || a[DATE]        > daysInMonth(a[YEAR], a[MONTH]) ? DATE :
            a[HOUR]        < 0 || a[HOUR]        > 24 || (a[HOUR] === 24 && (a[MINUTE] !== 0 || a[SECOND] !== 0 || a[MILLISECOND] !== 0)) ? HOUR :
            a[MINUTE]      < 0 || a[MINUTE]      > 59  ? MINUTE :
            a[SECOND]      < 0 || a[SECOND]      > 59  ? SECOND :
            a[MILLISECOND] < 0 || a[MILLISECOND] > 999 ? MILLISECOND :
            -1;

        if (getParsingFlags(m)._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
            overflow = DATE;
        }
        if (getParsingFlags(m)._overflowWeeks && overflow === -1) {
            overflow = WEEK;
        }
        if (getParsingFlags(m)._overflowWeekday && overflow === -1) {
            overflow = WEEKDAY;
        }

        getParsingFlags(m).overflow = overflow;
    }

    return m;
}

// iso 8601 regex
// 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)
var extendedIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;
var basicIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;

var tzRegex = /Z|[+-]\d\d(?::?\d\d)?/;

var isoDates = [
    ['YYYYYY-MM-DD', /[+-]\d{6}-\d\d-\d\d/],
    ['YYYY-MM-DD', /\d{4}-\d\d-\d\d/],
    ['GGGG-[W]WW-E', /\d{4}-W\d\d-\d/],
    ['GGGG-[W]WW', /\d{4}-W\d\d/, false],
    ['YYYY-DDD', /\d{4}-\d{3}/],
    ['YYYY-MM', /\d{4}-\d\d/, false],
    ['YYYYYYMMDD', /[+-]\d{10}/],
    ['YYYYMMDD', /\d{8}/],
    // YYYYMM is NOT allowed by the standard
    ['GGGG[W]WWE', /\d{4}W\d{3}/],
    ['GGGG[W]WW', /\d{4}W\d{2}/, false],
    ['YYYYDDD', /\d{7}/]
];

// iso time formats and regexes
var isoTimes = [
    ['HH:mm:ss.SSSS', /\d\d:\d\d:\d\d\.\d+/],
    ['HH:mm:ss,SSSS', /\d\d:\d\d:\d\d,\d+/],
    ['HH:mm:ss', /\d\d:\d\d:\d\d/],
    ['HH:mm', /\d\d:\d\d/],
    ['HHmmss.SSSS', /\d\d\d\d\d\d\.\d+/],
    ['HHmmss,SSSS', /\d\d\d\d\d\d,\d+/],
    ['HHmmss', /\d\d\d\d\d\d/],
    ['HHmm', /\d\d\d\d/],
    ['HH', /\d\d/]
];

var aspNetJsonRegex = /^\/?Date\((\-?\d+)/i;

// date from iso format
function configFromISO(config) {
    var i, l,
        string = config._i,
        match = extendedIsoRegex.exec(string) || basicIsoRegex.exec(string),
        allowTime, dateFormat, timeFormat, tzFormat;

    if (match) {
        getParsingFlags(config).iso = true;

        for (i = 0, l = isoDates.length; i < l; i++) {
            if (isoDates[i][1].exec(match[1])) {
                dateFormat = isoDates[i][0];
                allowTime = isoDates[i][2] !== false;
                break;
            }
        }
        if (dateFormat == null) {
            config._isValid = false;
            return;
        }
        if (match[3]) {
            for (i = 0, l = isoTimes.length; i < l; i++) {
                if (isoTimes[i][1].exec(match[3])) {
                    // match[2] should be 'T' or space
                    timeFormat = (match[2] || ' ') + isoTimes[i][0];
                    break;
                }
            }
            if (timeFormat == null) {
                config._isValid = false;
                return;
            }
        }
        if (!allowTime && timeFormat != null) {
            config._isValid = false;
            return;
        }
        if (match[4]) {
            if (tzRegex.exec(match[4])) {
                tzFormat = 'Z';
            } else {
                config._isValid = false;
                return;
            }
        }
        config._f = dateFormat + (timeFormat || '') + (tzFormat || '');
        configFromStringAndFormat(config);
    } else {
        config._isValid = false;
    }
}

// RFC 2822 regex: For details see https://tools.ietf.org/html/rfc2822#section-3.3
var basicRfcRegex = /^((?:Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d?\d\s(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(?:\d\d)?\d\d\s)(\d\d:\d\d)(\:\d\d)?(\s(?:UT|GMT|[ECMP][SD]T|[A-IK-Za-ik-z]|[+-]\d{4}))$/;

// date and time from ref 2822 format
function configFromRFC2822(config) {
    var string, match, dayFormat,
        dateFormat, timeFormat, tzFormat;
    var timezones = {
        ' GMT': ' +0000',
        ' EDT': ' -0400',
        ' EST': ' -0500',
        ' CDT': ' -0500',
        ' CST': ' -0600',
        ' MDT': ' -0600',
        ' MST': ' -0700',
        ' PDT': ' -0700',
        ' PST': ' -0800'
    };
    var military = 'YXWVUTSRQPONZABCDEFGHIKLM';
    var timezone, timezoneIndex;

    string = config._i
        .replace(/\([^\)]*\)|[\n\t]/g, ' ') // Remove comments and folding whitespace
        .replace(/(\s\s+)/g, ' ') // Replace multiple-spaces with a single space
        .replace(/^\s|\s$/g, ''); // Remove leading and trailing spaces
    match = basicRfcRegex.exec(string);

    if (match) {
        dayFormat = match[1] ? 'ddd' + ((match[1].length === 5) ? ', ' : ' ') : '';
        dateFormat = 'D MMM ' + ((match[2].length > 10) ? 'YYYY ' : 'YY ');
        timeFormat = 'HH:mm' + (match[4] ? ':ss' : '');

        // TODO: Replace the vanilla JS Date object with an indepentent day-of-week check.
        if (match[1]) { // day of week given
            var momentDate = new Date(match[2]);
            var momentDay = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'][momentDate.getDay()];

            if (match[1].substr(0,3) !== momentDay) {
                getParsingFlags(config).weekdayMismatch = true;
                config._isValid = false;
                return;
            }
        }

        switch (match[5].length) {
            case 2: // military
                if (timezoneIndex === 0) {
                    timezone = ' +0000';
                } else {
                    timezoneIndex = military.indexOf(match[5][1].toUpperCase()) - 12;
                    timezone = ((timezoneIndex < 0) ? ' -' : ' +') +
                        (('' + timezoneIndex).replace(/^-?/, '0')).match(/..$/)[0] + '00';
                }
                break;
            case 4: // Zone
                timezone = timezones[match[5]];
                break;
            default: // UT or +/-9999
                timezone = timezones[' GMT'];
        }
        match[5] = timezone;
        config._i = match.splice(1).join('');
        tzFormat = ' ZZ';
        config._f = dayFormat + dateFormat + timeFormat + tzFormat;
        configFromStringAndFormat(config);
        getParsingFlags(config).rfc2822 = true;
    } else {
        config._isValid = false;
    }
}

// date from iso format or fallback
function configFromString(config) {
    var matched = aspNetJsonRegex.exec(config._i);

    if (matched !== null) {
        config._d = new Date(+matched[1]);
        return;
    }

    configFromISO(config);
    if (config._isValid === false) {
        delete config._isValid;
    } else {
        return;
    }

    configFromRFC2822(config);
    if (config._isValid === false) {
        delete config._isValid;
    } else {
        return;
    }

    // Final attempt, use Input Fallback
    hooks.createFromInputFallback(config);
}

hooks.createFromInputFallback = deprecate(
    'value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), ' +
    'which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are ' +
    'discouraged and will be removed in an upcoming major release. Please refer to ' +
    'http://momentjs.com/guides/#/warnings/js-date/ for more info.',
    function (config) {
        config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
    }
);

// Pick the first defined of two or three arguments.
function defaults(a, b, c) {
    if (a != null) {
        return a;
    }
    if (b != null) {
        return b;
    }
    return c;
}

function currentDateArray(config) {
    // hooks is actually the exported moment object
    var nowValue = new Date(hooks.now());
    if (config._useUTC) {
        return [nowValue.getUTCFullYear(), nowValue.getUTCMonth(), nowValue.getUTCDate()];
    }
    return [nowValue.getFullYear(), nowValue.getMonth(), nowValue.getDate()];
}

// convert an array to a date.
// the array should mirror the parameters below
// note: all values past the year are optional and will default to the lowest possible value.
// [year, month, day , hour, minute, second, millisecond]
function configFromArray (config) {
    var i, date, input = [], currentDate, yearToUse;

    if (config._d) {
        return;
    }

    currentDate = currentDateArray(config);

    //compute day of the year from weeks and weekdays
    if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
        dayOfYearFromWeekInfo(config);
    }

    //if the day of the year is set, figure out what it is
    if (config._dayOfYear != null) {
        yearToUse = defaults(config._a[YEAR], currentDate[YEAR]);

        if (config._dayOfYear > daysInYear(yearToUse) || config._dayOfYear === 0) {
            getParsingFlags(config)._overflowDayOfYear = true;
        }

        date = createUTCDate(yearToUse, 0, config._dayOfYear);
        config._a[MONTH] = date.getUTCMonth();
        config._a[DATE] = date.getUTCDate();
    }

    // Default to current date.
    // * if no year, month, day of month are given, default to today
    // * if day of month is given, default month and year
    // * if month is given, default only year
    // * if year is given, don't default anything
    for (i = 0; i < 3 && config._a[i] == null; ++i) {
        config._a[i] = input[i] = currentDate[i];
    }

    // Zero out whatever was not defaulted, including time
    for (; i < 7; i++) {
        config._a[i] = input[i] = (config._a[i] == null) ? (i === 2 ? 1 : 0) : config._a[i];
    }

    // Check for 24:00:00.000
    if (config._a[HOUR] === 24 &&
            config._a[MINUTE] === 0 &&
            config._a[SECOND] === 0 &&
            config._a[MILLISECOND] === 0) {
        config._nextDay = true;
        config._a[HOUR] = 0;
    }

    config._d = (config._useUTC ? createUTCDate : createDate).apply(null, input);
    // Apply timezone offset from input. The actual utcOffset can be changed
    // with parseZone.
    if (config._tzm != null) {
        config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
    }

    if (config._nextDay) {
        config._a[HOUR] = 24;
    }
}

function dayOfYearFromWeekInfo(config) {
    var w, weekYear, week, weekday, dow, doy, temp, weekdayOverflow;

    w = config._w;
    if (w.GG != null || w.W != null || w.E != null) {
        dow = 1;
        doy = 4;

        // TODO: We need to take the current isoWeekYear, but that depends on
        // how we interpret now (local, utc, fixed offset). So create
        // a now version of current config (take local/utc/offset flags, and
        // create now).
        weekYear = defaults(w.GG, config._a[YEAR], weekOfYear(createLocal(), 1, 4).year);
        week = defaults(w.W, 1);
        weekday = defaults(w.E, 1);
        if (weekday < 1 || weekday > 7) {
            weekdayOverflow = true;
        }
    } else {
        dow = config._locale._week.dow;
        doy = config._locale._week.doy;

        var curWeek = weekOfYear(createLocal(), dow, doy);

        weekYear = defaults(w.gg, config._a[YEAR], curWeek.year);

        // Default to current week.
        week = defaults(w.w, curWeek.week);

        if (w.d != null) {
            // weekday -- low day numbers are considered next week
            weekday = w.d;
            if (weekday < 0 || weekday > 6) {
                weekdayOverflow = true;
            }
        } else if (w.e != null) {
            // local weekday -- counting starts from begining of week
            weekday = w.e + dow;
            if (w.e < 0 || w.e > 6) {
                weekdayOverflow = true;
            }
        } else {
            // default to begining of week
            weekday = dow;
        }
    }
    if (week < 1 || week > weeksInYear(weekYear, dow, doy)) {
        getParsingFlags(config)._overflowWeeks = true;
    } else if (weekdayOverflow != null) {
        getParsingFlags(config)._overflowWeekday = true;
    } else {
        temp = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy);
        config._a[YEAR] = temp.year;
        config._dayOfYear = temp.dayOfYear;
    }
}

// constant that refers to the ISO standard
hooks.ISO_8601 = function () {};

// constant that refers to the RFC 2822 form
hooks.RFC_2822 = function () {};

// date from string and format string
function configFromStringAndFormat(config) {
    // TODO: Move this to another part of the creation flow to prevent circular deps
    if (config._f === hooks.ISO_8601) {
        configFromISO(config);
        return;
    }
    if (config._f === hooks.RFC_2822) {
        configFromRFC2822(config);
        return;
    }
    config._a = [];
    getParsingFlags(config).empty = true;

    // This array is used to make a Date, either with `new Date` or `Date.UTC`
    var string = '' + config._i,
        i, parsedInput, tokens, token, skipped,
        stringLength = string.length,
        totalParsedInputLength = 0;

    tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];

    for (i = 0; i < tokens.length; i++) {
        token = tokens[i];
        parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0];
        // console.log('token', token, 'parsedInput', parsedInput,
        //         'regex', getParseRegexForToken(token, config));
        if (parsedInput) {
            skipped = string.substr(0, string.indexOf(parsedInput));
            if (skipped.length > 0) {
                getParsingFlags(config).unusedInput.push(skipped);
            }
            string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
            totalParsedInputLength += parsedInput.length;
        }
        // don't parse if it's not a known token
        if (formatTokenFunctions[token]) {
            if (parsedInput) {
                getParsingFlags(config).empty = false;
            }
            else {
                getParsingFlags(config).unusedTokens.push(token);
            }
            addTimeToArrayFromToken(token, parsedInput, config);
        }
        else if (config._strict && !parsedInput) {
            getParsingFlags(config).unusedTokens.push(token);
        }
    }

    // add remaining unparsed input length to the string
    getParsingFlags(config).charsLeftOver = stringLength - totalParsedInputLength;
    if (string.length > 0) {
        getParsingFlags(config).unusedInput.push(string);
    }

    // clear _12h flag if hour is <= 12
    if (config._a[HOUR] <= 12 &&
        getParsingFlags(config).bigHour === true &&
        config._a[HOUR] > 0) {
        getParsingFlags(config).bigHour = undefined;
    }

    getParsingFlags(config).parsedDateParts = config._a.slice(0);
    getParsingFlags(config).meridiem = config._meridiem;
    // handle meridiem
    config._a[HOUR] = meridiemFixWrap(config._locale, config._a[HOUR], config._meridiem);

    configFromArray(config);
    checkOverflow(config);
}


function meridiemFixWrap (locale, hour, meridiem) {
    var isPm;

    if (meridiem == null) {
        // nothing to do
        return hour;
    }
    if (locale.meridiemHour != null) {
        return locale.meridiemHour(hour, meridiem);
    } else if (locale.isPM != null) {
        // Fallback
        isPm = locale.isPM(meridiem);
        if (isPm && hour < 12) {
            hour += 12;
        }
        if (!isPm && hour === 12) {
            hour = 0;
        }
        return hour;
    } else {
        // this is not supposed to happen
        return hour;
    }
}

// date from string and array of format strings
function configFromStringAndArray(config) {
    var tempConfig,
        bestMoment,

        scoreToBeat,
        i,
        currentScore;

    if (config._f.length === 0) {
        getParsingFlags(config).invalidFormat = true;
        config._d = new Date(NaN);
        return;
    }

    for (i = 0; i < config._f.length; i++) {
        currentScore = 0;
        tempConfig = copyConfig({}, config);
        if (config._useUTC != null) {
            tempConfig._useUTC = config._useUTC;
        }
        tempConfig._f = config._f[i];
        configFromStringAndFormat(tempConfig);

        if (!isValid(tempConfig)) {
            continue;
        }

        // if there is any input that was not parsed add a penalty for that format
        currentScore += getParsingFlags(tempConfig).charsLeftOver;

        //or tokens
        currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;

        getParsingFlags(tempConfig).score = currentScore;

        if (scoreToBeat == null || currentScore < scoreToBeat) {
            scoreToBeat = currentScore;
            bestMoment = tempConfig;
        }
    }

    extend(config, bestMoment || tempConfig);
}

function configFromObject(config) {
    if (config._d) {
        return;
    }

    var i = normalizeObjectUnits(config._i);
    config._a = map([i.year, i.month, i.day || i.date, i.hour, i.minute, i.second, i.millisecond], function (obj) {
        return obj && parseInt(obj, 10);
    });

    configFromArray(config);
}

function createFromConfig (config) {
    var res = new Moment(checkOverflow(prepareConfig(config)));
    if (res._nextDay) {
        // Adding is smart enough around DST
        res.add(1, 'd');
        res._nextDay = undefined;
    }

    return res;
}

function prepareConfig (config) {
    var input = config._i,
        format = config._f;

    config._locale = config._locale || getLocale(config._l);

    if (input === null || (format === undefined && input === '')) {
        return createInvalid({nullInput: true});
    }

    if (typeof input === 'string') {
        config._i = input = config._locale.preparse(input);
    }

    if (isMoment(input)) {
        return new Moment(checkOverflow(input));
    } else if (isDate(input)) {
        config._d = input;
    } else if (isArray(format)) {
        configFromStringAndArray(config);
    } else if (format) {
        configFromStringAndFormat(config);
    }  else {
        configFromInput(config);
    }

    if (!isValid(config)) {
        config._d = null;
    }

    return config;
}

function configFromInput(config) {
    var input = config._i;
    if (isUndefined(input)) {
        config._d = new Date(hooks.now());
    } else if (isDate(input)) {
        config._d = new Date(input.valueOf());
    } else if (typeof input === 'string') {
        configFromString(config);
    } else if (isArray(input)) {
        config._a = map(input.slice(0), function (obj) {
            return parseInt(obj, 10);
        });
        configFromArray(config);
    } else if (isObject(input)) {
        configFromObject(config);
    } else if (isNumber(input)) {
        // from milliseconds
        config._d = new Date(input);
    } else {
        hooks.createFromInputFallback(config);
    }
}

function createLocalOrUTC (input, format, locale, strict, isUTC) {
    var c = {};

    if (locale === true || locale === false) {
        strict = locale;
        locale = undefined;
    }

    if ((isObject(input) && isObjectEmpty(input)) ||
            (isArray(input) && input.length === 0)) {
        input = undefined;
    }
    // object construction must be done this way.
    // https://github.com/moment/moment/issues/1423
    c._isAMomentObject = true;
    c._useUTC = c._isUTC = isUTC;
    c._l = locale;
    c._i = input;
    c._f = format;
    c._strict = strict;

    return createFromConfig(c);
}

function createLocal (input, format, locale, strict) {
    return createLocalOrUTC(input, format, locale, strict, false);
}

var prototypeMin = deprecate(
    'moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/',
    function () {
        var other = createLocal.apply(null, arguments);
        if (this.isValid() && other.isValid()) {
            return other < this ? this : other;
        } else {
            return createInvalid();
        }
    }
);

var prototypeMax = deprecate(
    'moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/',
    function () {
        var other = createLocal.apply(null, arguments);
        if (this.isValid() && other.isValid()) {
            return other > this ? this : other;
        } else {
            return createInvalid();
        }
    }
);

// Pick a moment m from moments so that m[fn](other) is true for all
// other. This relies on the function fn to be transitive.
//
// moments should either be an array of moment objects or an array, whose
// first element is an array of moment objects.
function pickBy(fn, moments) {
    var res, i;
    if (moments.length === 1 && isArray(moments[0])) {
        moments = moments[0];
    }
    if (!moments.length) {
        return createLocal();
    }
    res = moments[0];
    for (i = 1; i < moments.length; ++i) {
        if (!moments[i].isValid() || moments[i][fn](res)) {
            res = moments[i];
        }
    }
    return res;
}

// TODO: Use [].sort instead?
function min () {
    var args = [].slice.call(arguments, 0);

    return pickBy('isBefore', args);
}

function max () {
    var args = [].slice.call(arguments, 0);

    return pickBy('isAfter', args);
}

var now = function () {
    return Date.now ? Date.now() : +(new Date());
};

var ordering = ['year', 'quarter', 'month', 'week', 'day', 'hour', 'minute', 'second', 'millisecond'];

function isDurationValid(m) {
    for (var key in m) {
        if (!(ordering.indexOf(key) !== -1 && (m[key] == null || !isNaN(m[key])))) {
            return false;
        }
    }

    var unitHasDecimal = false;
    for (var i = 0; i < ordering.length; ++i) {
        if (m[ordering[i]]) {
            if (unitHasDecimal) {
                return false; // only allow non-integers for smallest unit
            }
            if (parseFloat(m[ordering[i]]) !== toInt(m[ordering[i]])) {
                unitHasDecimal = true;
            }
        }
    }

    return true;
}

function isValid$1() {
    return this._isValid;
}

function createInvalid$1() {
    return createDuration(NaN);
}

function Duration (duration) {
    var normalizedInput = normalizeObjectUnits(duration),
        years = normalizedInput.year || 0,
        quarters = normalizedInput.quarter || 0,
        months = normalizedInput.month || 0,
        weeks = normalizedInput.week || 0,
        days = normalizedInput.day || 0,
        hours = normalizedInput.hour || 0,
        minutes = normalizedInput.minute || 0,
        seconds = normalizedInput.second || 0,
        milliseconds = normalizedInput.millisecond || 0;

    this._isValid = isDurationValid(normalizedInput);

    // representation for dateAddRemove
    this._milliseconds = +milliseconds +
        seconds * 1e3 + // 1000
        minutes * 6e4 + // 1000 * 60
        hours * 1000 * 60 * 60; //using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
    // Because of dateAddRemove treats 24 hours as different from a
    // day when working around DST, we need to store them separately
    this._days = +days +
        weeks * 7;
    // It is impossible translate months into days without knowing
    // which months you are are talking about, so we have to store
    // it separately.
    this._months = +months +
        quarters * 3 +
        years * 12;

    this._data = {};

    this._locale = getLocale();

    this._bubble();
}

function isDuration (obj) {
    return obj instanceof Duration;
}

function absRound (number) {
    if (number < 0) {
        return Math.round(-1 * number) * -1;
    } else {
        return Math.round(number);
    }
}

// FORMATTING

function offset (token, separator) {
    addFormatToken(token, 0, 0, function () {
        var offset = this.utcOffset();
        var sign = '+';
        if (offset < 0) {
            offset = -offset;
            sign = '-';
        }
        return sign + zeroFill(~~(offset / 60), 2) + separator + zeroFill(~~(offset) % 60, 2);
    });
}

offset('Z', ':');
offset('ZZ', '');

// PARSING

addRegexToken('Z',  matchShortOffset);
addRegexToken('ZZ', matchShortOffset);
addParseToken(['Z', 'ZZ'], function (input, array, config) {
    config._useUTC = true;
    config._tzm = offsetFromString(matchShortOffset, input);
});

// HELPERS

// timezone chunker
// '+10:00' > ['10',  '00']
// '-1530'  > ['-15', '30']
var chunkOffset = /([\+\-]|\d\d)/gi;

function offsetFromString(matcher, string) {
    var matches = (string || '').match(matcher);

    if (matches === null) {
        return null;
    }

    var chunk   = matches[matches.length - 1] || [];
    var parts   = (chunk + '').match(chunkOffset) || ['-', 0, 0];
    var minutes = +(parts[1] * 60) + toInt(parts[2]);

    return minutes === 0 ?
      0 :
      parts[0] === '+' ? minutes : -minutes;
}

// Return a moment from input, that is local/utc/zone equivalent to model.
function cloneWithOffset(input, model) {
    var res, diff;
    if (model._isUTC) {
        res = model.clone();
        diff = (isMoment(input) || isDate(input) ? input.valueOf() : createLocal(input).valueOf()) - res.valueOf();
        // Use low-level api, because this fn is low-level api.
        res._d.setTime(res._d.valueOf() + diff);
        hooks.updateOffset(res, false);
        return res;
    } else {
        return createLocal(input).local();
    }
}

function getDateOffset (m) {
    // On Firefox.24 Date#getTimezoneOffset returns a floating point.
    // https://github.com/moment/moment/pull/1871
    return -Math.round(m._d.getTimezoneOffset() / 15) * 15;
}

// HOOKS

// This function will be called whenever a moment is mutated.
// It is intended to keep the offset in sync with the timezone.
hooks.updateOffset = function () {};

// MOMENTS

// keepLocalTime = true means only change the timezone, without
// affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
// 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
// +0200, so we adjust the time as needed, to be valid.
//
// Keeping the time actually adds/subtracts (one hour)
// from the actual represented time. That is why we call updateOffset
// a second time. In case it wants us to change the offset again
// _changeInProgress == true case, then we have to adjust, because
// there is no such time in the given timezone.
function getSetOffset (input, keepLocalTime, keepMinutes) {
    var offset = this._offset || 0,
        localAdjust;
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    if (input != null) {
        if (typeof input === 'string') {
            input = offsetFromString(matchShortOffset, input);
            if (input === null) {
                return this;
            }
        } else if (Math.abs(input) < 16 && !keepMinutes) {
            input = input * 60;
        }
        if (!this._isUTC && keepLocalTime) {
            localAdjust = getDateOffset(this);
        }
        this._offset = input;
        this._isUTC = true;
        if (localAdjust != null) {
            this.add(localAdjust, 'm');
        }
        if (offset !== input) {
            if (!keepLocalTime || this._changeInProgress) {
                addSubtract(this, createDuration(input - offset, 'm'), 1, false);
            } else if (!this._changeInProgress) {
                this._changeInProgress = true;
                hooks.updateOffset(this, true);
                this._changeInProgress = null;
            }
        }
        return this;
    } else {
        return this._isUTC ? offset : getDateOffset(this);
    }
}

function getSetZone (input, keepLocalTime) {
    if (input != null) {
        if (typeof input !== 'string') {
            input = -input;
        }

        this.utcOffset(input, keepLocalTime);

        return this;
    } else {
        return -this.utcOffset();
    }
}

function setOffsetToUTC (keepLocalTime) {
    return this.utcOffset(0, keepLocalTime);
}

function setOffsetToLocal (keepLocalTime) {
    if (this._isUTC) {
        this.utcOffset(0, keepLocalTime);
        this._isUTC = false;

        if (keepLocalTime) {
            this.subtract(getDateOffset(this), 'm');
        }
    }
    return this;
}

function setOffsetToParsedOffset () {
    if (this._tzm != null) {
        this.utcOffset(this._tzm, false, true);
    } else if (typeof this._i === 'string') {
        var tZone = offsetFromString(matchOffset, this._i);
        if (tZone != null) {
            this.utcOffset(tZone);
        }
        else {
            this.utcOffset(0, true);
        }
    }
    return this;
}

function hasAlignedHourOffset (input) {
    if (!this.isValid()) {
        return false;
    }
    input = input ? createLocal(input).utcOffset() : 0;

    return (this.utcOffset() - input) % 60 === 0;
}

function isDaylightSavingTime () {
    return (
        this.utcOffset() > this.clone().month(0).utcOffset() ||
        this.utcOffset() > this.clone().month(5).utcOffset()
    );
}

function isDaylightSavingTimeShifted () {
    if (!isUndefined(this._isDSTShifted)) {
        return this._isDSTShifted;
    }

    var c = {};

    copyConfig(c, this);
    c = prepareConfig(c);

    if (c._a) {
        var other = c._isUTC ? createUTC(c._a) : createLocal(c._a);
        this._isDSTShifted = this.isValid() &&
            compareArrays(c._a, other.toArray()) > 0;
    } else {
        this._isDSTShifted = false;
    }

    return this._isDSTShifted;
}

function isLocal () {
    return this.isValid() ? !this._isUTC : false;
}

function isUtcOffset () {
    return this.isValid() ? this._isUTC : false;
}

function isUtc () {
    return this.isValid() ? this._isUTC && this._offset === 0 : false;
}

// ASP.NET json date format regex
var aspNetRegex = /^(\-)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/;

// from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
// somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
// and further modified to allow for strings containing both week and day
var isoRegex = /^(-)?P(?:(-?[0-9,.]*)Y)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)W)?(?:(-?[0-9,.]*)D)?(?:T(?:(-?[0-9,.]*)H)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)S)?)?$/;

function createDuration (input, key) {
    var duration = input,
        // matching against regexp is expensive, do it on demand
        match = null,
        sign,
        ret,
        diffRes;

    if (isDuration(input)) {
        duration = {
            ms : input._milliseconds,
            d  : input._days,
            M  : input._months
        };
    } else if (isNumber(input)) {
        duration = {};
        if (key) {
            duration[key] = input;
        } else {
            duration.milliseconds = input;
        }
    } else if (!!(match = aspNetRegex.exec(input))) {
        sign = (match[1] === '-') ? -1 : 1;
        duration = {
            y  : 0,
            d  : toInt(match[DATE])                         * sign,
            h  : toInt(match[HOUR])                         * sign,
            m  : toInt(match[MINUTE])                       * sign,
            s  : toInt(match[SECOND])                       * sign,
            ms : toInt(absRound(match[MILLISECOND] * 1000)) * sign // the millisecond decimal point is included in the match
        };
    } else if (!!(match = isoRegex.exec(input))) {
        sign = (match[1] === '-') ? -1 : 1;
        duration = {
            y : parseIso(match[2], sign),
            M : parseIso(match[3], sign),
            w : parseIso(match[4], sign),
            d : parseIso(match[5], sign),
            h : parseIso(match[6], sign),
            m : parseIso(match[7], sign),
            s : parseIso(match[8], sign)
        };
    } else if (duration == null) {// checks for null or undefined
        duration = {};
    } else if (typeof duration === 'object' && ('from' in duration || 'to' in duration)) {
        diffRes = momentsDifference(createLocal(duration.from), createLocal(duration.to));

        duration = {};
        duration.ms = diffRes.milliseconds;
        duration.M = diffRes.months;
    }

    ret = new Duration(duration);

    if (isDuration(input) && hasOwnProp(input, '_locale')) {
        ret._locale = input._locale;
    }

    return ret;
}

createDuration.fn = Duration.prototype;
createDuration.invalid = createInvalid$1;

function parseIso (inp, sign) {
    // We'd normally use ~~inp for this, but unfortunately it also
    // converts floats to ints.
    // inp may be undefined, so careful calling replace on it.
    var res = inp && parseFloat(inp.replace(',', '.'));
    // apply sign while we're at it
    return (isNaN(res) ? 0 : res) * sign;
}

function positiveMomentsDifference(base, other) {
    var res = {milliseconds: 0, months: 0};

    res.months = other.month() - base.month() +
        (other.year() - base.year()) * 12;
    if (base.clone().add(res.months, 'M').isAfter(other)) {
        --res.months;
    }

    res.milliseconds = +other - +(base.clone().add(res.months, 'M'));

    return res;
}

function momentsDifference(base, other) {
    var res;
    if (!(base.isValid() && other.isValid())) {
        return {milliseconds: 0, months: 0};
    }

    other = cloneWithOffset(other, base);
    if (base.isBefore(other)) {
        res = positiveMomentsDifference(base, other);
    } else {
        res = positiveMomentsDifference(other, base);
        res.milliseconds = -res.milliseconds;
        res.months = -res.months;
    }

    return res;
}

// TODO: remove 'name' arg after deprecation is removed
function createAdder(direction, name) {
    return function (val, period) {
        var dur, tmp;
        //invert the arguments, but complain about it
        if (period !== null && !isNaN(+period)) {
            deprecateSimple(name, 'moment().' + name  + '(period, number) is deprecated. Please use moment().' + name + '(number, period). ' +
            'See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info.');
            tmp = val; val = period; period = tmp;
        }

        val = typeof val === 'string' ? +val : val;
        dur = createDuration(val, period);
        addSubtract(this, dur, direction);
        return this;
    };
}

function addSubtract (mom, duration, isAdding, updateOffset) {
    var milliseconds = duration._milliseconds,
        days = absRound(duration._days),
        months = absRound(duration._months);

    if (!mom.isValid()) {
        // No op
        return;
    }

    updateOffset = updateOffset == null ? true : updateOffset;

    if (milliseconds) {
        mom._d.setTime(mom._d.valueOf() + milliseconds * isAdding);
    }
    if (days) {
        set$1(mom, 'Date', get(mom, 'Date') + days * isAdding);
    }
    if (months) {
        setMonth(mom, get(mom, 'Month') + months * isAdding);
    }
    if (updateOffset) {
        hooks.updateOffset(mom, days || months);
    }
}

var add      = createAdder(1, 'add');
var subtract = createAdder(-1, 'subtract');

function getCalendarFormat(myMoment, now) {
    var diff = myMoment.diff(now, 'days', true);
    return diff < -6 ? 'sameElse' :
            diff < -1 ? 'lastWeek' :
            diff < 0 ? 'lastDay' :
            diff < 1 ? 'sameDay' :
            diff < 2 ? 'nextDay' :
            diff < 7 ? 'nextWeek' : 'sameElse';
}

function calendar$1 (time, formats) {
    // We want to compare the start of today, vs this.
    // Getting start-of-today depends on whether we're local/utc/offset or not.
    var now = time || createLocal(),
        sod = cloneWithOffset(now, this).startOf('day'),
        format = hooks.calendarFormat(this, sod) || 'sameElse';

    var output = formats && (isFunction(formats[format]) ? formats[format].call(this, now) : formats[format]);

    return this.format(output || this.localeData().calendar(format, this, createLocal(now)));
}

function clone () {
    return new Moment(this);
}

function isAfter (input, units) {
    var localInput = isMoment(input) ? input : createLocal(input);
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() > localInput.valueOf();
    } else {
        return localInput.valueOf() < this.clone().startOf(units).valueOf();
    }
}

function isBefore (input, units) {
    var localInput = isMoment(input) ? input : createLocal(input);
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() < localInput.valueOf();
    } else {
        return this.clone().endOf(units).valueOf() < localInput.valueOf();
    }
}

function isBetween (from, to, units, inclusivity) {
    inclusivity = inclusivity || '()';
    return (inclusivity[0] === '(' ? this.isAfter(from, units) : !this.isBefore(from, units)) &&
        (inclusivity[1] === ')' ? this.isBefore(to, units) : !this.isAfter(to, units));
}

function isSame (input, units) {
    var localInput = isMoment(input) ? input : createLocal(input),
        inputMs;
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(units || 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() === localInput.valueOf();
    } else {
        inputMs = localInput.valueOf();
        return this.clone().startOf(units).valueOf() <= inputMs && inputMs <= this.clone().endOf(units).valueOf();
    }
}

function isSameOrAfter (input, units) {
    return this.isSame(input, units) || this.isAfter(input,units);
}

function isSameOrBefore (input, units) {
    return this.isSame(input, units) || this.isBefore(input,units);
}

function diff (input, units, asFloat) {
    var that,
        zoneDelta,
        delta, output;

    if (!this.isValid()) {
        return NaN;
    }

    that = cloneWithOffset(input, this);

    if (!that.isValid()) {
        return NaN;
    }

    zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4;

    units = normalizeUnits(units);

    if (units === 'year' || units === 'month' || units === 'quarter') {
        output = monthDiff(this, that);
        if (units === 'quarter') {
            output = output / 3;
        } else if (units === 'year') {
            output = output / 12;
        }
    } else {
        delta = this - that;
        output = units === 'second' ? delta / 1e3 : // 1000
            units === 'minute' ? delta / 6e4 : // 1000 * 60
            units === 'hour' ? delta / 36e5 : // 1000 * 60 * 60
            units === 'day' ? (delta - zoneDelta) / 864e5 : // 1000 * 60 * 60 * 24, negate dst
            units === 'week' ? (delta - zoneDelta) / 6048e5 : // 1000 * 60 * 60 * 24 * 7, negate dst
            delta;
    }
    return asFloat ? output : absFloor(output);
}

function monthDiff (a, b) {
    // difference in months
    var wholeMonthDiff = ((b.year() - a.year()) * 12) + (b.month() - a.month()),
        // b is in (anchor - 1 month, anchor + 1 month)
        anchor = a.clone().add(wholeMonthDiff, 'months'),
        anchor2, adjust;

    if (b - anchor < 0) {
        anchor2 = a.clone().add(wholeMonthDiff - 1, 'months');
        // linear across the month
        adjust = (b - anchor) / (anchor - anchor2);
    } else {
        anchor2 = a.clone().add(wholeMonthDiff + 1, 'months');
        // linear across the month
        adjust = (b - anchor) / (anchor2 - anchor);
    }

    //check for negative zero, return zero if negative zero
    return -(wholeMonthDiff + adjust) || 0;
}

hooks.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ';
hooks.defaultFormatUtc = 'YYYY-MM-DDTHH:mm:ss[Z]';

function toString () {
    return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
}

function toISOString() {
    if (!this.isValid()) {
        return null;
    }
    var m = this.clone().utc();
    if (m.year() < 0 || m.year() > 9999) {
        return formatMoment(m, 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
    }
    if (isFunction(Date.prototype.toISOString)) {
        // native implementation is ~50x faster, use it when we can
        return this.toDate().toISOString();
    }
    return formatMoment(m, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
}

/**
 * Return a human readable representation of a moment that can
 * also be evaluated to get a new moment which is the same
 *
 * @link https://nodejs.org/dist/latest/docs/api/util.html#util_custom_inspect_function_on_objects
 */
function inspect () {
    if (!this.isValid()) {
        return 'moment.invalid(/* ' + this._i + ' */)';
    }
    var func = 'moment';
    var zone = '';
    if (!this.isLocal()) {
        func = this.utcOffset() === 0 ? 'moment.utc' : 'moment.parseZone';
        zone = 'Z';
    }
    var prefix = '[' + func + '("]';
    var year = (0 <= this.year() && this.year() <= 9999) ? 'YYYY' : 'YYYYYY';
    var datetime = '-MM-DD[T]HH:mm:ss.SSS';
    var suffix = zone + '[")]';

    return this.format(prefix + year + datetime + suffix);
}

function format (inputString) {
    if (!inputString) {
        inputString = this.isUtc() ? hooks.defaultFormatUtc : hooks.defaultFormat;
    }
    var output = formatMoment(this, inputString);
    return this.localeData().postformat(output);
}

function from (time, withoutSuffix) {
    if (this.isValid() &&
            ((isMoment(time) && time.isValid()) ||
             createLocal(time).isValid())) {
        return createDuration({to: this, from: time}).locale(this.locale()).humanize(!withoutSuffix);
    } else {
        return this.localeData().invalidDate();
    }
}

function fromNow (withoutSuffix) {
    return this.from(createLocal(), withoutSuffix);
}

function to (time, withoutSuffix) {
    if (this.isValid() &&
            ((isMoment(time) && time.isValid()) ||
             createLocal(time).isValid())) {
        return createDuration({from: this, to: time}).locale(this.locale()).humanize(!withoutSuffix);
    } else {
        return this.localeData().invalidDate();
    }
}

function toNow (withoutSuffix) {
    return this.to(createLocal(), withoutSuffix);
}

// If passed a locale key, it will set the locale for this
// instance.  Otherwise, it will return the locale configuration
// variables for this instance.
function locale (key) {
    var newLocaleData;

    if (key === undefined) {
        return this._locale._abbr;
    } else {
        newLocaleData = getLocale(key);
        if (newLocaleData != null) {
            this._locale = newLocaleData;
        }
        return this;
    }
}

var lang = deprecate(
    'moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.',
    function (key) {
        if (key === undefined) {
            return this.localeData();
        } else {
            return this.locale(key);
        }
    }
);

function localeData () {
    return this._locale;
}

function startOf (units) {
    units = normalizeUnits(units);
    // the following switch intentionally omits break keywords
    // to utilize falling through the cases.
    switch (units) {
        case 'year':
            this.month(0);
            /* falls through */
        case 'quarter':
        case 'month':
            this.date(1);
            /* falls through */
        case 'week':
        case 'isoWeek':
        case 'day':
        case 'date':
            this.hours(0);
            /* falls through */
        case 'hour':
            this.minutes(0);
            /* falls through */
        case 'minute':
            this.seconds(0);
            /* falls through */
        case 'second':
            this.milliseconds(0);
    }

    // weeks are a special case
    if (units === 'week') {
        this.weekday(0);
    }
    if (units === 'isoWeek') {
        this.isoWeekday(1);
    }

    // quarters are also special
    if (units === 'quarter') {
        this.month(Math.floor(this.month() / 3) * 3);
    }

    return this;
}

function endOf (units) {
    units = normalizeUnits(units);
    if (units === undefined || units === 'millisecond') {
        return this;
    }

    // 'date' is an alias for 'day', so it should be considered as such.
    if (units === 'date') {
        units = 'day';
    }

    return this.startOf(units).add(1, (units === 'isoWeek' ? 'week' : units)).subtract(1, 'ms');
}

function valueOf () {
    return this._d.valueOf() - ((this._offset || 0) * 60000);
}

function unix () {
    return Math.floor(this.valueOf() / 1000);
}

function toDate () {
    return new Date(this.valueOf());
}

function toArray () {
    var m = this;
    return [m.year(), m.month(), m.date(), m.hour(), m.minute(), m.second(), m.millisecond()];
}

function toObject () {
    var m = this;
    return {
        years: m.year(),
        months: m.month(),
        date: m.date(),
        hours: m.hours(),
        minutes: m.minutes(),
        seconds: m.seconds(),
        milliseconds: m.milliseconds()
    };
}

function toJSON () {
    // new Date(NaN).toJSON() === null
    return this.isValid() ? this.toISOString() : null;
}

function isValid$2 () {
    return isValid(this);
}

function parsingFlags () {
    return extend({}, getParsingFlags(this));
}

function invalidAt () {
    return getParsingFlags(this).overflow;
}

function creationData() {
    return {
        input: this._i,
        format: this._f,
        locale: this._locale,
        isUTC: this._isUTC,
        strict: this._strict
    };
}

// FORMATTING

addFormatToken(0, ['gg', 2], 0, function () {
    return this.weekYear() % 100;
});

addFormatToken(0, ['GG', 2], 0, function () {
    return this.isoWeekYear() % 100;
});

function addWeekYearFormatToken (token, getter) {
    addFormatToken(0, [token, token.length], 0, getter);
}

addWeekYearFormatToken('gggg',     'weekYear');
addWeekYearFormatToken('ggggg',    'weekYear');
addWeekYearFormatToken('GGGG',  'isoWeekYear');
addWeekYearFormatToken('GGGGG', 'isoWeekYear');

// ALIASES

addUnitAlias('weekYear', 'gg');
addUnitAlias('isoWeekYear', 'GG');

// PRIORITY

addUnitPriority('weekYear', 1);
addUnitPriority('isoWeekYear', 1);


// PARSING

addRegexToken('G',      matchSigned);
addRegexToken('g',      matchSigned);
addRegexToken('GG',     match1to2, match2);
addRegexToken('gg',     match1to2, match2);
addRegexToken('GGGG',   match1to4, match4);
addRegexToken('gggg',   match1to4, match4);
addRegexToken('GGGGG',  match1to6, match6);
addRegexToken('ggggg',  match1to6, match6);

addWeekParseToken(['gggg', 'ggggg', 'GGGG', 'GGGGG'], function (input, week, config, token) {
    week[token.substr(0, 2)] = toInt(input);
});

addWeekParseToken(['gg', 'GG'], function (input, week, config, token) {
    week[token] = hooks.parseTwoDigitYear(input);
});

// MOMENTS

function getSetWeekYear (input) {
    return getSetWeekYearHelper.call(this,
            input,
            this.week(),
            this.weekday(),
            this.localeData()._week.dow,
            this.localeData()._week.doy);
}

function getSetISOWeekYear (input) {
    return getSetWeekYearHelper.call(this,
            input, this.isoWeek(), this.isoWeekday(), 1, 4);
}

function getISOWeeksInYear () {
    return weeksInYear(this.year(), 1, 4);
}

function getWeeksInYear () {
    var weekInfo = this.localeData()._week;
    return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
}

function getSetWeekYearHelper(input, week, weekday, dow, doy) {
    var weeksTarget;
    if (input == null) {
        return weekOfYear(this, dow, doy).year;
    } else {
        weeksTarget = weeksInYear(input, dow, doy);
        if (week > weeksTarget) {
            week = weeksTarget;
        }
        return setWeekAll.call(this, input, week, weekday, dow, doy);
    }
}

function setWeekAll(weekYear, week, weekday, dow, doy) {
    var dayOfYearData = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy),
        date = createUTCDate(dayOfYearData.year, 0, dayOfYearData.dayOfYear);

    this.year(date.getUTCFullYear());
    this.month(date.getUTCMonth());
    this.date(date.getUTCDate());
    return this;
}

// FORMATTING

addFormatToken('Q', 0, 'Qo', 'quarter');

// ALIASES

addUnitAlias('quarter', 'Q');

// PRIORITY

addUnitPriority('quarter', 7);

// PARSING

addRegexToken('Q', match1);
addParseToken('Q', function (input, array) {
    array[MONTH] = (toInt(input) - 1) * 3;
});

// MOMENTS

function getSetQuarter (input) {
    return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
}

// FORMATTING

addFormatToken('D', ['DD', 2], 'Do', 'date');

// ALIASES

addUnitAlias('date', 'D');

// PRIOROITY
addUnitPriority('date', 9);

// PARSING

addRegexToken('D',  match1to2);
addRegexToken('DD', match1to2, match2);
addRegexToken('Do', function (isStrict, locale) {
    // TODO: Remove "ordinalParse" fallback in next major release.
    return isStrict ?
      (locale._dayOfMonthOrdinalParse || locale._ordinalParse) :
      locale._dayOfMonthOrdinalParseLenient;
});

addParseToken(['D', 'DD'], DATE);
addParseToken('Do', function (input, array) {
    array[DATE] = toInt(input.match(match1to2)[0], 10);
});

// MOMENTS

var getSetDayOfMonth = makeGetSet('Date', true);

// FORMATTING

addFormatToken('DDD', ['DDDD', 3], 'DDDo', 'dayOfYear');

// ALIASES

addUnitAlias('dayOfYear', 'DDD');

// PRIORITY
addUnitPriority('dayOfYear', 4);

// PARSING

addRegexToken('DDD',  match1to3);
addRegexToken('DDDD', match3);
addParseToken(['DDD', 'DDDD'], function (input, array, config) {
    config._dayOfYear = toInt(input);
});

// HELPERS

// MOMENTS

function getSetDayOfYear (input) {
    var dayOfYear = Math.round((this.clone().startOf('day') - this.clone().startOf('year')) / 864e5) + 1;
    return input == null ? dayOfYear : this.add((input - dayOfYear), 'd');
}

// FORMATTING

addFormatToken('m', ['mm', 2], 0, 'minute');

// ALIASES

addUnitAlias('minute', 'm');

// PRIORITY

addUnitPriority('minute', 14);

// PARSING

addRegexToken('m',  match1to2);
addRegexToken('mm', match1to2, match2);
addParseToken(['m', 'mm'], MINUTE);

// MOMENTS

var getSetMinute = makeGetSet('Minutes', false);

// FORMATTING

addFormatToken('s', ['ss', 2], 0, 'second');

// ALIASES

addUnitAlias('second', 's');

// PRIORITY

addUnitPriority('second', 15);

// PARSING

addRegexToken('s',  match1to2);
addRegexToken('ss', match1to2, match2);
addParseToken(['s', 'ss'], SECOND);

// MOMENTS

var getSetSecond = makeGetSet('Seconds', false);

// FORMATTING

addFormatToken('S', 0, 0, function () {
    return ~~(this.millisecond() / 100);
});

addFormatToken(0, ['SS', 2], 0, function () {
    return ~~(this.millisecond() / 10);
});

addFormatToken(0, ['SSS', 3], 0, 'millisecond');
addFormatToken(0, ['SSSS', 4], 0, function () {
    return this.millisecond() * 10;
});
addFormatToken(0, ['SSSSS', 5], 0, function () {
    return this.millisecond() * 100;
});
addFormatToken(0, ['SSSSSS', 6], 0, function () {
    return this.millisecond() * 1000;
});
addFormatToken(0, ['SSSSSSS', 7], 0, function () {
    return this.millisecond() * 10000;
});
addFormatToken(0, ['SSSSSSSS', 8], 0, function () {
    return this.millisecond() * 100000;
});
addFormatToken(0, ['SSSSSSSSS', 9], 0, function () {
    return this.millisecond() * 1000000;
});


// ALIASES

addUnitAlias('millisecond', 'ms');

// PRIORITY

addUnitPriority('millisecond', 16);

// PARSING

addRegexToken('S',    match1to3, match1);
addRegexToken('SS',   match1to3, match2);
addRegexToken('SSS',  match1to3, match3);

var token;
for (token = 'SSSS'; token.length <= 9; token += 'S') {
    addRegexToken(token, matchUnsigned);
}

function parseMs(input, array) {
    array[MILLISECOND] = toInt(('0.' + input) * 1000);
}

for (token = 'S'; token.length <= 9; token += 'S') {
    addParseToken(token, parseMs);
}
// MOMENTS

var getSetMillisecond = makeGetSet('Milliseconds', false);

// FORMATTING

addFormatToken('z',  0, 0, 'zoneAbbr');
addFormatToken('zz', 0, 0, 'zoneName');

// MOMENTS

function getZoneAbbr () {
    return this._isUTC ? 'UTC' : '';
}

function getZoneName () {
    return this._isUTC ? 'Coordinated Universal Time' : '';
}

var proto = Moment.prototype;

proto.add               = add;
proto.calendar          = calendar$1;
proto.clone             = clone;
proto.diff              = diff;
proto.endOf             = endOf;
proto.format            = format;
proto.from              = from;
proto.fromNow           = fromNow;
proto.to                = to;
proto.toNow             = toNow;
proto.get               = stringGet;
proto.invalidAt         = invalidAt;
proto.isAfter           = isAfter;
proto.isBefore          = isBefore;
proto.isBetween         = isBetween;
proto.isSame            = isSame;
proto.isSameOrAfter     = isSameOrAfter;
proto.isSameOrBefore    = isSameOrBefore;
proto.isValid           = isValid$2;
proto.lang              = lang;
proto.locale            = locale;
proto.localeData        = localeData;
proto.max               = prototypeMax;
proto.min               = prototypeMin;
proto.parsingFlags      = parsingFlags;
proto.set               = stringSet;
proto.startOf           = startOf;
proto.subtract          = subtract;
proto.toArray           = toArray;
proto.toObject          = toObject;
proto.toDate            = toDate;
proto.toISOString       = toISOString;
proto.inspect           = inspect;
proto.toJSON            = toJSON;
proto.toString          = toString;
proto.unix              = unix;
proto.valueOf           = valueOf;
proto.creationData      = creationData;

// Year
proto.year       = getSetYear;
proto.isLeapYear = getIsLeapYear;

// Week Year
proto.weekYear    = getSetWeekYear;
proto.isoWeekYear = getSetISOWeekYear;

// Quarter
proto.quarter = proto.quarters = getSetQuarter;

// Month
proto.month       = getSetMonth;
proto.daysInMonth = getDaysInMonth;

// Week
proto.week           = proto.weeks        = getSetWeek;
proto.isoWeek        = proto.isoWeeks     = getSetISOWeek;
proto.weeksInYear    = getWeeksInYear;
proto.isoWeeksInYear = getISOWeeksInYear;

// Day
proto.date       = getSetDayOfMonth;
proto.day        = proto.days             = getSetDayOfWeek;
proto.weekday    = getSetLocaleDayOfWeek;
proto.isoWeekday = getSetISODayOfWeek;
proto.dayOfYear  = getSetDayOfYear;

// Hour
proto.hour = proto.hours = getSetHour;

// Minute
proto.minute = proto.minutes = getSetMinute;

// Second
proto.second = proto.seconds = getSetSecond;

// Millisecond
proto.millisecond = proto.milliseconds = getSetMillisecond;

// Offset
proto.utcOffset            = getSetOffset;
proto.utc                  = setOffsetToUTC;
proto.local                = setOffsetToLocal;
proto.parseZone            = setOffsetToParsedOffset;
proto.hasAlignedHourOffset = hasAlignedHourOffset;
proto.isDST                = isDaylightSavingTime;
proto.isLocal              = isLocal;
proto.isUtcOffset          = isUtcOffset;
proto.isUtc                = isUtc;
proto.isUTC                = isUtc;

// Timezone
proto.zoneAbbr = getZoneAbbr;
proto.zoneName = getZoneName;

// Deprecations
proto.dates  = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
proto.years  = deprecate('years accessor is deprecated. Use year instead', getSetYear);
proto.zone   = deprecate('moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/', getSetZone);
proto.isDSTShifted = deprecate('isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information', isDaylightSavingTimeShifted);

function createUnix (input) {
    return createLocal(input * 1000);
}

function createInZone () {
    return createLocal.apply(null, arguments).parseZone();
}

function preParsePostFormat (string) {
    return string;
}

var proto$1 = Locale.prototype;

proto$1.calendar        = calendar;
proto$1.longDateFormat  = longDateFormat;
proto$1.invalidDate     = invalidDate;
proto$1.ordinal         = ordinal;
proto$1.preparse        = preParsePostFormat;
proto$1.postformat      = preParsePostFormat;
proto$1.relativeTime    = relativeTime;
proto$1.pastFuture      = pastFuture;
proto$1.set             = set;

// Month
proto$1.months            =        localeMonths;
proto$1.monthsShort       =        localeMonthsShort;
proto$1.monthsParse       =        localeMonthsParse;
proto$1.monthsRegex       = monthsRegex;
proto$1.monthsShortRegex  = monthsShortRegex;

// Week
proto$1.week = localeWeek;
proto$1.firstDayOfYear = localeFirstDayOfYear;
proto$1.firstDayOfWeek = localeFirstDayOfWeek;

// Day of Week
proto$1.weekdays       =        localeWeekdays;
proto$1.weekdaysMin    =        localeWeekdaysMin;
proto$1.weekdaysShort  =        localeWeekdaysShort;
proto$1.weekdaysParse  =        localeWeekdaysParse;

proto$1.weekdaysRegex       =        weekdaysRegex;
proto$1.weekdaysShortRegex  =        weekdaysShortRegex;
proto$1.weekdaysMinRegex    =        weekdaysMinRegex;

// Hours
proto$1.isPM = localeIsPM;
proto$1.meridiem = localeMeridiem;

function get$1 (format, index, field, setter) {
    var locale = getLocale();
    var utc = createUTC().set(setter, index);
    return locale[field](utc, format);
}

function listMonthsImpl (format, index, field) {
    if (isNumber(format)) {
        index = format;
        format = undefined;
    }

    format = format || '';

    if (index != null) {
        return get$1(format, index, field, 'month');
    }

    var i;
    var out = [];
    for (i = 0; i < 12; i++) {
        out[i] = get$1(format, i, field, 'month');
    }
    return out;
}

// ()
// (5)
// (fmt, 5)
// (fmt)
// (true)
// (true, 5)
// (true, fmt, 5)
// (true, fmt)
function listWeekdaysImpl (localeSorted, format, index, field) {
    if (typeof localeSorted === 'boolean') {
        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';
    } else {
        format = localeSorted;
        index = format;
        localeSorted = false;

        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';
    }

    var locale = getLocale(),
        shift = localeSorted ? locale._week.dow : 0;

    if (index != null) {
        return get$1(format, (index + shift) % 7, field, 'day');
    }

    var i;
    var out = [];
    for (i = 0; i < 7; i++) {
        out[i] = get$1(format, (i + shift) % 7, field, 'day');
    }
    return out;
}

function listMonths (format, index) {
    return listMonthsImpl(format, index, 'months');
}

function listMonthsShort (format, index) {
    return listMonthsImpl(format, index, 'monthsShort');
}

function listWeekdays (localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdays');
}

function listWeekdaysShort (localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdaysShort');
}

function listWeekdaysMin (localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdaysMin');
}

getSetGlobalLocale('en', {
    dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/,
    ordinal : function (number) {
        var b = number % 10,
            output = (toInt(number % 100 / 10) === 1) ? 'th' :
            (b === 1) ? 'st' :
            (b === 2) ? 'nd' :
            (b === 3) ? 'rd' : 'th';
        return number + output;
    }
});

// Side effect imports
hooks.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', getSetGlobalLocale);
hooks.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', getLocale);

var mathAbs = Math.abs;

function abs () {
    var data           = this._data;

    this._milliseconds = mathAbs(this._milliseconds);
    this._days         = mathAbs(this._days);
    this._months       = mathAbs(this._months);

    data.milliseconds  = mathAbs(data.milliseconds);
    data.seconds       = mathAbs(data.seconds);
    data.minutes       = mathAbs(data.minutes);
    data.hours         = mathAbs(data.hours);
    data.months        = mathAbs(data.months);
    data.years         = mathAbs(data.years);

    return this;
}

function addSubtract$1 (duration, input, value, direction) {
    var other = createDuration(input, value);

    duration._milliseconds += direction * other._milliseconds;
    duration._days         += direction * other._days;
    duration._months       += direction * other._months;

    return duration._bubble();
}

// supports only 2.0-style add(1, 's') or add(duration)
function add$1 (input, value) {
    return addSubtract$1(this, input, value, 1);
}

// supports only 2.0-style subtract(1, 's') or subtract(duration)
function subtract$1 (input, value) {
    return addSubtract$1(this, input, value, -1);
}

function absCeil (number) {
    if (number < 0) {
        return Math.floor(number);
    } else {
        return Math.ceil(number);
    }
}

function bubble () {
    var milliseconds = this._milliseconds;
    var days         = this._days;
    var months       = this._months;
    var data         = this._data;
    var seconds, minutes, hours, years, monthsFromDays;

    // if we have a mix of positive and negative values, bubble down first
    // check: https://github.com/moment/moment/issues/2166
    if (!((milliseconds >= 0 && days >= 0 && months >= 0) ||
            (milliseconds <= 0 && days <= 0 && months <= 0))) {
        milliseconds += absCeil(monthsToDays(months) + days) * 864e5;
        days = 0;
        months = 0;
    }

    // The following code bubbles up values, see the tests for
    // examples of what that means.
    data.milliseconds = milliseconds % 1000;

    seconds           = absFloor(milliseconds / 1000);
    data.seconds      = seconds % 60;

    minutes           = absFloor(seconds / 60);
    data.minutes      = minutes % 60;

    hours             = absFloor(minutes / 60);
    data.hours        = hours % 24;

    days += absFloor(hours / 24);

    // convert days to months
    monthsFromDays = absFloor(daysToMonths(days));
    months += monthsFromDays;
    days -= absCeil(monthsToDays(monthsFromDays));

    // 12 months -> 1 year
    years = absFloor(months / 12);
    months %= 12;

    data.days   = days;
    data.months = months;
    data.years  = years;

    return this;
}

function daysToMonths (days) {
    // 400 years have 146097 days (taking into account leap year rules)
    // 400 years have 12 months === 4800
    return days * 4800 / 146097;
}

function monthsToDays (months) {
    // the reverse of daysToMonths
    return months * 146097 / 4800;
}

function as (units) {
    if (!this.isValid()) {
        return NaN;
    }
    var days;
    var months;
    var milliseconds = this._milliseconds;

    units = normalizeUnits(units);

    if (units === 'month' || units === 'year') {
        days   = this._days   + milliseconds / 864e5;
        months = this._months + daysToMonths(days);
        return units === 'month' ? months : months / 12;
    } else {
        // handle milliseconds separately because of floating point math errors (issue #1867)
        days = this._days + Math.round(monthsToDays(this._months));
        switch (units) {
            case 'week'   : return days / 7     + milliseconds / 6048e5;
            case 'day'    : return days         + milliseconds / 864e5;
            case 'hour'   : return days * 24    + milliseconds / 36e5;
            case 'minute' : return days * 1440  + milliseconds / 6e4;
            case 'second' : return days * 86400 + milliseconds / 1000;
            // Math.floor prevents floating point math errors here
            case 'millisecond': return Math.floor(days * 864e5) + milliseconds;
            default: throw new Error('Unknown unit ' + units);
        }
    }
}

// TODO: Use this.as('ms')?
function valueOf$1 () {
    if (!this.isValid()) {
        return NaN;
    }
    return (
        this._milliseconds +
        this._days * 864e5 +
        (this._months % 12) * 2592e6 +
        toInt(this._months / 12) * 31536e6
    );
}

function makeAs (alias) {
    return function () {
        return this.as(alias);
    };
}

var asMilliseconds = makeAs('ms');
var asSeconds      = makeAs('s');
var asMinutes      = makeAs('m');
var asHours        = makeAs('h');
var asDays         = makeAs('d');
var asWeeks        = makeAs('w');
var asMonths       = makeAs('M');
var asYears        = makeAs('y');

function get$2 (units) {
    units = normalizeUnits(units);
    return this.isValid() ? this[units + 's']() : NaN;
}

function makeGetter(name) {
    return function () {
        return this.isValid() ? this._data[name] : NaN;
    };
}

var milliseconds = makeGetter('milliseconds');
var seconds      = makeGetter('seconds');
var minutes      = makeGetter('minutes');
var hours        = makeGetter('hours');
var days         = makeGetter('days');
var months       = makeGetter('months');
var years        = makeGetter('years');

function weeks () {
    return absFloor(this.days() / 7);
}

var round = Math.round;
var thresholds = {
    ss: 44,         // a few seconds to seconds
    s : 45,         // seconds to minute
    m : 45,         // minutes to hour
    h : 22,         // hours to day
    d : 26,         // days to month
    M : 11          // months to year
};

// helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
    return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
}

function relativeTime$1 (posNegDuration, withoutSuffix, locale) {
    var duration = createDuration(posNegDuration).abs();
    var seconds  = round(duration.as('s'));
    var minutes  = round(duration.as('m'));
    var hours    = round(duration.as('h'));
    var days     = round(duration.as('d'));
    var months   = round(duration.as('M'));
    var years    = round(duration.as('y'));

    var a = seconds <= thresholds.ss && ['s', seconds]  ||
            seconds < thresholds.s   && ['ss', seconds] ||
            minutes <= 1             && ['m']           ||
            minutes < thresholds.m   && ['mm', minutes] ||
            hours   <= 1             && ['h']           ||
            hours   < thresholds.h   && ['hh', hours]   ||
            days    <= 1             && ['d']           ||
            days    < thresholds.d   && ['dd', days]    ||
            months  <= 1             && ['M']           ||
            months  < thresholds.M   && ['MM', months]  ||
            years   <= 1             && ['y']           || ['yy', years];

    a[2] = withoutSuffix;
    a[3] = +posNegDuration > 0;
    a[4] = locale;
    return substituteTimeAgo.apply(null, a);
}

// This function allows you to set the rounding function for relative time strings
function getSetRelativeTimeRounding (roundingFunction) {
    if (roundingFunction === undefined) {
        return round;
    }
    if (typeof(roundingFunction) === 'function') {
        round = roundingFunction;
        return true;
    }
    return false;
}

// This function allows you to set a threshold for relative time strings
function getSetRelativeTimeThreshold (threshold, limit) {
    if (thresholds[threshold] === undefined) {
        return false;
    }
    if (limit === undefined) {
        return thresholds[threshold];
    }
    thresholds[threshold] = limit;
    if (threshold === 's') {
        thresholds.ss = limit - 1;
    }
    return true;
}

function humanize (withSuffix) {
    if (!this.isValid()) {
        return this.localeData().invalidDate();
    }

    var locale = this.localeData();
    var output = relativeTime$1(this, !withSuffix, locale);

    if (withSuffix) {
        output = locale.pastFuture(+this, output);
    }

    return locale.postformat(output);
}

var abs$1 = Math.abs;

function toISOString$1() {
    // for ISO strings we do not use the normal bubbling rules:
    //  * milliseconds bubble up until they become hours
    //  * days do not bubble at all
    //  * months bubble up until they become years
    // This is because there is no context-free conversion between hours and days
    // (think of clock changes)
    // and also not between days and months (28-31 days per month)
    if (!this.isValid()) {
        return this.localeData().invalidDate();
    }

    var seconds = abs$1(this._milliseconds) / 1000;
    var days         = abs$1(this._days);
    var months       = abs$1(this._months);
    var minutes, hours, years;

    // 3600 seconds -> 60 minutes -> 1 hour
    minutes           = absFloor(seconds / 60);
    hours             = absFloor(minutes / 60);
    seconds %= 60;
    minutes %= 60;

    // 12 months -> 1 year
    years  = absFloor(months / 12);
    months %= 12;


    // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
    var Y = years;
    var M = months;
    var D = days;
    var h = hours;
    var m = minutes;
    var s = seconds;
    var total = this.asSeconds();

    if (!total) {
        // this is the same as C#'s (Noda) and python (isodate)...
        // but not other JS (goog.date)
        return 'P0D';
    }

    return (total < 0 ? '-' : '') +
        'P' +
        (Y ? Y + 'Y' : '') +
        (M ? M + 'M' : '') +
        (D ? D + 'D' : '') +
        ((h || m || s) ? 'T' : '') +
        (h ? h + 'H' : '') +
        (m ? m + 'M' : '') +
        (s ? s + 'S' : '');
}

var proto$2 = Duration.prototype;

proto$2.isValid        = isValid$1;
proto$2.abs            = abs;
proto$2.add            = add$1;
proto$2.subtract       = subtract$1;
proto$2.as             = as;
proto$2.asMilliseconds = asMilliseconds;
proto$2.asSeconds      = asSeconds;
proto$2.asMinutes      = asMinutes;
proto$2.asHours        = asHours;
proto$2.asDays         = asDays;
proto$2.asWeeks        = asWeeks;
proto$2.asMonths       = asMonths;
proto$2.asYears        = asYears;
proto$2.valueOf        = valueOf$1;
proto$2._bubble        = bubble;
proto$2.get            = get$2;
proto$2.milliseconds   = milliseconds;
proto$2.seconds        = seconds;
proto$2.minutes        = minutes;
proto$2.hours          = hours;
proto$2.days           = days;
proto$2.weeks          = weeks;
proto$2.months         = months;
proto$2.years          = years;
proto$2.humanize       = humanize;
proto$2.toISOString    = toISOString$1;
proto$2.toString       = toISOString$1;
proto$2.toJSON         = toISOString$1;
proto$2.locale         = locale;
proto$2.localeData     = localeData;

// Deprecations
proto$2.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', toISOString$1);
proto$2.lang = lang;

// Side effect imports

// FORMATTING

addFormatToken('X', 0, 0, 'unix');
addFormatToken('x', 0, 0, 'valueOf');

// PARSING

addRegexToken('x', matchSigned);
addRegexToken('X', matchTimestamp);
addParseToken('X', function (input, array, config) {
    config._d = new Date(parseFloat(input, 10) * 1000);
});
addParseToken('x', function (input, array, config) {
    config._d = new Date(toInt(input));
});

// Side effect imports


hooks.version = '2.18.1';

setHookCallback(createLocal);

hooks.fn                    = proto;
hooks.min                   = min;
hooks.max                   = max;
hooks.now                   = now;
hooks.utc                   = createUTC;
hooks.unix                  = createUnix;
hooks.months                = listMonths;
hooks.isDate                = isDate;
hooks.locale                = getSetGlobalLocale;
hooks.invalid               = createInvalid;
hooks.duration              = createDuration;
hooks.isMoment              = isMoment;
hooks.weekdays              = listWeekdays;
hooks.parseZone             = createInZone;
hooks.localeData            = getLocale;
hooks.isDuration            = isDuration;
hooks.monthsShort           = listMonthsShort;
hooks.weekdaysMin           = listWeekdaysMin;
hooks.defineLocale          = defineLocale;
hooks.updateLocale          = updateLocale;
hooks.locales               = listLocales;
hooks.weekdaysShort         = listWeekdaysShort;
hooks.normalizeUnits        = normalizeUnits;
hooks.relativeTimeRounding = getSetRelativeTimeRounding;
hooks.relativeTimeThreshold = getSetRelativeTimeThreshold;
hooks.calendarFormat        = getCalendarFormat;
hooks.prototype             = proto;

return hooks;

})));
/*! version : 4.17.47
 =========================================================
 bootstrap-datetimejs
 https://github.com/Eonasdan/bootstrap-datetimepicker
 Copyright (c) 2015 Jonathan Peterson
 =========================================================
 */
/*
 The MIT License (MIT)

 Copyright (c) 2015 Jonathan Peterson

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
/*global define:false */
/*global exports:false */
/*global require:false */
/*global jQuery:false */
/*global moment:false */
(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // AMD is used - Register as an anonymous module.
        define(['jquery', 'moment'], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory(require('jquery'), require('moment'));
    } else {
        // Neither AMD nor CommonJS used. Use global variables.
        if (typeof jQuery === 'undefined') {
            throw 'bootstrap-datetimepicker requires jQuery to be loaded first';
        }
        if (typeof moment === 'undefined') {
            throw 'bootstrap-datetimepicker requires Moment.js to be loaded first';
        }
        factory(jQuery, moment);
    }
}(function ($, moment) {
    'use strict';
    if (!moment) {
        throw new Error('bootstrap-datetimepicker requires Moment.js to be loaded first');
    }

    var dateTimePicker = function (element, options) {
        var picker = {},
            date,
            viewDate,
            unset = true,
            input,
            component = false,
            widget = false,
            use24Hours,
            minViewModeNumber = 0,
            actualFormat,
            parseFormats,
            currentViewMode,
            datePickerModes = [
                {
                    clsName: 'days',
                    navFnc: 'M',
                    navStep: 1
                },
                {
                    clsName: 'months',
                    navFnc: 'y',
                    navStep: 1
                },
                {
                    clsName: 'years',
                    navFnc: 'y',
                    navStep: 10
                },
                {
                    clsName: 'decades',
                    navFnc: 'y',
                    navStep: 100
                }
            ],
            viewModes = ['days', 'months', 'years', 'decades'],
            verticalModes = ['top', 'bottom', 'auto'],
            horizontalModes = ['left', 'right', 'auto'],
            toolbarPlacements = ['default', 'top', 'bottom'],
            keyMap = {
                'up': 38,
                38: 'up',
                'down': 40,
                40: 'down',
                'left': 37,
                37: 'left',
                'right': 39,
                39: 'right',
                'tab': 9,
                9: 'tab',
                'escape': 27,
                27: 'escape',
                'enter': 13,
                13: 'enter',
                'pageUp': 33,
                33: 'pageUp',
                'pageDown': 34,
                34: 'pageDown',
                'shift': 16,
                16: 'shift',
                'control': 17,
                17: 'control',
                'space': 32,
                32: 'space',
                't': 84,
                84: 't',
                'delete': 46,
                46: 'delete'
            },
            keyState = {},

            /********************************************************************************
             *
             * Private functions
             *
             ********************************************************************************/

            hasTimeZone = function () {
                return moment.tz !== undefined && options.timeZone !== undefined && options.timeZone !== null && options.timeZone !== '';
            },

            getMoment = function (d) {
                var returnMoment;

                if (d === undefined || d === null) {
                    returnMoment = moment(); //TODO should this use format? and locale?
                } else if (moment.isDate(d) || moment.isMoment(d)) {
                    // If the date that is passed in is already a Date() or moment() object,
                    // pass it directly to moment.
                    returnMoment = moment(d);
                } else if (hasTimeZone()) { // There is a string to parse and a default time zone
                    // parse with the tz function which takes a default time zone if it is not in the format string
                    returnMoment = moment.tz(d, parseFormats, options.useStrict, options.timeZone);
                } else {
                    returnMoment = moment(d, parseFormats, options.useStrict);
                }

                if (hasTimeZone()) {
                    returnMoment.tz(options.timeZone);
                }

                return returnMoment;
            },

            isEnabled = function (granularity) {
                if (typeof granularity !== 'string' || granularity.length > 1) {
                    throw new TypeError('isEnabled expects a single character string parameter');
                }
                switch (granularity) {
                    case 'y':
                        return actualFormat.indexOf('Y') !== -1;
                    case 'M':
                        return actualFormat.indexOf('M') !== -1;
                    case 'd':
                        return actualFormat.toLowerCase().indexOf('d') !== -1;
                    case 'h':
                    case 'H':
                        return actualFormat.toLowerCase().indexOf('h') !== -1;
                    case 'm':
                        return actualFormat.indexOf('m') !== -1;
                    case 's':
                        return actualFormat.indexOf('s') !== -1;
                    default:
                        return false;
                }
            },

            hasTime = function () {
                return (isEnabled('h') || isEnabled('m') || isEnabled('s'));
            },

            hasDate = function () {
                return (isEnabled('y') || isEnabled('M') || isEnabled('d'));
            },

            getDatePickerTemplate = function () {
                var headTemplate = $('<thead>')
                        .append($('<tr>')
                            .append($('<th>').addClass('prev').attr('data-action', 'previous')
                                .append($('<span>').addClass(options.icons.previous))
                                )
                            .append($('<th>').addClass('picker-switch').attr('data-action', 'pickerSwitch').attr('colspan', (options.calendarWeeks ? '6' : '5')))
                            .append($('<th>').addClass('next').attr('data-action', 'next')
                                .append($('<span>').addClass(options.icons.next))
                                )
                            ),
                    contTemplate = $('<tbody>')
                        .append($('<tr>')
                            .append($('<td>').attr('colspan', (options.calendarWeeks ? '8' : '7')))
                            );

                return [
                    $('<div>').addClass('datepicker-days')
                        .append($('<table>').addClass('table-condensed')
                            .append(headTemplate)
                            .append($('<tbody>'))
                            ),
                    $('<div>').addClass('datepicker-months')
                        .append($('<table>').addClass('table-condensed')
                            .append(headTemplate.clone())
                            .append(contTemplate.clone())
                            ),
                    $('<div>').addClass('datepicker-years')
                        .append($('<table>').addClass('table-condensed')
                            .append(headTemplate.clone())
                            .append(contTemplate.clone())
                            ),
                    $('<div>').addClass('datepicker-decades')
                        .append($('<table>').addClass('table-condensed')
                            .append(headTemplate.clone())
                            .append(contTemplate.clone())
                            )
                ];
            },

            getTimePickerMainTemplate = function () {
                var topRow = $('<tr>'),
                    middleRow = $('<tr>'),
                    bottomRow = $('<tr>');

                if (isEnabled('h')) {
                    topRow.append($('<td>')
                        .append($('<a>').attr({ href: '#', tabindex: '-1', 'title': options.tooltips.incrementHour }).addClass('btn').attr('data-action', 'incrementHours').append($('<span>').addClass(options.icons.up))));
                    middleRow.append($('<td>')
                        .append($('<span>').addClass('timepicker-hour').attr({ 'data-time-component': 'hours', 'title': options.tooltips.pickHour }).attr('data-action', 'showHours')));
                    bottomRow.append($('<td>')
                        .append($('<a>').attr({ href: '#', tabindex: '-1', 'title': options.tooltips.decrementHour }).addClass('btn').attr('data-action', 'decrementHours').append($('<span>').addClass(options.icons.down))));
                }
                if (isEnabled('m')) {
                    if (isEnabled('h')) {
                        topRow.append($('<td>').addClass('separator'));
                        middleRow.append($('<td>').addClass('separator').html(':'));
                        bottomRow.append($('<td>').addClass('separator'));
                    }
                    topRow.append($('<td>')
                        .append($('<a>').attr({ href: '#', tabindex: '-1', 'title': options.tooltips.incrementMinute }).addClass('btn').attr('data-action', 'incrementMinutes')
                            .append($('<span>').addClass(options.icons.up))));
                    middleRow.append($('<td>')
                        .append($('<span>').addClass('timepicker-minute').attr({ 'data-time-component': 'minutes', 'title': options.tooltips.pickMinute }).attr('data-action', 'showMinutes')));
                    bottomRow.append($('<td>')
                        .append($('<a>').attr({ href: '#', tabindex: '-1', 'title': options.tooltips.decrementMinute }).addClass('btn').attr('data-action', 'decrementMinutes')
                            .append($('<span>').addClass(options.icons.down))));
                }
                if (isEnabled('s')) {
                    if (isEnabled('m')) {
                        topRow.append($('<td>').addClass('separator'));
                        middleRow.append($('<td>').addClass('separator').html(':'));
                        bottomRow.append($('<td>').addClass('separator'));
                    }
                    topRow.append($('<td>')
                        .append($('<a>').attr({ href: '#', tabindex: '-1', 'title': options.tooltips.incrementSecond }).addClass('btn').attr('data-action', 'incrementSeconds')
                            .append($('<span>').addClass(options.icons.up))));
                    middleRow.append($('<td>')
                        .append($('<span>').addClass('timepicker-second').attr({ 'data-time-component': 'seconds', 'title': options.tooltips.pickSecond }).attr('data-action', 'showSeconds')));
                    bottomRow.append($('<td>')
                        .append($('<a>').attr({ href: '#', tabindex: '-1', 'title': options.tooltips.decrementSecond }).addClass('btn').attr('data-action', 'decrementSeconds')
                            .append($('<span>').addClass(options.icons.down))));
                }

                if (!use24Hours) {
                    topRow.append($('<td>').addClass('separator'));
                    middleRow.append($('<td>')
                        .append($('<button>').addClass('btn btn-primary').attr({ 'data-action': 'togglePeriod', tabindex: '-1', 'title': options.tooltips.togglePeriod })));
                    bottomRow.append($('<td>').addClass('separator'));
                }

                return $('<div>').addClass('timepicker-picker')
                    .append($('<table>').addClass('table-condensed')
                        .append([topRow, middleRow, bottomRow]));
            },

            getTimePickerTemplate = function () {
                var hoursView = $('<div>').addClass('timepicker-hours')
                        .append($('<table>').addClass('table-condensed')),
                    minutesView = $('<div>').addClass('timepicker-minutes')
                        .append($('<table>').addClass('table-condensed')),
                    secondsView = $('<div>').addClass('timepicker-seconds')
                        .append($('<table>').addClass('table-condensed')),
                    ret = [getTimePickerMainTemplate()];

                if (isEnabled('h')) {
                    ret.push(hoursView);
                }
                if (isEnabled('m')) {
                    ret.push(minutesView);
                }
                if (isEnabled('s')) {
                    ret.push(secondsView);
                }

                return ret;
            },

            getToolbar = function () {
                var row = [];
                if (options.showTodayButton) {
                    row.push($('<td>').append($('<a>').attr({ 'data-action': 'today', 'title': options.tooltips.today }).append($('<span>').addClass(options.icons.today))));
                }
                if (!options.sideBySide && hasDate() && hasTime()) {
                    row.push($('<td>').append($('<a>').attr({ 'data-action': 'togglePicker', 'title': options.tooltips.selectTime }).append($('<span>').addClass(options.icons.time))));
                }
                if (options.showClear) {
                    row.push($('<td>').append($('<a>').attr({ 'data-action': 'clear', 'title': options.tooltips.clear }).append($('<span>').addClass(options.icons.clear))));
                }
                if (options.showClose) {
                    row.push($('<td>').append($('<a>').attr({ 'data-action': 'close', 'title': options.tooltips.close }).append($('<span>').addClass(options.icons.close))));
                }
                return $('<table>').addClass('table-condensed').append($('<tbody>').append($('<tr>').append(row)));
            },

            getTemplate = function () {
                var template = $('<div>').addClass('bootstrap-datetimepicker-widget dropdown-menu'),
                    dateView = $('<div>').addClass('datepicker').append(getDatePickerTemplate()),
                    timeView = $('<div>').addClass('timepicker').append(getTimePickerTemplate()),
                    content = $('<ul>').addClass('list-unstyled'),
                    toolbar = $('<li>').addClass('picker-switch' + (options.collapse ? ' accordion-toggle' : '')).append(getToolbar());

                if (options.inline) {
                    template.removeClass('dropdown-menu');
                }

                if (use24Hours) {
                    template.addClass('usetwentyfour');
                }

                if (isEnabled('s') && !use24Hours) {
                    template.addClass('wider');
                }

                if (options.sideBySide && hasDate() && hasTime()) {
                    template.addClass('timepicker-sbs');
                    if (options.toolbarPlacement === 'top') {
                        template.append(toolbar);
                    }
                    template.append(
                        $('<div>').addClass('row')
                            .append(dateView.addClass('col-md-6'))
                            .append(timeView.addClass('col-md-6'))
                    );
                    if (options.toolbarPlacement === 'bottom') {
                        template.append(toolbar);
                    }
                    return template;
                }

                if (options.toolbarPlacement === 'top') {
                    content.append(toolbar);
                }
                if (hasDate()) {
                    content.append($('<li>').addClass((options.collapse && hasTime() ? 'collapse in' : '')).append(dateView));
                }
                if (options.toolbarPlacement === 'default') {
                    content.append(toolbar);
                }
                if (hasTime()) {
                    content.append($('<li>').addClass((options.collapse && hasDate() ? 'collapse' : '')).append(timeView));
                }
                if (options.toolbarPlacement === 'bottom') {
                    content.append(toolbar);
                }
                return template.append(content);
            },

            dataToOptions = function () {
                var eData,
                    dataOptions = {};

                if (element.is('input') || options.inline) {
                    eData = element.data();
                } else {
                    eData = element.find('input').data();
                }

                if (eData.dateOptions && eData.dateOptions instanceof Object) {
                    dataOptions = $.extend(true, dataOptions, eData.dateOptions);
                }

                $.each(options, function (key) {
                    var attributeName = 'date' + key.charAt(0).toUpperCase() + key.slice(1);
                    if (eData[attributeName] !== undefined) {
                        dataOptions[key] = eData[attributeName];
                    }
                });
                return dataOptions;
            },

            place = function () {
                var position = (component || element).position(),
                    offset = (component || element).offset(),
                    vertical = options.widgetPositioning.vertical,
                    horizontal = options.widgetPositioning.horizontal,
                    parent;

                if (options.widgetParent) {
                    parent = options.widgetParent.append(widget);
                } else if (element.is('input')) {
                    parent = element.after(widget).parent();
                } else if (options.inline) {
                    parent = element.append(widget);
                    return;
                } else {
                    parent = element;
                    element.children().first().after(widget);
                }

                // Top and bottom logic
                if (vertical === 'auto') {
                    if (offset.top + widget.height() * 1.5 >= $(window).height() + $(window).scrollTop() &&
                        widget.height() + element.outerHeight() < offset.top) {
                        vertical = 'top';
                    } else {
                        vertical = 'bottom';
                    }
                }

                // Left and right logic
                if (horizontal === 'auto') {
                    if (parent.width() < offset.left + widget.outerWidth() / 2 &&
                        offset.left + widget.outerWidth() > $(window).width()) {
                        horizontal = 'right';
                    } else {
                        horizontal = 'left';
                    }
                }

                if (vertical === 'top') {
                    widget.addClass('top').removeClass('bottom');
                } else {
                    widget.addClass('bottom').removeClass('top');
                }

                if (horizontal === 'right') {
                    widget.addClass('pull-right');
                } else {
                    widget.removeClass('pull-right');
                }

                // find the first parent element that has a non-static css positioning
                if (parent.css('position') === 'static') {
                    parent = parent.parents().filter(function () {
                        return $(this).css('position') !== 'static';
                    }).first();
                }

                if (parent.length === 0) {
                    throw new Error('datetimepicker component should be placed within a non-static positioned container');
                }

                widget.css({
                    top: vertical === 'top' ? 'auto' : position.top + element.outerHeight(),
                    bottom: vertical === 'top' ? parent.outerHeight() - (parent === element ? 0 : position.top) : 'auto',
                    left: horizontal === 'left' ? (parent === element ? 0 : position.left) : 'auto',
                    right: horizontal === 'left' ? 'auto' : parent.outerWidth() - element.outerWidth() - (parent === element ? 0 : position.left)
                });
            },

            notifyEvent = function (e) {
                if (e.type === 'dp.change' && ((e.date && e.date.isSame(e.oldDate)) || (!e.date && !e.oldDate))) {
                    return;
                }
                element.trigger(e);
            },

            viewUpdate = function (e) {
                if (e === 'y') {
                    e = 'YYYY';
                }
                notifyEvent({
                    type: 'dp.update',
                    change: e,
                    viewDate: viewDate.clone()
                });
            },

            showMode = function (dir) {
                if (!widget) {
                    return;
                }
                if (dir) {
                    currentViewMode = Math.max(minViewModeNumber, Math.min(3, currentViewMode + dir));
                }
                widget.find('.datepicker > div').hide().filter('.datepicker-' + datePickerModes[currentViewMode].clsName).show();
            },

            fillDow = function () {
                var row = $('<tr>'),
                    currentDate = viewDate.clone().startOf('w').startOf('d');

                if (options.calendarWeeks === true) {
                    row.append($('<th>').addClass('cw').text('#'));
                }

                while (currentDate.isBefore(viewDate.clone().endOf('w'))) {
                    row.append($('<th>').addClass('dow').text(currentDate.format('dd')));
                    currentDate.add(1, 'd');
                }
                widget.find('.datepicker-days thead').append(row);
            },

            isInDisabledDates = function (testDate) {
                return options.disabledDates[testDate.format('YYYY-MM-DD')] === true;
            },

            isInEnabledDates = function (testDate) {
                return options.enabledDates[testDate.format('YYYY-MM-DD')] === true;
            },

            isInDisabledHours = function (testDate) {
                return options.disabledHours[testDate.format('H')] === true;
            },

            isInEnabledHours = function (testDate) {
                return options.enabledHours[testDate.format('H')] === true;
            },

            isValid = function (targetMoment, granularity) {
                if (!targetMoment.isValid()) {
                    return false;
                }
                if (options.disabledDates && granularity === 'd' && isInDisabledDates(targetMoment)) {
                    return false;
                }
                if (options.enabledDates && granularity === 'd' && !isInEnabledDates(targetMoment)) {
                    return false;
                }
                if (options.minDate && targetMoment.isBefore(options.minDate, granularity)) {
                    return false;
                }
                if (options.maxDate && targetMoment.isAfter(options.maxDate, granularity)) {
                    return false;
                }
                if (options.daysOfWeekDisabled && granularity === 'd' && options.daysOfWeekDisabled.indexOf(targetMoment.day()) !== -1) {
                    return false;
                }
                if (options.disabledHours && (granularity === 'h' || granularity === 'm' || granularity === 's') && isInDisabledHours(targetMoment)) {
                    return false;
                }
                if (options.enabledHours && (granularity === 'h' || granularity === 'm' || granularity === 's') && !isInEnabledHours(targetMoment)) {
                    return false;
                }
                if (options.disabledTimeIntervals && (granularity === 'h' || granularity === 'm' || granularity === 's')) {
                    var found = false;
                    $.each(options.disabledTimeIntervals, function () {
                        if (targetMoment.isBetween(this[0], this[1])) {
                            found = true;
                            return false;
                        }
                    });
                    if (found) {
                        return false;
                    }
                }
                return true;
            },

            fillMonths = function () {
                var spans = [],
                    monthsShort = viewDate.clone().startOf('y').startOf('d');
                while (monthsShort.isSame(viewDate, 'y')) {
                    spans.push($('<span>').attr('data-action', 'selectMonth').addClass('month').text(monthsShort.format('MMM')));
                    monthsShort.add(1, 'M');
                }
                widget.find('.datepicker-months td').empty().append(spans);
            },

            updateMonths = function () {
                var monthsView = widget.find('.datepicker-months'),
                    monthsViewHeader = monthsView.find('th'),
                    months = monthsView.find('tbody').find('span');

                monthsViewHeader.eq(0).find('span').attr('title', options.tooltips.prevYear);
                monthsViewHeader.eq(1).attr('title', options.tooltips.selectYear);
                monthsViewHeader.eq(2).find('span').attr('title', options.tooltips.nextYear);

                monthsView.find('.disabled').removeClass('disabled');

                if (!isValid(viewDate.clone().subtract(1, 'y'), 'y')) {
                    monthsViewHeader.eq(0).addClass('disabled');
                }

                monthsViewHeader.eq(1).text(viewDate.year());

                if (!isValid(viewDate.clone().add(1, 'y'), 'y')) {
                    monthsViewHeader.eq(2).addClass('disabled');
                }

                months.removeClass('active');
                if (date.isSame(viewDate, 'y') && !unset) {
                    months.eq(date.month()).addClass('active');
                }

                months.each(function (index) {
                    if (!isValid(viewDate.clone().month(index), 'M')) {
                        $(this).addClass('disabled');
                    }
                });
            },

            updateYears = function () {
                var yearsView = widget.find('.datepicker-years'),
                    yearsViewHeader = yearsView.find('th'),
                    startYear = viewDate.clone().subtract(5, 'y'),
                    endYear = viewDate.clone().add(6, 'y'),
                    html = '';

                yearsViewHeader.eq(0).find('span').attr('title', options.tooltips.prevDecade);
                yearsViewHeader.eq(1).attr('title', options.tooltips.selectDecade);
                yearsViewHeader.eq(2).find('span').attr('title', options.tooltips.nextDecade);

                yearsView.find('.disabled').removeClass('disabled');

                if (options.minDate && options.minDate.isAfter(startYear, 'y')) {
                    yearsViewHeader.eq(0).addClass('disabled');
                }

                yearsViewHeader.eq(1).text(startYear.year() + '-' + endYear.year());

                if (options.maxDate && options.maxDate.isBefore(endYear, 'y')) {
                    yearsViewHeader.eq(2).addClass('disabled');
                }

                while (!startYear.isAfter(endYear, 'y')) {
                    html += '<span data-action="selectYear" class="year' + (startYear.isSame(date, 'y') && !unset ? ' active' : '') + (!isValid(startYear, 'y') ? ' disabled' : '') + '">' + startYear.year() + '</span>';
                    startYear.add(1, 'y');
                }

                yearsView.find('td').html(html);
            },

            updateDecades = function () {
                var decadesView = widget.find('.datepicker-decades'),
                    decadesViewHeader = decadesView.find('th'),
                    startDecade = moment({ y: viewDate.year() - (viewDate.year() % 100) - 1 }),
                    endDecade = startDecade.clone().add(100, 'y'),
                    startedAt = startDecade.clone(),
                    minDateDecade = false,
                    maxDateDecade = false,
                    endDecadeYear,
                    html = '';

                decadesViewHeader.eq(0).find('span').attr('title', options.tooltips.prevCentury);
                decadesViewHeader.eq(2).find('span').attr('title', options.tooltips.nextCentury);

                decadesView.find('.disabled').removeClass('disabled');

                if (startDecade.isSame(moment({ y: 1900 })) || (options.minDate && options.minDate.isAfter(startDecade, 'y'))) {
                    decadesViewHeader.eq(0).addClass('disabled');
                }

                decadesViewHeader.eq(1).text(startDecade.year() + '-' + endDecade.year());

                if (startDecade.isSame(moment({ y: 2000 })) || (options.maxDate && options.maxDate.isBefore(endDecade, 'y'))) {
                    decadesViewHeader.eq(2).addClass('disabled');
                }

                while (!startDecade.isAfter(endDecade, 'y')) {
                    endDecadeYear = startDecade.year() + 12;
                    minDateDecade = options.minDate && options.minDate.isAfter(startDecade, 'y') && options.minDate.year() <= endDecadeYear;
                    maxDateDecade = options.maxDate && options.maxDate.isAfter(startDecade, 'y') && options.maxDate.year() <= endDecadeYear;
                    html += '<span data-action="selectDecade" class="decade' + (date.isAfter(startDecade) && date.year() <= endDecadeYear ? ' active' : '') +
                        (!isValid(startDecade, 'y') && !minDateDecade && !maxDateDecade ? ' disabled' : '') + '" data-selection="' + (startDecade.year() + 6) + '">' + (startDecade.year() + 1) + ' - ' + (startDecade.year() + 12) + '</span>';
                    startDecade.add(12, 'y');
                }
                html += '<span></span><span></span><span></span>'; //push the dangling block over, at least this way it's even

                decadesView.find('td').html(html);
                decadesViewHeader.eq(1).text((startedAt.year() + 1) + '-' + (startDecade.year()));
            },

            fillDate = function () {
                var daysView = widget.find('.datepicker-days'),
                    daysViewHeader = daysView.find('th'),
                    currentDate,
                    html = [],
                    row,
                    clsNames = [],
                    i;

                if (!hasDate()) {
                    return;
                }

                daysViewHeader.eq(0).find('span').attr('title', options.tooltips.prevMonth);
                daysViewHeader.eq(1).attr('title', options.tooltips.selectMonth);
                daysViewHeader.eq(2).find('span').attr('title', options.tooltips.nextMonth);

                daysView.find('.disabled').removeClass('disabled');
                daysViewHeader.eq(1).text(viewDate.format(options.dayViewHeaderFormat));

                if (!isValid(viewDate.clone().subtract(1, 'M'), 'M')) {
                    daysViewHeader.eq(0).addClass('disabled');
                }
                if (!isValid(viewDate.clone().add(1, 'M'), 'M')) {
                    daysViewHeader.eq(2).addClass('disabled');
                }

                currentDate = viewDate.clone().startOf('M').startOf('w').startOf('d');

                for (i = 0; i < 42; i++) { //always display 42 days (should show 6 weeks)
                    if (currentDate.weekday() === 0) {
                        row = $('<tr>');
                        if (options.calendarWeeks) {
                            row.append('<td class="cw">' + currentDate.week() + '</td>');
                        }
                        html.push(row);
                    }
                    clsNames = ['day'];
                    if (currentDate.isBefore(viewDate, 'M')) {
                        clsNames.push('old');
                    }
                    if (currentDate.isAfter(viewDate, 'M')) {
                        clsNames.push('new');
                    }
                    if (currentDate.isSame(date, 'd') && !unset) {
                        clsNames.push('active');
                    }
                    if (!isValid(currentDate, 'd')) {
                        clsNames.push('disabled');
                    }
                    if (currentDate.isSame(getMoment(), 'd')) {
                        clsNames.push('today');
                    }
                    if (currentDate.day() === 0 || currentDate.day() === 6) {
                        clsNames.push('weekend');
                    }
                    notifyEvent({
                        type: 'dp.classify',
                        date: currentDate,
                        classNames: clsNames
                    });
                    row.append('<td data-action="selectDay" data-day="' + currentDate.format('L') + '" class="' + clsNames.join(' ') + '">' + currentDate.date() + '</td>');
                    currentDate.add(1, 'd');
                }

                daysView.find('tbody').empty().append(html);

                updateMonths();

                updateYears();

                updateDecades();
            },

            fillHours = function () {
                var table = widget.find('.timepicker-hours table'),
                    currentHour = viewDate.clone().startOf('d'),
                    html = [],
                    row = $('<tr>');

                if (viewDate.hour() > 11 && !use24Hours) {
                    currentHour.hour(12);
                }
                while (currentHour.isSame(viewDate, 'd') && (use24Hours || (viewDate.hour() < 12 && currentHour.hour() < 12) || viewDate.hour() > 11)) {
                    if (currentHour.hour() % 4 === 0) {
                        row = $('<tr>');
                        html.push(row);
                    }
                    row.append('<td data-action="selectHour" class="hour' + (!isValid(currentHour, 'h') ? ' disabled' : '') + '">' + currentHour.format(use24Hours ? 'HH' : 'hh') + '</td>');
                    currentHour.add(1, 'h');
                }
                table.empty().append(html);
            },

            fillMinutes = function () {
                var table = widget.find('.timepicker-minutes table'),
                    currentMinute = viewDate.clone().startOf('h'),
                    html = [],
                    row = $('<tr>'),
                    step = options.stepping === 1 ? 5 : options.stepping;

                while (viewDate.isSame(currentMinute, 'h')) {
                    if (currentMinute.minute() % (step * 4) === 0) {
                        row = $('<tr>');
                        html.push(row);
                    }
                    row.append('<td data-action="selectMinute" class="minute' + (!isValid(currentMinute, 'm') ? ' disabled' : '') + '">' + currentMinute.format('mm') + '</td>');
                    currentMinute.add(step, 'm');
                }
                table.empty().append(html);
            },

            fillSeconds = function () {
                var table = widget.find('.timepicker-seconds table'),
                    currentSecond = viewDate.clone().startOf('m'),
                    html = [],
                    row = $('<tr>');

                while (viewDate.isSame(currentSecond, 'm')) {
                    if (currentSecond.second() % 20 === 0) {
                        row = $('<tr>');
                        html.push(row);
                    }
                    row.append('<td data-action="selectSecond" class="second' + (!isValid(currentSecond, 's') ? ' disabled' : '') + '">' + currentSecond.format('ss') + '</td>');
                    currentSecond.add(5, 's');
                }

                table.empty().append(html);
            },

            fillTime = function () {
                var toggle, newDate, timeComponents = widget.find('.timepicker span[data-time-component]');

                if (!use24Hours) {
                    toggle = widget.find('.timepicker [data-action=togglePeriod]');
                    newDate = date.clone().add((date.hours() >= 12) ? -12 : 12, 'h');

                    toggle.text(date.format('A'));

                    if (isValid(newDate, 'h')) {
                        toggle.removeClass('disabled');
                    } else {
                        toggle.addClass('disabled');
                    }
                }
                timeComponents.filter('[data-time-component=hours]').text(date.format(use24Hours ? 'HH' : 'hh'));
                timeComponents.filter('[data-time-component=minutes]').text(date.format('mm'));
                timeComponents.filter('[data-time-component=seconds]').text(date.format('ss'));

                fillHours();
                fillMinutes();
                fillSeconds();
            },

            update = function () {
                if (!widget) {
                    return;
                }
                fillDate();
                fillTime();
            },

            setValue = function (targetMoment) {
                var oldDate = unset ? null : date;

                // case of calling setValue(null or false)
                if (!targetMoment) {
                    unset = true;
                    input.val('');
                    element.data('date', '');
                    notifyEvent({
                        type: 'dp.change',
                        date: false,
                        oldDate: oldDate
                    });
                    update();
                    return;
                }

                targetMoment = targetMoment.clone().locale(options.locale);

                if (hasTimeZone()) {
                    targetMoment.tz(options.timeZone);
                }

                if (options.stepping !== 1) {
                    targetMoment.minutes((Math.round(targetMoment.minutes() / options.stepping) * options.stepping)).seconds(0);

                    while (options.minDate && targetMoment.isBefore(options.minDate)) {
                        targetMoment.add(options.stepping, 'minutes');
                    }
                }

                if (isValid(targetMoment)) {
                    date = targetMoment;
                    viewDate = date.clone();
                    input.val(date.format(actualFormat));
                    element.data('date', date.format(actualFormat));
                    unset = false;
                    update();
                    notifyEvent({
                        type: 'dp.change',
                        date: date.clone(),
                        oldDate: oldDate
                    });
                } else {
                    if (!options.keepInvalid) {
                        input.val(unset ? '' : date.format(actualFormat));
                    } else {
                        notifyEvent({
                            type: 'dp.change',
                            date: targetMoment,
                            oldDate: oldDate
                        });
                    }
                    notifyEvent({
                        type: 'dp.error',
                        date: targetMoment,
                        oldDate: oldDate
                    });
                }
            },

            /**
             * Hides the widget. Possibly will emit dp.hide
             */
            hide = function () {
                var transitioning = false;
                if (!widget) {
                    return picker;
                }
                // Ignore event if in the middle of a picker transition
                widget.find('.collapse').each(function () {
                    var collapseData = $(this).data('collapse');
                    if (collapseData && collapseData.transitioning) {
                        transitioning = true;
                        return false;
                    }
                    return true;
                });
                if (transitioning) {
                    return picker;
                }
                if (component && component.hasClass('btn')) {
                    component.toggleClass('active');
                }
                widget.hide();

                $(window).off('resize', place);
                widget.off('click', '[data-action]');
                widget.off('mousedown', false);

                widget.remove();
                widget = false;

                notifyEvent({
                    type: 'dp.hide',
                    date: date.clone()
                });

                input.blur();

                viewDate = date.clone();

                return picker;
            },

            clear = function () {
                setValue(null);
            },

            parseInputDate = function (inputDate) {
                if (options.parseInputDate === undefined) {
                    if (!moment.isMoment(inputDate) || inputDate instanceof Date) {
                        inputDate = getMoment(inputDate);
                    }
                } else {
                    inputDate = options.parseInputDate(inputDate);
                }
                //inputDate.locale(options.locale);
                return inputDate;
            },

            /********************************************************************************
             *
             * Widget UI interaction functions
             *
             ********************************************************************************/
            actions = {
                next: function () {
                    var navFnc = datePickerModes[currentViewMode].navFnc;
                    viewDate.add(datePickerModes[currentViewMode].navStep, navFnc);
                    fillDate();
                    viewUpdate(navFnc);
                },

                previous: function () {
                    var navFnc = datePickerModes[currentViewMode].navFnc;
                    viewDate.subtract(datePickerModes[currentViewMode].navStep, navFnc);
                    fillDate();
                    viewUpdate(navFnc);
                },

                pickerSwitch: function () {
                    showMode(1);
                },

                selectMonth: function (e) {
                    var month = $(e.target).closest('tbody').find('span').index($(e.target));
                    viewDate.month(month);
                    if (currentViewMode === minViewModeNumber) {
                        setValue(date.clone().year(viewDate.year()).month(viewDate.month()));
                        if (!options.inline) {
                            hide();
                        }
                    } else {
                        showMode(-1);
                        fillDate();
                    }
                    viewUpdate('M');
                },

                selectYear: function (e) {
                    var year = parseInt($(e.target).text(), 10) || 0;
                    viewDate.year(year);
                    if (currentViewMode === minViewModeNumber) {
                        setValue(date.clone().year(viewDate.year()));
                        if (!options.inline) {
                            hide();
                        }
                    } else {
                        showMode(-1);
                        fillDate();
                    }
                    viewUpdate('YYYY');
                },

                selectDecade: function (e) {
                    var year = parseInt($(e.target).data('selection'), 10) || 0;
                    viewDate.year(year);
                    if (currentViewMode === minViewModeNumber) {
                        setValue(date.clone().year(viewDate.year()));
                        if (!options.inline) {
                            hide();
                        }
                    } else {
                        showMode(-1);
                        fillDate();
                    }
                    viewUpdate('YYYY');
                },

                selectDay: function (e) {
                    var day = viewDate.clone();
                    if ($(e.target).is('.old')) {
                        day.subtract(1, 'M');
                    }
                    if ($(e.target).is('.new')) {
                        day.add(1, 'M');
                    }
                    setValue(day.date(parseInt($(e.target).text(), 10)));
                    if (!hasTime() && !options.keepOpen && !options.inline) {
                        hide();
                    }
                },

                incrementHours: function () {
                    var newDate = date.clone().add(1, 'h');
                    if (isValid(newDate, 'h')) {
                        setValue(newDate);
                    }
                },

                incrementMinutes: function () {
                    var newDate = date.clone().add(options.stepping, 'm');
                    if (isValid(newDate, 'm')) {
                        setValue(newDate);
                    }
                },

                incrementSeconds: function () {
                    var newDate = date.clone().add(1, 's');
                    if (isValid(newDate, 's')) {
                        setValue(newDate);
                    }
                },

                decrementHours: function () {
                    var newDate = date.clone().subtract(1, 'h');
                    if (isValid(newDate, 'h')) {
                        setValue(newDate);
                    }
                },

                decrementMinutes: function () {
                    var newDate = date.clone().subtract(options.stepping, 'm');
                    if (isValid(newDate, 'm')) {
                        setValue(newDate);
                    }
                },

                decrementSeconds: function () {
                    var newDate = date.clone().subtract(1, 's');
                    if (isValid(newDate, 's')) {
                        setValue(newDate);
                    }
                },

                togglePeriod: function () {
                    setValue(date.clone().add((date.hours() >= 12) ? -12 : 12, 'h'));
                },

                togglePicker: function (e) {
                    var $this = $(e.target),
                        $parent = $this.closest('ul'),
                        expanded = $parent.find('.in'),
                        closed = $parent.find('.collapse:not(.in)'),
                        collapseData;

                    if (expanded && expanded.length) {
                        collapseData = expanded.data('collapse');
                        if (collapseData && collapseData.transitioning) {
                            return;
                        }
                        if (expanded.collapse) { // if collapse plugin is available through bootstrap.js then use it
                            expanded.collapse('hide');
                            closed.collapse('show');
                        } else { // otherwise just toggle in class on the two views
                            expanded.removeClass('in');
                            closed.addClass('in');
                        }
                        if ($this.is('span')) {
                            $this.toggleClass(options.icons.time + ' ' + options.icons.date);
                        } else {
                            $this.find('span').toggleClass(options.icons.time + ' ' + options.icons.date);
                        }

                        // NOTE: uncomment if toggled state will be restored in show()
                        //if (component) {
                        //    component.find('span').toggleClass(options.icons.time + ' ' + options.icons.date);
                        //}
                    }
                },

                showPicker: function () {
                    widget.find('.timepicker > div:not(.timepicker-picker)').hide();
                    widget.find('.timepicker .timepicker-picker').show();
                },

                showHours: function () {
                    widget.find('.timepicker .timepicker-picker').hide();
                    widget.find('.timepicker .timepicker-hours').show();
                },

                showMinutes: function () {
                    widget.find('.timepicker .timepicker-picker').hide();
                    widget.find('.timepicker .timepicker-minutes').show();
                },

                showSeconds: function () {
                    widget.find('.timepicker .timepicker-picker').hide();
                    widget.find('.timepicker .timepicker-seconds').show();
                },

                selectHour: function (e) {
                    var hour = parseInt($(e.target).text(), 10);

                    if (!use24Hours) {
                        if (date.hours() >= 12) {
                            if (hour !== 12) {
                                hour += 12;
                            }
                        } else {
                            if (hour === 12) {
                                hour = 0;
                            }
                        }
                    }
                    setValue(date.clone().hours(hour));
                    actions.showPicker.call(picker);
                },

                selectMinute: function (e) {
                    setValue(date.clone().minutes(parseInt($(e.target).text(), 10)));
                    actions.showPicker.call(picker);
                },

                selectSecond: function (e) {
                    setValue(date.clone().seconds(parseInt($(e.target).text(), 10)));
                    actions.showPicker.call(picker);
                },

                clear: clear,

                today: function () {
                    var todaysDate = getMoment();
                    if (isValid(todaysDate, 'd')) {
                        setValue(todaysDate);
                    }
                },

                close: hide
            },

            doAction = function (e) {
                if ($(e.currentTarget).is('.disabled')) {
                    return false;
                }
                actions[$(e.currentTarget).data('action')].apply(picker, arguments);
                return false;
            },

            /**
             * Shows the widget. Possibly will emit dp.show and dp.change
             */
            show = function () {
                var currentMoment,
                    useCurrentGranularity = {
                        'year': function (m) {
                            return m.month(0).date(1).hours(0).seconds(0).minutes(0);
                        },
                        'month': function (m) {
                            return m.date(1).hours(0).seconds(0).minutes(0);
                        },
                        'day': function (m) {
                            return m.hours(0).seconds(0).minutes(0);
                        },
                        'hour': function (m) {
                            return m.seconds(0).minutes(0);
                        },
                        'minute': function (m) {
                            return m.seconds(0);
                        }
                    };

                if (input.prop('disabled') || (!options.ignoreReadonly && input.prop('readonly')) || widget) {
                    return picker;
                }
                if (input.val() !== undefined && input.val().trim().length !== 0) {
                    setValue(parseInputDate(input.val().trim()));
                } else if (unset && options.useCurrent && (options.inline || (input.is('input') && input.val().trim().length === 0))) {
                    currentMoment = getMoment();
                    if (typeof options.useCurrent === 'string') {
                        currentMoment = useCurrentGranularity[options.useCurrent](currentMoment);
                    }
                    setValue(currentMoment);
                }
                widget = getTemplate();

                fillDow();
                fillMonths();

                widget.find('.timepicker-hours').hide();
                widget.find('.timepicker-minutes').hide();
                widget.find('.timepicker-seconds').hide();

                update();
                showMode();

                $(window).on('resize', place);
                widget.on('click', '[data-action]', doAction); // this handles clicks on the widget
                widget.on('mousedown', false);

                if (component && component.hasClass('btn')) {
                    component.toggleClass('active');
                }
                place();
                widget.show();
                if (options.focusOnShow && !input.is(':focus')) {
                    input.focus();
                }

                notifyEvent({
                    type: 'dp.show'
                });
                return picker;
            },

            /**
             * Shows or hides the widget
             */
            toggle = function () {
                return (widget ? hide() : show());
            },

            keydown = function (e) {
                var handler = null,
                    index,
                    index2,
                    pressedKeys = [],
                    pressedModifiers = {},
                    currentKey = e.which,
                    keyBindKeys,
                    allModifiersPressed,
                    pressed = 'p';

                keyState[currentKey] = pressed;

                for (index in keyState) {
                    if (keyState.hasOwnProperty(index) && keyState[index] === pressed) {
                        pressedKeys.push(index);
                        if (parseInt(index, 10) !== currentKey) {
                            pressedModifiers[index] = true;
                        }
                    }
                }

                for (index in options.keyBinds) {
                    if (options.keyBinds.hasOwnProperty(index) && typeof (options.keyBinds[index]) === 'function') {
                        keyBindKeys = index.split(' ');
                        if (keyBindKeys.length === pressedKeys.length && keyMap[currentKey] === keyBindKeys[keyBindKeys.length - 1]) {
                            allModifiersPressed = true;
                            for (index2 = keyBindKeys.length - 2; index2 >= 0; index2--) {
                                if (!(keyMap[keyBindKeys[index2]] in pressedModifiers)) {
                                    allModifiersPressed = false;
                                    break;
                                }
                            }
                            if (allModifiersPressed) {
                                handler = options.keyBinds[index];
                                break;
                            }
                        }
                    }
                }

                if (handler) {
                    handler.call(picker, widget);
                    e.stopPropagation();
                    e.preventDefault();
                }
            },

            keyup = function (e) {
                keyState[e.which] = 'r';
                e.stopPropagation();
                e.preventDefault();
            },

            change = function (e) {
                var val = $(e.target).val().trim(),
                    parsedDate = val ? parseInputDate(val) : null;
                setValue(parsedDate);
                e.stopImmediatePropagation();
                return false;
            },

            attachDatePickerElementEvents = function () {
                input.on({
                    'change': change,
                    'blur': options.debug ? '' : hide,
                    'keydown': keydown,
                    'keyup': keyup,
                    'focus': options.allowInputToggle ? show : ''
                });

                if (element.is('input')) {
                    input.on({
                        'focus': show
                    });
                } else if (component) {
                    component.on('click', toggle);
                    component.on('mousedown', false);
                }
            },

            detachDatePickerElementEvents = function () {
                input.off({
                    'change': change,
                    'blur': blur,
                    'keydown': keydown,
                    'keyup': keyup,
                    'focus': options.allowInputToggle ? hide : ''
                });

                if (element.is('input')) {
                    input.off({
                        'focus': show
                    });
                } else if (component) {
                    component.off('click', toggle);
                    component.off('mousedown', false);
                }
            },

            indexGivenDates = function (givenDatesArray) {
                // Store given enabledDates and disabledDates as keys.
                // This way we can check their existence in O(1) time instead of looping through whole array.
                // (for example: options.enabledDates['2014-02-27'] === true)
                var givenDatesIndexed = {};
                $.each(givenDatesArray, function () {
                    var dDate = parseInputDate(this);
                    if (dDate.isValid()) {
                        givenDatesIndexed[dDate.format('YYYY-MM-DD')] = true;
                    }
                });
                return (Object.keys(givenDatesIndexed).length) ? givenDatesIndexed : false;
            },

            indexGivenHours = function (givenHoursArray) {
                // Store given enabledHours and disabledHours as keys.
                // This way we can check their existence in O(1) time instead of looping through whole array.
                // (for example: options.enabledHours['2014-02-27'] === true)
                var givenHoursIndexed = {};
                $.each(givenHoursArray, function () {
                    givenHoursIndexed[this] = true;
                });
                return (Object.keys(givenHoursIndexed).length) ? givenHoursIndexed : false;
            },

            initFormatting = function () {
                var format = options.format || 'L LT';

                actualFormat = format.replace(/(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g, function (formatInput) {
                    var newinput = date.localeData().longDateFormat(formatInput) || formatInput;
                    return newinput.replace(/(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g, function (formatInput2) { //temp fix for #740
                        return date.localeData().longDateFormat(formatInput2) || formatInput2;
                    });
                });


                parseFormats = options.extraFormats ? options.extraFormats.slice() : [];
                if (parseFormats.indexOf(format) < 0 && parseFormats.indexOf(actualFormat) < 0) {
                    parseFormats.push(actualFormat);
                }

                use24Hours = (actualFormat.toLowerCase().indexOf('a') < 1 && actualFormat.replace(/\[.*?\]/g, '').indexOf('h') < 1);

                if (isEnabled('y')) {
                    minViewModeNumber = 2;
                }
                if (isEnabled('M')) {
                    minViewModeNumber = 1;
                }
                if (isEnabled('d')) {
                    minViewModeNumber = 0;
                }

                currentViewMode = Math.max(minViewModeNumber, currentViewMode);

                if (!unset) {
                    setValue(date);
                }
            };

        /********************************************************************************
         *
         * Public API functions
         * =====================
         *
         * Important: Do not expose direct references to private objects or the options
         * object to the outer world. Always return a clone when returning values or make
         * a clone when setting a private variable.
         *
         ********************************************************************************/
        picker.destroy = function () {
            ///<summary>Destroys the widget and removes all attached event listeners</summary>
            hide();
            detachDatePickerElementEvents();
            element.removeData('DateTimePicker');
            element.removeData('date');
        };

        picker.toggle = toggle;

        picker.show = show;

        picker.hide = hide;

        picker.disable = function () {
            ///<summary>Disables the input element, the component is attached to, by adding a disabled="true" attribute to it.
            ///If the widget was visible before that call it is hidden. Possibly emits dp.hide</summary>
            hide();
            if (component && component.hasClass('btn')) {
                component.addClass('disabled');
            }
            input.prop('disabled', true);
            return picker;
        };

        picker.enable = function () {
            ///<summary>Enables the input element, the component is attached to, by removing disabled attribute from it.</summary>
            if (component && component.hasClass('btn')) {
                component.removeClass('disabled');
            }
            input.prop('disabled', false);
            return picker;
        };

        picker.ignoreReadonly = function (ignoreReadonly) {
            if (arguments.length === 0) {
                return options.ignoreReadonly;
            }
            if (typeof ignoreReadonly !== 'boolean') {
                throw new TypeError('ignoreReadonly () expects a boolean parameter');
            }
            options.ignoreReadonly = ignoreReadonly;
            return picker;
        };

        picker.options = function (newOptions) {
            if (arguments.length === 0) {
                return $.extend(true, {}, options);
            }

            if (!(newOptions instanceof Object)) {
                throw new TypeError('options() options parameter should be an object');
            }
            $.extend(true, options, newOptions);
            $.each(options, function (key, value) {
                if (picker[key] !== undefined) {
                    picker[key](value);
                } else {
                    throw new TypeError('option ' + key + ' is not recognized!');
                }
            });
            return picker;
        };

        picker.date = function (newDate) {
            ///<signature helpKeyword="$.fn.datetimepicker.date">
            ///<summary>Returns the component's model current date, a moment object or null if not set.</summary>
            ///<returns type="Moment">date.clone()</returns>
            ///</signature>
            ///<signature>
            ///<summary>Sets the components model current moment to it. Passing a null value unsets the components model current moment. Parsing of the newDate parameter is made using moment library with the options.format and options.useStrict components configuration.</summary>
            ///<param name="newDate" locid="$.fn.datetimepicker.date_p:newDate">Takes string, Date, moment, null parameter.</param>
            ///</signature>
            if (arguments.length === 0) {
                if (unset) {
                    return null;
                }
                return date.clone();
            }

            if (newDate !== null && typeof newDate !== 'string' && !moment.isMoment(newDate) && !(newDate instanceof Date)) {
                throw new TypeError('date() parameter must be one of [null, string, moment or Date]');
            }

            setValue(newDate === null ? null : parseInputDate(newDate));
            return picker;
        };

        picker.format = function (newFormat) {
            ///<summary>test su</summary>
            ///<param name="newFormat">info about para</param>
            ///<returns type="string|boolean">returns foo</returns>
            if (arguments.length === 0) {
                return options.format;
            }

            if ((typeof newFormat !== 'string') && ((typeof newFormat !== 'boolean') || (newFormat !== false))) {
                throw new TypeError('format() expects a string or boolean:false parameter ' + newFormat);
            }

            options.format = newFormat;
            if (actualFormat) {
                initFormatting(); // reinit formatting
            }
            return picker;
        };

        picker.timeZone = function (newZone) {
            if (arguments.length === 0) {
                return options.timeZone;
            }

            if (typeof newZone !== 'string') {
                throw new TypeError('newZone() expects a string parameter');
            }

            options.timeZone = newZone;

            return picker;
        };

        picker.dayViewHeaderFormat = function (newFormat) {
            if (arguments.length === 0) {
                return options.dayViewHeaderFormat;
            }

            if (typeof newFormat !== 'string') {
                throw new TypeError('dayViewHeaderFormat() expects a string parameter');
            }

            options.dayViewHeaderFormat = newFormat;
            return picker;
        };

        picker.extraFormats = function (formats) {
            if (arguments.length === 0) {
                return options.extraFormats;
            }

            if (formats !== false && !(formats instanceof Array)) {
                throw new TypeError('extraFormats() expects an array or false parameter');
            }

            options.extraFormats = formats;
            if (parseFormats) {
                initFormatting(); // reinit formatting
            }
            return picker;
        };

        picker.disabledDates = function (dates) {
            ///<signature helpKeyword="$.fn.datetimepicker.disabledDates">
            ///<summary>Returns an array with the currently set disabled dates on the component.</summary>
            ///<returns type="array">options.disabledDates</returns>
            ///</signature>
            ///<signature>
            ///<summary>Setting this takes precedence over options.minDate, options.maxDate configuration. Also calling this function removes the configuration of
            ///options.enabledDates if such exist.</summary>
            ///<param name="dates" locid="$.fn.datetimepicker.disabledDates_p:dates">Takes an [ string or Date or moment ] of values and allows the user to select only from those days.</param>
            ///</signature>
            if (arguments.length === 0) {
                return (options.disabledDates ? $.extend({}, options.disabledDates) : options.disabledDates);
            }

            if (!dates) {
                options.disabledDates = false;
                update();
                return picker;
            }
            if (!(dates instanceof Array)) {
                throw new TypeError('disabledDates() expects an array parameter');
            }
            options.disabledDates = indexGivenDates(dates);
            options.enabledDates = false;
            update();
            return picker;
        };

        picker.enabledDates = function (dates) {
            ///<signature helpKeyword="$.fn.datetimepicker.enabledDates">
            ///<summary>Returns an array with the currently set enabled dates on the component.</summary>
            ///<returns type="array">options.enabledDates</returns>
            ///</signature>
            ///<signature>
            ///<summary>Setting this takes precedence over options.minDate, options.maxDate configuration. Also calling this function removes the configuration of options.disabledDates if such exist.</summary>
            ///<param name="dates" locid="$.fn.datetimepicker.enabledDates_p:dates">Takes an [ string or Date or moment ] of values and allows the user to select only from those days.</param>
            ///</signature>
            if (arguments.length === 0) {
                return (options.enabledDates ? $.extend({}, options.enabledDates) : options.enabledDates);
            }

            if (!dates) {
                options.enabledDates = false;
                update();
                return picker;
            }
            if (!(dates instanceof Array)) {
                throw new TypeError('enabledDates() expects an array parameter');
            }
            options.enabledDates = indexGivenDates(dates);
            options.disabledDates = false;
            update();
            return picker;
        };

        picker.daysOfWeekDisabled = function (daysOfWeekDisabled) {
            if (arguments.length === 0) {
                return options.daysOfWeekDisabled.splice(0);
            }

            if ((typeof daysOfWeekDisabled === 'boolean') && !daysOfWeekDisabled) {
                options.daysOfWeekDisabled = false;
                update();
                return picker;
            }

            if (!(daysOfWeekDisabled instanceof Array)) {
                throw new TypeError('daysOfWeekDisabled() expects an array parameter');
            }
            options.daysOfWeekDisabled = daysOfWeekDisabled.reduce(function (previousValue, currentValue) {
                currentValue = parseInt(currentValue, 10);
                if (currentValue > 6 || currentValue < 0 || isNaN(currentValue)) {
                    return previousValue;
                }
                if (previousValue.indexOf(currentValue) === -1) {
                    previousValue.push(currentValue);
                }
                return previousValue;
            }, []).sort();
            if (options.useCurrent && !options.keepInvalid) {
                var tries = 0;
                while (!isValid(date, 'd')) {
                    date.add(1, 'd');
                    if (tries === 31) {
                        throw 'Tried 31 times to find a valid date';
                    }
                    tries++;
                }
                setValue(date);
            }
            update();
            return picker;
        };

        picker.maxDate = function (maxDate) {
            if (arguments.length === 0) {
                return options.maxDate ? options.maxDate.clone() : options.maxDate;
            }

            if ((typeof maxDate === 'boolean') && maxDate === false) {
                options.maxDate = false;
                update();
                return picker;
            }

            if (typeof maxDate === 'string') {
                if (maxDate === 'now' || maxDate === 'moment') {
                    maxDate = getMoment();
                }
            }

            var parsedDate = parseInputDate(maxDate);

            if (!parsedDate.isValid()) {
                throw new TypeError('maxDate() Could not parse date parameter: ' + maxDate);
            }
            if (options.minDate && parsedDate.isBefore(options.minDate)) {
                throw new TypeError('maxDate() date parameter is before options.minDate: ' + parsedDate.format(actualFormat));
            }
            options.maxDate = parsedDate;
            if (options.useCurrent && !options.keepInvalid && date.isAfter(maxDate)) {
                setValue(options.maxDate);
            }
            if (viewDate.isAfter(parsedDate)) {
                viewDate = parsedDate.clone().subtract(options.stepping, 'm');
            }
            update();
            return picker;
        };

        picker.minDate = function (minDate) {
            if (arguments.length === 0) {
                return options.minDate ? options.minDate.clone() : options.minDate;
            }

            if ((typeof minDate === 'boolean') && minDate === false) {
                options.minDate = false;
                update();
                return picker;
            }

            if (typeof minDate === 'string') {
                if (minDate === 'now' || minDate === 'moment') {
                    minDate = getMoment();
                }
            }

            var parsedDate = parseInputDate(minDate);

            if (!parsedDate.isValid()) {
                throw new TypeError('minDate() Could not parse date parameter: ' + minDate);
            }
            if (options.maxDate && parsedDate.isAfter(options.maxDate)) {
                throw new TypeError('minDate() date parameter is after options.maxDate: ' + parsedDate.format(actualFormat));
            }
            options.minDate = parsedDate;
            if (options.useCurrent && !options.keepInvalid && date.isBefore(minDate)) {
                setValue(options.minDate);
            }
            if (viewDate.isBefore(parsedDate)) {
                viewDate = parsedDate.clone().add(options.stepping, 'm');
            }
            update();
            return picker;
        };

        picker.defaultDate = function (defaultDate) {
            ///<signature helpKeyword="$.fn.datetimepicker.defaultDate">
            ///<summary>Returns a moment with the options.defaultDate option configuration or false if not set</summary>
            ///<returns type="Moment">date.clone()</returns>
            ///</signature>
            ///<signature>
            ///<summary>Will set the picker's inital date. If a boolean:false value is passed the options.defaultDate parameter is cleared.</summary>
            ///<param name="defaultDate" locid="$.fn.datetimepicker.defaultDate_p:defaultDate">Takes a string, Date, moment, boolean:false</param>
            ///</signature>
            if (arguments.length === 0) {
                return options.defaultDate ? options.defaultDate.clone() : options.defaultDate;
            }
            if (!defaultDate) {
                options.defaultDate = false;
                return picker;
            }

            if (typeof defaultDate === 'string') {
                if (defaultDate === 'now' || defaultDate === 'moment') {
                    defaultDate = getMoment();
                } else {
                    defaultDate = getMoment(defaultDate);
                }
            }

            var parsedDate = parseInputDate(defaultDate);
            if (!parsedDate.isValid()) {
                throw new TypeError('defaultDate() Could not parse date parameter: ' + defaultDate);
            }
            if (!isValid(parsedDate)) {
                throw new TypeError('defaultDate() date passed is invalid according to component setup validations');
            }

            options.defaultDate = parsedDate;

            if ((options.defaultDate && options.inline) || input.val().trim() === '') {
                setValue(options.defaultDate);
            }
            return picker;
        };

        picker.locale = function (locale) {
            if (arguments.length === 0) {
                return options.locale;
            }

            if (!moment.localeData(locale)) {
                throw new TypeError('locale() locale ' + locale + ' is not loaded from moment locales!');
            }

            options.locale = locale;
            date.locale(options.locale);
            viewDate.locale(options.locale);

            if (actualFormat) {
                initFormatting(); // reinit formatting
            }
            if (widget) {
                hide();
                show();
            }
            return picker;
        };

        picker.stepping = function (stepping) {
            if (arguments.length === 0) {
                return options.stepping;
            }

            stepping = parseInt(stepping, 10);
            if (isNaN(stepping) || stepping < 1) {
                stepping = 1;
            }
            options.stepping = stepping;
            return picker;
        };

        picker.useCurrent = function (useCurrent) {
            var useCurrentOptions = ['year', 'month', 'day', 'hour', 'minute'];
            if (arguments.length === 0) {
                return options.useCurrent;
            }

            if ((typeof useCurrent !== 'boolean') && (typeof useCurrent !== 'string')) {
                throw new TypeError('useCurrent() expects a boolean or string parameter');
            }
            if (typeof useCurrent === 'string' && useCurrentOptions.indexOf(useCurrent.toLowerCase()) === -1) {
                throw new TypeError('useCurrent() expects a string parameter of ' + useCurrentOptions.join(', '));
            }
            options.useCurrent = useCurrent;
            return picker;
        };

        picker.collapse = function (collapse) {
            if (arguments.length === 0) {
                return options.collapse;
            }

            if (typeof collapse !== 'boolean') {
                throw new TypeError('collapse() expects a boolean parameter');
            }
            if (options.collapse === collapse) {
                return picker;
            }
            options.collapse = collapse;
            if (widget) {
                hide();
                show();
            }
            return picker;
        };

        picker.icons = function (icons) {
            if (arguments.length === 0) {
                return $.extend({}, options.icons);
            }

            if (!(icons instanceof Object)) {
                throw new TypeError('icons() expects parameter to be an Object');
            }
            $.extend(options.icons, icons);
            if (widget) {
                hide();
                show();
            }
            return picker;
        };

        picker.tooltips = function (tooltips) {
            if (arguments.length === 0) {
                return $.extend({}, options.tooltips);
            }

            if (!(tooltips instanceof Object)) {
                throw new TypeError('tooltips() expects parameter to be an Object');
            }
            $.extend(options.tooltips, tooltips);
            if (widget) {
                hide();
                show();
            }
            return picker;
        };

        picker.useStrict = function (useStrict) {
            if (arguments.length === 0) {
                return options.useStrict;
            }

            if (typeof useStrict !== 'boolean') {
                throw new TypeError('useStrict() expects a boolean parameter');
            }
            options.useStrict = useStrict;
            return picker;
        };

        picker.sideBySide = function (sideBySide) {
            if (arguments.length === 0) {
                return options.sideBySide;
            }

            if (typeof sideBySide !== 'boolean') {
                throw new TypeError('sideBySide() expects a boolean parameter');
            }
            options.sideBySide = sideBySide;
            if (widget) {
                hide();
                show();
            }
            return picker;
        };

        picker.viewMode = function (viewMode) {
            if (arguments.length === 0) {
                return options.viewMode;
            }

            if (typeof viewMode !== 'string') {
                throw new TypeError('viewMode() expects a string parameter');
            }

            if (viewModes.indexOf(viewMode) === -1) {
                throw new TypeError('viewMode() parameter must be one of (' + viewModes.join(', ') + ') value');
            }

            options.viewMode = viewMode;
            currentViewMode = Math.max(viewModes.indexOf(viewMode), minViewModeNumber);

            showMode();
            return picker;
        };

        picker.toolbarPlacement = function (toolbarPlacement) {
            if (arguments.length === 0) {
                return options.toolbarPlacement;
            }

            if (typeof toolbarPlacement !== 'string') {
                throw new TypeError('toolbarPlacement() expects a string parameter');
            }
            if (toolbarPlacements.indexOf(toolbarPlacement) === -1) {
                throw new TypeError('toolbarPlacement() parameter must be one of (' + toolbarPlacements.join(', ') + ') value');
            }
            options.toolbarPlacement = toolbarPlacement;

            if (widget) {
                hide();
                show();
            }
            return picker;
        };

        picker.widgetPositioning = function (widgetPositioning) {
            if (arguments.length === 0) {
                return $.extend({}, options.widgetPositioning);
            }

            if (({}).toString.call(widgetPositioning) !== '[object Object]') {
                throw new TypeError('widgetPositioning() expects an object variable');
            }
            if (widgetPositioning.horizontal) {
                if (typeof widgetPositioning.horizontal !== 'string') {
                    throw new TypeError('widgetPositioning() horizontal variable must be a string');
                }
                widgetPositioning.horizontal = widgetPositioning.horizontal.toLowerCase();
                if (horizontalModes.indexOf(widgetPositioning.horizontal) === -1) {
                    throw new TypeError('widgetPositioning() expects horizontal parameter to be one of (' + horizontalModes.join(', ') + ')');
                }
                options.widgetPositioning.horizontal = widgetPositioning.horizontal;
            }
            if (widgetPositioning.vertical) {
                if (typeof widgetPositioning.vertical !== 'string') {
                    throw new TypeError('widgetPositioning() vertical variable must be a string');
                }
                widgetPositioning.vertical = widgetPositioning.vertical.toLowerCase();
                if (verticalModes.indexOf(widgetPositioning.vertical) === -1) {
                    throw new TypeError('widgetPositioning() expects vertical parameter to be one of (' + verticalModes.join(', ') + ')');
                }
                options.widgetPositioning.vertical = widgetPositioning.vertical;
            }
            update();
            return picker;
        };

        picker.calendarWeeks = function (calendarWeeks) {
            if (arguments.length === 0) {
                return options.calendarWeeks;
            }

            if (typeof calendarWeeks !== 'boolean') {
                throw new TypeError('calendarWeeks() expects parameter to be a boolean value');
            }

            options.calendarWeeks = calendarWeeks;
            update();
            return picker;
        };

        picker.showTodayButton = function (showTodayButton) {
            if (arguments.length === 0) {
                return options.showTodayButton;
            }

            if (typeof showTodayButton !== 'boolean') {
                throw new TypeError('showTodayButton() expects a boolean parameter');
            }

            options.showTodayButton = showTodayButton;
            if (widget) {
                hide();
                show();
            }
            return picker;
        };

        picker.showClear = function (showClear) {
            if (arguments.length === 0) {
                return options.showClear;
            }

            if (typeof showClear !== 'boolean') {
                throw new TypeError('showClear() expects a boolean parameter');
            }

            options.showClear = showClear;
            if (widget) {
                hide();
                show();
            }
            return picker;
        };

        picker.widgetParent = function (widgetParent) {
            if (arguments.length === 0) {
                return options.widgetParent;
            }

            if (typeof widgetParent === 'string') {
                widgetParent = $(widgetParent);
            }

            if (widgetParent !== null && (typeof widgetParent !== 'string' && !(widgetParent instanceof $))) {
                throw new TypeError('widgetParent() expects a string or a jQuery object parameter');
            }

            options.widgetParent = widgetParent;
            if (widget) {
                hide();
                show();
            }
            return picker;
        };

        picker.keepOpen = function (keepOpen) {
            if (arguments.length === 0) {
                return options.keepOpen;
            }

            if (typeof keepOpen !== 'boolean') {
                throw new TypeError('keepOpen() expects a boolean parameter');
            }

            options.keepOpen = keepOpen;
            return picker;
        };

        picker.focusOnShow = function (focusOnShow) {
            if (arguments.length === 0) {
                return options.focusOnShow;
            }

            if (typeof focusOnShow !== 'boolean') {
                throw new TypeError('focusOnShow() expects a boolean parameter');
            }

            options.focusOnShow = focusOnShow;
            return picker;
        };

        picker.inline = function (inline) {
            if (arguments.length === 0) {
                return options.inline;
            }

            if (typeof inline !== 'boolean') {
                throw new TypeError('inline() expects a boolean parameter');
            }

            options.inline = inline;
            return picker;
        };

        picker.clear = function () {
            clear();
            return picker;
        };

        picker.keyBinds = function (keyBinds) {
            if (arguments.length === 0) {
                return options.keyBinds;
            }

            options.keyBinds = keyBinds;
            return picker;
        };

        picker.getMoment = function (d) {
            return getMoment(d);
        };

        picker.debug = function (debug) {
            if (typeof debug !== 'boolean') {
                throw new TypeError('debug() expects a boolean parameter');
            }

            options.debug = debug;
            return picker;
        };

        picker.allowInputToggle = function (allowInputToggle) {
            if (arguments.length === 0) {
                return options.allowInputToggle;
            }

            if (typeof allowInputToggle !== 'boolean') {
                throw new TypeError('allowInputToggle() expects a boolean parameter');
            }

            options.allowInputToggle = allowInputToggle;
            return picker;
        };

        picker.showClose = function (showClose) {
            if (arguments.length === 0) {
                return options.showClose;
            }

            if (typeof showClose !== 'boolean') {
                throw new TypeError('showClose() expects a boolean parameter');
            }

            options.showClose = showClose;
            return picker;
        };

        picker.keepInvalid = function (keepInvalid) {
            if (arguments.length === 0) {
                return options.keepInvalid;
            }

            if (typeof keepInvalid !== 'boolean') {
                throw new TypeError('keepInvalid() expects a boolean parameter');
            }
            options.keepInvalid = keepInvalid;
            return picker;
        };

        picker.datepickerInput = function (datepickerInput) {
            if (arguments.length === 0) {
                return options.datepickerInput;
            }

            if (typeof datepickerInput !== 'string') {
                throw new TypeError('datepickerInput() expects a string parameter');
            }

            options.datepickerInput = datepickerInput;
            return picker;
        };

        picker.parseInputDate = function (parseInputDate) {
            if (arguments.length === 0) {
                return options.parseInputDate;
            }

            if (typeof parseInputDate !== 'function') {
                throw new TypeError('parseInputDate() sholud be as function');
            }

            options.parseInputDate = parseInputDate;

            return picker;
        };

        picker.disabledTimeIntervals = function (disabledTimeIntervals) {
            ///<signature helpKeyword="$.fn.datetimepicker.disabledTimeIntervals">
            ///<summary>Returns an array with the currently set disabled dates on the component.</summary>
            ///<returns type="array">options.disabledTimeIntervals</returns>
            ///</signature>
            ///<signature>
            ///<summary>Setting this takes precedence over options.minDate, options.maxDate configuration. Also calling this function removes the configuration of
            ///options.enabledDates if such exist.</summary>
            ///<param name="dates" locid="$.fn.datetimepicker.disabledTimeIntervals_p:dates">Takes an [ string or Date or moment ] of values and allows the user to select only from those days.</param>
            ///</signature>
            if (arguments.length === 0) {
                return (options.disabledTimeIntervals ? $.extend({}, options.disabledTimeIntervals) : options.disabledTimeIntervals);
            }

            if (!disabledTimeIntervals) {
                options.disabledTimeIntervals = false;
                update();
                return picker;
            }
            if (!(disabledTimeIntervals instanceof Array)) {
                throw new TypeError('disabledTimeIntervals() expects an array parameter');
            }
            options.disabledTimeIntervals = disabledTimeIntervals;
            update();
            return picker;
        };

        picker.disabledHours = function (hours) {
            ///<signature helpKeyword="$.fn.datetimepicker.disabledHours">
            ///<summary>Returns an array with the currently set disabled hours on the component.</summary>
            ///<returns type="array">options.disabledHours</returns>
            ///</signature>
            ///<signature>
            ///<summary>Setting this takes precedence over options.minDate, options.maxDate configuration. Also calling this function removes the configuration of
            ///options.enabledHours if such exist.</summary>
            ///<param name="hours" locid="$.fn.datetimepicker.disabledHours_p:hours">Takes an [ int ] of values and disallows the user to select only from those hours.</param>
            ///</signature>
            if (arguments.length === 0) {
                return (options.disabledHours ? $.extend({}, options.disabledHours) : options.disabledHours);
            }

            if (!hours) {
                options.disabledHours = false;
                update();
                return picker;
            }
            if (!(hours instanceof Array)) {
                throw new TypeError('disabledHours() expects an array parameter');
            }
            options.disabledHours = indexGivenHours(hours);
            options.enabledHours = false;
            if (options.useCurrent && !options.keepInvalid) {
                var tries = 0;
                while (!isValid(date, 'h')) {
                    date.add(1, 'h');
                    if (tries === 24) {
                        throw 'Tried 24 times to find a valid date';
                    }
                    tries++;
                }
                setValue(date);
            }
            update();
            return picker;
        };

        picker.enabledHours = function (hours) {
            ///<signature helpKeyword="$.fn.datetimepicker.enabledHours">
            ///<summary>Returns an array with the currently set enabled hours on the component.</summary>
            ///<returns type="array">options.enabledHours</returns>
            ///</signature>
            ///<signature>
            ///<summary>Setting this takes precedence over options.minDate, options.maxDate configuration. Also calling this function removes the configuration of options.disabledHours if such exist.</summary>
            ///<param name="hours" locid="$.fn.datetimepicker.enabledHours_p:hours">Takes an [ int ] of values and allows the user to select only from those hours.</param>
            ///</signature>
            if (arguments.length === 0) {
                return (options.enabledHours ? $.extend({}, options.enabledHours) : options.enabledHours);
            }

            if (!hours) {
                options.enabledHours = false;
                update();
                return picker;
            }
            if (!(hours instanceof Array)) {
                throw new TypeError('enabledHours() expects an array parameter');
            }
            options.enabledHours = indexGivenHours(hours);
            options.disabledHours = false;
            if (options.useCurrent && !options.keepInvalid) {
                var tries = 0;
                while (!isValid(date, 'h')) {
                    date.add(1, 'h');
                    if (tries === 24) {
                        throw 'Tried 24 times to find a valid date';
                    }
                    tries++;
                }
                setValue(date);
            }
            update();
            return picker;
        };
        /**
         * Returns the component's model current viewDate, a moment object or null if not set. Passing a null value unsets the components model current moment. Parsing of the newDate parameter is made using moment library with the options.format and options.useStrict components configuration.
         * @param {Takes string, viewDate, moment, null parameter.} newDate
         * @returns {viewDate.clone()}
         */
        picker.viewDate = function (newDate) {
            if (arguments.length === 0) {
                return viewDate.clone();
            }

            if (!newDate) {
                viewDate = date.clone();
                return picker;
            }

            if (typeof newDate !== 'string' && !moment.isMoment(newDate) && !(newDate instanceof Date)) {
                throw new TypeError('viewDate() parameter must be one of [string, moment or Date]');
            }

            viewDate = parseInputDate(newDate);
            viewUpdate();
            return picker;
        };

        // initializing element and component attributes
        if (element.is('input')) {
            input = element;
        } else {
            input = element.find(options.datepickerInput);
            if (input.length === 0) {
                input = element.find('input');
            } else if (!input.is('input')) {
                throw new Error('CSS class "' + options.datepickerInput + '" cannot be applied to non input element');
            }
        }

        if (element.hasClass('input-group')) {
            // in case there is more then one 'input-group-addon' Issue #48
            if (element.find('.datepickerbutton').length === 0) {
                component = element.find('.input-group-addon');
            } else {
                component = element.find('.datepickerbutton');
            }
        }

        if (!options.inline && !input.is('input')) {
            throw new Error('Could not initialize DateTimePicker without an input element');
        }

        // Set defaults for date here now instead of in var declaration
        date = getMoment();
        viewDate = date.clone();

        $.extend(true, options, dataToOptions());

        picker.options(options);

        initFormatting();

        attachDatePickerElementEvents();

        if (input.prop('disabled')) {
            picker.disable();
        }
        if (input.is('input') && input.val().trim().length !== 0) {
            setValue(parseInputDate(input.val().trim()));
        }
        else if (options.defaultDate && input.attr('placeholder') === undefined) {
            setValue(options.defaultDate);
        }
        if (options.inline) {
            show();
        }
        return picker;
    };

    /********************************************************************************
     *
     * jQuery plugin constructor and defaults object
     *
     ********************************************************************************/

    /**
    * See (http://jquery.com/).
    * @name jQuery
    * @class
    * See the jQuery Library  (http://jquery.com/) for full details.  This just
    * documents the function and classes that are added to jQuery by this plug-in.
    */
    /**
     * See (http://jquery.com/)
     * @name fn
     * @class
     * See the jQuery Library  (http://jquery.com/) for full details.  This just
     * documents the function and classes that are added to jQuery by this plug-in.
     * @memberOf jQuery
     */
    /**
     * Show comments
     * @class datetimepicker
     * @memberOf jQuery.fn
     */
    $.fn.datetimepicker = function (options) {
        options = options || {};

        var args = Array.prototype.slice.call(arguments, 1),
            isInstance = true,
            thisMethods = ['destroy', 'hide', 'show', 'toggle'],
            returnValue;

        if (typeof options === 'object') {
            return this.each(function () {
                var $this = $(this),
                    _options;
                if (!$this.data('DateTimePicker')) {
                    // create a private copy of the defaults object
                    _options = $.extend(true, {}, $.fn.datetimepicker.defaults, options);
                    $this.data('DateTimePicker', dateTimePicker($this, _options));
                }
            });
        } else if (typeof options === 'string') {
            this.each(function () {
                var $this = $(this),
                    instance = $this.data('DateTimePicker');
                if (!instance) {
                    throw new Error('bootstrap-datetimepicker("' + options + '") method was called on an element that is not using DateTimePicker');
                }

                returnValue = instance[options].apply(instance, args);
                isInstance = returnValue === instance;
            });

            if (isInstance || $.inArray(options, thisMethods) > -1) {
                return this;
            }

            return returnValue;
        }

        throw new TypeError('Invalid arguments for DateTimePicker: ' + options);
    };

    $.fn.datetimepicker.defaults = {
        timeZone: '',
        format: false,
        dayViewHeaderFormat: 'MMMM YYYY',
        extraFormats: false,
        stepping: 1,
        minDate: false,
        maxDate: false,
        useCurrent: true,
        collapse: true,
        locale: moment.locale(),
        defaultDate: false,
        disabledDates: false,
        enabledDates: false,
        icons: {
            time: 'glyphicon glyphicon-time',
            date: 'glyphicon glyphicon-calendar',
            up: 'glyphicon glyphicon-chevron-up',
            down: 'glyphicon glyphicon-chevron-down',
            previous: 'glyphicon glyphicon-chevron-left',
            next: 'glyphicon glyphicon-chevron-right',
            today: 'glyphicon glyphicon-screenshot',
            clear: 'glyphicon glyphicon-trash',
            close: 'glyphicon glyphicon-remove'
        },
        tooltips: {
            today: 'Go to today',
            clear: 'Clear selection',
            close: 'Close the picker',
            selectMonth: 'Select Month',
            prevMonth: 'Previous Month',
            nextMonth: 'Next Month',
            selectYear: 'Select Year',
            prevYear: 'Previous Year',
            nextYear: 'Next Year',
            selectDecade: 'Select Decade',
            prevDecade: 'Previous Decade',
            nextDecade: 'Next Decade',
            prevCentury: 'Previous Century',
            nextCentury: 'Next Century',
            pickHour: 'Pick Hour',
            incrementHour: 'Increment Hour',
            decrementHour: 'Decrement Hour',
            pickMinute: 'Pick Minute',
            incrementMinute: 'Increment Minute',
            decrementMinute: 'Decrement Minute',
            pickSecond: 'Pick Second',
            incrementSecond: 'Increment Second',
            decrementSecond: 'Decrement Second',
            togglePeriod: 'Toggle Period',
            selectTime: 'Select Time'
        },
        useStrict: false,
        sideBySide: false,
        daysOfWeekDisabled: false,
        calendarWeeks: false,
        viewMode: 'days',
        toolbarPlacement: 'default',
        showTodayButton: false,
        showClear: false,
        showClose: false,
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'auto'
        },
        widgetParent: null,
        ignoreReadonly: false,
        keepOpen: false,
        focusOnShow: true,
        inline: false,
        keepInvalid: false,
        datepickerInput: '.datepickerinput',
        keyBinds: {
            up: function (widget) {
                if (!widget) {
                    return;
                }
                var d = this.date() || this.getMoment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().subtract(7, 'd'));
                } else {
                    this.date(d.clone().add(this.stepping(), 'm'));
                }
            },
            down: function (widget) {
                if (!widget) {
                    this.show();
                    return;
                }
                var d = this.date() || this.getMoment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().add(7, 'd'));
                } else {
                    this.date(d.clone().subtract(this.stepping(), 'm'));
                }
            },
            'control up': function (widget) {
                if (!widget) {
                    return;
                }
                var d = this.date() || this.getMoment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().subtract(1, 'y'));
                } else {
                    this.date(d.clone().add(1, 'h'));
                }
            },
            'control down': function (widget) {
                if (!widget) {
                    return;
                }
                var d = this.date() || this.getMoment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().add(1, 'y'));
                } else {
                    this.date(d.clone().subtract(1, 'h'));
                }
            },
            left: function (widget) {
                if (!widget) {
                    return;
                }
                var d = this.date() || this.getMoment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().subtract(1, 'd'));
                }
            },
            right: function (widget) {
                if (!widget) {
                    return;
                }
                var d = this.date() || this.getMoment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().add(1, 'd'));
                }
            },
            pageUp: function (widget) {
                if (!widget) {
                    return;
                }
                var d = this.date() || this.getMoment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().subtract(1, 'M'));
                }
            },
            pageDown: function (widget) {
                if (!widget) {
                    return;
                }
                var d = this.date() || this.getMoment();
                if (widget.find('.datepicker').is(':visible')) {
                    this.date(d.clone().add(1, 'M'));
                }
            },
            enter: function () {
                this.hide();
            },
            escape: function () {
                this.hide();
            },
            //tab: function (widget) { //this break the flow of the form. disabling for now
            //    var toggle = widget.find('.picker-switch a[data-action="togglePicker"]');
            //    if(toggle.length > 0) toggle.click();
            //},
            'control space': function (widget) {
                if (!widget) {
                    return;
                }
                if (widget.find('.timepicker').is(':visible')) {
                    widget.find('.btn[data-action="togglePeriod"]').click();
                }
            },
            t: function () {
                this.date(this.getMoment());
            },
            'delete': function () {
                this.clear();
            }
        },
        debug: false,
        allowInputToggle: false,
        disabledTimeIntervals: false,
        disabledHours: false,
        enabledHours: false,
        viewDate: false
    };

    return $.fn.datetimepicker;
}));

/*!
 * Bootstrap v3.3.7 (http://getbootstrap.com)
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under the MIT license
 */
if("undefined"==typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");+function(a){"use strict";var b=a.fn.jquery.split(" ")[0].split(".");if(b[0]<2&&b[1]<9||1==b[0]&&9==b[1]&&b[2]<1||b[0]>3)throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4")}(jQuery),+function(a){"use strict";function b(){var a=document.createElement("bootstrap"),b={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var c in b)if(void 0!==a.style[c])return{end:b[c]};return!1}a.fn.emulateTransitionEnd=function(b){var c=!1,d=this;a(this).one("bsTransitionEnd",function(){c=!0});var e=function(){c||a(d).trigger(a.support.transition.end)};return setTimeout(e,b),this},a(function(){a.support.transition=b(),a.support.transition&&(a.event.special.bsTransitionEnd={bindType:a.support.transition.end,delegateType:a.support.transition.end,handle:function(b){if(a(b.target).is(this))return b.handleObj.handler.apply(this,arguments)}})})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var c=a(this),e=c.data("bs.alert");e||c.data("bs.alert",e=new d(this)),"string"==typeof b&&e[b].call(c)})}var c='[data-dismiss="alert"]',d=function(b){a(b).on("click",c,this.close)};d.VERSION="3.3.7",d.TRANSITION_DURATION=150,d.prototype.close=function(b){function c(){g.detach().trigger("closed.bs.alert").remove()}var e=a(this),f=e.attr("data-target");f||(f=e.attr("href"),f=f&&f.replace(/.*(?=#[^\s]*$)/,""));var g=a("#"===f?[]:f);b&&b.preventDefault(),g.length||(g=e.closest(".alert")),g.trigger(b=a.Event("close.bs.alert")),b.isDefaultPrevented()||(g.removeClass("in"),a.support.transition&&g.hasClass("fade")?g.one("bsTransitionEnd",c).emulateTransitionEnd(d.TRANSITION_DURATION):c())};var e=a.fn.alert;a.fn.alert=b,a.fn.alert.Constructor=d,a.fn.alert.noConflict=function(){return a.fn.alert=e,this},a(document).on("click.bs.alert.data-api",c,d.prototype.close)}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.button"),f="object"==typeof b&&b;e||d.data("bs.button",e=new c(this,f)),"toggle"==b?e.toggle():b&&e.setState(b)})}var c=function(b,d){this.$element=a(b),this.options=a.extend({},c.DEFAULTS,d),this.isLoading=!1};c.VERSION="3.3.7",c.DEFAULTS={loadingText:"loading..."},c.prototype.setState=function(b){var c="disabled",d=this.$element,e=d.is("input")?"val":"html",f=d.data();b+="Text",null==f.resetText&&d.data("resetText",d[e]()),setTimeout(a.proxy(function(){d[e](null==f[b]?this.options[b]:f[b]),"loadingText"==b?(this.isLoading=!0,d.addClass(c).attr(c,c).prop(c,!0)):this.isLoading&&(this.isLoading=!1,d.removeClass(c).removeAttr(c).prop(c,!1))},this),0)},c.prototype.toggle=function(){var a=!0,b=this.$element.closest('[data-toggle="buttons"]');if(b.length){var c=this.$element.find("input");"radio"==c.prop("type")?(c.prop("checked")&&(a=!1),b.find(".active").removeClass("active"),this.$element.addClass("active")):"checkbox"==c.prop("type")&&(c.prop("checked")!==this.$element.hasClass("active")&&(a=!1),this.$element.toggleClass("active")),c.prop("checked",this.$element.hasClass("active")),a&&c.trigger("change")}else this.$element.attr("aria-pressed",!this.$element.hasClass("active")),this.$element.toggleClass("active")};var d=a.fn.button;a.fn.button=b,a.fn.button.Constructor=c,a.fn.button.noConflict=function(){return a.fn.button=d,this},a(document).on("click.bs.button.data-api",'[data-toggle^="button"]',function(c){var d=a(c.target).closest(".btn");b.call(d,"toggle"),a(c.target).is('input[type="radio"], input[type="checkbox"]')||(c.preventDefault(),d.is("input,button")?d.trigger("focus"):d.find("input:visible,button:visible").first().trigger("focus"))}).on("focus.bs.button.data-api blur.bs.button.data-api",'[data-toggle^="button"]',function(b){a(b.target).closest(".btn").toggleClass("focus",/^focus(in)?$/.test(b.type))})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.carousel"),f=a.extend({},c.DEFAULTS,d.data(),"object"==typeof b&&b),g="string"==typeof b?b:f.slide;e||d.data("bs.carousel",e=new c(this,f)),"number"==typeof b?e.to(b):g?e[g]():f.interval&&e.pause().cycle()})}var c=function(b,c){this.$element=a(b),this.$indicators=this.$element.find(".carousel-indicators"),this.options=c,this.paused=null,this.sliding=null,this.interval=null,this.$active=null,this.$items=null,this.options.keyboard&&this.$element.on("keydown.bs.carousel",a.proxy(this.keydown,this)),"hover"==this.options.pause&&!("ontouchstart"in document.documentElement)&&this.$element.on("mouseenter.bs.carousel",a.proxy(this.pause,this)).on("mouseleave.bs.carousel",a.proxy(this.cycle,this))};c.VERSION="3.3.7",c.TRANSITION_DURATION=600,c.DEFAULTS={interval:5e3,pause:"hover",wrap:!0,keyboard:!0},c.prototype.keydown=function(a){if(!/input|textarea/i.test(a.target.tagName)){switch(a.which){case 37:this.prev();break;case 39:this.next();break;default:return}a.preventDefault()}},c.prototype.cycle=function(b){return b||(this.paused=!1),this.interval&&clearInterval(this.interval),this.options.interval&&!this.paused&&(this.interval=setInterval(a.proxy(this.next,this),this.options.interval)),this},c.prototype.getItemIndex=function(a){return this.$items=a.parent().children(".item"),this.$items.index(a||this.$active)},c.prototype.getItemForDirection=function(a,b){var c=this.getItemIndex(b),d="prev"==a&&0===c||"next"==a&&c==this.$items.length-1;if(d&&!this.options.wrap)return b;var e="prev"==a?-1:1,f=(c+e)%this.$items.length;return this.$items.eq(f)},c.prototype.to=function(a){var b=this,c=this.getItemIndex(this.$active=this.$element.find(".item.active"));if(!(a>this.$items.length-1||a<0))return this.sliding?this.$element.one("slid.bs.carousel",function(){b.to(a)}):c==a?this.pause().cycle():this.slide(a>c?"next":"prev",this.$items.eq(a))},c.prototype.pause=function(b){return b||(this.paused=!0),this.$element.find(".next, .prev").length&&a.support.transition&&(this.$element.trigger(a.support.transition.end),this.cycle(!0)),this.interval=clearInterval(this.interval),this},c.prototype.next=function(){if(!this.sliding)return this.slide("next")},c.prototype.prev=function(){if(!this.sliding)return this.slide("prev")},c.prototype.slide=function(b,d){var e=this.$element.find(".item.active"),f=d||this.getItemForDirection(b,e),g=this.interval,h="next"==b?"left":"right",i=this;if(f.hasClass("active"))return this.sliding=!1;var j=f[0],k=a.Event("slide.bs.carousel",{relatedTarget:j,direction:h});if(this.$element.trigger(k),!k.isDefaultPrevented()){if(this.sliding=!0,g&&this.pause(),this.$indicators.length){this.$indicators.find(".active").removeClass("active");var l=a(this.$indicators.children()[this.getItemIndex(f)]);l&&l.addClass("active")}var m=a.Event("slid.bs.carousel",{relatedTarget:j,direction:h});return a.support.transition&&this.$element.hasClass("slide")?(f.addClass(b),f[0].offsetWidth,e.addClass(h),f.addClass(h),e.one("bsTransitionEnd",function(){f.removeClass([b,h].join(" ")).addClass("active"),e.removeClass(["active",h].join(" ")),i.sliding=!1,setTimeout(function(){i.$element.trigger(m)},0)}).emulateTransitionEnd(c.TRANSITION_DURATION)):(e.removeClass("active"),f.addClass("active"),this.sliding=!1,this.$element.trigger(m)),g&&this.cycle(),this}};var d=a.fn.carousel;a.fn.carousel=b,a.fn.carousel.Constructor=c,a.fn.carousel.noConflict=function(){return a.fn.carousel=d,this};var e=function(c){var d,e=a(this),f=a(e.attr("data-target")||(d=e.attr("href"))&&d.replace(/.*(?=#[^\s]+$)/,""));if(f.hasClass("carousel")){var g=a.extend({},f.data(),e.data()),h=e.attr("data-slide-to");h&&(g.interval=!1),b.call(f,g),h&&f.data("bs.carousel").to(h),c.preventDefault()}};a(document).on("click.bs.carousel.data-api","[data-slide]",e).on("click.bs.carousel.data-api","[data-slide-to]",e),a(window).on("load",function(){a('[data-ride="carousel"]').each(function(){var c=a(this);b.call(c,c.data())})})}(jQuery),+function(a){"use strict";function b(b){var c,d=b.attr("data-target")||(c=b.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,"");return a(d)}function c(b){return this.each(function(){var c=a(this),e=c.data("bs.collapse"),f=a.extend({},d.DEFAULTS,c.data(),"object"==typeof b&&b);!e&&f.toggle&&/show|hide/.test(b)&&(f.toggle=!1),e||c.data("bs.collapse",e=new d(this,f)),"string"==typeof b&&e[b]()})}var d=function(b,c){this.$element=a(b),this.options=a.extend({},d.DEFAULTS,c),this.$trigger=a('[data-toggle="collapse"][href="#'+b.id+'"],[data-toggle="collapse"][data-target="#'+b.id+'"]'),this.transitioning=null,this.options.parent?this.$parent=this.getParent():this.addAriaAndCollapsedClass(this.$element,this.$trigger),this.options.toggle&&this.toggle()};d.VERSION="3.3.7",d.TRANSITION_DURATION=350,d.DEFAULTS={toggle:!0},d.prototype.dimension=function(){var a=this.$element.hasClass("width");return a?"width":"height"},d.prototype.show=function(){if(!this.transitioning&&!this.$element.hasClass("in")){var b,e=this.$parent&&this.$parent.children(".panel").children(".in, .collapsing");if(!(e&&e.length&&(b=e.data("bs.collapse"),b&&b.transitioning))){var f=a.Event("show.bs.collapse");if(this.$element.trigger(f),!f.isDefaultPrevented()){e&&e.length&&(c.call(e,"hide"),b||e.data("bs.collapse",null));var g=this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[g](0).attr("aria-expanded",!0),this.$trigger.removeClass("collapsed").attr("aria-expanded",!0),this.transitioning=1;var h=function(){this.$element.removeClass("collapsing").addClass("collapse in")[g](""),this.transitioning=0,this.$element.trigger("shown.bs.collapse")};if(!a.support.transition)return h.call(this);var i=a.camelCase(["scroll",g].join("-"));this.$element.one("bsTransitionEnd",a.proxy(h,this)).emulateTransitionEnd(d.TRANSITION_DURATION)[g](this.$element[0][i])}}}},d.prototype.hide=function(){if(!this.transitioning&&this.$element.hasClass("in")){var b=a.Event("hide.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.dimension();this.$element[c](this.$element[c]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded",!1),this.$trigger.addClass("collapsed").attr("aria-expanded",!1),this.transitioning=1;var e=function(){this.transitioning=0,this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")};return a.support.transition?void this.$element[c](0).one("bsTransitionEnd",a.proxy(e,this)).emulateTransitionEnd(d.TRANSITION_DURATION):e.call(this)}}},d.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()},d.prototype.getParent=function(){return a(this.options.parent).find('[data-toggle="collapse"][data-parent="'+this.options.parent+'"]').each(a.proxy(function(c,d){var e=a(d);this.addAriaAndCollapsedClass(b(e),e)},this)).end()},d.prototype.addAriaAndCollapsedClass=function(a,b){var c=a.hasClass("in");a.attr("aria-expanded",c),b.toggleClass("collapsed",!c).attr("aria-expanded",c)};var e=a.fn.collapse;a.fn.collapse=c,a.fn.collapse.Constructor=d,a.fn.collapse.noConflict=function(){return a.fn.collapse=e,this},a(document).on("click.bs.collapse.data-api",'[data-toggle="collapse"]',function(d){var e=a(this);e.attr("data-target")||d.preventDefault();var f=b(e),g=f.data("bs.collapse"),h=g?"toggle":e.data();c.call(f,h)})}(jQuery),+function(a){"use strict";function b(b){var c=b.attr("data-target");c||(c=b.attr("href"),c=c&&/#[A-Za-z]/.test(c)&&c.replace(/.*(?=#[^\s]*$)/,""));var d=c&&a(c);return d&&d.length?d:b.parent()}function c(c){c&&3===c.which||(a(e).remove(),a(f).each(function(){var d=a(this),e=b(d),f={relatedTarget:this};e.hasClass("open")&&(c&&"click"==c.type&&/input|textarea/i.test(c.target.tagName)&&a.contains(e[0],c.target)||(e.trigger(c=a.Event("hide.bs.dropdown",f)),c.isDefaultPrevented()||(d.attr("aria-expanded","false"),e.removeClass("open").trigger(a.Event("hidden.bs.dropdown",f)))))}))}function d(b){return this.each(function(){var c=a(this),d=c.data("bs.dropdown");d||c.data("bs.dropdown",d=new g(this)),"string"==typeof b&&d[b].call(c)})}var e=".dropdown-backdrop",f='[data-toggle="dropdown"]',g=function(b){a(b).on("click.bs.dropdown",this.toggle)};g.VERSION="3.3.7",g.prototype.toggle=function(d){var e=a(this);if(!e.is(".disabled, :disabled")){var f=b(e),g=f.hasClass("open");if(c(),!g){"ontouchstart"in document.documentElement&&!f.closest(".navbar-nav").length&&a(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(a(this)).on("click",c);var h={relatedTarget:this};if(f.trigger(d=a.Event("show.bs.dropdown",h)),d.isDefaultPrevented())return;e.trigger("focus").attr("aria-expanded","true"),f.toggleClass("open").trigger(a.Event("shown.bs.dropdown",h))}return!1}},g.prototype.keydown=function(c){if(/(38|40|27|32)/.test(c.which)&&!/input|textarea/i.test(c.target.tagName)){var d=a(this);if(c.preventDefault(),c.stopPropagation(),!d.is(".disabled, :disabled")){var e=b(d),g=e.hasClass("open");if(!g&&27!=c.which||g&&27==c.which)return 27==c.which&&e.find(f).trigger("focus"),d.trigger("click");var h=" li:not(.disabled):visible a",i=e.find(".dropdown-menu"+h);if(i.length){var j=i.index(c.target);38==c.which&&j>0&&j--,40==c.which&&j<i.length-1&&j++,~j||(j=0),i.eq(j).trigger("focus")}}}};var h=a.fn.dropdown;a.fn.dropdown=d,a.fn.dropdown.Constructor=g,a.fn.dropdown.noConflict=function(){return a.fn.dropdown=h,this},a(document).on("click.bs.dropdown.data-api",c).on("click.bs.dropdown.data-api",".dropdown form",function(a){a.stopPropagation()}).on("click.bs.dropdown.data-api",f,g.prototype.toggle).on("keydown.bs.dropdown.data-api",f,g.prototype.keydown).on("keydown.bs.dropdown.data-api",".dropdown-menu",g.prototype.keydown)}(jQuery),+function(a){"use strict";function b(b,d){return this.each(function(){var e=a(this),f=e.data("bs.modal"),g=a.extend({},c.DEFAULTS,e.data(),"object"==typeof b&&b);f||e.data("bs.modal",f=new c(this,g)),"string"==typeof b?f[b](d):g.show&&f.show(d)})}var c=function(b,c){this.options=c,this.$body=a(document.body),this.$element=a(b),this.$dialog=this.$element.find(".modal-dialog"),this.$backdrop=null,this.isShown=null,this.originalBodyPad=null,this.scrollbarWidth=0,this.ignoreBackdropClick=!1,this.options.remote&&this.$element.find(".modal-content").load(this.options.remote,a.proxy(function(){this.$element.trigger("loaded.bs.modal")},this))};c.VERSION="3.3.7",c.TRANSITION_DURATION=300,c.BACKDROP_TRANSITION_DURATION=150,c.DEFAULTS={backdrop:!0,keyboard:!0,show:!0},c.prototype.toggle=function(a){return this.isShown?this.hide():this.show(a)},c.prototype.show=function(b){var d=this,e=a.Event("show.bs.modal",{relatedTarget:b});this.$element.trigger(e),this.isShown||e.isDefaultPrevented()||(this.isShown=!0,this.checkScrollbar(),this.setScrollbar(),this.$body.addClass("modal-open"),this.escape(),this.resize(),this.$element.on("click.dismiss.bs.modal",'[data-dismiss="modal"]',a.proxy(this.hide,this)),this.$dialog.on("mousedown.dismiss.bs.modal",function(){d.$element.one("mouseup.dismiss.bs.modal",function(b){a(b.target).is(d.$element)&&(d.ignoreBackdropClick=!0)})}),this.backdrop(function(){var e=a.support.transition&&d.$element.hasClass("fade");d.$element.parent().length||d.$element.appendTo(d.$body),d.$element.show().scrollTop(0),d.adjustDialog(),e&&d.$element[0].offsetWidth,d.$element.addClass("in"),d.enforceFocus();var f=a.Event("shown.bs.modal",{relatedTarget:b});e?d.$dialog.one("bsTransitionEnd",function(){d.$element.trigger("focus").trigger(f)}).emulateTransitionEnd(c.TRANSITION_DURATION):d.$element.trigger("focus").trigger(f)}))},c.prototype.hide=function(b){b&&b.preventDefault(),b=a.Event("hide.bs.modal"),this.$element.trigger(b),this.isShown&&!b.isDefaultPrevented()&&(this.isShown=!1,this.escape(),this.resize(),a(document).off("focusin.bs.modal"),this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"),this.$dialog.off("mousedown.dismiss.bs.modal"),a.support.transition&&this.$element.hasClass("fade")?this.$element.one("bsTransitionEnd",a.proxy(this.hideModal,this)).emulateTransitionEnd(c.TRANSITION_DURATION):this.hideModal())},c.prototype.enforceFocus=function(){a(document).off("focusin.bs.modal").on("focusin.bs.modal",a.proxy(function(a){document===a.target||this.$element[0]===a.target||this.$element.has(a.target).length||this.$element.trigger("focus")},this))},c.prototype.escape=function(){this.isShown&&this.options.keyboard?this.$element.on("keydown.dismiss.bs.modal",a.proxy(function(a){27==a.which&&this.hide()},this)):this.isShown||this.$element.off("keydown.dismiss.bs.modal")},c.prototype.resize=function(){this.isShown?a(window).on("resize.bs.modal",a.proxy(this.handleUpdate,this)):a(window).off("resize.bs.modal")},c.prototype.hideModal=function(){var a=this;this.$element.hide(),this.backdrop(function(){a.$body.removeClass("modal-open"),a.resetAdjustments(),a.resetScrollbar(),a.$element.trigger("hidden.bs.modal")})},c.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null},c.prototype.backdrop=function(b){var d=this,e=this.$element.hasClass("fade")?"fade":"";if(this.isShown&&this.options.backdrop){var f=a.support.transition&&e;if(this.$backdrop=a(document.createElement("div")).addClass("modal-backdrop "+e).appendTo(this.$body),this.$element.on("click.dismiss.bs.modal",a.proxy(function(a){return this.ignoreBackdropClick?void(this.ignoreBackdropClick=!1):void(a.target===a.currentTarget&&("static"==this.options.backdrop?this.$element[0].focus():this.hide()))},this)),f&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in"),!b)return;f?this.$backdrop.one("bsTransitionEnd",b).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION):b()}else if(!this.isShown&&this.$backdrop){this.$backdrop.removeClass("in");var g=function(){d.removeBackdrop(),b&&b()};a.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one("bsTransitionEnd",g).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION):g()}else b&&b()},c.prototype.handleUpdate=function(){this.adjustDialog()},c.prototype.adjustDialog=function(){var a=this.$element[0].scrollHeight>document.documentElement.clientHeight;this.$element.css({paddingLeft:!this.bodyIsOverflowing&&a?this.scrollbarWidth:"",paddingRight:this.bodyIsOverflowing&&!a?this.scrollbarWidth:""})},c.prototype.resetAdjustments=function(){this.$element.css({paddingLeft:"",paddingRight:""})},c.prototype.checkScrollbar=function(){var a=window.innerWidth;if(!a){var b=document.documentElement.getBoundingClientRect();a=b.right-Math.abs(b.left)}this.bodyIsOverflowing=document.body.clientWidth<a,this.scrollbarWidth=this.measureScrollbar()},c.prototype.setScrollbar=function(){var a=parseInt(this.$body.css("padding-right")||0,10);this.originalBodyPad=document.body.style.paddingRight||"",this.bodyIsOverflowing&&this.$body.css("padding-right",a+this.scrollbarWidth)},c.prototype.resetScrollbar=function(){this.$body.css("padding-right",this.originalBodyPad)},c.prototype.measureScrollbar=function(){var a=document.createElement("div");a.className="modal-scrollbar-measure",this.$body.append(a);var b=a.offsetWidth-a.clientWidth;return this.$body[0].removeChild(a),b};var d=a.fn.modal;a.fn.modal=b,a.fn.modal.Constructor=c,a.fn.modal.noConflict=function(){return a.fn.modal=d,this},a(document).on("click.bs.modal.data-api",'[data-toggle="modal"]',function(c){var d=a(this),e=d.attr("href"),f=a(d.attr("data-target")||e&&e.replace(/.*(?=#[^\s]+$)/,"")),g=f.data("bs.modal")?"toggle":a.extend({remote:!/#/.test(e)&&e},f.data(),d.data());d.is("a")&&c.preventDefault(),f.one("show.bs.modal",function(a){a.isDefaultPrevented()||f.one("hidden.bs.modal",function(){d.is(":visible")&&d.trigger("focus")})}),b.call(f,g,this)})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.tooltip"),f="object"==typeof b&&b;!e&&/destroy|hide/.test(b)||(e||d.data("bs.tooltip",e=new c(this,f)),"string"==typeof b&&e[b]())})}var c=function(a,b){this.type=null,this.options=null,this.enabled=null,this.timeout=null,this.hoverState=null,this.$element=null,this.inState=null,this.init("tooltip",a,b)};c.VERSION="3.3.7",c.TRANSITION_DURATION=150,c.DEFAULTS={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1,viewport:{selector:"body",padding:0}},c.prototype.init=function(b,c,d){if(this.enabled=!0,this.type=b,this.$element=a(c),this.options=this.getOptions(d),this.$viewport=this.options.viewport&&a(a.isFunction(this.options.viewport)?this.options.viewport.call(this,this.$element):this.options.viewport.selector||this.options.viewport),this.inState={click:!1,hover:!1,focus:!1},this.$element[0]instanceof document.constructor&&!this.options.selector)throw new Error("`selector` option must be specified when initializing "+this.type+" on the window.document object!");for(var e=this.options.trigger.split(" "),f=e.length;f--;){var g=e[f];if("click"==g)this.$element.on("click."+this.type,this.options.selector,a.proxy(this.toggle,this));else if("manual"!=g){var h="hover"==g?"mouseenter":"focusin",i="hover"==g?"mouseleave":"focusout";this.$element.on(h+"."+this.type,this.options.selector,a.proxy(this.enter,this)),this.$element.on(i+"."+this.type,this.options.selector,a.proxy(this.leave,this))}}this.options.selector?this._options=a.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()},c.prototype.getDefaults=function(){return c.DEFAULTS},c.prototype.getOptions=function(b){return b=a.extend({},this.getDefaults(),this.$element.data(),b),b.delay&&"number"==typeof b.delay&&(b.delay={show:b.delay,hide:b.delay}),b},c.prototype.getDelegateOptions=function(){var b={},c=this.getDefaults();return this._options&&a.each(this._options,function(a,d){c[a]!=d&&(b[a]=d)}),b},c.prototype.enter=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget).data("bs."+this.type);return c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c)),b instanceof a.Event&&(c.inState["focusin"==b.type?"focus":"hover"]=!0),c.tip().hasClass("in")||"in"==c.hoverState?void(c.hoverState="in"):(clearTimeout(c.timeout),c.hoverState="in",c.options.delay&&c.options.delay.show?void(c.timeout=setTimeout(function(){"in"==c.hoverState&&c.show()},c.options.delay.show)):c.show())},c.prototype.isInStateTrue=function(){for(var a in this.inState)if(this.inState[a])return!0;return!1},c.prototype.leave=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget).data("bs."+this.type);if(c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c)),b instanceof a.Event&&(c.inState["focusout"==b.type?"focus":"hover"]=!1),!c.isInStateTrue())return clearTimeout(c.timeout),c.hoverState="out",c.options.delay&&c.options.delay.hide?void(c.timeout=setTimeout(function(){"out"==c.hoverState&&c.hide()},c.options.delay.hide)):c.hide()},c.prototype.show=function(){var b=a.Event("show.bs."+this.type);if(this.hasContent()&&this.enabled){this.$element.trigger(b);var d=a.contains(this.$element[0].ownerDocument.documentElement,this.$element[0]);if(b.isDefaultPrevented()||!d)return;var e=this,f=this.tip(),g=this.getUID(this.type);this.setContent(),f.attr("id",g),this.$element.attr("aria-describedby",g),this.options.animation&&f.addClass("fade");var h="function"==typeof this.options.placement?this.options.placement.call(this,f[0],this.$element[0]):this.options.placement,i=/\s?auto?\s?/i,j=i.test(h);j&&(h=h.replace(i,"")||"top"),f.detach().css({top:0,left:0,display:"block"}).addClass(h).data("bs."+this.type,this),this.options.container?f.appendTo(this.options.container):f.insertAfter(this.$element),this.$element.trigger("inserted.bs."+this.type);var k=this.getPosition(),l=f[0].offsetWidth,m=f[0].offsetHeight;if(j){var n=h,o=this.getPosition(this.$viewport);h="bottom"==h&&k.bottom+m>o.bottom?"top":"top"==h&&k.top-m<o.top?"bottom":"right"==h&&k.right+l>o.width?"left":"left"==h&&k.left-l<o.left?"right":h,f.removeClass(n).addClass(h)}var p=this.getCalculatedOffset(h,k,l,m);this.applyPlacement(p,h);var q=function(){var a=e.hoverState;e.$element.trigger("shown.bs."+e.type),e.hoverState=null,"out"==a&&e.leave(e)};a.support.transition&&this.$tip.hasClass("fade")?f.one("bsTransitionEnd",q).emulateTransitionEnd(c.TRANSITION_DURATION):q()}},c.prototype.applyPlacement=function(b,c){var d=this.tip(),e=d[0].offsetWidth,f=d[0].offsetHeight,g=parseInt(d.css("margin-top"),10),h=parseInt(d.css("margin-left"),10);isNaN(g)&&(g=0),isNaN(h)&&(h=0),b.top+=g,b.left+=h,a.offset.setOffset(d[0],a.extend({using:function(a){d.css({top:Math.round(a.top),left:Math.round(a.left)})}},b),0),d.addClass("in");var i=d[0].offsetWidth,j=d[0].offsetHeight;"top"==c&&j!=f&&(b.top=b.top+f-j);var k=this.getViewportAdjustedDelta(c,b,i,j);k.left?b.left+=k.left:b.top+=k.top;var l=/top|bottom/.test(c),m=l?2*k.left-e+i:2*k.top-f+j,n=l?"offsetWidth":"offsetHeight";d.offset(b),this.replaceArrow(m,d[0][n],l)},c.prototype.replaceArrow=function(a,b,c){this.arrow().css(c?"left":"top",50*(1-a/b)+"%").css(c?"top":"left","")},c.prototype.setContent=function(){var a=this.tip(),b=this.getTitle();a.find(".tooltip-inner")[this.options.html?"html":"text"](b),a.removeClass("fade in top bottom left right")},c.prototype.hide=function(b){function d(){"in"!=e.hoverState&&f.detach(),e.$element&&e.$element.removeAttr("aria-describedby").trigger("hidden.bs."+e.type),b&&b()}var e=this,f=a(this.$tip),g=a.Event("hide.bs."+this.type);if(this.$element.trigger(g),!g.isDefaultPrevented())return f.removeClass("in"),a.support.transition&&f.hasClass("fade")?f.one("bsTransitionEnd",d).emulateTransitionEnd(c.TRANSITION_DURATION):d(),this.hoverState=null,this},c.prototype.fixTitle=function(){var a=this.$element;(a.attr("title")||"string"!=typeof a.attr("data-original-title"))&&a.attr("data-original-title",a.attr("title")||"").attr("title","")},c.prototype.hasContent=function(){return this.getTitle()},c.prototype.getPosition=function(b){b=b||this.$element;var c=b[0],d="BODY"==c.tagName,e=c.getBoundingClientRect();null==e.width&&(e=a.extend({},e,{width:e.right-e.left,height:e.bottom-e.top}));var f=window.SVGElement&&c instanceof window.SVGElement,g=d?{top:0,left:0}:f?null:b.offset(),h={scroll:d?document.documentElement.scrollTop||document.body.scrollTop:b.scrollTop()},i=d?{width:a(window).width(),height:a(window).height()}:null;return a.extend({},e,h,i,g)},c.prototype.getCalculatedOffset=function(a,b,c,d){return"bottom"==a?{top:b.top+b.height,left:b.left+b.width/2-c/2}:"top"==a?{top:b.top-d,left:b.left+b.width/2-c/2}:"left"==a?{top:b.top+b.height/2-d/2,left:b.left-c}:{top:b.top+b.height/2-d/2,left:b.left+b.width}},c.prototype.getViewportAdjustedDelta=function(a,b,c,d){var e={top:0,left:0};if(!this.$viewport)return e;var f=this.options.viewport&&this.options.viewport.padding||0,g=this.getPosition(this.$viewport);if(/right|left/.test(a)){var h=b.top-f-g.scroll,i=b.top+f-g.scroll+d;h<g.top?e.top=g.top-h:i>g.top+g.height&&(e.top=g.top+g.height-i)}else{var j=b.left-f,k=b.left+f+c;j<g.left?e.left=g.left-j:k>g.right&&(e.left=g.left+g.width-k)}return e},c.prototype.getTitle=function(){var a,b=this.$element,c=this.options;return a=b.attr("data-original-title")||("function"==typeof c.title?c.title.call(b[0]):c.title)},c.prototype.getUID=function(a){do a+=~~(1e6*Math.random());while(document.getElementById(a));return a},c.prototype.tip=function(){if(!this.$tip&&(this.$tip=a(this.options.template),1!=this.$tip.length))throw new Error(this.type+" `template` option must consist of exactly 1 top-level element!");return this.$tip},c.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")},c.prototype.enable=function(){this.enabled=!0},c.prototype.disable=function(){this.enabled=!1},c.prototype.toggleEnabled=function(){this.enabled=!this.enabled},c.prototype.toggle=function(b){var c=this;b&&(c=a(b.currentTarget).data("bs."+this.type),c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c))),b?(c.inState.click=!c.inState.click,c.isInStateTrue()?c.enter(c):c.leave(c)):c.tip().hasClass("in")?c.leave(c):c.enter(c)},c.prototype.destroy=function(){var a=this;clearTimeout(this.timeout),this.hide(function(){a.$element.off("."+a.type).removeData("bs."+a.type),a.$tip&&a.$tip.detach(),a.$tip=null,a.$arrow=null,a.$viewport=null,a.$element=null})};var d=a.fn.tooltip;a.fn.tooltip=b,a.fn.tooltip.Constructor=c,a.fn.tooltip.noConflict=function(){return a.fn.tooltip=d,this}}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.popover"),f="object"==typeof b&&b;!e&&/destroy|hide/.test(b)||(e||d.data("bs.popover",e=new c(this,f)),"string"==typeof b&&e[b]())})}var c=function(a,b){this.init("popover",a,b)};if(!a.fn.tooltip)throw new Error("Popover requires tooltip.js");c.VERSION="3.3.7",c.DEFAULTS=a.extend({},a.fn.tooltip.Constructor.DEFAULTS,{placement:"right",trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'}),c.prototype=a.extend({},a.fn.tooltip.Constructor.prototype),c.prototype.constructor=c,c.prototype.getDefaults=function(){return c.DEFAULTS},c.prototype.setContent=function(){var a=this.tip(),b=this.getTitle(),c=this.getContent();a.find(".popover-title")[this.options.html?"html":"text"](b),a.find(".popover-content").children().detach().end()[this.options.html?"string"==typeof c?"html":"append":"text"](c),a.removeClass("fade top bottom left right in"),a.find(".popover-title").html()||a.find(".popover-title").hide()},c.prototype.hasContent=function(){return this.getTitle()||this.getContent()},c.prototype.getContent=function(){var a=this.$element,b=this.options;return a.attr("data-content")||("function"==typeof b.content?b.content.call(a[0]):b.content)},c.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".arrow")};var d=a.fn.popover;a.fn.popover=b,a.fn.popover.Constructor=c,a.fn.popover.noConflict=function(){return a.fn.popover=d,this}}(jQuery),+function(a){"use strict";function b(c,d){this.$body=a(document.body),this.$scrollElement=a(a(c).is(document.body)?window:c),this.options=a.extend({},b.DEFAULTS,d),this.selector=(this.options.target||"")+" .nav li > a",this.offsets=[],this.targets=[],this.activeTarget=null,this.scrollHeight=0,this.$scrollElement.on("scroll.bs.scrollspy",a.proxy(this.process,this)),this.refresh(),this.process()}function c(c){return this.each(function(){var d=a(this),e=d.data("bs.scrollspy"),f="object"==typeof c&&c;e||d.data("bs.scrollspy",e=new b(this,f)),"string"==typeof c&&e[c]()})}b.VERSION="3.3.7",b.DEFAULTS={offset:10},b.prototype.getScrollHeight=function(){return this.$scrollElement[0].scrollHeight||Math.max(this.$body[0].scrollHeight,document.documentElement.scrollHeight)},b.prototype.refresh=function(){var b=this,c="offset",d=0;this.offsets=[],this.targets=[],this.scrollHeight=this.getScrollHeight(),a.isWindow(this.$scrollElement[0])||(c="position",d=this.$scrollElement.scrollTop()),this.$body.find(this.selector).map(function(){var b=a(this),e=b.data("target")||b.attr("href"),f=/^#./.test(e)&&a(e);return f&&f.length&&f.is(":visible")&&[[f[c]().top+d,e]]||null}).sort(function(a,b){return a[0]-b[0]}).each(function(){b.offsets.push(this[0]),b.targets.push(this[1])})},b.prototype.process=function(){var a,b=this.$scrollElement.scrollTop()+this.options.offset,c=this.getScrollHeight(),d=this.options.offset+c-this.$scrollElement.height(),e=this.offsets,f=this.targets,g=this.activeTarget;if(this.scrollHeight!=c&&this.refresh(),b>=d)return g!=(a=f[f.length-1])&&this.activate(a);if(g&&b<e[0])return this.activeTarget=null,this.clear();for(a=e.length;a--;)g!=f[a]&&b>=e[a]&&(void 0===e[a+1]||b<e[a+1])&&this.activate(f[a])},b.prototype.activate=function(b){
this.activeTarget=b,this.clear();var c=this.selector+'[data-target="'+b+'"],'+this.selector+'[href="'+b+'"]',d=a(c).parents("li").addClass("active");d.parent(".dropdown-menu").length&&(d=d.closest("li.dropdown").addClass("active")),d.trigger("activate.bs.scrollspy")},b.prototype.clear=function(){a(this.selector).parentsUntil(this.options.target,".active").removeClass("active")};var d=a.fn.scrollspy;a.fn.scrollspy=c,a.fn.scrollspy.Constructor=b,a.fn.scrollspy.noConflict=function(){return a.fn.scrollspy=d,this},a(window).on("load.bs.scrollspy.data-api",function(){a('[data-spy="scroll"]').each(function(){var b=a(this);c.call(b,b.data())})})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.tab");e||d.data("bs.tab",e=new c(this)),"string"==typeof b&&e[b]()})}var c=function(b){this.element=a(b)};c.VERSION="3.3.7",c.TRANSITION_DURATION=150,c.prototype.show=function(){var b=this.element,c=b.closest("ul:not(.dropdown-menu)"),d=b.data("target");if(d||(d=b.attr("href"),d=d&&d.replace(/.*(?=#[^\s]*$)/,"")),!b.parent("li").hasClass("active")){var e=c.find(".active:last a"),f=a.Event("hide.bs.tab",{relatedTarget:b[0]}),g=a.Event("show.bs.tab",{relatedTarget:e[0]});if(e.trigger(f),b.trigger(g),!g.isDefaultPrevented()&&!f.isDefaultPrevented()){var h=a(d);this.activate(b.closest("li"),c),this.activate(h,h.parent(),function(){e.trigger({type:"hidden.bs.tab",relatedTarget:b[0]}),b.trigger({type:"shown.bs.tab",relatedTarget:e[0]})})}}},c.prototype.activate=function(b,d,e){function f(){g.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!1),b.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded",!0),h?(b[0].offsetWidth,b.addClass("in")):b.removeClass("fade"),b.parent(".dropdown-menu").length&&b.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!0),e&&e()}var g=d.find("> .active"),h=e&&a.support.transition&&(g.length&&g.hasClass("fade")||!!d.find("> .fade").length);g.length&&h?g.one("bsTransitionEnd",f).emulateTransitionEnd(c.TRANSITION_DURATION):f(),g.removeClass("in")};var d=a.fn.tab;a.fn.tab=b,a.fn.tab.Constructor=c,a.fn.tab.noConflict=function(){return a.fn.tab=d,this};var e=function(c){c.preventDefault(),b.call(a(this),"show")};a(document).on("click.bs.tab.data-api",'[data-toggle="tab"]',e).on("click.bs.tab.data-api",'[data-toggle="pill"]',e)}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.affix"),f="object"==typeof b&&b;e||d.data("bs.affix",e=new c(this,f)),"string"==typeof b&&e[b]()})}var c=function(b,d){this.options=a.extend({},c.DEFAULTS,d),this.$target=a(this.options.target).on("scroll.bs.affix.data-api",a.proxy(this.checkPosition,this)).on("click.bs.affix.data-api",a.proxy(this.checkPositionWithEventLoop,this)),this.$element=a(b),this.affixed=null,this.unpin=null,this.pinnedOffset=null,this.checkPosition()};c.VERSION="3.3.7",c.RESET="affix affix-top affix-bottom",c.DEFAULTS={offset:0,target:window},c.prototype.getState=function(a,b,c,d){var e=this.$target.scrollTop(),f=this.$element.offset(),g=this.$target.height();if(null!=c&&"top"==this.affixed)return e<c&&"top";if("bottom"==this.affixed)return null!=c?!(e+this.unpin<=f.top)&&"bottom":!(e+g<=a-d)&&"bottom";var h=null==this.affixed,i=h?e:f.top,j=h?g:b;return null!=c&&e<=c?"top":null!=d&&i+j>=a-d&&"bottom"},c.prototype.getPinnedOffset=function(){if(this.pinnedOffset)return this.pinnedOffset;this.$element.removeClass(c.RESET).addClass("affix");var a=this.$target.scrollTop(),b=this.$element.offset();return this.pinnedOffset=b.top-a},c.prototype.checkPositionWithEventLoop=function(){setTimeout(a.proxy(this.checkPosition,this),1)},c.prototype.checkPosition=function(){if(this.$element.is(":visible")){var b=this.$element.height(),d=this.options.offset,e=d.top,f=d.bottom,g=Math.max(a(document).height(),a(document.body).height());"object"!=typeof d&&(f=e=d),"function"==typeof e&&(e=d.top(this.$element)),"function"==typeof f&&(f=d.bottom(this.$element));var h=this.getState(g,b,e,f);if(this.affixed!=h){null!=this.unpin&&this.$element.css("top","");var i="affix"+(h?"-"+h:""),j=a.Event(i+".bs.affix");if(this.$element.trigger(j),j.isDefaultPrevented())return;this.affixed=h,this.unpin="bottom"==h?this.getPinnedOffset():null,this.$element.removeClass(c.RESET).addClass(i).trigger(i.replace("affix","affixed")+".bs.affix")}"bottom"==h&&this.$element.offset({top:g-b-f})}};var d=a.fn.affix;a.fn.affix=b,a.fn.affix.Constructor=c,a.fn.affix.noConflict=function(){return a.fn.affix=d,this},a(window).on("load",function(){a('[data-spy="affix"]').each(function(){var c=a(this),d=c.data();d.offset=d.offset||{},null!=d.offsetBottom&&(d.offset.bottom=d.offsetBottom),null!=d.offsetTop&&(d.offset.top=d.offsetTop),b.call(c,d)})})}(jQuery);
/*!
 * jQuery outside events - v1.1 - 3/16/2010
 * http://benalman.com/projects/jquery-outside-events-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */

// Script: jQuery outside events
//
// *Version: 1.1, Last updated: 3/16/2010*
// 
// Project Home - http://benalman.com/projects/jquery-outside-events-plugin/
// GitHub       - http://github.com/cowboy/jquery-outside-events/
// Source       - http://github.com/cowboy/jquery-outside-events/raw/master/jquery.ba-outside-events.js
// (Minified)   - http://github.com/cowboy/jquery-outside-events/raw/master/jquery.ba-outside-events.min.js (0.9kb)
// 
// About: License
// 
// Copyright (c) 2010 "Cowboy" Ben Alman,
// Dual licensed under the MIT and GPL licenses.
// http://benalman.com/about/license/
// 
// About: Examples
// 
// These working examples, complete with fully commented code, illustrate a few
// ways in which this plugin can be used.
// 
// clickoutside - http://benalman.com/code/projects/jquery-outside-events/examples/clickoutside/
// dblclickoutside - http://benalman.com/code/projects/jquery-outside-events/examples/dblclickoutside/
// mouseoveroutside - http://benalman.com/code/projects/jquery-outside-events/examples/mouseoveroutside/
// focusoutside - http://benalman.com/code/projects/jquery-outside-events/examples/focusoutside/
// 
// About: Support and Testing
// 
// Information about what version or versions of jQuery this plugin has been
// tested with, what browsers it has been tested in, and where the unit tests
// reside (so you can test it yourself).
// 
// jQuery Versions - 1.4.2
// Browsers Tested - Internet Explorer 6-8, Firefox 2-3.6, Safari 3-4, Chrome, Opera 9.6-10.1.
// Unit Tests      - http://benalman.com/code/projects/jquery-outside-events/unit/
// 
// About: Release History
// 
// 1.1 - (3/16/2010) Made "clickoutside" plugin more general, resulting in a
//       whole new plugin with more than a dozen default "outside" events and
//       a method that can be used to add new ones.
// 1.0 - (2/27/2010) Initial release
//
// Topic: Default "outside" events
// 
// Note that each "outside" event is powered by an "originating" event. Only
// when the originating event is triggered on an element outside the element
// to which that outside event is bound will the bound event be triggered.
// 
// Because each outside event is powered by a separate originating event,
// stopping propagation of that originating event will prevent its related
// outside event from triggering.
// 
//  OUTSIDE EVENT     - ORIGINATING EVENT
//  clickoutside      - click
//  dblclickoutside   - dblclick
//  focusoutside      - focusin
//  bluroutside       - focusout
//  mousemoveoutside  - mousemove
//  mousedownoutside  - mousedown
//  mouseupoutside    - mouseup
//  mouseoveroutside  - mouseover
//  mouseoutoutside   - mouseout
//  keydownoutside    - keydown
//  keypressoutside   - keypress
//  keyupoutside      - keyup
//  changeoutside     - change
//  selectoutside     - select
//  submitoutside     - submit

(function($,doc,outside){
    '$:nomunge'; // Used by YUI compressor.
    
    $.map(
      // All these events will get an "outside" event counterpart by default.
      'click dblclick mousemove mousedown mouseup mouseover mouseout change select submit keydown keypress keyup'.split(' '),
      function( event_name ) { jq_addOutsideEvent( event_name ); }
    );
    
    // The focus and blur events are really focusin and focusout when it comes
    // to delegation, so they are a special case.
    jq_addOutsideEvent( 'focusin',  'focus' + outside );
    jq_addOutsideEvent( 'focusout', 'blur' + outside );
    
    // Method: jQuery.addOutsideEvent
    // 
    // Register a new "outside" event to be with this method. Adding an outside
    // event that already exists will probably blow things up, so check the
    // <Default "outside" events> list before trying to add a new one.
    // 
    // Usage:
    // 
    // > jQuery.addOutsideEvent( event_name [, outside_event_name ] );
    // 
    // Arguments:
    // 
    //  event_name - (String) The name of the originating event that the new
    //    "outside" event will be powered by. This event can be a native or
    //    custom event, as long as it bubbles up the DOM tree.
    //  outside_event_name - (String) An optional name for the new "outside"
    //    event. If omitted, the outside event will be named whatever the
    //    value of `event_name` is plus the "outside" suffix.
    // 
    // Returns:
    // 
    //  Nothing.
    
    $.addOutsideEvent = jq_addOutsideEvent;
    
    function jq_addOutsideEvent( event_name, outside_event_name ) {
      
      // The "outside" event name.
      outside_event_name = outside_event_name || event_name + outside;
      
      // A jQuery object containing all elements to which the "outside" event is
      // bound.
      var elems = $(),
        
        // The "originating" event, namespaced for easy unbinding.
        event_namespaced = event_name + '.' + outside_event_name + '-special-event';
      
      // Event: outside events
      // 
      // An "outside" event is triggered on an element when its corresponding
      // "originating" event is triggered on an element outside the element in
      // question. See the <Default "outside" events> list for more information.
      // 
      // Usage:
      // 
      // > jQuery('selector').bind( 'clickoutside', function(event) {
      // >   var clicked_elem = $(event.target);
      // >   ...
      // > });
      // 
      // > jQuery('selector').bind( 'dblclickoutside', function(event) {
      // >   var double_clicked_elem = $(event.target);
      // >   ...
      // > });
      // 
      // > jQuery('selector').bind( 'mouseoveroutside', function(event) {
      // >   var moused_over_elem = $(event.target);
      // >   ...
      // > });
      // 
      // > jQuery('selector').bind( 'focusoutside', function(event) {
      // >   var focused_elem = $(event.target);
      // >   ...
      // > });
      // 
      // You get the idea, right?
      
      $.event.special[ outside_event_name ] = {
        
        // Called only when the first "outside" event callback is bound per
        // element.
        setup: function(){
          
          // Add this element to the list of elements to which this "outside"
          // event is bound.
          elems = elems.add( this );
          
          // If this is the first element getting the event bound, bind a handler
          // to document to catch all corresponding "originating" events.
          if ( elems.length === 1 ) {
            $(doc).bind( event_namespaced, handle_event );
          }
        },
        
        // Called only when the last "outside" event callback is unbound per
        // element.
        teardown: function(){
          
          // Remove this element from the list of elements to which this
          // "outside" event is bound.
          elems = elems.not( this );
          
          // If this is the last element removed, remove the "originating" event
          // handler on document that powers this "outside" event.
          if ( elems.length === 0 ) {
            $(doc).unbind( event_namespaced );
          }
        },
        
        // Called every time a "outside" event callback is bound to an element.
        add: function( handleObj ) {
          var old_handler = handleObj.handler;
          
          // This function is executed every time the event is triggered. This is
          // used to override the default event.target reference with one that is
          // more useful.
          handleObj.handler = function( event, elem ) {
            
            // Set the event object's .target property to the element that the
            // user interacted with, not the element that the "outside" event was
            // was triggered on.
            event.target = elem;
            
            // Execute the actual bound handler.
            old_handler.apply( this, arguments );
          };
        }
      };
      
      // When the "originating" event is triggered..
      function handle_event( event ) {
        
        // Iterate over all elements to which this "outside" event is bound.
        $(elems).each(function(){
          var elem = $(this);
          
          // If this element isn't the element on which the event was triggered,
          // and this element doesn't contain said element, then said element is
          // considered to be outside, and the "outside" event will be triggered!
          if ( this !== event.target && !elem.has(event.target).length ) {
            
            // Use triggerHandler instead of trigger so that the "outside" event
            // doesn't bubble. Pass in the "originating" event's .target so that
            // the "outside" event.target can be overridden with something more
            // meaningful.
            elem.triggerHandler( outside_event_name, [ event.target ] );
          }
        });
      };
      
    };
    
  })(jQuery,document,"outside");
/*
 * http://benalman.com/projects/jquery-outside-events-plugin/
 */
(function($,c,b){$.map("click dblclick mousemove mousedown mouseup mouseover mouseout change select submit keydown keypress keyup".split(" "),function(d){a(d)});a("focusin","focus"+b);a("focusout","blur"+b);$.addOutsideEvent=a;function a(g,e){e=e||g+b;var d=$(),h=g+"."+e+"-special-event";$.event.special[e]={setup:function(){d=d.add(this);if(d.length===1){$(c).bind(h,f)}},teardown:function(){d=d.not(this);if(d.length===0){$(c).unbind(h)}},add:function(i){var j=i.handler;i.handler=function(l,k){l.target=k;j.apply(this,arguments)}}};function f(i){$(d).each(function(){var j=$(this);if(this!==i.target&&!j.has(i.target).length){j.triggerHandler(e,[i.target])}})}}})(jQuery,document,"outside");
/**
 * Draw Circle Chart 
 * Create by     : mnwp
 * Created date  : 10.15.14
 * ======================================================
 * HTML DEMO     :
 * <div class="chrt" id="graph" data-percent="88"></div>
 * @Param - [ elem ] - string - '.chrt'
 */
function drawCircle (elem) {
    if (!$(elem).length) {return;}

    var el = $(elem); // get canvas

    el.each(function (n,e) {
        el = $(this);

        var options = {
            percent:   el.data('percent') || 25,
            size:      el.data('size')    || 158,
            lineWidth: el.data('line')    || 15,
            rotate:    el.data('rotate')  || 0,
            bgColor:   el.data('bgcolor')   || '#dbffeb',
            lineColor: el.data('linecolor') || '#abf587',
            useSpan :  el.data('useSpan')   || false
        }

        var canvas = document.createElement('canvas');
        var span = document.createElement('span');
        span.textContent = options.percent + '%';
            
        if (typeof(G_vmlCanvasManager) !== 'undefined') {
            G_vmlCanvasManager.initElement(canvas);
        }

        var ctx = canvas.getContext('2d');
        canvas.width = canvas.height = options.size;

        if (options.useSpan) {
            el.append(span);
        }        
        el.append(canvas);

        ctx.translate(options.size / 2, options.size / 2); // change center
        ctx.rotate((-1 / 2 + options.rotate / 180) * Math.PI); // rotate -90 deg

        //imd = ctx.getImageData(0, 0, 240, 240);
        var radius = (options.size - options.lineWidth) / 2;

        var drawCircle = function(color, lineWidth, percent) {
                percent = Math.min(Math.max(0, percent || 1), 1);
                ctx.beginPath();
                ctx.arc(0, 0, radius, 0, Math.PI * 2 * percent, false);
                ctx.strokeStyle = color;
                ctx.lineCap = 'butt'; // butt, round or square
                ctx.lineWidth = lineWidth
                ctx.stroke();
        };

        drawCircle(options.bgColor, options.lineWidth, 100 / 100);
        drawCircle(options.lineColor, options.lineWidth, options.percent / 100);
    });    
}
/**
 * RUN Drawing
 */
jQuery(document).ready(function($){
//    drawCircle ('.chrt');
});

/*!
 * pickadate.js v3.5.6, 2015/04/20
 * By Amsul, http://amsul.ca
 * Hosted on http://amsul.github.io/pickadate.js
 * Licensed under MIT
 */

(function ( factory ) {

    // AMD.
    if ( typeof define == 'function' && define.amd )
        define( 'picker', ['jquery'], factory )

    // Node.js/browserify.
    else if ( typeof exports == 'object' )
        module.exports = factory( require('jquery') )

    // Browser globals.
    else this.Picker = factory( jQuery )

}(function( $ ) {

var $window = $( window )
var $document = $( document )
var $html = $( document.documentElement )
var supportsTransitions = document.documentElement.style.transition != null


/**
 * The picker constructor that creates a blank picker.
 */
function PickerConstructor( ELEMENT, NAME, COMPONENT, OPTIONS ) {

    // If there’s no element, return the picker constructor.
    if ( !ELEMENT ) return PickerConstructor


    var
        IS_DEFAULT_THEME = false,


        // The state of the picker.
        STATE = {
            id: ELEMENT.id || 'P' + Math.abs( ~~(Math.random() * new Date()) )
        },


        // Merge the defaults and options passed.
        SETTINGS = COMPONENT ? $.extend( true, {}, COMPONENT.defaults, OPTIONS ) : OPTIONS || {},


        // Merge the default classes with the settings classes.
        CLASSES = $.extend( {}, PickerConstructor.klasses(), SETTINGS.klass ),


        // The element node wrapper into a jQuery object.
        $ELEMENT = $( ELEMENT ),


        // Pseudo picker constructor.
        PickerInstance = function() {
            return this.start()
        },


        // The picker prototype.
        P = PickerInstance.prototype = {

            constructor: PickerInstance,

            $node: $ELEMENT,


            /**
             * Initialize everything
             */
            start: function() {

                // If it’s already started, do nothing.
                if ( STATE && STATE.start ) return P


                // Update the picker states.
                STATE.methods = {}
                STATE.start = true
                STATE.open = false
                STATE.type = ELEMENT.type


                // Confirm focus state, convert into text input to remove UA stylings,
                // and set as readonly to prevent keyboard popup.
                ELEMENT.autofocus = ELEMENT == getActiveElement()
                ELEMENT.readOnly = !SETTINGS.editable
                ELEMENT.id = ELEMENT.id || STATE.id
                if ( ELEMENT.type != 'text' ) {
                    ELEMENT.type = 'text'
                }


                // Create a new picker component with the settings.
                P.component = new COMPONENT(P, SETTINGS)


                // Create the picker root and then prepare it.
                P.$root = $( '<div class="' + CLASSES.picker + '" id="' + ELEMENT.id + '_root" />' )
                prepareElementRoot()


                // Create the picker holder and then prepare it.
                P.$holder = $( createWrappedComponent() ).appendTo( P.$root )
                prepareElementHolder()


                // If there’s a format for the hidden input element, create the element.
                if ( SETTINGS.formatSubmit ) {
                    prepareElementHidden()
                }


                // Prepare the input element.
                prepareElement()


                // Insert the hidden input as specified in the settings.
                if ( SETTINGS.containerHidden ) $( SETTINGS.containerHidden ).append( P._hidden )
                else $ELEMENT.after( P._hidden )


                // Insert the root as specified in the settings.
                if ( SETTINGS.container ) $( SETTINGS.container ).append( P.$root )
                else $ELEMENT.after( P.$root )


                // Bind the default component and settings events.
                P.on({
                    start: P.component.onStart,
                    render: P.component.onRender,
                    stop: P.component.onStop,
                    open: P.component.onOpen,
                    close: P.component.onClose,
                    set: P.component.onSet
                }).on({
                    start: SETTINGS.onStart,
                    render: SETTINGS.onRender,
                    stop: SETTINGS.onStop,
                    open: SETTINGS.onOpen,
                    close: SETTINGS.onClose,
                    set: SETTINGS.onSet
                })


                // Once we’re all set, check the theme in use.
                IS_DEFAULT_THEME = isUsingDefaultTheme( P.$holder[0] )


                // If the element has autofocus, open the picker.
                if ( ELEMENT.autofocus ) {
                    P.open()
                }


                // Trigger queued the “start” and “render” events.
                return P.trigger( 'start' ).trigger( 'render' )
            }, //start


            /**
             * Render a new picker
             */
            render: function( entireComponent ) {

                // Insert a new component holder in the root or box.
                if ( entireComponent ) {
                    P.$holder = $( createWrappedComponent() )
                    prepareElementHolder()
                    P.$root.html( P.$holder )
                }
                else P.$root.find( '.' + CLASSES.box ).html( P.component.nodes( STATE.open ) )

                // Trigger the queued “render” events.
                return P.trigger( 'render' )
            }, //render


            /**
             * Destroy everything
             */
            stop: function() {

                // If it’s already stopped, do nothing.
                if ( !STATE.start ) return P

                // Then close the picker.
                P.close()

                // Remove the hidden field.
                if ( P._hidden ) {
                    P._hidden.parentNode.removeChild( P._hidden )
                }

                // Remove the root.
                P.$root.remove()

                // Remove the input class, remove the stored data, and unbind
                // the events (after a tick for IE - see `P.close`).
                $ELEMENT.removeClass( CLASSES.input ).removeData( NAME )
                setTimeout( function() {
                    $ELEMENT.off( '.' + STATE.id )
                }, 0)

                // Restore the element state
                ELEMENT.type = STATE.type
                ELEMENT.readOnly = false

                // Trigger the queued “stop” events.
                P.trigger( 'stop' )

                // Reset the picker states.
                STATE.methods = {}
                STATE.start = false

                return P
            }, //stop


            /**
             * Open up the picker
             */
            open: function( dontGiveFocus ) {

                // If it’s already open, do nothing.
                if ( STATE.open ) return P

                // Add the “active” class.
                $ELEMENT.addClass( CLASSES.active )
                aria( ELEMENT, 'expanded', true )

                // * A Firefox bug, when `html` has `overflow:hidden`, results in
                //   killing transitions :(. So add the “opened” state on the next tick.
                //   Bug: https://bugzilla.mozilla.org/show_bug.cgi?id=625289
                setTimeout( function() {

                    // Add the “opened” class to the picker root.
                    P.$root.addClass( CLASSES.opened )
                    aria( P.$root[0], 'hidden', false )

                }, 0 )

                // If we have to give focus, bind the element and doc events.
                if ( dontGiveFocus !== false ) {

                    // Set it as open.
                    STATE.open = true

                    // Prevent the page from scrolling.
                    if ( IS_DEFAULT_THEME ) {
                        $html.
                            css( 'overflow', 'hidden' ).
                            css( 'padding-right', '+=' + getScrollbarWidth() )
                    }

                    // Pass focus to the root element’s jQuery object.
                    focusPickerOnceOpened()

                    // Bind the document events.
                    $document.on( 'click.' + STATE.id + ' focusin.' + STATE.id, function( event ) {

                        var target = event.target

                        // If the target of the event is not the element, close the picker picker.
                        // * Don’t worry about clicks or focusins on the root because those don’t bubble up.
                        //   Also, for Firefox, a click on an `option` element bubbles up directly
                        //   to the doc. So make sure the target wasn't the doc.
                        // * In Firefox stopPropagation() doesn’t prevent right-click events from bubbling,
                        //   which causes the picker to unexpectedly close when right-clicking it. So make
                        //   sure the event wasn’t a right-click.
                        if ( target != ELEMENT && target != document && event.which != 3 ) {

                            // If the target was the holder that covers the screen,
                            // keep the element focused to maintain tabindex.
                            P.close( target === P.$holder[0] )
                        }

                    }).on( 'keydown.' + STATE.id, function( event ) {

                        var
                            // Get the keycode.
                            keycode = event.keyCode,

                            // Translate that to a selection change.
                            keycodeToMove = P.component.key[ keycode ],

                            // Grab the target.
                            target = event.target


                        // On escape, close the picker and give focus.
                        if ( keycode == 27 ) {
                            P.close( true )
                        }


                        // Check if there is a key movement or “enter” keypress on the element.
                        else if ( target == P.$holder[0] && ( keycodeToMove || keycode == 13 ) ) {

                            // Prevent the default action to stop page movement.
                            event.preventDefault()

                            // Trigger the key movement action.
                            if ( keycodeToMove ) {
                                PickerConstructor._.trigger( P.component.key.go, P, [ PickerConstructor._.trigger( keycodeToMove ) ] )
                            }

                            // On “enter”, if the highlighted item isn’t disabled, set the value and close.
                            else if ( !P.$root.find( '.' + CLASSES.highlighted ).hasClass( CLASSES.disabled ) ) {
                                P.set( 'select', P.component.item.highlight )
                                if ( SETTINGS.closeOnSelect ) {
                                    P.close( true )
                                }
                            }
                        }


                        // If the target is within the root and “enter” is pressed,
                        // prevent the default action and trigger a click on the target instead.
                        else if ( $.contains( P.$root[0], target ) && keycode == 13 ) {
                            event.preventDefault()
                            target.click()
                        }
                    })
                }

                // Trigger the queued “open” events.
                return P.trigger( 'open' )
            }, //open


            /**
             * Close the picker
             */
            close: function( giveFocus ) {

                // If we need to give focus, do it before changing states.
                if ( giveFocus ) {
                    if ( SETTINGS.editable ) {
                        ELEMENT.focus()
                    }
                    else {
                        // ....ah yes! It would’ve been incomplete without a crazy workaround for IE :|
                        // The focus is triggered *after* the close has completed - causing it
                        // to open again. So unbind and rebind the event at the next tick.
                        P.$holder.off( 'focus.toOpen' ).focus()
                        setTimeout( function() {
                            P.$holder.on( 'focus.toOpen', handleFocusToOpenEvent )
                        }, 0 )
                    }
                }

                // Remove the “active” class.
                $ELEMENT.removeClass( CLASSES.active )
                aria( ELEMENT, 'expanded', false )

                // * A Firefox bug, when `html` has `overflow:hidden`, results in
                //   killing transitions :(. So remove the “opened” state on the next tick.
                //   Bug: https://bugzilla.mozilla.org/show_bug.cgi?id=625289
                setTimeout( function() {

                    // Remove the “opened” and “focused” class from the picker root.
                    P.$root.removeClass( CLASSES.opened + ' ' + CLASSES.focused )
                    aria( P.$root[0], 'hidden', true )

                }, 0 )

                // If it’s already closed, do nothing more.
                if ( !STATE.open ) return P

                // Set it as closed.
                STATE.open = false

                // Allow the page to scroll.
                if ( IS_DEFAULT_THEME ) {
                    $html.
                        css( 'overflow', '' ).
                        css( 'padding-right', '-=' + getScrollbarWidth() )
                }

                // Unbind the document events.
                $document.off( '.' + STATE.id )

                // Trigger the queued “close” events.
                return P.trigger( 'close' )
            }, //close


            /**
             * Clear the values
             */
            clear: function( options ) {
                return P.set( 'clear', null, options )
            }, //clear


            /**
             * Set something
             */
            set: function( thing, value, options ) {

                var thingItem, thingValue,
                    thingIsObject = $.isPlainObject( thing ),
                    thingObject = thingIsObject ? thing : {}

                // Make sure we have usable options.
                options = thingIsObject && $.isPlainObject( value ) ? value : options || {}

                if ( thing ) {

                    // If the thing isn’t an object, make it one.
                    if ( !thingIsObject ) {
                        thingObject[ thing ] = value
                    }

                    // Go through the things of items to set.
                    for ( thingItem in thingObject ) {

                        // Grab the value of the thing.
                        thingValue = thingObject[ thingItem ]

                        // First, if the item exists and there’s a value, set it.
                        if ( thingItem in P.component.item ) {
                            if ( thingValue === undefined ) thingValue = null
                            P.component.set( thingItem, thingValue, options )
                        }

                        // Then, check to update the element value and broadcast a change.
                        if ( thingItem == 'select' || thingItem == 'clear' ) {
                            $ELEMENT.
                                val( thingItem == 'clear' ? '' : P.get( thingItem, SETTINGS.format ) ).
                                trigger( 'change' )
                        }
                    }

                    // Render a new picker.
                    P.render()
                }

                // When the method isn’t muted, trigger queued “set” events and pass the `thingObject`.
                return options.muted ? P : P.trigger( 'set', thingObject )
            }, //set


            /**
             * Get something
             */
            get: function( thing, format ) {

                // Make sure there’s something to get.
                thing = thing || 'value'

                // If a picker state exists, return that.
                if ( STATE[ thing ] != null ) {
                    return STATE[ thing ]
                }

                // Return the submission value, if that.
                if ( thing == 'valueSubmit' ) {
                    if ( P._hidden ) {
                        return P._hidden.value
                    }
                    thing = 'value'
                }

                // Return the value, if that.
                if ( thing == 'value' ) {
                    return ELEMENT.value
                }

                // Check if a component item exists, return that.
                if ( thing in P.component.item ) {
                    if ( typeof format == 'string' ) {
                        var thingValue = P.component.get( thing )
                        return thingValue ?
                            PickerConstructor._.trigger(
                                P.component.formats.toString,
                                P.component,
                                [ format, thingValue ]
                            ) : ''
                    }
                    return P.component.get( thing )
                }
            }, //get



            /**
             * Bind events on the things.
             */
            on: function( thing, method, internal ) {

                var thingName, thingMethod,
                    thingIsObject = $.isPlainObject( thing ),
                    thingObject = thingIsObject ? thing : {}

                if ( thing ) {

                    // If the thing isn’t an object, make it one.
                    if ( !thingIsObject ) {
                        thingObject[ thing ] = method
                    }

                    // Go through the things to bind to.
                    for ( thingName in thingObject ) {

                        // Grab the method of the thing.
                        thingMethod = thingObject[ thingName ]

                        // If it was an internal binding, prefix it.
                        if ( internal ) {
                            thingName = '_' + thingName
                        }

                        // Make sure the thing methods collection exists.
                        STATE.methods[ thingName ] = STATE.methods[ thingName ] || []

                        // Add the method to the relative method collection.
                        STATE.methods[ thingName ].push( thingMethod )
                    }
                }

                return P
            }, //on



            /**
             * Unbind events on the things.
             */
            off: function() {
                var i, thingName,
                    names = arguments;
                for ( i = 0, namesCount = names.length; i < namesCount; i += 1 ) {
                    thingName = names[i]
                    if ( thingName in STATE.methods ) {
                        delete STATE.methods[thingName]
                    }
                }
                return P
            },


            /**
             * Fire off method events.
             */
            trigger: function( name, data ) {
                var _trigger = function( name ) {
                    var methodList = STATE.methods[ name ]
                    if ( methodList ) {
                        methodList.map( function( method ) {
                            PickerConstructor._.trigger( method, P, [ data ] )
                        })
                    }
                }
                _trigger( '_' + name )
                _trigger( name )
                return P
            } //trigger
        } //PickerInstance.prototype


    /**
     * Wrap the picker holder components together.
     */
    function createWrappedComponent() {

        // Create a picker wrapper holder
        return PickerConstructor._.node( 'div',

            // Create a picker wrapper node
            PickerConstructor._.node( 'div',

                // Create a picker frame
                PickerConstructor._.node( 'div',

                    // Create a picker box node
                    PickerConstructor._.node( 'div',

                        // Create the components nodes.
                        P.component.nodes( STATE.open ),

                        // The picker box class
                        CLASSES.box
                    ),

                    // Picker wrap class
                    CLASSES.wrap
                ),

                // Picker frame class
                CLASSES.frame
            ),

            // Picker holder class
            CLASSES.holder,

            'tabindex="-1"'
        ) //endreturn
    } //createWrappedComponent



    /**
     * Prepare the input element with all bindings.
     */
    function prepareElement() {

        $ELEMENT.

            // Store the picker data by component name.
            data(NAME, P).

            // Add the “input” class name.
            addClass(CLASSES.input).

            // If there’s a `data-value`, update the value of the element.
            val( $ELEMENT.data('value') ?
                P.get('select', SETTINGS.format) :
                ELEMENT.value
            )


        // Only bind keydown events if the element isn’t editable.
        if ( !SETTINGS.editable ) {

            $ELEMENT.

                // On focus/click, open the picker.
                on( 'focus.' + STATE.id + ' click.' + STATE.id, function(event) {
                    event.preventDefault()
                    P.open()
                }).

                // Handle keyboard event based on the picker being opened or not.
                on( 'keydown.' + STATE.id, handleKeydownEvent )
        }


        // Update the aria attributes.
        aria(ELEMENT, {
            haspopup: true,
            expanded: false,
            readonly: false,
            owns: ELEMENT.id + '_root'
        })
    }


    /**
     * Prepare the root picker element with all bindings.
     */
    function prepareElementRoot() {
        aria( P.$root[0], 'hidden', true )
    }


     /**
      * Prepare the holder picker element with all bindings.
      */
    function prepareElementHolder() {

        P.$holder.

            on({

                // For iOS8.
                keydown: handleKeydownEvent,

                'focus.toOpen': handleFocusToOpenEvent,

                blur: function() {
                    // Remove the “target” class.
                    $ELEMENT.removeClass( CLASSES.target )
                },

                // When something within the holder is focused, stop from bubbling
                // to the doc and remove the “focused” state from the root.
                focusin: function( event ) {
                    P.$root.removeClass( CLASSES.focused )
                    event.stopPropagation()
                },

                // When something within the holder is clicked, stop it
                // from bubbling to the doc.
                'mousedown click': function( event ) {

                    var target = event.target

                    // Make sure the target isn’t the root holder so it can bubble up.
                    if ( target != P.$holder[0] ) {

                        event.stopPropagation()

                        // * For mousedown events, cancel the default action in order to
                        //   prevent cases where focus is shifted onto external elements
                        //   when using things like jQuery mobile or MagnificPopup (ref: #249 & #120).
                        //   Also, for Firefox, don’t prevent action on the `option` element.
                        if ( event.type == 'mousedown' && !$( target ).is( 'input, select, textarea, button, option' )) {

                            event.preventDefault()

                            // Re-focus onto the holder so that users can click away
                            // from elements focused within the picker.
                            P.$holder[0].focus()
                        }
                    }
                }

            }).

            // If there’s a click on an actionable element, carry out the actions.
            on( 'click', '[data-pick], [data-nav], [data-clear], [data-close]', function() {

                var $target = $( this ),
                    targetData = $target.data(),
                    targetDisabled = $target.hasClass( CLASSES.navDisabled ) || $target.hasClass( CLASSES.disabled ),

                    // * For IE, non-focusable elements can be active elements as well
                    //   (http://stackoverflow.com/a/2684561).
                    activeElement = getActiveElement()
                    activeElement = activeElement && ( activeElement.type || activeElement.href )

                // If it’s disabled or nothing inside is actively focused, re-focus the element.
                if ( targetDisabled || activeElement && !$.contains( P.$root[0], activeElement ) ) {
                    P.$holder[0].focus()
                }

                // If something is superficially changed, update the `highlight` based on the `nav`.
                if ( !targetDisabled && targetData.nav ) {
                    P.set( 'highlight', P.component.item.highlight, { nav: targetData.nav } )
                }

                // If something is picked, set `select` then close with focus.
                else if ( !targetDisabled && 'pick' in targetData ) {
                    P.set( 'select', targetData.pick )
                    if ( SETTINGS.closeOnSelect ) {
                        P.close( true )
                    }
                }

                // If a “clear” button is pressed, empty the values and close with focus.
                else if ( targetData.clear ) {
                    P.clear()
                    if ( SETTINGS.closeOnClear ) {
                        P.close( true )
                    }
                }

                else if ( targetData.close ) {
                    P.close( true )
                }

            }) //P.$holder

    }


     /**
      * Prepare the hidden input element along with all bindings.
      */
    function prepareElementHidden() {

        var name

        if ( SETTINGS.hiddenName === true ) {
            name = ELEMENT.name
            ELEMENT.name = ''
        }
        else {
            name = [
                typeof SETTINGS.hiddenPrefix == 'string' ? SETTINGS.hiddenPrefix : '',
                typeof SETTINGS.hiddenSuffix == 'string' ? SETTINGS.hiddenSuffix : '_submit'
            ]
            name = name[0] + ELEMENT.name + name[1]
        }

        P._hidden = $(
            '<input ' +
            'type=hidden ' +

            // Create the name using the original input’s with a prefix and suffix.
            'name="' + name + '"' +

            // If the element has a value, set the hidden value as well.
            (
                $ELEMENT.data('value') || ELEMENT.value ?
                    ' value="' + P.get('select', SETTINGS.formatSubmit) + '"' :
                    ''
            ) +
            '>'
        )[0]

        $ELEMENT.

            // If the value changes, update the hidden input with the correct format.
            on('change.' + STATE.id, function() {
                P._hidden.value = ELEMENT.value ?
                    P.get('select', SETTINGS.formatSubmit) :
                    ''
            })
    }


    // Wait for transitions to end before focusing the holder. Otherwise, while
    // using the `container` option, the view jumps to the container.
    function focusPickerOnceOpened() {

        if (IS_DEFAULT_THEME && supportsTransitions) {
            P.$holder.find('.' + CLASSES.frame).one('transitionend', function() {
                P.$holder[0].focus()
            })
        }
        else {
            P.$holder[0].focus()
        }
    }


    function handleFocusToOpenEvent(event) {

        // Stop the event from propagating to the doc.
        event.stopPropagation()

        // Add the “target” class.
        $ELEMENT.addClass( CLASSES.target )

        // Add the “focused” class to the root.
        P.$root.addClass( CLASSES.focused )

        // And then finally open the picker.
        P.open()
    }


    // For iOS8.
    function handleKeydownEvent( event ) {

        var keycode = event.keyCode,

            // Check if one of the delete keys was pressed.
            isKeycodeDelete = /^(8|46)$/.test(keycode)

        // For some reason IE clears the input value on “escape”.
        if ( keycode == 27 ) {
            P.close( true )
            return false
        }

        // Check if `space` or `delete` was pressed or the picker is closed with a key movement.
        if ( keycode == 32 || isKeycodeDelete || !STATE.open && P.component.key[keycode] ) {

            // Prevent it from moving the page and bubbling to doc.
            event.preventDefault()
            event.stopPropagation()

            // If `delete` was pressed, clear the values and close the picker.
            // Otherwise open the picker.
            if ( isKeycodeDelete ) { P.clear().close() }
            else { P.open() }
        }
    }


    // Return a new picker instance.
    return new PickerInstance()
} //PickerConstructor



/**
 * The default classes and prefix to use for the HTML classes.
 */
PickerConstructor.klasses = function( prefix ) {
    prefix = prefix || 'picker'
    return {

        picker: prefix,
        opened: prefix + '--opened',
        focused: prefix + '--focused',

        input: prefix + '__input',
        active: prefix + '__input--active',
        target: prefix + '__input--target',

        holder: prefix + '__holder',

        frame: prefix + '__frame',
        wrap: prefix + '__wrap',

        box: prefix + '__box'
    }
} //PickerConstructor.klasses



/**
 * Check if the default theme is being used.
 */
function isUsingDefaultTheme( element ) {

    var theme,
        prop = 'position'

    // For IE.
    if ( element.currentStyle ) {
        theme = element.currentStyle[prop]
    }

    // For normal browsers.
    else if ( window.getComputedStyle ) {
        theme = getComputedStyle( element )[prop]
    }

    return theme == 'fixed'
}



/**
 * Get the width of the browser’s scrollbar.
 * Taken from: https://github.com/VodkaBears/Remodal/blob/master/src/jquery.remodal.js
 */
function getScrollbarWidth() {

    if ( $html.height() <= $window.height() ) {
        return 0
    }

    var $outer = $( '<div style="visibility:hidden;width:100px" />' ).
        appendTo( 'body' )

    // Get the width without scrollbars.
    var widthWithoutScroll = $outer[0].offsetWidth

    // Force adding scrollbars.
    $outer.css( 'overflow', 'scroll' )

    // Add the inner div.
    var $inner = $( '<div style="width:100%" />' ).appendTo( $outer )

    // Get the width with scrollbars.
    var widthWithScroll = $inner[0].offsetWidth

    // Remove the divs.
    $outer.remove()

    // Return the difference between the widths.
    return widthWithoutScroll - widthWithScroll
}



/**
 * PickerConstructor helper methods.
 */
PickerConstructor._ = {

    /**
     * Create a group of nodes. Expects:
     * `
        {
            min:    {Integer},
            max:    {Integer},
            i:      {Integer},
            node:   {String},
            item:   {Function}
        }
     * `
     */
    group: function( groupObject ) {

        var
            // Scope for the looped object
            loopObjectScope,

            // Create the nodes list
            nodesList = '',

            // The counter starts from the `min`
            counter = PickerConstructor._.trigger( groupObject.min, groupObject )


        // Loop from the `min` to `max`, incrementing by `i`
        for ( ; counter <= PickerConstructor._.trigger( groupObject.max, groupObject, [ counter ] ); counter += groupObject.i ) {

            // Trigger the `item` function within scope of the object
            loopObjectScope = PickerConstructor._.trigger( groupObject.item, groupObject, [ counter ] )

            // Splice the subgroup and create nodes out of the sub nodes
            nodesList += PickerConstructor._.node(
                groupObject.node,
                loopObjectScope[ 0 ],   // the node
                loopObjectScope[ 1 ],   // the classes
                loopObjectScope[ 2 ]    // the attributes
            )
        }

        // Return the list of nodes
        return nodesList
    }, //group


    /**
     * Create a dom node string
     */
    node: function( wrapper, item, klass, attribute ) {

        // If the item is false-y, just return an empty string
        if ( !item ) return ''

        // If the item is an array, do a join
        item = $.isArray( item ) ? item.join( '' ) : item

        // Check for the class
        klass = klass ? ' class="' + klass + '"' : ''

        // Check for any attributes
        attribute = attribute ? ' ' + attribute : ''

        // Return the wrapped item
        return '<' + wrapper + klass + attribute + '>' + item + '</' + wrapper + '>'
    }, //node


    /**
     * Lead numbers below 10 with a zero.
     */
    lead: function( number ) {
        return ( number < 10 ? '0': '' ) + number
    },


    /**
     * Trigger a function otherwise return the value.
     */
    trigger: function( callback, scope, args ) {
        return typeof callback == 'function' ? callback.apply( scope, args || [] ) : callback
    },


    /**
     * If the second character is a digit, length is 2 otherwise 1.
     */
    digits: function( string ) {
        return ( /\d/ ).test( string[ 1 ] ) ? 2 : 1
    },


    /**
     * Tell if something is a date object.
     */
    isDate: function( value ) {
        return {}.toString.call( value ).indexOf( 'Date' ) > -1 && this.isInteger( value.getDate() )
    },


    /**
     * Tell if something is an integer.
     */
    isInteger: function( value ) {
        return {}.toString.call( value ).indexOf( 'Number' ) > -1 && value % 1 === 0
    },


    /**
     * Create ARIA attribute strings.
     */
    ariaAttr: ariaAttr
} //PickerConstructor._



/**
 * Extend the picker with a component and defaults.
 */
PickerConstructor.extend = function( name, Component ) {

    // Extend jQuery.
    $.fn[ name ] = function( options, action ) {

        // Grab the component data.
        var componentData = this.data( name )

        // If the picker is requested, return the data object.
        if ( options == 'picker' ) {
            return componentData
        }

        // If the component data exists and `options` is a string, carry out the action.
        if ( componentData && typeof options == 'string' ) {
            return PickerConstructor._.trigger( componentData[ options ], componentData, [ action ] )
        }

        // Otherwise go through each matched element and if the component
        // doesn’t exist, create a new picker using `this` element
        // and merging the defaults and options with a deep copy.
        return this.each( function() {
            var $this = $( this )
            if ( !$this.data( name ) ) {
                new PickerConstructor( this, name, Component, options )
            }
        })
    }

    // Set the defaults.
    $.fn[ name ].defaults = Component.defaults
} //PickerConstructor.extend



function aria(element, attribute, value) {
    if ( $.isPlainObject(attribute) ) {
        for ( var key in attribute ) {
            ariaSet(element, key, attribute[key])
        }
    }
    else {
        ariaSet(element, attribute, value)
    }
}
function ariaSet(element, attribute, value) {
    element.setAttribute(
        (attribute == 'role' ? '' : 'aria-') + attribute,
        value
    )
}
function ariaAttr(attribute, data) {
    if ( !$.isPlainObject(attribute) ) {
        attribute = { attribute: data }
    }
    data = ''
    for ( var key in attribute ) {
        var attr = (key == 'role' ? '' : 'aria-') + key,
            attrVal = attribute[key]
        data += attrVal == null ? '' : attr + '="' + attribute[key] + '"'
    }
    return data
}

// IE8 bug throws an error for activeElements within iframes.
function getActiveElement() {
    try {
        return document.activeElement
    } catch ( err ) { }
}



// Expose the picker constructor.
return PickerConstructor


}));




/*!
 * Date picker for pickadate.js v3.5.6
 * http://amsul.github.io/pickadate.js/date.htm
 */

(function ( factory ) {

    // AMD.
    if ( typeof define == 'function' && define.amd )
        define( ['./picker', 'jquery'], factory )

    // Node.js/browserify.
    else if ( typeof exports == 'object' )
        module.exports = factory( require('./picker.js'), require('jquery') )

    // Browser globals.
    else factory( Picker, jQuery )

}(function( Picker, $ ) {


/**
 * Globals and constants
 */
var DAYS_IN_WEEK = 7,
    WEEKS_IN_CALENDAR = 6,
    _ = Picker._



/**
 * The date picker constructor
 */
function DatePicker( picker, settings ) {

    var calendar = this,
        element = picker.$node[ 0 ],
        elementValue = element.value,
        elementDataValue = picker.$node.data( 'value' ),
        valueString = elementDataValue || elementValue,
        formatString = elementDataValue ? settings.formatSubmit : settings.format,
        isRTL = function() {

            return element.currentStyle ?

                // For IE.
                element.currentStyle.direction == 'rtl' :

                // For normal browsers.
                getComputedStyle( picker.$root[0] ).direction == 'rtl'
        }

    calendar.settings = settings
    calendar.$node = picker.$node

    // The queue of methods that will be used to build item objects.
    calendar.queue = {
        min: 'measure create',
        max: 'measure create',
        now: 'now create',
        select: 'parse create validate',
        highlight: 'parse navigate create validate',
        view: 'parse create validate viewset',
        disable: 'deactivate',
        enable: 'activate'
    }

    // The component's item object.
    calendar.item = {}

    calendar.item.clear = null
    calendar.item.disable = ( settings.disable || [] ).slice( 0 )
    calendar.item.enable = -(function( collectionDisabled ) {
        return collectionDisabled[ 0 ] === true ? collectionDisabled.shift() : -1
    })( calendar.item.disable )

    calendar.
        set( 'min', settings.min ).
        set( 'max', settings.max ).
        set( 'now' )

    // When there’s a value, set the `select`, which in turn
    // also sets the `highlight` and `view`.
    if ( valueString ) {
        calendar.set( 'select', valueString, {
            format: formatString,
            defaultValue: true
        })
    }

    // If there’s no value, default to highlighting “today”.
    else {
        calendar.
            set( 'select', null ).
            set( 'highlight', calendar.item.now )
    }


    // The keycode to movement mapping.
    calendar.key = {
        40: 7, // Down
        38: -7, // Up
        39: function() { return isRTL() ? -1 : 1 }, // Right
        37: function() { return isRTL() ? 1 : -1 }, // Left
        go: function( timeChange ) {
            var highlightedObject = calendar.item.highlight,
                targetDate = new Date( highlightedObject.year, highlightedObject.month, highlightedObject.date + timeChange )
            calendar.set(
                'highlight',
                targetDate,
                { interval: timeChange }
            )
            this.render()
        }
    }


    // Bind some picker events.
    picker.
        on( 'render', function() {
            picker.$root.find( '.' + settings.klass.selectMonth ).on( 'change', function() {
                var value = this.value
                if ( value ) {
                    picker.set( 'highlight', [ picker.get( 'view' ).year, value, picker.get( 'highlight' ).date ] )
                    picker.$root.find( '.' + settings.klass.selectMonth ).trigger( 'focus' )
                }
            })
            picker.$root.find( '.' + settings.klass.selectYear ).on( 'change', function() {
                var value = this.value
                if ( value ) {
                    picker.set( 'highlight', [ value, picker.get( 'view' ).month, picker.get( 'highlight' ).date ] )
                    picker.$root.find( '.' + settings.klass.selectYear ).trigger( 'focus' )
                }
            })
        }, 1 ).
        on( 'open', function() {
            var includeToday = ''
            if ( calendar.disabled( calendar.get('now') ) ) {
                includeToday = ':not(.' + settings.klass.buttonToday + ')'
            }
            picker.$root.find( 'button' + includeToday + ', select' ).attr( 'disabled', false )
        }, 1 ).
        on( 'close', function() {
            picker.$root.find( 'button, select' ).attr( 'disabled', true )
        }, 1 )

} //DatePicker


/**
 * Set a datepicker item object.
 */
DatePicker.prototype.set = function( type, value, options ) {

    var calendar = this,
        calendarItem = calendar.item

    // If the value is `null` just set it immediately.
    if ( value === null ) {
        if ( type == 'clear' ) type = 'select'
        calendarItem[ type ] = value
        return calendar
    }

    // Otherwise go through the queue of methods, and invoke the functions.
    // Update this as the time unit, and set the final value as this item.
    // * In the case of `enable`, keep the queue but set `disable` instead.
    //   And in the case of `flip`, keep the queue but set `enable` instead.
    calendarItem[ ( type == 'enable' ? 'disable' : type == 'flip' ? 'enable' : type ) ] = calendar.queue[ type ].split( ' ' ).map( function( method ) {
        value = calendar[ method ]( type, value, options )
        return value
    }).pop()

    // Check if we need to cascade through more updates.
    if ( type == 'select' ) {
        calendar.set( 'highlight', calendarItem.select, options )
    }
    else if ( type == 'highlight' ) {
        calendar.set( 'view', calendarItem.highlight, options )
    }
    else if ( type.match( /^(flip|min|max|disable|enable)$/ ) ) {
        if ( calendarItem.select && calendar.disabled( calendarItem.select ) ) {
            calendar.set( 'select', calendarItem.select, options )
        }
        if ( calendarItem.highlight && calendar.disabled( calendarItem.highlight ) ) {
            calendar.set( 'highlight', calendarItem.highlight, options )
        }
    }

    return calendar
} //DatePicker.prototype.set


/**
 * Get a datepicker item object.
 */
DatePicker.prototype.get = function( type ) {
    return this.item[ type ]
} //DatePicker.prototype.get


/**
 * Create a picker date object.
 */
DatePicker.prototype.create = function( type, value, options ) {

    var isInfiniteValue,
        calendar = this

    // If there’s no value, use the type as the value.
    value = value === undefined ? type : value


    // If it’s infinity, update the value.
    if ( value == -Infinity || value == Infinity ) {
        isInfiniteValue = value
    }

    // If it’s an object, use the native date object.
    else if ( $.isPlainObject( value ) && _.isInteger( value.pick ) ) {
        value = value.obj
    }

    // If it’s an array, convert it into a date and make sure
    // that it’s a valid date – otherwise default to today.
    else if ( $.isArray( value ) ) {
        value = new Date( value[ 0 ], value[ 1 ], value[ 2 ] )
        value = _.isDate( value ) ? value : calendar.create().obj
    }

    // If it’s a number or date object, make a normalized date.
    else if ( _.isInteger( value ) || _.isDate( value ) ) {
        value = calendar.normalize( new Date( value ), options )
    }

    // If it’s a literal true or any other case, set it to now.
    else /*if ( value === true )*/ {
        value = calendar.now( type, value, options )
    }

    // Return the compiled object.
    return {
        year: isInfiniteValue || value.getFullYear(),
        month: isInfiniteValue || value.getMonth(),
        date: isInfiniteValue || value.getDate(),
        day: isInfiniteValue || value.getDay(),
        obj: isInfiniteValue || value,
        pick: isInfiniteValue || value.getTime()
    }
} //DatePicker.prototype.create


/**
 * Create a range limit object using an array, date object,
 * literal “true”, or integer relative to another time.
 */
DatePicker.prototype.createRange = function( from, to ) {

    var calendar = this,
        createDate = function( date ) {
            if ( date === true || $.isArray( date ) || _.isDate( date ) ) {
                return calendar.create( date )
            }
            return date
        }

    // Create objects if possible.
    if ( !_.isInteger( from ) ) {
        from = createDate( from )
    }
    if ( !_.isInteger( to ) ) {
        to = createDate( to )
    }

    // Create relative dates.
    if ( _.isInteger( from ) && $.isPlainObject( to ) ) {
        from = [ to.year, to.month, to.date + from ];
    }
    else if ( _.isInteger( to ) && $.isPlainObject( from ) ) {
        to = [ from.year, from.month, from.date + to ];
    }

    return {
        from: createDate( from ),
        to: createDate( to )
    }
} //DatePicker.prototype.createRange


/**
 * Check if a date unit falls within a date range object.
 */
DatePicker.prototype.withinRange = function( range, dateUnit ) {
    range = this.createRange(range.from, range.to)
    return dateUnit.pick >= range.from.pick && dateUnit.pick <= range.to.pick
}


/**
 * Check if two date range objects overlap.
 */
DatePicker.prototype.overlapRanges = function( one, two ) {

    var calendar = this

    // Convert the ranges into comparable dates.
    one = calendar.createRange( one.from, one.to )
    two = calendar.createRange( two.from, two.to )

    return calendar.withinRange( one, two.from ) || calendar.withinRange( one, two.to ) ||
        calendar.withinRange( two, one.from ) || calendar.withinRange( two, one.to )
}


/**
 * Get the date today.
 */
DatePicker.prototype.now = function( type, value, options ) {
    value = new Date()
    if ( options && options.rel ) {
        value.setDate( value.getDate() + options.rel )
    }
    return this.normalize( value, options )
}


/**
 * Navigate to next/prev month.
 */
DatePicker.prototype.navigate = function( type, value, options ) {

    var targetDateObject,
        targetYear,
        targetMonth,
        targetDate,
        isTargetArray = $.isArray( value ),
        isTargetObject = $.isPlainObject( value ),
        viewsetObject = this.item.view/*,
        safety = 100*/


    if ( isTargetArray || isTargetObject ) {

        if ( isTargetObject ) {
            targetYear = value.year
            targetMonth = value.month
            targetDate = value.date
        }
        else {
            targetYear = +value[0]
            targetMonth = +value[1]
            targetDate = +value[2]
        }

        // If we’re navigating months but the view is in a different
        // month, navigate to the view’s year and month.
        if ( options && options.nav && viewsetObject && viewsetObject.month !== targetMonth ) {
            targetYear = viewsetObject.year
            targetMonth = viewsetObject.month
        }

        // Figure out the expected target year and month.
        targetDateObject = new Date( targetYear, targetMonth + ( options && options.nav ? options.nav : 0 ), 1 )
        targetYear = targetDateObject.getFullYear()
        targetMonth = targetDateObject.getMonth()

        // If the month we’re going to doesn’t have enough days,
        // keep decreasing the date until we reach the month’s last date.
        while ( /*safety &&*/ new Date( targetYear, targetMonth, targetDate ).getMonth() !== targetMonth ) {
            targetDate -= 1
            /*safety -= 1
            if ( !safety ) {
                throw 'Fell into an infinite loop while navigating to ' + new Date( targetYear, targetMonth, targetDate ) + '.'
            }*/
        }

        value = [ targetYear, targetMonth, targetDate ]
    }

    return value
} //DatePicker.prototype.navigate


/**
 * Normalize a date by setting the hours to midnight.
 */
DatePicker.prototype.normalize = function( value/*, options*/ ) {
    value.setHours( 0, 0, 0, 0 )
    return value
}


/**
 * Measure the range of dates.
 */
DatePicker.prototype.measure = function( type, value/*, options*/ ) {

    var calendar = this

    // If it’s anything false-y, remove the limits.
    if ( !value ) {
        value = type == 'min' ? -Infinity : Infinity
    }

    // If it’s a string, parse it.
    else if ( typeof value == 'string' ) {
        value = calendar.parse( type, value )
    }

    // If it's an integer, get a date relative to today.
    else if ( _.isInteger( value ) ) {
        value = calendar.now( type, value, { rel: value } )
    }

    return value
} ///DatePicker.prototype.measure


/**
 * Create a viewset object based on navigation.
 */
DatePicker.prototype.viewset = function( type, dateObject/*, options*/ ) {
    return this.create([ dateObject.year, dateObject.month, 1 ])
}


/**
 * Validate a date as enabled and shift if needed.
 */
DatePicker.prototype.validate = function( type, dateObject, options ) {

    var calendar = this,

        // Keep a reference to the original date.
        originalDateObject = dateObject,

        // Make sure we have an interval.
        interval = options && options.interval ? options.interval : 1,

        // Check if the calendar enabled dates are inverted.
        isFlippedBase = calendar.item.enable === -1,

        // Check if we have any enabled dates after/before now.
        hasEnabledBeforeTarget, hasEnabledAfterTarget,

        // The min & max limits.
        minLimitObject = calendar.item.min,
        maxLimitObject = calendar.item.max,

        // Check if we’ve reached the limit during shifting.
        reachedMin, reachedMax,

        // Check if the calendar is inverted and at least one weekday is enabled.
        hasEnabledWeekdays = isFlippedBase && calendar.item.disable.filter( function( value ) {

            // If there’s a date, check where it is relative to the target.
            if ( $.isArray( value ) ) {
                var dateTime = calendar.create( value ).pick
                if ( dateTime < dateObject.pick ) hasEnabledBeforeTarget = true
                else if ( dateTime > dateObject.pick ) hasEnabledAfterTarget = true
            }

            // Return only integers for enabled weekdays.
            return _.isInteger( value )
        }).length/*,

        safety = 100*/



    // Cases to validate for:
    // [1] Not inverted and date disabled.
    // [2] Inverted and some dates enabled.
    // [3] Not inverted and out of range.
    //
    // Cases to **not** validate for:
    // • Navigating months.
    // • Not inverted and date enabled.
    // • Inverted and all dates disabled.
    // • ..and anything else.
    if ( !options || (!options.nav && !options.defaultValue) ) if (
        /* 1 */ ( !isFlippedBase && calendar.disabled( dateObject ) ) ||
        /* 2 */ ( isFlippedBase && calendar.disabled( dateObject ) && ( hasEnabledWeekdays || hasEnabledBeforeTarget || hasEnabledAfterTarget ) ) ||
        /* 3 */ ( !isFlippedBase && (dateObject.pick <= minLimitObject.pick || dateObject.pick >= maxLimitObject.pick) )
    ) {


        // When inverted, flip the direction if there aren’t any enabled weekdays
        // and there are no enabled dates in the direction of the interval.
        if ( isFlippedBase && !hasEnabledWeekdays && ( ( !hasEnabledAfterTarget && interval > 0 ) || ( !hasEnabledBeforeTarget && interval < 0 ) ) ) {
            interval *= -1
        }


        // Keep looping until we reach an enabled date.
        while ( /*safety &&*/ calendar.disabled( dateObject ) ) {

            /*safety -= 1
            if ( !safety ) {
                throw 'Fell into an infinite loop while validating ' + dateObject.obj + '.'
            }*/


            // If we’ve looped into the next/prev month with a large interval, return to the original date and flatten the interval.
            if ( Math.abs( interval ) > 1 && ( dateObject.month < originalDateObject.month || dateObject.month > originalDateObject.month ) ) {
                dateObject = originalDateObject
                interval = interval > 0 ? 1 : -1
            }


            // If we’ve reached the min/max limit, reverse the direction, flatten the interval and set it to the limit.
            if ( dateObject.pick <= minLimitObject.pick ) {
                reachedMin = true
                interval = 1
                dateObject = calendar.create([
                    minLimitObject.year,
                    minLimitObject.month,
                    minLimitObject.date + (dateObject.pick === minLimitObject.pick ? 0 : -1)
                ])
            }
            else if ( dateObject.pick >= maxLimitObject.pick ) {
                reachedMax = true
                interval = -1
                dateObject = calendar.create([
                    maxLimitObject.year,
                    maxLimitObject.month,
                    maxLimitObject.date + (dateObject.pick === maxLimitObject.pick ? 0 : 1)
                ])
            }


            // If we’ve reached both limits, just break out of the loop.
            if ( reachedMin && reachedMax ) {
                break
            }


            // Finally, create the shifted date using the interval and keep looping.
            dateObject = calendar.create([ dateObject.year, dateObject.month, dateObject.date + interval ])
        }

    } //endif


    // Return the date object settled on.
    return dateObject
} //DatePicker.prototype.validate


/**
 * Check if a date is disabled.
 */
DatePicker.prototype.disabled = function( dateToVerify ) {

    var
        calendar = this,

        // Filter through the disabled dates to check if this is one.
        isDisabledMatch = calendar.item.disable.filter( function( dateToDisable ) {

            // If the date is a number, match the weekday with 0index and `firstDay` check.
            if ( _.isInteger( dateToDisable ) ) {
                return dateToVerify.day === ( calendar.settings.firstDay ? dateToDisable : dateToDisable - 1 ) % 7
            }

            // If it’s an array or a native JS date, create and match the exact date.
            if ( $.isArray( dateToDisable ) || _.isDate( dateToDisable ) ) {
                return dateToVerify.pick === calendar.create( dateToDisable ).pick
            }

            // If it’s an object, match a date within the “from” and “to” range.
            if ( $.isPlainObject( dateToDisable ) ) {
                return calendar.withinRange( dateToDisable, dateToVerify )
            }
        })

    // If this date matches a disabled date, confirm it’s not inverted.
    isDisabledMatch = isDisabledMatch.length && !isDisabledMatch.filter(function( dateToDisable ) {
        return $.isArray( dateToDisable ) && dateToDisable[3] == 'inverted' ||
            $.isPlainObject( dateToDisable ) && dateToDisable.inverted
    }).length

    // Check the calendar “enabled” flag and respectively flip the
    // disabled state. Then also check if it’s beyond the min/max limits.
    return calendar.item.enable === -1 ? !isDisabledMatch : isDisabledMatch ||
        dateToVerify.pick < calendar.item.min.pick ||
        dateToVerify.pick > calendar.item.max.pick

} //DatePicker.prototype.disabled


/**
 * Parse a string into a usable type.
 */
DatePicker.prototype.parse = function( type, value, options ) {

    var calendar = this,
        parsingObject = {}

    // If it’s already parsed, we’re good.
    if ( !value || typeof value != 'string' ) {
        return value
    }

    // We need a `.format` to parse the value with.
    if ( !( options && options.format ) ) {
        options = options || {}
        options.format = calendar.settings.format
    }

    // Convert the format into an array and then map through it.
    calendar.formats.toArray( options.format ).map( function( label ) {

        var
            // Grab the formatting label.
            formattingLabel = calendar.formats[ label ],

            // The format length is from the formatting label function or the
            // label length without the escaping exclamation (!) mark.
            formatLength = formattingLabel ? _.trigger( formattingLabel, calendar, [ value, parsingObject ] ) : label.replace( /^!/, '' ).length

        // If there's a format label, split the value up to the format length.
        // Then add it to the parsing object with appropriate label.
        if ( formattingLabel ) {
            parsingObject[ label ] = value.substr( 0, formatLength )
        }

        // Update the value as the substring from format length to end.
        value = value.substr( formatLength )
    })

    // Compensate for month 0index.
    return [
        parsingObject.yyyy || parsingObject.yy,
        +( parsingObject.mm || parsingObject.m ) - 1,
        parsingObject.dd || parsingObject.d
    ]
} //DatePicker.prototype.parse


/**
 * Various formats to display the object in.
 */
DatePicker.prototype.formats = (function() {

    // Return the length of the first word in a collection.
    function getWordLengthFromCollection( string, collection, dateObject ) {

        // Grab the first word from the string.
        // Regex pattern from http://stackoverflow.com/q/150033
        var word = string.match( /[^\x00-\x7F]+|\w+/ )[ 0 ]

        // If there's no month index, add it to the date object
        if ( !dateObject.mm && !dateObject.m ) {
            dateObject.m = collection.indexOf( word ) + 1
        }

        // Return the length of the word.
        return word.length
    }

    // Get the length of the first word in a string.
    function getFirstWordLength( string ) {
        return string.match( /\w+/ )[ 0 ].length
    }

    return {

        d: function( string, dateObject ) {

            // If there's string, then get the digits length.
            // Otherwise return the selected date.
            return string ? _.digits( string ) : dateObject.date
        },
        dd: function( string, dateObject ) {

            // If there's a string, then the length is always 2.
            // Otherwise return the selected date with a leading zero.
            return string ? 2 : _.lead( dateObject.date )
        },
        ddd: function( string, dateObject ) {

            // If there's a string, then get the length of the first word.
            // Otherwise return the short selected weekday.
            return string ? getFirstWordLength( string ) : this.settings.weekdaysShort[ dateObject.day ]
        },
        dddd: function( string, dateObject ) {

            // If there's a string, then get the length of the first word.
            // Otherwise return the full selected weekday.
            return string ? getFirstWordLength( string ) : this.settings.weekdaysFull[ dateObject.day ]
        },
        m: function( string, dateObject ) {

            // If there's a string, then get the length of the digits
            // Otherwise return the selected month with 0index compensation.
            return string ? _.digits( string ) : dateObject.month + 1
        },
        mm: function( string, dateObject ) {

            // If there's a string, then the length is always 2.
            // Otherwise return the selected month with 0index and leading zero.
            return string ? 2 : _.lead( dateObject.month + 1 )
        },
        mmm: function( string, dateObject ) {

            var collection = this.settings.monthsShort

            // If there's a string, get length of the relevant month from the short
            // months collection. Otherwise return the selected month from that collection.
            return string ? getWordLengthFromCollection( string, collection, dateObject ) : collection[ dateObject.month ]
        },
        mmmm: function( string, dateObject ) {

            var collection = this.settings.monthsFull

            // If there's a string, get length of the relevant month from the full
            // months collection. Otherwise return the selected month from that collection.
            return string ? getWordLengthFromCollection( string, collection, dateObject ) : collection[ dateObject.month ]
        },
        yy: function( string, dateObject ) {

            // If there's a string, then the length is always 2.
            // Otherwise return the selected year by slicing out the first 2 digits.
            return string ? 2 : ( '' + dateObject.year ).slice( 2 )
        },
        yyyy: function( string, dateObject ) {

            // If there's a string, then the length is always 4.
            // Otherwise return the selected year.
            return string ? 4 : dateObject.year
        },

        // Create an array by splitting the formatting string passed.
        toArray: function( formatString ) { return formatString.split( /(d{1,4}|m{1,4}|y{4}|yy|!.)/g ) },

        // Format an object into a string using the formatting options.
        toString: function ( formatString, itemObject ) {
            var calendar = this
            return calendar.formats.toArray( formatString ).map( function( label ) {
                return _.trigger( calendar.formats[ label ], calendar, [ 0, itemObject ] ) || label.replace( /^!/, '' )
            }).join( '' )
        }
    }
})() //DatePicker.prototype.formats




/**
 * Check if two date units are the exact.
 */
DatePicker.prototype.isDateExact = function( one, two ) {

    var calendar = this

    // When we’re working with weekdays, do a direct comparison.
    if (
        ( _.isInteger( one ) && _.isInteger( two ) ) ||
        ( typeof one == 'boolean' && typeof two == 'boolean' )
     ) {
        return one === two
    }

    // When we’re working with date representations, compare the “pick” value.
    if (
        ( _.isDate( one ) || $.isArray( one ) ) &&
        ( _.isDate( two ) || $.isArray( two ) )
    ) {
        return calendar.create( one ).pick === calendar.create( two ).pick
    }

    // When we’re working with range objects, compare the “from” and “to”.
    if ( $.isPlainObject( one ) && $.isPlainObject( two ) ) {
        return calendar.isDateExact( one.from, two.from ) && calendar.isDateExact( one.to, two.to )
    }

    return false
}


/**
 * Check if two date units overlap.
 */
DatePicker.prototype.isDateOverlap = function( one, two ) {

    var calendar = this,
        firstDay = calendar.settings.firstDay ? 1 : 0

    // When we’re working with a weekday index, compare the days.
    if ( _.isInteger( one ) && ( _.isDate( two ) || $.isArray( two ) ) ) {
        one = one % 7 + firstDay
        return one === calendar.create( two ).day + 1
    }
    if ( _.isInteger( two ) && ( _.isDate( one ) || $.isArray( one ) ) ) {
        two = two % 7 + firstDay
        return two === calendar.create( one ).day + 1
    }

    // When we’re working with range objects, check if the ranges overlap.
    if ( $.isPlainObject( one ) && $.isPlainObject( two ) ) {
        return calendar.overlapRanges( one, two )
    }

    return false
}


/**
 * Flip the “enabled” state.
 */
DatePicker.prototype.flipEnable = function(val) {
    var itemObject = this.item
    itemObject.enable = val || (itemObject.enable == -1 ? 1 : -1)
}


/**
 * Mark a collection of dates as “disabled”.
 */
DatePicker.prototype.deactivate = function( type, datesToDisable ) {

    var calendar = this,
        disabledItems = calendar.item.disable.slice(0)


    // If we’re flipping, that’s all we need to do.
    if ( datesToDisable == 'flip' ) {
        calendar.flipEnable()
    }

    else if ( datesToDisable === false ) {
        calendar.flipEnable(1)
        disabledItems = []
    }

    else if ( datesToDisable === true ) {
        calendar.flipEnable(-1)
        disabledItems = []
    }

    // Otherwise go through the dates to disable.
    else {

        datesToDisable.map(function( unitToDisable ) {

            var matchFound

            // When we have disabled items, check for matches.
            // If something is matched, immediately break out.
            for ( var index = 0; index < disabledItems.length; index += 1 ) {
                if ( calendar.isDateExact( unitToDisable, disabledItems[index] ) ) {
                    matchFound = true
                    break
                }
            }

            // If nothing was found, add the validated unit to the collection.
            if ( !matchFound ) {
                if (
                    _.isInteger( unitToDisable ) ||
                    _.isDate( unitToDisable ) ||
                    $.isArray( unitToDisable ) ||
                    ( $.isPlainObject( unitToDisable ) && unitToDisable.from && unitToDisable.to )
                ) {
                    disabledItems.push( unitToDisable )
                }
            }
        })
    }

    // Return the updated collection.
    return disabledItems
} //DatePicker.prototype.deactivate


/**
 * Mark a collection of dates as “enabled”.
 */
DatePicker.prototype.activate = function( type, datesToEnable ) {

    var calendar = this,
        disabledItems = calendar.item.disable,
        disabledItemsCount = disabledItems.length

    // If we’re flipping, that’s all we need to do.
    if ( datesToEnable == 'flip' ) {
        calendar.flipEnable()
    }

    else if ( datesToEnable === true ) {
        calendar.flipEnable(1)
        disabledItems = []
    }

    else if ( datesToEnable === false ) {
        calendar.flipEnable(-1)
        disabledItems = []
    }

    // Otherwise go through the disabled dates.
    else {

        datesToEnable.map(function( unitToEnable ) {

            var matchFound,
                disabledUnit,
                index,
                isExactRange

            // Go through the disabled items and try to find a match.
            for ( index = 0; index < disabledItemsCount; index += 1 ) {

                disabledUnit = disabledItems[index]

                // When an exact match is found, remove it from the collection.
                if ( calendar.isDateExact( disabledUnit, unitToEnable ) ) {
                    matchFound = disabledItems[index] = null
                    isExactRange = true
                    break
                }

                // When an overlapped match is found, add the “inverted” state to it.
                else if ( calendar.isDateOverlap( disabledUnit, unitToEnable ) ) {
                    if ( $.isPlainObject( unitToEnable ) ) {
                        unitToEnable.inverted = true
                        matchFound = unitToEnable
                    }
                    else if ( $.isArray( unitToEnable ) ) {
                        matchFound = unitToEnable
                        if ( !matchFound[3] ) matchFound.push( 'inverted' )
                    }
                    else if ( _.isDate( unitToEnable ) ) {
                        matchFound = [ unitToEnable.getFullYear(), unitToEnable.getMonth(), unitToEnable.getDate(), 'inverted' ]
                    }
                    break
                }
            }

            // If a match was found, remove a previous duplicate entry.
            if ( matchFound ) for ( index = 0; index < disabledItemsCount; index += 1 ) {
                if ( calendar.isDateExact( disabledItems[index], unitToEnable ) ) {
                    disabledItems[index] = null
                    break
                }
            }

            // In the event that we’re dealing with an exact range of dates,
            // make sure there are no “inverted” dates because of it.
            if ( isExactRange ) for ( index = 0; index < disabledItemsCount; index += 1 ) {
                if ( calendar.isDateOverlap( disabledItems[index], unitToEnable ) ) {
                    disabledItems[index] = null
                    break
                }
            }

            // If something is still matched, add it into the collection.
            if ( matchFound ) {
                disabledItems.push( matchFound )
            }
        })
    }

    // Return the updated collection.
    return disabledItems.filter(function( val ) { return val != null })
} //DatePicker.prototype.activate


/**
 * Create a string for the nodes in the picker.
 */
DatePicker.prototype.nodes = function( isOpen ) {

    var
        calendar = this,
        settings = calendar.settings,
        calendarItem = calendar.item,
        nowObject = calendarItem.now,
        selectedObject = calendarItem.select,
        highlightedObject = calendarItem.highlight,
        viewsetObject = calendarItem.view,
        disabledCollection = calendarItem.disable,
        minLimitObject = calendarItem.min,
        maxLimitObject = calendarItem.max,


        // Create the calendar table head using a copy of weekday labels collection.
        // * We do a copy so we don't mutate the original array.
        tableHead = (function( collection, fullCollection ) {

            // If the first day should be Monday, move Sunday to the end.
            if ( settings.firstDay ) {
                collection.push( collection.shift() )
                fullCollection.push( fullCollection.shift() )
            }

            // Create and return the table head group.
            return _.node(
                'thead',
                _.node(
                    'tr',
                    _.group({
                        min: 0,
                        max: DAYS_IN_WEEK - 1,
                        i: 1,
                        node: 'th',
                        item: function( counter ) {
                            return [
                                collection[ counter ],
                                settings.klass.weekdays,
                                'scope=col title="' + fullCollection[ counter ] + '"'
                            ]
                        }
                    })
                )
            ) //endreturn
        })( ( settings.showWeekdaysFull ? settings.weekdaysFull : settings.weekdaysShort ).slice( 0 ), settings.weekdaysFull.slice( 0 ) ), //tableHead


        // Create the nav for next/prev month.
        createMonthNav = function( next ) {

            // Otherwise, return the created month tag.
            return _.node(
                'div',
                ' ',
                settings.klass[ 'nav' + ( next ? 'Next' : 'Prev' ) ] + (

                    // If the focused month is outside the range, disabled the button.
                    ( next && viewsetObject.year >= maxLimitObject.year && viewsetObject.month >= maxLimitObject.month ) ||
                    ( !next && viewsetObject.year <= minLimitObject.year && viewsetObject.month <= minLimitObject.month ) ?
                    ' ' + settings.klass.navDisabled : ''
                ),
                'data-nav=' + ( next || -1 ) + ' ' +
                _.ariaAttr({
                    role: 'button',
                    controls: calendar.$node[0].id + '_table'
                }) + ' ' +
                'title="' + (next ? settings.labelMonthNext : settings.labelMonthPrev ) + '"'
            ) //endreturn
        }, //createMonthNav


        // Create the month label.
        createMonthLabel = function() {

            var monthsCollection = settings.showMonthsShort ? settings.monthsShort : settings.monthsFull

            // If there are months to select, add a dropdown menu.
            if ( settings.selectMonths ) {

                return _.node( 'select',
                    _.group({
                        min: 0,
                        max: 11,
                        i: 1,
                        node: 'option',
                        item: function( loopedMonth ) {

                            return [

                                // The looped month and no classes.
                                monthsCollection[ loopedMonth ], 0,

                                // Set the value and selected index.
                                'value=' + loopedMonth +
                                ( viewsetObject.month == loopedMonth ? ' selected' : '' ) +
                                (
                                    (
                                        ( viewsetObject.year == minLimitObject.year && loopedMonth < minLimitObject.month ) ||
                                        ( viewsetObject.year == maxLimitObject.year && loopedMonth > maxLimitObject.month )
                                    ) ?
                                    ' disabled' : ''
                                )
                            ]
                        }
                    }),
                    settings.klass.selectMonth,
                    ( isOpen ? '' : 'disabled' ) + ' ' +
                    _.ariaAttr({ controls: calendar.$node[0].id + '_table' }) + ' ' +
                    'title="' + settings.labelMonthSelect + '"'
                )
            }

            // If there's a need for a month selector
            return _.node( 'div', monthsCollection[ viewsetObject.month ], settings.klass.month )
        }, //createMonthLabel


        // Create the year label.
        createYearLabel = function() {

            var focusedYear = viewsetObject.year,

            // If years selector is set to a literal "true", set it to 5. Otherwise
            // divide in half to get half before and half after focused year.
            numberYears = settings.selectYears === true ? 5 : ~~( settings.selectYears / 2 )

            // If there are years to select, add a dropdown menu.
            if ( numberYears ) {

                var
                    minYear = minLimitObject.year,
                    maxYear = maxLimitObject.year,
                    lowestYear = focusedYear - numberYears,
                    highestYear = focusedYear + numberYears

                // If the min year is greater than the lowest year, increase the highest year
                // by the difference and set the lowest year to the min year.
                if ( minYear > lowestYear ) {
                    highestYear += minYear - lowestYear
                    lowestYear = minYear
                }

                // If the max year is less than the highest year, decrease the lowest year
                // by the lower of the two: available and needed years. Then set the
                // highest year to the max year.
                if ( maxYear < highestYear ) {

                    var availableYears = lowestYear - minYear,
                        neededYears = highestYear - maxYear

                    lowestYear -= availableYears > neededYears ? neededYears : availableYears
                    highestYear = maxYear
                }

                return _.node( 'select',
                    _.group({
                        min: lowestYear,
                        max: highestYear,
                        i: 1,
                        node: 'option',
                        item: function( loopedYear ) {
                            return [

                                // The looped year and no classes.
                                loopedYear, 0,

                                // Set the value and selected index.
                                'value=' + loopedYear + ( focusedYear == loopedYear ? ' selected' : '' )
                            ]
                        }
                    }),
                    settings.klass.selectYear,
                    ( isOpen ? '' : 'disabled' ) + ' ' + _.ariaAttr({ controls: calendar.$node[0].id + '_table' }) + ' ' +
                    'title="' + settings.labelYearSelect + '"'
                )
            }

            // Otherwise just return the year focused
            return _.node( 'div', focusedYear, settings.klass.year )
        } //createYearLabel


    // Create and return the entire calendar.
    return _.node(
        'div',
        ( settings.selectYears ? createYearLabel() + createMonthLabel() : createMonthLabel() + createYearLabel() ) +
        createMonthNav() + createMonthNav( 1 ),
        settings.klass.header
    ) + _.node(
        'table',
        tableHead +
        _.node(
            'tbody',
            _.group({
                min: 0,
                max: WEEKS_IN_CALENDAR - 1,
                i: 1,
                node: 'tr',
                item: function( rowCounter ) {

                    // If Monday is the first day and the month starts on Sunday, shift the date back a week.
                    var shiftDateBy = settings.firstDay && calendar.create([ viewsetObject.year, viewsetObject.month, 1 ]).day === 0 ? -7 : 0

                    return [
                        _.group({
                            min: DAYS_IN_WEEK * rowCounter - viewsetObject.day + shiftDateBy + 1, // Add 1 for weekday 0index
                            max: function() {
                                return this.min + DAYS_IN_WEEK - 1
                            },
                            i: 1,
                            node: 'td',
                            item: function( targetDate ) {

                                // Convert the time date from a relative date to a target date.
                                targetDate = calendar.create([ viewsetObject.year, viewsetObject.month, targetDate + ( settings.firstDay ? 1 : 0 ) ])

                                var isSelected = selectedObject && selectedObject.pick == targetDate.pick,
                                    isHighlighted = highlightedObject && highlightedObject.pick == targetDate.pick,
                                    isDisabled = disabledCollection && calendar.disabled( targetDate ) || targetDate.pick < minLimitObject.pick || targetDate.pick > maxLimitObject.pick,
                                    formattedDate = _.trigger( calendar.formats.toString, calendar, [ settings.format, targetDate ] )

                                return [
                                    _.node(
                                        'div',
                                        targetDate.date,
                                        (function( klasses ) {

                                            // Add the `infocus` or `outfocus` classes based on month in view.
                                            klasses.push( viewsetObject.month == targetDate.month ? settings.klass.infocus : settings.klass.outfocus )

                                            // Add the `today` class if needed.
                                            if ( nowObject.pick == targetDate.pick ) {
                                                klasses.push( settings.klass.now )
                                            }

                                            // Add the `selected` class if something's selected and the time matches.
                                            if ( isSelected ) {
                                                klasses.push( settings.klass.selected )
                                            }

                                            // Add the `highlighted` class if something's highlighted and the time matches.
                                            if ( isHighlighted ) {
                                                klasses.push( settings.klass.highlighted )
                                            }

                                            // Add the `disabled` class if something's disabled and the object matches.
                                            if ( isDisabled ) {
                                                klasses.push( settings.klass.disabled )
                                            }

                                            return klasses.join( ' ' )
                                        })([ settings.klass.day ]),
                                        'data-pick=' + targetDate.pick + ' ' + _.ariaAttr({
                                            role: 'gridcell',
                                            label: formattedDate,
                                            selected: isSelected && calendar.$node.val() === formattedDate ? true : null,
                                            activedescendant: isHighlighted ? true : null,
                                            disabled: isDisabled ? true : null
                                        })
                                    ),
                                    '',
                                    _.ariaAttr({ role: 'presentation' })
                                ] //endreturn
                            }
                        })
                    ] //endreturn
                }
            })
        ),
        settings.klass.table,
        'id="' + calendar.$node[0].id + '_table' + '" ' + _.ariaAttr({
            role: 'grid',
            controls: calendar.$node[0].id,
            readonly: true
        })
    ) +

    // * For Firefox forms to submit, make sure to set the buttons’ `type` attributes as “button”.
    _.node(
        'div',
        _.node( 'button', settings.today, settings.klass.buttonToday,
            'type=button data-pick=' + nowObject.pick +
            ( isOpen && !calendar.disabled(nowObject) ? '' : ' disabled' ) + ' ' +
            _.ariaAttr({ controls: calendar.$node[0].id }) ) +
        _.node( 'button', settings.clear, settings.klass.buttonClear,
            'type=button data-clear=1' +
            ( isOpen ? '' : ' disabled' ) + ' ' +
            _.ariaAttr({ controls: calendar.$node[0].id }) ) +
        _.node('button', settings.close, settings.klass.buttonClose,
            'type=button data-close=true ' +
            ( isOpen ? '' : ' disabled' ) + ' ' +
            _.ariaAttr({ controls: calendar.$node[0].id }) ),
        settings.klass.footer
    ) //endreturn
} //DatePicker.prototype.nodes




/**
 * The date picker defaults.
 */
DatePicker.defaults = (function( prefix ) {

    return {

        // The title label to use for the month nav buttons
        labelMonthNext: 'Next month',
        labelMonthPrev: 'Previous month',

        // The title label to use for the dropdown selectors
        labelMonthSelect: 'Select a month',
        labelYearSelect: 'Select a year',

        // Months and weekdays
        monthsFull: [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ],
        monthsShort: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ],
        weekdaysFull: [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ],
        weekdaysShort: [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ],

        // Today and clear
        today: 'Today',
        clear: 'Clear',
        close: 'Close',

        // Picker close behavior
        closeOnSelect: true,
        closeOnClear: true,

        // The format to show on the `input` element
        format: 'd mmmm, yyyy',

        // Classes
        klass: {

            table: prefix + 'table',

            header: prefix + 'header',

            navPrev: prefix + 'nav--prev',
            navNext: prefix + 'nav--next',
            navDisabled: prefix + 'nav--disabled',

            month: prefix + 'month',
            year: prefix + 'year',

            selectMonth: prefix + 'select--month',
            selectYear: prefix + 'select--year',

            weekdays: prefix + 'weekday',

            day: prefix + 'day',
            disabled: prefix + 'day--disabled',
            selected: prefix + 'day--selected',
            highlighted: prefix + 'day--highlighted',
            now: prefix + 'day--today',
            infocus: prefix + 'day--infocus',
            outfocus: prefix + 'day--outfocus',

            footer: prefix + 'footer',

            buttonClear: prefix + 'button--clear',
            buttonToday: prefix + 'button--today',
            buttonClose: prefix + 'button--close'
        }
    }
})( Picker.klasses().picker + '__' )





/**
 * Extend the picker to add the date picker.
 */
Picker.extend( 'pickadate', DatePicker )


}));




(function(){var k=window.AmCharts;k.AmSlicedChart=k.Class({inherits:k.AmChart,construct:function(a){this.createEvents("rollOverSlice","rollOutSlice","clickSlice","pullOutSlice","pullInSlice","rightClickSlice");k.AmSlicedChart.base.construct.call(this,a);this.colors="#FF0F00 #FF6600 #FF9E01 #FCD202 #F8FF01 #B0DE09 #04D215 #0D8ECF #0D52D1 #2A0CD0 #8A0CCF #CD0D74 #754DEB #DDDDDD #999999 #333333 #000000 #57032A #CA9726 #990000 #4B0C25".split(" ");this.alpha=1;this.groupPercent=0;this.groupedTitle="Other";
this.groupedPulled=!1;this.groupedAlpha=1;this.marginLeft=0;this.marginBottom=this.marginTop=10;this.marginRight=0;this.hoverAlpha=1;this.outlineColor="#FFFFFF";this.outlineAlpha=0;this.outlineThickness=1;this.startAlpha=0;this.startDuration=1;this.startEffect="bounce";this.sequencedAnimation=!0;this.pullOutDuration=1;this.pullOutEffect="bounce";this.pullOnHover=this.pullOutOnlyOne=!1;this.labelsEnabled=!0;this.labelTickColor="#000000";this.labelTickAlpha=.2;this.hideLabelsPercent=0;this.urlTarget=
"_self";this.autoMarginOffset=10;this.gradientRatio=[];this.maxLabelWidth=200;this.accessibleLabel="[[title]]: [[percents]]% [[value]] [[description]]";k.applyTheme(this,a,"AmSlicedChart")},initChart:function(){k.AmSlicedChart.base.initChart.call(this);this.dataChanged&&(this.parseData(),this.dispatchDataUpdated=!0,this.dataChanged=!1,this.setLegendData(this.chartData));this.drawChart()},handleLegendEvent:function(a){var b=a.type,c=a.dataItem,d=this.legend;if(c.wedge&&c){var g=c.hidden;a=a.event;
switch(b){case "clickMarker":g||d.switchable||this.clickSlice(c,a);break;case "clickLabel":g||this.clickSlice(c,a,!1);break;case "rollOverItem":g||this.rollOverSlice(c,!1,a);break;case "rollOutItem":g||this.rollOutSlice(c,a);break;case "hideItem":this.hideSlice(c,a);break;case "showItem":this.showSlice(c,a)}}},invalidateVisibility:function(){this.recalculatePercents();this.initChart();var a=this.legend;a&&a.invalidateSize()},addEventListeners:function(a,b){var c=this;a.mouseover(function(a){c.rollOverSlice(b,
!0,a)}).mouseout(function(a){c.rollOutSlice(b,a)}).touchend(function(a){c.rollOverSlice(b,a)}).mouseup(function(a){c.clickSlice(b,a)}).contextmenu(function(a){c.handleRightClick(b,a)})},formatString:function(a,b,c){a=k.formatValue(a,b,["value"],this.nf,"",this.usePrefixes,this.prefixesOfSmallNumbers,this.prefixesOfBigNumbers);var d=this.pf.precision;isNaN(this.tempPrec)||(this.pf.precision=this.tempPrec);a=k.formatValue(a,b,["percents"],this.pf);a=k.massReplace(a,{"[[title]]":b.title,"[[description]]":b.description});
this.pf.precision=d;-1!=a.indexOf("[[")&&(a=k.formatDataContextValue(a,b.dataContext));a=c?k.fixNewLines(a):k.fixBrakes(a);return a=k.cleanFromEmpty(a)},startSlices:function(){var a;for(a=0;a<this.chartData.length;a++)0<this.startDuration&&this.sequencedAnimation?this.setStartTO(a):this.startSlice(this.chartData[a])},setStartTO:function(a){var b=this;a=setTimeout(function(){b.startSequenced.call(b)},b.startDuration/b.chartData.length*500*a);b.timeOuts.push(a)},pullSlices:function(a){var b=this.chartData,
c;for(c=0;c<b.length;c++){var d=b[c];d.pulled&&this.pullSlice(d,1,a)}},startSequenced:function(){var a=this.chartData,b;for(b=0;b<a.length;b++)if(!a[b].started){this.startSlice(this.chartData[b]);break}},startSlice:function(a){a.started=!0;var b=a.wedge,c=this.startDuration,d=a.labelSet;b&&0<c&&(0<a.alpha&&b.show(),b.translate(a.startX,a.startY),this.animatable.push(b),b.animate({opacity:1,translate:"0,0"},c,this.startEffect));d&&0<c&&(0<a.alpha&&d.show(),d.translate(a.startX,a.startY),d.animate({opacity:1,
translate:"0,0"},c,this.startEffect))},showLabels:function(){var a=this.chartData,b;for(b=0;b<a.length;b++){var c=a[b];if(0<c.alpha){var d=c.label;d&&d.show();(c=c.tick)&&c.show()}}},showSlice:function(a){isNaN(a)?a.hidden=!1:this.chartData[a].hidden=!1;this.invalidateVisibility()},hideSlice:function(a){isNaN(a)?a.hidden=!0:this.chartData[a].hidden=!0;this.hideBalloon();this.invalidateVisibility()},rollOverSlice:function(a,b,c){isNaN(a)||(a=this.chartData[a]);clearTimeout(this.hoverInt);if(!a.hidden){this.pullOnHover&&
this.pullSlice(a,1);1>this.hoverAlpha&&a.wedge&&a.wedge.attr({opacity:this.hoverAlpha});var d=a.balloonX,g=a.balloonY;a.pulled&&(d+=a.pullX,g+=a.pullY);var f=this.formatString(this.balloonText,a,!0),h=this.balloonFunction;h&&(f=h(a,f));h=k.adjustLuminosity(a.color,-.15);f?this.showBalloon(f,h,b,d,g):this.hideBalloon();0===a.value&&this.hideBalloon();this.fire({type:"rollOverSlice",dataItem:a,chart:this,event:c})}},rollOutSlice:function(a,b){isNaN(a)||(a=this.chartData[a]);a.wedge&&a.wedge.attr({opacity:1});
this.hideBalloon();this.fire({type:"rollOutSlice",dataItem:a,chart:this,event:b})},clickSlice:function(a,b,c){this.checkTouchDuration(b)&&(isNaN(a)||(a=this.chartData[a]),a.pulled?this.pullSlice(a,0):this.pullSlice(a,1),k.getURL(a.url,this.urlTarget),c||this.fire({type:"clickSlice",dataItem:a,chart:this,event:b}))},handleRightClick:function(a,b){isNaN(a)||(a=this.chartData[a]);this.fire({type:"rightClickSlice",dataItem:a,chart:this,event:b})},drawTicks:function(){var a=this.chartData,b;for(b=0;b<
a.length;b++){var c=a[b];if(c.label&&!c.skipTick){var d=c.ty,d=k.line(this.container,[c.tx0,c.tx,c.tx2],[c.ty0,d,d],this.labelTickColor,this.labelTickAlpha);k.setCN(this,d,this.type+"-tick");k.setCN(this,d,c.className,!0);c.tick=d;c.wedge.push(d);"AmFunnelChart"==this.cname&&d.toBack()}}},initialStart:function(){var a=this,b=a.startDuration,c=setTimeout(function(){a.showLabels.call(a)},1E3*b);a.timeOuts.push(c);a.chartCreated?a.pullSlices(!0):(a.startSlices(),0<b?(b=setTimeout(function(){a.pullSlices.call(a)},
1200*b),a.timeOuts.push(b)):a.pullSlices(!0))},pullSlice:function(a,b,c){var d=this.pullOutDuration;!0===c&&(d=0);if(c=a.wedge)0<d?(c.animate({translate:b*a.pullX+","+b*a.pullY},d,this.pullOutEffect),a.labelSet&&a.labelSet.animate({translate:b*a.pullX+","+b*a.pullY},d,this.pullOutEffect)):(a.labelSet&&a.labelSet.translate(b*a.pullX,b*a.pullY),c.translate(b*a.pullX,b*a.pullY));1==b?(a.pulled=!0,this.pullOutOnlyOne&&this.pullInAll(a.index),a={type:"pullOutSlice",dataItem:a,chart:this}):(a.pulled=!1,
a={type:"pullInSlice",dataItem:a,chart:this});this.fire(a)},pullInAll:function(a){var b=this.chartData,c;for(c=0;c<this.chartData.length;c++)c!=a&&b[c].pulled&&this.pullSlice(b[c],0)},pullOutAll:function(){var a=this.chartData,b;for(b=0;b<a.length;b++)a[b].pulled||this.pullSlice(a[b],1)},parseData:function(){var a=[];this.chartData=a;var b=this.dataProvider;isNaN(this.pieAlpha)||(this.alpha=this.pieAlpha);if(void 0!==b){var c=b.length,d=0,g,f,h;for(g=0;g<c;g++){f={};var e=b[g];f.dataContext=e;null!==
e[this.valueField]&&(f.value=Number(e[this.valueField]));(h=e[this.titleField])||(h="");f.title=h;f.pulled=k.toBoolean(e[this.pulledField],!1);(h=e[this.descriptionField])||(h="");f.description=h;f.labelRadius=Number(e[this.labelRadiusField]);f.switchable=!0;f.className=e[this.classNameField];f.url=e[this.urlField];h=e[this.patternField];!h&&this.patterns&&(h=this.patterns[g]);f.pattern=h;f.visibleInLegend=k.toBoolean(e[this.visibleInLegendField],!0);h=e[this.alphaField];f.alpha=void 0!==h?Number(h):
this.alpha;h=e[this.colorField];void 0!==h&&(f.color=h);f.labelColor=k.toColor(e[this.labelColorField]);d+=f.value;f.hidden=!1;a[g]=f}for(g=b=0;g<c;g++)f=a[g],f.percents=f.value/d*100,f.percents<this.groupPercent&&b++;1<b&&(this.groupValue=0,this.removeSmallSlices(),a.push({title:this.groupedTitle,value:this.groupValue,percents:this.groupValue/d*100,pulled:this.groupedPulled,color:this.groupedColor,url:this.groupedUrl,description:this.groupedDescription,alpha:this.groupedAlpha,pattern:this.groupedPattern,
className:this.groupedClassName,dataContext:{}}));c=this.baseColor;c||(c=this.pieBaseColor);d=this.brightnessStep;d||(d=this.pieBrightnessStep);for(g=0;g<a.length;g++)c?h=k.adjustLuminosity(c,g*d/100):(h=this.colors[g],void 0===h&&(h=k.randomColor())),void 0===a[g].color&&(a[g].color=h);this.recalculatePercents()}},recalculatePercents:function(){var a=this.chartData,b=0,c,d;for(c=0;c<a.length;c++)d=a[c],!d.hidden&&0<d.value&&(b+=d.value);for(c=0;c<a.length;c++)d=this.chartData[c],d.percents=!d.hidden&&
0<d.value?100*d.value/b:0},removeSmallSlices:function(){var a=this.chartData,b;for(b=a.length-1;0<=b;b--)a[b].percents<this.groupPercent&&(this.groupValue+=a[b].value,a.splice(b,1))},animateAgain:function(){var a=this;a.startSlices();for(var b=0;b<a.chartData.length;b++){var c=a.chartData[b];c.started=!1;var d=c.wedge;d&&(d.setAttr("opacity",a.startAlpha),d.translate(c.startX,c.startY));if(d=c.labelSet)d.setAttr("opacity",a.startAlpha),d.translate(c.startX,c.startY)}b=a.startDuration;0<b?(b=setTimeout(function(){a.pullSlices.call(a)},
1200*b),a.timeOuts.push(b)):a.pullSlices()},measureMaxLabel:function(){var a=this.chartData,b=0,c;for(c=0;c<a.length;c++){var d=a[c],g=this.formatString(this.labelText,d),f=this.labelFunction;f&&(g=f(d,g));d=k.text(this.container,g,this.color,this.fontFamily,this.fontSize);g=d.getBBox().width;g>b&&(b=g);d.remove()}return b}})})();(function(){var k=window.AmCharts;k.AmPieChart=k.Class({inherits:k.AmSlicedChart,construct:function(a){this.type="pie";k.AmPieChart.base.construct.call(this,a);this.cname="AmPieChart";this.pieBrightnessStep=30;this.minRadius=10;this.depth3D=0;this.startAngle=90;this.angle=this.innerRadius=0;this.startRadius="500%";this.pullOutRadius="20%";this.labelRadius=20;this.labelText="[[title]]: [[percents]]%";this.balloonText="[[title]]: [[percents]]% ([[value]])\n[[description]]";this.previousScale=1;this.adjustPrecision=
!1;this.gradientType="radial";k.applyTheme(this,a,this.cname)},drawChart:function(){k.AmPieChart.base.drawChart.call(this);var a=this.chartData;if(k.ifArray(a)){if(0<this.realWidth&&0<this.realHeight){k.VML&&(this.startAlpha=1);var b=this.startDuration,c=this.container,d=this.updateWidth();this.realWidth=d;var g=this.updateHeight();this.realHeight=g;var f=k.toCoordinate,h=f(this.marginLeft,d),e=f(this.marginRight,d),z=f(this.marginTop,g)+this.getTitleHeight(),n=f(this.marginBottom,g)+this.depth3D,
A,B,m,w=k.toNumber(this.labelRadius),q=this.measureMaxLabel();q>this.maxLabelWidth&&(q=this.maxLabelWidth);this.labelText&&this.labelsEnabled||(w=q=0);A=void 0===this.pieX?(d-h-e)/2+h:f(this.pieX,this.realWidth);B=void 0===this.pieY?(g-z-n)/2+z:f(this.pieY,g);m=f(this.radius,d,g);m||(d=0<=w?d-h-e-2*q:d-h-e,g=g-z-n,m=Math.min(d,g),g<d&&(m/=1-this.angle/90,m>d&&(m=d)),g=k.toCoordinate(this.pullOutRadius,m),m=(0<=w?m-1.8*(w+g):m-1.8*g)/2);m<this.minRadius&&(m=this.minRadius);g=f(this.pullOutRadius,m);
z=k.toCoordinate(this.startRadius,m);f=f(this.innerRadius,m);f>=m&&(f=m-1);n=k.fitToBounds(this.startAngle,0,360);0<this.depth3D&&(n=270<=n?270:90);n-=90;360<n&&(n-=360);d=m-m*this.angle/90;for(h=q=0;h<a.length;h++)e=a[h],!0!==e.hidden&&(q+=k.roundTo(e.percents,this.pf.precision));q=k.roundTo(q,this.pf.precision);this.tempPrec=NaN;this.adjustPrecision&&100!=q&&(this.tempPrec=this.pf.precision+1);for(var E,h=0;h<a.length;h++)if(e=a[h],!0!==e.hidden&&(this.showZeroSlices||0!==e.percents)){var r=360*
e.percents/100,q=Math.sin((n+r/2)/180*Math.PI),C=d/m*-Math.cos((n+r/2)/180*Math.PI),p=this.outlineColor;p||(p=e.color);var u=this.alpha;isNaN(e.alpha)||(u=e.alpha);p={fill:e.color,stroke:p,"stroke-width":this.outlineThickness,"stroke-opacity":this.outlineAlpha,"fill-opacity":u};e.url&&(p.cursor="pointer");p=k.wedge(c,A,B,n,r,m,d,f,this.depth3D,p,this.gradientRatio,e.pattern,this.path,this.gradientType);k.setCN(this,p,"pie-item");k.setCN(this,p.wedge,"pie-slice");k.setCN(this,p,e.className,!0);this.addEventListeners(p,
e);e.startAngle=n;a[h].wedge=p;0<b&&(this.chartCreated||p.setAttr("opacity",this.startAlpha));e.ix=q;e.iy=C;e.wedge=p;e.index=h;e.label=null;u=c.set();if(this.labelsEnabled&&this.labelText&&e.percents>=this.hideLabelsPercent){var l=n+r/2;0>l&&(l+=360);360<l&&(l-=360);var t=w;isNaN(e.labelRadius)||(t=e.labelRadius,0>t&&(e.skipTick=!0));var r=A+q*(m+t),D=B+C*(m+t),x,v=0;isNaN(E)&&350<l&&1<a.length-h&&(E=h-1+Math.floor((a.length-h)/2));if(0<=t){var y;90>=l&&0<=l?(y=0,x="start",v=8):90<=l&&180>l?(y=1,
x="start",v=8):180<=l&&270>l?(y=2,x="end",v=-8):270<=l&&354>=l?(y=3,x="end",v=-8):354<=l&&(h>E?(y=0,x="start",v=8):(y=3,x="end",v=-8));e.labelQuarter=y}else x="middle";l=this.formatString(this.labelText,e);(t=this.labelFunction)&&(l=t(e,l));t=e.labelColor;t||(t=this.color);""!==l&&(l=k.wrappedText(c,l,t,this.fontFamily,this.fontSize,x,!1,this.maxLabelWidth),k.setCN(this,l,"pie-label"),k.setCN(this,l,e.className,!0),l.translate(r+1.5*v,D),0>w&&(l.node.style.pointerEvents="none"),l.node.style.cursor=
"default",e.ty=D,e.textX=r+1.5*v,u.push(l),this.axesSet.push(u),e.labelSet=u,e.label=l,this.addEventListeners(u,e));e.tx=r;e.tx2=r+v;e.tx0=A+q*m;e.ty0=B+C*m}r=f+(m-f)/2;e.pulled&&(r+=g);this.accessible&&this.accessibleLabel&&(D=this.formatString(this.accessibleLabel,e),this.makeAccessible(p,D));void 0!==this.tabIndex&&p.setAttr("tabindex",this.tabIndex);e.balloonX=q*r+A;e.balloonY=C*r+B;e.startX=Math.round(q*z);e.startY=Math.round(C*z);e.pullX=Math.round(q*g);e.pullY=Math.round(C*g);this.graphsSet.push(p);
if(0===e.alpha||0<b&&!this.chartCreated)p.hide(),u&&u.hide();n+=360*e.percents/100;360<n&&(n-=360)}0<w&&this.arrangeLabels();this.pieXReal=A;this.pieYReal=B;this.radiusReal=m;this.innerRadiusReal=f;0<w&&this.drawTicks();this.initialStart();this.setDepths()}(a=this.legend)&&a.invalidateSize()}else this.cleanChart();this.dispDUpd()},setDepths:function(){var a=this.chartData,b;for(b=0;b<a.length;b++){var c=a[b],d=c.wedge,c=c.startAngle;0<=c&&180>c?d.toFront():180<=c&&d.toBack()}},arrangeLabels:function(){var a=
this.chartData,b=a.length,c,d;for(d=b-1;0<=d;d--)c=a[d],0!==c.labelQuarter||c.hidden||this.checkOverlapping(d,c,0,!0,0);for(d=0;d<b;d++)c=a[d],1!=c.labelQuarter||c.hidden||this.checkOverlapping(d,c,1,!1,0);for(d=b-1;0<=d;d--)c=a[d],2!=c.labelQuarter||c.hidden||this.checkOverlapping(d,c,2,!0,0);for(d=0;d<b;d++)c=a[d],3!=c.labelQuarter||c.hidden||this.checkOverlapping(d,c,3,!1,0)},checkOverlapping:function(a,b,c,d,g){var f,h,e=this.chartData,k=e.length,n=b.label;if(n){if(!0===d)for(h=a+1;h<k;h++)e[h].labelQuarter==
c&&(f=this.checkOverlappingReal(b,e[h],c))&&(h=k);else for(h=a-1;0<=h;h--)e[h].labelQuarter==c&&(f=this.checkOverlappingReal(b,e[h],c))&&(h=0);!0===f&&200>g&&isNaN(b.labelRadius)&&(f=b.ty+3*b.iy,b.ty=f,n.translate(b.textX,f),this.checkOverlapping(a,b,c,d,g+1))}},checkOverlappingReal:function(a,b,c){var d=!1,g=a.label,f=b.label;a.labelQuarter!=c||a.hidden||b.hidden||!f||(g=g.getBBox(),c={},c.width=g.width,c.height=g.height,c.y=a.ty,c.x=a.tx,a=f.getBBox(),f={},f.width=a.width,f.height=a.height,f.y=
b.ty,f.x=b.tx,k.hitTest(c,f)&&(d=!0));return d}})})();

(function(){var f=window.AmCharts;f.AmRectangularChart=f.Class({inherits:f.AmCoordinateChart,construct:function(a){f.AmRectangularChart.base.construct.call(this,a);this.theme=a;this.createEvents("zoomed","changed");this.marginRight=this.marginBottom=this.marginTop=this.marginLeft=20;this.depth3D=this.angle=0;this.plotAreaFillColors="#FFFFFF";this.plotAreaFillAlphas=0;this.plotAreaBorderColor="#000000";this.plotAreaBorderAlpha=0;this.maxZoomFactor=20;this.zoomOutButtonImageSize=19;this.zoomOutButtonImage=
"lens";this.zoomOutText="Show all";this.zoomOutButtonColor="#e5e5e5";this.zoomOutButtonAlpha=0;this.zoomOutButtonRollOverAlpha=1;this.zoomOutButtonPadding=8;this.trendLines=[];this.autoMargins=!0;this.marginsUpdated=!1;this.autoMarginOffset=10;f.applyTheme(this,a,"AmRectangularChart")},initChart:function(){f.AmRectangularChart.base.initChart.call(this);this.updateDxy();!this.marginsUpdated&&this.autoMargins&&(this.resetMargins(),this.drawGraphs=!1);this.processScrollbars();this.updateMargins();this.updatePlotArea();
this.updateScrollbars();this.updateTrendLines();this.updateChartCursor();this.updateValueAxes();this.scrollbarOnly||this.updateGraphs()},drawChart:function(){f.AmRectangularChart.base.drawChart.call(this);this.drawPlotArea();if(f.ifArray(this.chartData)){var a=this.chartCursor;a&&a.draw()}},resetMargins:function(){var a={},b;if("xy"==this.type){var c=this.xAxes,d=this.yAxes;for(b=0;b<c.length;b++){var g=c[b];g.ignoreAxisWidth||(g.setOrientation(!0),g.fixAxisPosition(),a[g.position]=!0)}for(b=0;b<
d.length;b++)c=d[b],c.ignoreAxisWidth||(c.setOrientation(!1),c.fixAxisPosition(),a[c.position]=!0)}else{d=this.valueAxes;for(b=0;b<d.length;b++)c=d[b],c.ignoreAxisWidth||(c.setOrientation(this.rotate),c.fixAxisPosition(),a[c.position]=!0);(b=this.categoryAxis)&&!b.ignoreAxisWidth&&(b.setOrientation(!this.rotate),b.fixAxisPosition(),b.fixAxisPosition(),a[b.position]=!0)}a.left&&(this.marginLeft=0);a.right&&(this.marginRight=0);a.top&&(this.marginTop=0);a.bottom&&(this.marginBottom=0);this.fixMargins=
a},measureMargins:function(){var a=this.valueAxes,b,c=this.autoMarginOffset,d=this.fixMargins,g=this.realWidth,h=this.realHeight,e=c,f=c,k=g;b=h;var m;for(m=0;m<a.length;m++)a[m].handleSynchronization(),b=this.getAxisBounds(a[m],e,k,f,b),e=Math.round(b.l),k=Math.round(b.r),f=Math.round(b.t),b=Math.round(b.b);if(a=this.categoryAxis)b=this.getAxisBounds(a,e,k,f,b),e=Math.round(b.l),k=Math.round(b.r),f=Math.round(b.t),b=Math.round(b.b);d.left&&e<c&&(this.marginLeft=Math.round(-e+c),!isNaN(this.minMarginLeft)&&
this.marginLeft<this.minMarginLeft&&(this.marginLeft=this.minMarginLeft));d.right&&k>=g-c&&(this.marginRight=Math.round(k-g+c),!isNaN(this.minMarginRight)&&this.marginRight<this.minMarginRight&&(this.marginRight=this.minMarginRight));d.top&&f<c+this.titleHeight&&(this.marginTop=Math.round(this.marginTop-f+c+this.titleHeight),!isNaN(this.minMarginTop)&&this.marginTop<this.minMarginTop&&(this.marginTop=this.minMarginTop));d.bottom&&b>h-c&&(this.marginBottom=Math.round(this.marginBottom+b-h+c),!isNaN(this.minMarginBottom)&&
this.marginBottom<this.minMarginBottom&&(this.marginBottom=this.minMarginBottom));this.initChart()},getAxisBounds:function(a,b,c,d,g){if(!a.ignoreAxisWidth){var h=a.labelsSet,e=a.tickLength;a.inside&&(e=0);if(h)switch(h=a.getBBox(),a.position){case "top":a=h.y;d>a&&(d=a);break;case "bottom":a=h.y+h.height;g<a&&(g=a);break;case "right":a=h.x+h.width+e+3;c<a&&(c=a);break;case "left":a=h.x-e,b>a&&(b=a)}}return{l:b,t:d,r:c,b:g}},drawZoomOutButton:function(){var a=this;if(!a.zbSet){var b=a.container.set();
a.zoomButtonSet.push(b);var c=a.color,d=a.fontSize,g=a.zoomOutButtonImageSize,h=a.zoomOutButtonImage.replace(/\.[a-z]*$/i,""),e=a.langObj.zoomOutText||a.zoomOutText,l=a.zoomOutButtonColor,k=a.zoomOutButtonAlpha,m=a.zoomOutButtonFontSize,p=a.zoomOutButtonPadding;isNaN(m)||(d=m);(m=a.zoomOutButtonFontColor)&&(c=m);var m=a.zoomOutButton,n;m&&(m.fontSize&&(d=m.fontSize),m.color&&(c=m.color),m.backgroundColor&&(l=m.backgroundColor),isNaN(m.backgroundAlpha)||(a.zoomOutButtonRollOverAlpha=m.backgroundAlpha));
var u=m=0,u=a.pathToImages;if(h){if(f.isAbsolute(h)||void 0===u)u="";n=a.container.image(u+h+a.extension,0,0,g,g);f.setCN(a,n,"zoom-out-image");b.push(n);n=n.getBBox();m=n.width+5}void 0!==e&&(c=f.text(a.container,e,c,a.fontFamily,d,"start"),f.setCN(a,c,"zoom-out-label"),d=c.getBBox(),u=n?n.height/2-3:d.height/2,c.translate(m,u),b.push(c));n=b.getBBox();c=1;f.isModern||(c=0);l=f.rect(a.container,n.width+2*p+5,n.height+2*p-2,l,1,1,l,c);l.setAttr("opacity",k);l.translate(-p,-p);f.setCN(a,l,"zoom-out-bg");
b.push(l);l.toBack();a.zbBG=l;n=l.getBBox();b.translate(a.marginLeftReal+a.plotAreaWidth-n.width+p,a.marginTopReal+p);b.hide();b.mouseover(function(){a.rollOverZB()}).mouseout(function(){a.rollOutZB()}).click(function(){a.clickZB()}).touchstart(function(){a.rollOverZB()}).touchend(function(){a.rollOutZB();a.clickZB()});for(k=0;k<b.length;k++)b[k].attr({cursor:"pointer"});void 0!==a.zoomOutButtonTabIndex&&(b.setAttr("tabindex",a.zoomOutButtonTabIndex),b.setAttr("role","menuitem"),b.keyup(function(b){13==
b.keyCode&&a.clickZB()}));a.zbSet=b}},rollOverZB:function(){this.rolledOverZB=!0;this.zbBG.setAttr("opacity",this.zoomOutButtonRollOverAlpha)},rollOutZB:function(){this.rolledOverZB=!1;this.zbBG.setAttr("opacity",this.zoomOutButtonAlpha)},clickZB:function(){this.rolledOverZB=!1;this.zoomOut()},zoomOut:function(){this.zoomOutValueAxes()},drawPlotArea:function(){var a=this.dx,b=this.dy,c=this.marginLeftReal,d=this.marginTopReal,g=this.plotAreaWidth-1,h=this.plotAreaHeight-1,e=this.plotAreaFillColors,
l=this.plotAreaFillAlphas,k=this.plotAreaBorderColor,m=this.plotAreaBorderAlpha;"object"==typeof l&&(l=l[0]);e=f.polygon(this.container,[0,g,g,0,0],[0,0,h,h,0],e,l,1,k,m,this.plotAreaGradientAngle);f.setCN(this,e,"plot-area");e.translate(c+a,d+b);this.set.push(e);0!==a&&0!==b&&(e=this.plotAreaFillColors,"object"==typeof e&&(e=e[0]),e=f.adjustLuminosity(e,-.15),g=f.polygon(this.container,[0,a,g+a,g,0],[0,b,b,0,0],e,l,1,k,m),f.setCN(this,g,"plot-area-bottom"),g.translate(c,d+h),this.set.push(g),a=f.polygon(this.container,
[0,0,a,a,0],[0,h,h+b,b,0],e,l,1,k,m),f.setCN(this,a,"plot-area-left"),a.translate(c,d),this.set.push(a));(c=this.bbset)&&this.scrollbarOnly&&c.remove()},updatePlotArea:function(){var a=this.updateWidth(),b=this.updateHeight(),c=this.container;this.realWidth=a;this.realWidth=b;c&&this.container.setSize(a,b);var c=this.marginLeftReal,d=this.marginTopReal,a=a-c-this.marginRightReal-this.dx,b=b-d-this.marginBottomReal;1>a&&(a=1);1>b&&(b=1);this.plotAreaWidth=Math.round(a);this.plotAreaHeight=Math.round(b);
this.plotBalloonsSet.translate(c,d)},updateDxy:function(){this.dx=Math.round(this.depth3D*Math.cos(this.angle*Math.PI/180));this.dy=Math.round(-this.depth3D*Math.sin(this.angle*Math.PI/180));this.d3x=Math.round(this.columnSpacing3D*Math.cos(this.angle*Math.PI/180));this.d3y=Math.round(-this.columnSpacing3D*Math.sin(this.angle*Math.PI/180))},updateMargins:function(){var a=this.getTitleHeight();this.titleHeight=a;this.marginTopReal=this.marginTop-this.dy;this.fixMargins&&!this.fixMargins.top&&(this.marginTopReal+=
a);this.marginBottomReal=this.marginBottom;this.marginLeftReal=this.marginLeft;this.marginRightReal=this.marginRight},updateValueAxes:function(){var a=this.valueAxes,b;for(b=0;b<a.length;b++){var c=a[b];this.setAxisRenderers(c);this.updateObjectSize(c)}},setAxisRenderers:function(a){a.axisRenderer=f.RecAxis;a.guideFillRenderer=f.RecFill;a.axisItemRenderer=f.RecItem;a.marginsChanged=!0},updateGraphs:function(){var a=this.graphs,b;for(b=0;b<a.length;b++){var c=a[b];c.index=b;c.rotate=this.rotate;this.updateObjectSize(c)}},
updateObjectSize:function(a){a.width=this.plotAreaWidth-1;a.height=this.plotAreaHeight-1;a.x=this.marginLeftReal;a.y=this.marginTopReal;a.dx=this.dx;a.dy=this.dy},updateChartCursor:function(){var a=this.chartCursor;a&&(a=f.processObject(a,f.ChartCursor,this.theme),this.updateObjectSize(a),this.addChartCursor(a),a.chart=this)},processScrollbars:function(){var a=this.chartScrollbar;a&&(a=f.processObject(a,f.ChartScrollbar,this.theme),this.addChartScrollbar(a))},updateScrollbars:function(){},removeChartCursor:function(){f.callMethod("destroy",
[this.chartCursor]);this.chartCursor=null},zoomTrendLines:function(){var a=this.trendLines,b;for(b=0;b<a.length;b++){var c=a[b];c.valueAxis.recalculateToPercents?c.set&&c.set.hide():(c.x=this.marginLeftReal,c.y=this.marginTopReal,c.draw())}},handleCursorValueZoom:function(){},addTrendLine:function(a){this.trendLines.push(a)},zoomOutValueAxes:function(){for(var a=this.valueAxes,b=0;b<a.length;b++)a[b].zoomOut()},removeTrendLine:function(a){var b=this.trendLines,c;for(c=b.length-1;0<=c;c--)b[c]==a&&
b.splice(c,1)},adjustMargins:function(a,b){var c=a.position,d=a.scrollbarHeight+a.offset;a.enabled&&("top"==c?b?this.marginLeftReal+=d:this.marginTopReal+=d:b?this.marginRightReal+=d:this.marginBottomReal+=d)},getScrollbarPosition:function(a,b,c){var d="bottom",g="top";a.oppositeAxis||(g=d,d="top");a.position=b?"bottom"==c||"left"==c?d:g:"top"==c||"right"==c?d:g},updateChartScrollbar:function(a,b){if(a){a.rotate=b;var c=this.marginTopReal,d=this.marginLeftReal,g=a.scrollbarHeight,h=this.dx,e=this.dy,
f=a.offset;"top"==a.position?b?(a.y=c,a.x=d-g-f):(a.y=c-g+e-f,a.x=d+h):b?(a.y=c+e,a.x=d+this.plotAreaWidth+h+f):(a.y=c+this.plotAreaHeight+f,a.x=this.marginLeftReal)}},showZB:function(a){var b=this.zbSet;a&&(b=this.zoomOutText,""!==b&&b&&this.drawZoomOutButton());if(b=this.zbSet)this.zoomButtonSet.push(b),a?b.show():b.hide(),this.rollOutZB()},handleReleaseOutside:function(a){f.AmRectangularChart.base.handleReleaseOutside.call(this,a);(a=this.chartCursor)&&a.handleReleaseOutside&&a.handleReleaseOutside()},
handleMouseDown:function(a){f.AmRectangularChart.base.handleMouseDown.call(this,a);var b=this.chartCursor;b&&b.handleMouseDown&&!this.rolledOverZB&&b.handleMouseDown(a)},update:function(){f.AmRectangularChart.base.update.call(this);this.chartCursor&&this.chartCursor.update&&this.chartCursor.update()},handleScrollbarValueZoom:function(a){this.relativeZoomValueAxes(a.target.valueAxes,a.relativeStart,a.relativeEnd);this.zoomAxesAndGraphs()},zoomValueScrollbar:function(a){if(a&&a.enabled){var b=a.valueAxes[0],
c=b.relativeStart,d=b.relativeEnd;b.reversed&&(d=1-c,c=1-b.relativeEnd);a.percentZoom(c,d)}},zoomAxesAndGraphs:function(){if(!this.scrollbarOnly){var a=this.valueAxes,b;for(b=0;b<a.length;b++)a[b].zoom(this.start,this.end);a=this.graphs;for(b=0;b<a.length;b++)a[b].zoom(this.start,this.end);(b=this.chartCursor)&&b.clearSelection();this.zoomTrendLines()}},handleValueAxisZoomReal:function(a,b){var c=a.relativeStart,d=a.relativeEnd;if(c>d)var g=c,c=d,d=g;this.relativeZoomValueAxes(b,c,d);this.updateAfterValueZoom()},
updateAfterValueZoom:function(){this.zoomAxesAndGraphs();this.zoomScrollbar()},relativeZoomValueAxes:function(a,b,c){this.hideBalloonReal();b=f.fitToBounds(b,0,1);c=f.fitToBounds(c,0,1);if(b>c){var d=b;b=c;c=d}var d=1/this.maxZoomFactor,g=f.getDecimals(d)+4;c-b<d&&(c=b+(c-b)/2,b=c-d/2,c+=d/2,1<c&&(b-=c-1,c=1),0>b&&(b=0,c=d));b=f.roundTo(b,g);c=f.roundTo(c,g);d=!1;if(a){for(g=0;g<a.length;g++){var h=a[g].zoomToRelativeValues(b,c,!0);h&&(d=h)}this.showZB()}return d},addChartCursor:function(a){f.callMethod("destroy",
[this.chartCursor]);a&&(this.listenTo(a,"moved",this.handleCursorMove),this.listenTo(a,"zoomed",this.handleCursorZoom),this.listenTo(a,"zoomStarted",this.handleCursorZoomStarted),this.listenTo(a,"panning",this.handleCursorPanning),this.listenTo(a,"onHideCursor",this.handleCursorHide));this.chartCursor=a},handleCursorChange:function(){},handleCursorMove:function(a){var b,c=this.valueAxes;for(b=0;b<c.length;b++)if(!a.panning){var d=c[b];d&&d.showBalloon&&d.showBalloon(a.x,a.y)}},handleCursorZoom:function(a){if(this.skipZoomed)this.skipZoomed=
!1;else{var b=this.startX0,c=this.endX0,d=this.endY0,g=this.startY0,h=a.startX,e=a.endX,f=a.startY,k=a.endY;this.startX0=this.endX0=this.startY0=this.endY0=NaN;this.handleCursorZoomReal(b+h*(c-b),b+e*(c-b),g+f*(d-g),g+k*(d-g),a)}},handleCursorHide:function(){var a,b=this.valueAxes;for(a=0;a<b.length;a++)b[a].hideBalloon();b=this.graphs;for(a=0;a<b.length;a++)b[a].hideBalloonReal()}})})();(function(){var f=window.AmCharts;f.AmSerialChart=f.Class({inherits:f.AmRectangularChart,construct:function(a){this.type="serial";f.AmSerialChart.base.construct.call(this,a);this.cname="AmSerialChart";this.theme=a;this.columnSpacing=5;this.columnSpacing3D=0;this.columnWidth=.8;var b=new f.CategoryAxis(a);b.chart=this;this.categoryAxis=b;this.zoomOutOnDataUpdate=!0;this.mouseWheelZoomEnabled=this.mouseWheelScrollEnabled=this.rotate=this.skipZoom=!1;this.minSelectedTime=0;f.applyTheme(this,a,this.cname)},
initChart:function(){f.AmSerialChart.base.initChart.call(this);this.updateCategoryAxis(this.categoryAxis,this.rotate,"categoryAxis");if(this.dataChanged)this.parseData();else this.onDataUpdated();this.drawGraphs=!0},onDataUpdated:function(){var a=this.countColumns(),b=this.chartData,c=this.graphs,d;for(d=0;d<c.length;d++){var g=c[d];g.data=b;g.columnCount=a}0<b.length&&(this.firstTime=this.getStartTime(b[0].time),this.lastTime=this.getEndTime(b[b.length-1].time));this.drawChart();this.autoMargins&&
!this.marginsUpdated?(this.marginsUpdated=!0,this.measureMargins()):this.dispDUpd()},syncGrid:function(){if(this.synchronizeGrid){var a=this.valueAxes,b,c;if(0<a.length){var d=0;for(c=0;c<a.length;c++)b=a[c],d<b.gridCountReal&&(d=b.gridCountReal);var g=!1;for(c=0;c<a.length;c++)if(b=a[c],b.gridCountReal<d){var h=(d-b.gridCountReal)/2,e=g=h;0!==h-Math.round(h)&&(g-=.5,e+=.5);0<=b.min&&0>b.min-g*b.step&&(e+=g,g=0);0>=b.max&&0<b.max+e*b.step&&(g+=e,e=0);h=f.getDecimals(b.step);b.minimum=f.roundTo(b.min-
g*b.step,h);b.maximum=f.roundTo(b.max+e*b.step,h);b.setStep=b.step;g=b.strictMinMax=!0}g&&this.updateAfterValueZoom();for(c=0;c<a.length;c++)b=a[c],b.minimum=NaN,b.maximum=NaN,b.setStep=NaN,b.strictMinMax=!1}}},handleWheelReal:function(a,b){if(!this.wheelBusy){var c=this.categoryAxis,d=c.parseDates,g=c.minDuration(),h=1,e=1;this.mouseWheelZoomEnabled?b||(h=-1):b&&(h=-1);var f=this.chartCursor;if(f){var k=f.mouseX,f=f.mouseY;h!=e&&(k=this.rotate?f/this.plotAreaHeight:k/this.plotAreaWidth,h*=k,e*=1-
k);k=.05*(this.end-this.start);d&&(k=.05*(this.endTime-this.startTime)/g);1>k&&(k=1);h*=k;e*=k;if(!d||c.equalSpacing)h=Math.round(h),e=Math.round(e)}f=this.chartData.length;c=this.lastTime;k=this.firstTime;0>a?d?(f=this.endTime-this.startTime,d=this.startTime+h*g,g=this.endTime+e*g,0<e&&0<h&&g>=c&&(g=c,d=c-f),this.zoomToDates(new Date(d),new Date(g))):(0<e&&0<h&&this.end>=f-1&&(h=e=0),d=this.start+h,g=this.end+e,this.zoomToIndexes(d,g)):d?(f=this.endTime-this.startTime,d=this.startTime-h*g,g=this.endTime-
e*g,0<e&&0<h&&d<=k&&(d=k,g=k+f),this.zoomToDates(new Date(d),new Date(g))):(0<e&&0<h&&1>this.start&&(h=e=0),d=this.start-h,g=this.end-e,this.zoomToIndexes(d,g))}},validateData:function(a){this.marginsUpdated=!1;this.zoomOutOnDataUpdate&&!a&&(this.endTime=this.end=this.startTime=this.start=NaN);var b=a=!1,c=!1,d=this.chartScrollbar;d&&(d.dragging&&(a=!0,d.handleDragStop()),d.resizingRight&&(c=!0,d.rightDragStop()),d.resizingLeft&&(b=!0,d.leftDragStop()));f.AmSerialChart.base.validateData.call(this);
a&&d.handleDragStart();c&&d.rightDragStart();b&&d.leftDragStart()},drawChart:function(){if(0<this.realWidth&&0<this.realHeight){f.AmSerialChart.base.drawChart.call(this);var a=this.chartData;if(f.ifArray(a)){var b=this.chartScrollbar;!b||!this.marginsUpdated&&this.autoMargins||b.draw();(b=this.valueScrollbar)&&b.draw();var b=a.length-1,c,d;c=this.categoryAxis;if(c.parseDates&&!c.equalSpacing){if(c=this.startTime,d=this.endTime,isNaN(c)||isNaN(d))c=this.firstTime,d=this.lastTime}else{c=this.start;
d=this.end;if(isNaN(c)||isNaN(d))d=c=NaN;isNaN(c)&&(isNaN(this.startTime)||(c=this.getClosestIndex(a,"time",this.startTime,!0,0,a.length)));isNaN(d)&&(isNaN(this.endTime)||(d=this.getClosestIndex(a,"time",this.endTime,!1,0,a.length)));if(isNaN(c)||isNaN(d))c=0,d=b}this.endTime=this.startTime=this.end=this.start=void 0;this.zoom(c,d)}}else this.cleanChart()},cleanChart:function(){f.callMethod("destroy",[this.valueAxes,this.graphs,this.categoryAxis,this.chartScrollbar,this.chartCursor,this.valueScrollbar])},
updateCategoryAxis:function(a,b,c){a.chart=this;a.id=c;a.rotate=b;a.setOrientation(!this.rotate);a.init();this.setAxisRenderers(a);this.updateObjectSize(a)},updateValueAxes:function(){f.AmSerialChart.base.updateValueAxes.call(this);var a=this.valueAxes,b;for(b=0;b<a.length;b++){var c=a[b],d=this.rotate;c.rotate=d;c.setOrientation(d);d=this.categoryAxis;if(!d.startOnAxis||d.parseDates)c.expandMinMax=!0}},getStartTime:function(a){var b=this.categoryAxis;return f.resetDateToMin(new Date(a),b.minPeriod,
1,b.firstDayOfWeek).getTime()},getEndTime:function(a){var b=f.extractPeriod(this.categoryAxis.minPeriod);return f.changeDate(new Date(a),b.period,b.count,!0).getTime()-1},updateMargins:function(){f.AmSerialChart.base.updateMargins.call(this);var a=this.chartScrollbar;a&&(this.getScrollbarPosition(a,this.rotate,this.categoryAxis.position),this.adjustMargins(a,this.rotate));if(a=this.valueScrollbar)this.getScrollbarPosition(a,!this.rotate,this.valueAxes[0].position),this.adjustMargins(a,!this.rotate)},
updateScrollbars:function(){f.AmSerialChart.base.updateScrollbars.call(this);this.updateChartScrollbar(this.chartScrollbar,this.rotate);this.updateChartScrollbar(this.valueScrollbar,!this.rotate)},zoom:function(a,b){var c=this.categoryAxis;c.parseDates&&!c.equalSpacing?(this.timeZoom(a,b),isNaN(a)&&this.zoomOutValueAxes()):this.indexZoom(a,b);(c=this.chartCursor)&&(c.pan||c.hideCursorReal());this.updateLegendValues()},timeZoom:function(a,b){var c=this.maxSelectedTime;isNaN(c)||(b!=this.endTime&&b-
a>c&&(a=b-c),a!=this.startTime&&b-a>c&&(b=a+c));var d=this.minSelectedTime;if(0<d&&b-a<d){var g=Math.round(a+(b-a)/2),d=Math.round(d/2);a=g-d;b=g+d}d=this.chartData;g=this.categoryAxis;if(f.ifArray(d)&&(a!=this.startTime||b!=this.endTime)){var h=g.minDuration(),e=this.firstTime,l=this.lastTime;a||(a=e,isNaN(c)||(a=l-c));b||(b=l);a>l&&(a=l);b<e&&(b=e);a<e&&(a=e);b>l&&(b=l);b<a&&(b=a+h);b-a<h/5&&(b<l?b=a+h/5:a=b-h/5);this.startTime=a;this.endTime=b;c=d.length-1;h=this.getClosestIndex(d,"time",a,!0,
0,c);d=this.getClosestIndex(d,"time",b,!1,h,c);g.timeZoom(a,b);g.zoom(h,d);this.start=f.fitToBounds(h,0,c);this.end=f.fitToBounds(d,0,c);this.zoomAxesAndGraphs();this.zoomScrollbar();this.fixCursor();this.showZB();this.syncGrid();this.updateColumnsDepth();this.dispatchTimeZoomEvent()}},showZB:function(){var a,b=this.categoryAxis;b&&b.parseDates&&!b.equalSpacing&&(this.startTime>this.firstTime&&(a=!0),this.endTime<this.lastTime&&(a=!0));0<this.start&&(a=!0);this.end<this.chartData.length-1&&(a=!0);
if(b=this.valueAxes)b=b[0],isNaN(b.relativeStart)||(0!==f.roundTo(b.relativeStart,3)&&(a=!0),1!=f.roundTo(b.relativeEnd,3)&&(a=!0));f.AmSerialChart.base.showZB.call(this,a)},updateAfterValueZoom:function(){f.AmSerialChart.base.updateAfterValueZoom.call(this);this.updateColumnsDepth()},indexZoom:function(a,b){var c=this.maxSelectedSeries,d=!1;isNaN(c)||(b!=this.end&&b-a>c&&(a=b-c,d=!0),a!=this.start&&b-a>c&&(b=a+c,d=!0));if(d&&(d=this.chartScrollbar)&&d.dragger){var g=d.dragger.getBBox();d.maxWidth=
g.width;d.maxHeight=g.height}if(a!=this.start||b!=this.end)d=this.chartData.length-1,isNaN(a)&&(a=0,isNaN(c)||(a=d-c)),isNaN(b)&&(b=d),b<a&&(b=a),b>d&&(b=d),a>d&&(a=d-1),0>a&&(a=0),this.start=a,this.end=b,this.categoryAxis.zoom(a,b),this.zoomAxesAndGraphs(),this.zoomScrollbar(),this.fixCursor(),0!==a||b!=this.chartData.length-1?this.showZB(!0):this.showZB(!1),this.syncGrid(),this.updateColumnsDepth(),this.dispatchIndexZoomEvent()},updateGraphs:function(){f.AmSerialChart.base.updateGraphs.call(this);
var a=this.graphs,b;for(b=0;b<a.length;b++){var c=a[b];c.columnWidthReal=this.columnWidth;c.categoryAxis=this.categoryAxis;f.isString(c.fillToGraph)&&(c.fillToGraph=this.graphsById[c.fillToGraph])}},zoomAxesAndGraphs:function(){f.AmSerialChart.base.zoomAxesAndGraphs.call(this);this.updateColumnsDepth()},updateColumnsDepth:function(){if(0!==this.depth3D||0!==this.angle){var a,b=this.graphs,c;this.columnsArray=[];for(a=0;a<b.length;a++){c=b[a];var d=c.columnsArray;if(d){var g;for(g=0;g<d.length;g++)this.columnsArray.push(d[g])}}this.columnsArray.sort(this.compareDepth);
b=this.columnsSet;if(0<this.columnsArray.length){d=this.container.set();this.columnSet.push(d);for(a=0;a<this.columnsArray.length;a++)d.push(this.columnsArray[a].column.set);c&&d.translate(c.x,c.y);this.columnsSet=d}f.remove(b)}},compareDepth:function(a,b){return a.depth>b.depth?1:-1},zoomScrollbar:function(){var a=this.chartScrollbar,b=this.categoryAxis;if(a){if(!this.zoomedByScrollbar){var c=a.dragger;c&&c.stop()}this.zoomedByScrollbar=!1;b.parseDates&&!b.equalSpacing?a.timeZoom(this.startTime,
this.endTime):a.zoom(this.start,this.end)}this.zoomValueScrollbar(this.valueScrollbar)},updateTrendLines:function(){var a=this.trendLines,b;for(b=0;b<a.length;b++){var c=a[b],c=f.processObject(c,f.TrendLine,this.theme);a[b]=c;c.chart=this;c.id||(c.id="trendLineAuto"+b+"_"+(new Date).getTime());f.isString(c.valueAxis)&&(c.valueAxis=this.getValueAxisById(c.valueAxis));c.valueAxis||(c.valueAxis=this.valueAxes[0]);c.categoryAxis=this.categoryAxis}},countColumns:function(){var a=0,b=this.valueAxes.length,
c=this.graphs.length,d,g,f=!1,e,l;for(l=0;l<b;l++){g=this.valueAxes[l];var k=g.stackType,m=0;if("100%"==k||"regular"==k)for(f=!1,e=0;e<c;e++)d=this.graphs[e],d.tcc=1,d.valueAxis==g&&"column"==d.type&&(!f&&d.stackable&&(a++,f=!0),(!d.stackable&&d.clustered||d.newStack&&0!==m)&&a++,d.columnIndex=a-1,d.clustered||(d.columnIndex=0),m++);if("none"==k||"3d"==k){f=!1;for(e=0;e<c;e++)d=this.graphs[e],d.valueAxis==g&&"column"==d.type&&(d.clustered?(d.tcc=1,d.newStack&&(a=0),d.hidden||(d.columnIndex=a,a++)):
d.hidden||(f=!0,d.tcc=1,d.columnIndex=0));f&&0===a&&(a=1)}if("3d"==k){g=1;for(l=0;l<c;l++)d=this.graphs[l],d.newStack&&g++,d.depthCount=g,d.tcc=a;a=g}}return a},parseData:function(){f.AmSerialChart.base.parseData.call(this);this.parseSerialData(this.dataProvider)},getCategoryIndexByValue:function(a){var b=this.chartData,c;for(c=0;c<b.length;c++)if(b[c].category==a)return c},handleScrollbarZoom:function(a){this.zoomedByScrollbar=!0;this.zoom(a.start,a.end)},dispatchTimeZoomEvent:function(){if(this.drawGraphs&&
(this.prevStartTime!=this.startTime||this.prevEndTime!=this.endTime)){var a={type:"zoomed"};a.startDate=new Date(this.startTime);a.endDate=new Date(this.endTime);a.startIndex=this.start;a.endIndex=this.end;this.startIndex=this.start;this.endIndex=this.end;this.startDate=a.startDate;this.endDate=a.endDate;this.prevStartTime=this.startTime;this.prevEndTime=this.endTime;var b=this.categoryAxis,c=f.extractPeriod(b.minPeriod).period,b=b.dateFormatsObject[c];a.startValue=f.formatDate(a.startDate,b,this);
a.endValue=f.formatDate(a.endDate,b,this);a.chart=this;a.target=this;this.fire(a)}},dispatchIndexZoomEvent:function(){if(this.drawGraphs&&(this.prevStartIndex!=this.start||this.prevEndIndex!=this.end)){this.startIndex=this.start;this.endIndex=this.end;var a=this.chartData;if(f.ifArray(a)&&!isNaN(this.start)&&!isNaN(this.end)){var b={chart:this,target:this,type:"zoomed"};b.startIndex=this.start;b.endIndex=this.end;b.startValue=a[this.start].category;b.endValue=a[this.end].category;this.categoryAxis.parseDates&&
(this.startTime=a[this.start].time,this.endTime=a[this.end].time,b.startDate=new Date(this.startTime),b.endDate=new Date(this.endTime));this.prevStartIndex=this.start;this.prevEndIndex=this.end;this.fire(b)}}},updateLegendValues:function(){this.legend&&this.legend.updateValues()},getClosestIndex:function(a,b,c,d,g,f){0>g&&(g=0);f>a.length-1&&(f=a.length-1);var e=g+Math.round((f-g)/2),l=a[e][b];return c==l?e:1>=f-g?d?g:Math.abs(a[g][b]-c)<Math.abs(a[f][b]-c)?g:f:c==l?e:c<l?this.getClosestIndex(a,b,
c,d,g,e):this.getClosestIndex(a,b,c,d,e,f)},zoomToIndexes:function(a,b){var c=this.chartData;if(c){var d=c.length;0<d&&(0>a&&(a=0),b>d-1&&(b=d-1),d=this.categoryAxis,d.parseDates&&!d.equalSpacing?this.zoom(c[a].time,this.getEndTime(c[b].time)):this.zoom(a,b))}},zoomToDates:function(a,b){var c=this.chartData;if(c)if(this.categoryAxis.equalSpacing){var d=this.getClosestIndex(c,"time",a.getTime(),!0,0,c.length);b=f.resetDateToMin(b,this.categoryAxis.minPeriod,1);c=this.getClosestIndex(c,"time",b.getTime(),
!1,0,c.length);this.zoom(d,c)}else this.zoom(a.getTime(),b.getTime())},zoomToCategoryValues:function(a,b){this.chartData&&this.zoom(this.getCategoryIndexByValue(a),this.getCategoryIndexByValue(b))},formatPeriodString:function(a,b){if(b){b.periodDataItem={};b.periodPercentDataItem={};var c=["value","open","low","high","close"],d="value open low high close average sum count".split(" "),g=b.valueAxis,h=this.chartData,e=b.numberFormatter;e||(e=this.nf);for(var l=0;l<c.length;l++){for(var k=c[l],m=0,p=
0,n=0,u=0,v,x,E,t,r,B,q,w,y,C,F=this.start;F<=this.end;F++){var D=h[F];if(D){var A=D.axes[g.id].graphs[b.id];if(A){if(A.values){var z=A.values[k],D=D.x.categoryAxis;if(this.rotate){if(0>D||D>A.graph.height)z=NaN}else if(0>D||D>A.graph.width)z=NaN;if(!isNaN(z)){isNaN(v)&&(v=z);x=z;if(isNaN(E)||E>z)E=z;if(isNaN(t)||t<z)t=z;r=f.getDecimals(m);D=f.getDecimals(z);m+=z;m=f.roundTo(m,Math.max(r,D));p++;r=m/p}}if(A.percents&&(A=A.percents[k],!isNaN(A))){isNaN(B)&&(B=A);q=A;if(isNaN(w)||w>A)w=A;if(isNaN(y)||
y<A)y=A;C=f.getDecimals(n);z=f.getDecimals(A);n+=A;n=f.roundTo(n,Math.max(C,z));u++;C=n/u}}}}m={open:v,close:x,high:t,low:E,average:r,sum:m,count:p};n={open:B,close:q,high:y,low:w,average:C,sum:n,count:u};a=f.formatValue(a,m,d,e,k+"\\.",this.usePrefixes,this.prefixesOfSmallNumbers,this.prefixesOfBigNumbers);a=f.formatValue(a,n,d,this.pf,"percents\\."+k+"\\.");b.periodDataItem[k]=m;b.periodPercentDataItem[k]=n}}return a=f.cleanFromEmpty(a)},formatString:function(a,b,c){if(b){var d=b.graph;if(void 0!==
a){if(-1!=a.indexOf("[[category]]")){var g=b.serialDataItem.category;if(this.categoryAxis.parseDates){var h=this.balloonDateFormat,e=this.chartCursor;e&&e.categoryBalloonDateFormat&&(h=e.categoryBalloonDateFormat);h=f.formatDate(g,h,this);-1!=h.indexOf("fff")&&(h=f.formatMilliseconds(h,g));g=h}a=a.replace(/\[\[category\]\]/g,String(g.replace("$","$$$")))}g=d.numberFormatter;g||(g=this.nf);h=b.graph.valueAxis;(e=h.duration)&&!isNaN(b.values.value)&&(e=f.formatDuration(b.values.value,e,"",h.durationUnits,
h.maxInterval,g),a=a.replace(RegExp("\\[\\[value\\]\\]","g"),e));"date"==h.type&&(h=f.formatDate(new Date(b.values.value),d.dateFormat,this),e=RegExp("\\[\\[value\\]\\]","g"),a=a.replace(e,h),h=f.formatDate(new Date(b.values.open),d.dateFormat,this),e=RegExp("\\[\\[open\\]\\]","g"),a=a.replace(e,h));d="value open low high close total".split(" ");h=this.pf;a=f.formatValue(a,b.percents,d,h,"percents\\.");a=f.formatValue(a,b.values,d,g,"",this.usePrefixes,this.prefixesOfSmallNumbers,this.prefixesOfBigNumbers);
a=f.formatValue(a,b.values,["percents"],h);-1!=a.indexOf("[[")&&(a=f.formatDataContextValue(a,b.dataContext));-1!=a.indexOf("[[")&&b.graph.customData&&(a=f.formatDataContextValue(a,b.graph.customData));a=f.AmSerialChart.base.formatString.call(this,a,b,c)}return a}},updateChartCursor:function(){f.AmSerialChart.base.updateChartCursor.call(this);var a=this.chartCursor,b=this.categoryAxis;if(a){var c=a.categoryBalloonAlpha,d=a.categoryBalloonColor,g=a.color;void 0===d&&(d=a.cursorColor);var h=a.valueZoomable,
e=a.zoomable,l=a.valueLineEnabled;this.rotate?(a.vLineEnabled=l,a.hZoomEnabled=h,a.vZoomEnabled=e):(a.hLineEnabled=l,a.vZoomEnabled=h,a.hZoomEnabled=e);if(a.valueLineBalloonEnabled)for(l=0;l<this.valueAxes.length;l++)h=this.valueAxes[l],(e=h.balloon)||(e={}),e=f.extend(e,this.balloon,!0),e.fillColor=d,e.balloonColor=d,e.fillAlpha=c,e.borderColor=d,e.color=g,h.balloon=e;else for(e=0;e<this.valueAxes.length;e++)h=this.valueAxes[e],h.balloon&&(h.balloon=null);b&&(b.balloonTextFunction=a.categoryBalloonFunction,
a.categoryLineAxis=b,b.balloonText=a.categoryBalloonText,a.categoryBalloonEnabled&&((e=b.balloon)||(e={}),e=f.extend(e,this.balloon,!0),e.fillColor=d,e.balloonColor=d,e.fillAlpha=c,e.borderColor=d,e.color=g,b.balloon=e),b.balloon&&(b.balloon.enabled=a.categoryBalloonEnabled))}},addChartScrollbar:function(a){f.callMethod("destroy",[this.chartScrollbar]);a&&(a.chart=this,this.listenTo(a,"zoomed",this.handleScrollbarZoom));this.rotate?void 0===a.width&&(a.width=a.scrollbarHeight):void 0===a.height&&
(a.height=a.scrollbarHeight);a.gridAxis=this.categoryAxis;this.chartScrollbar=a},addValueScrollbar:function(a){f.callMethod("destroy",[this.valueScrollbar]);a&&(a.chart=this,this.listenTo(a,"zoomed",this.handleScrollbarValueZoom),this.listenTo(a,"zoomStarted",this.handleCursorZoomStarted));var b=a.scrollbarHeight;this.rotate?void 0===a.height&&(a.height=b):void 0===a.width&&(a.width=b);a.gridAxis||(a.gridAxis=this.valueAxes[0]);a.valueAxes=this.valueAxes;this.valueScrollbar=a},removeChartScrollbar:function(){f.callMethod("destroy",
[this.chartScrollbar]);this.chartScrollbar=null},removeValueScrollbar:function(){f.callMethod("destroy",[this.valueScrollbar]);this.valueScrollbar=null},handleReleaseOutside:function(a){f.AmSerialChart.base.handleReleaseOutside.call(this,a);f.callMethod("handleReleaseOutside",[this.chartScrollbar,this.valueScrollbar])},update:function(){f.AmSerialChart.base.update.call(this);this.chartScrollbar&&this.chartScrollbar.update&&this.chartScrollbar.update();this.valueScrollbar&&this.valueScrollbar.update&&
this.valueScrollbar.update()},processScrollbars:function(){f.AmSerialChart.base.processScrollbars.call(this);var a=this.valueScrollbar;a&&(a=f.processObject(a,f.ChartScrollbar,this.theme),a.id="valueScrollbar",this.addValueScrollbar(a))},handleValueAxisZoom:function(a){this.handleValueAxisZoomReal(a,this.valueAxes)},zoomOut:function(){f.AmSerialChart.base.zoomOut.call(this);this.zoom();this.syncGrid()},getNextItem:function(a){var b=a.index,c=this.chartData,d=a.graph;if(b+1<c.length)for(b+=1;b<c.length;b++)if(a=
c[b])if(a=a.axes[d.valueAxis.id].graphs[d.id],!isNaN(a.y))return a},handleCursorZoomReal:function(a,b,c,d,g){var f=g.target,e,l;this.rotate?(isNaN(a)||isNaN(b)||this.relativeZoomValueAxes(this.valueAxes,a,b)&&this.updateAfterValueZoom(),f.vZoomEnabled&&(e=g.start,l=g.end)):(isNaN(c)||isNaN(d)||this.relativeZoomValueAxes(this.valueAxes,c,d)&&this.updateAfterValueZoom(),f.hZoomEnabled&&(e=g.start,l=g.end));isNaN(e)||isNaN(l)||(a=this.categoryAxis,a.parseDates&&!a.equalSpacing?this.zoomToDates(new Date(e),
new Date(l)):this.zoomToIndexes(e,l))},handleCursorZoomStarted:function(){var a=this.valueAxes;if(a){var a=a[0],b=a.relativeStart,c=a.relativeEnd;a.reversed&&(b=1-a.relativeEnd,c=1-a.relativeStart);this.rotate?(this.startX0=b,this.endX0=c):(this.startY0=b,this.endY0=c)}this.categoryAxis&&(this.start0=this.start,this.end0=this.end,this.startTime0=this.startTime,this.endTime0=this.endTime)},fixCursor:function(){this.chartCursor&&this.chartCursor.fixPosition();this.prevCursorItem=null},handleCursorMove:function(a){f.AmSerialChart.base.handleCursorMove.call(this,
a);var b=a.target,c=this.categoryAxis;if(a.panning)this.handleCursorHide(a);else if(this.chartData&&!b.isHidden){var d=this.graphs;if(d){var g;g=c.xToIndex(this.rotate?a.y:a.x);if(g=this.chartData[g]){var h,e,l,k;if(b.oneBalloonOnly&&b.valueBalloonsEnabled){var m=Infinity;for(h=d.length-1;0<=h;h--)if(e=d[h],e.balloon.enabled&&e.showBalloon&&!e.hidden){l=e.valueAxis.id;l=g.axes[l].graphs[e.id];if(b.showNextAvailable&&isNaN(l.y)&&(l=this.getNextItem(l),!l))continue;l=l.y;"top"==e.showBalloonAt&&(l=
0);"bottom"==e.showBalloonAt&&(l=this.height);var p=b.mouseX,n=b.mouseY;l=this.rotate?Math.abs(p-l):Math.abs(n-l);l<m&&(m=l,k=e)}b.mostCloseGraph=k}if(this.prevCursorItem!=g||k!=this.prevMostCloseGraph){m=[];for(h=0;h<d.length;h++){e=d[h];l=e.valueAxis.id;l=g.axes[l].graphs[e.id];if(b.showNextAvailable&&isNaN(l.y)&&(l=this.getNextItem(l),!l&&e.balloon)){e.balloon.hide();continue}k&&e!=k?(e.showGraphBalloon(l,b.pointer,!1,b.graphBulletSize,b.graphBulletAlpha),e.balloon.hide(0)):b.valueBalloonsEnabled?
(e.balloon.showBullet=b.bulletsEnabled,e.balloon.bulletSize=b.bulletSize/2,a.hideBalloons||(e.showGraphBalloon(l,b.pointer,!1,b.graphBulletSize,b.graphBulletAlpha),e.balloon.set&&m.push({balloon:e.balloon,y:e.balloon.pointToY}))):(e.currentDataItem=l,e.resizeBullet(l,b.graphBulletSize,b.graphBulletAlpha))}b.avoidBalloonOverlapping&&this.arrangeBalloons(m);this.prevCursorItem=g}this.prevMostCloseGraph=k}}c.showBalloon(a.x,a.y,b.categoryBalloonDateFormat,a.skip);this.updateLegendValues()}},handleCursorHide:function(a){f.AmSerialChart.base.handleCursorHide.call(this,
a);a=this.categoryAxis;this.prevCursorItem=null;this.updateLegendValues();a&&a.hideBalloon();a=this.graphs;var b;for(b=0;b<a.length;b++)a[b].currentDataItem=null},handleCursorPanning:function(a){var b=a.target,c,d=a.deltaX,g=a.deltaY,h=a.delta2X,e=a.delta2Y;a=!1;if(this.rotate){isNaN(h)&&(h=d,a=!0);var l=this.endX0;c=this.startX0;var k=l-c,l=l-k*h,m=k;a||(m=0);a=f.fitToBounds(c-k*d,0,1-m)}else isNaN(e)&&(e=g,a=!0),l=this.endY0,c=this.startY0,k=l-c,l+=k*g,m=k,a||(m=0),a=f.fitToBounds(c+k*e,0,1-m);
c=f.fitToBounds(l,m,1);var p;b.valueZoomable&&(p=this.relativeZoomValueAxes(this.valueAxes,a,c));var n;c=this.categoryAxis;this.rotate&&(d=g,h=e);a=!1;isNaN(h)&&(h=d,a=!0);if(b.zoomable&&(0<Math.abs(d)||0<Math.abs(h)))if(c.parseDates&&!c.equalSpacing){if(e=this.startTime0,g=this.endTime0,c=g-e,h*=c,k=this.firstTime,l=this.lastTime,m=c,a||(m=0),a=Math.round(f.fitToBounds(e-c*d,k,l-m)),h=Math.round(f.fitToBounds(g-h,k+m,l)),this.startTime!=a||this.endTime!=h)n={chart:this,target:b,type:"zoomed",start:a,
end:h},this.skipZoomed=!0,b.fire(n),this.zoom(a,h),n=!0}else if(e=this.start0,g=this.end0,c=g-e,d=Math.round(c*d),h=Math.round(c*h),k=this.chartData.length-1,a||(c=0),a=f.fitToBounds(e-d,0,k-c),c=f.fitToBounds(g-h,c,k),this.start!=a||this.end!=c)this.skipZoomed=!0,b.fire({chart:this,target:b,type:"zoomed",start:a,end:c}),this.zoom(a,c),n=!0;!n&&p&&this.updateAfterValueZoom()},arrangeBalloons:function(a){var b=this.plotAreaHeight;a.sort(this.compareY);var c,d,f,h=this.plotAreaWidth,e=a.length;for(c=
0;c<e;c++)d=a[c].balloon,d.setBounds(0,0,h,b),d.restorePrevious(),d.draw(),b=d.yPos-3;a.reverse();for(c=0;c<e;c++){d=a[c].balloon;var b=d.bottom,l=d.bottom-d.yPos;0<c&&b-l<f+3&&d.setBounds&&(d.setBounds(0,f+3,h,f+l+3),d.restorePrevious(),d.draw());d.set&&d.set.show();f=d.bottom}},compareY:function(a,b){return a.y<b.y?1:-1}})})();(function(){var f=window.AmCharts;f.Cuboid=f.Class({construct:function(a,b,c,d,f,h,e,l,k,m,p,n,u,v,x,E,t){this.set=a.set();this.container=a;this.h=Math.round(c);this.w=Math.round(b);this.dx=d;this.dy=f;this.colors=h;this.alpha=e;this.bwidth=l;this.bcolor=k;this.balpha=m;this.dashLength=v;this.topRadius=E;this.pattern=x;this.rotate=u;this.bcn=t;u?0>b&&0===p&&(p=180):0>c&&270==p&&(p=90);this.gradientRotation=p;0===d&&0===f&&(this.cornerRadius=n);this.draw()},draw:function(){var a=this.set;a.clear();
var b=this.container,c=b.chart,d=this.w,g=this.h,h=this.dx,e=this.dy,l=this.colors,k=this.alpha,m=this.bwidth,p=this.bcolor,n=this.balpha,u=this.gradientRotation,v=this.cornerRadius,x=this.dashLength,E=this.pattern,t=this.topRadius,r=this.bcn,B=l,q=l;"object"==typeof l&&(B=l[0],q=l[l.length-1]);var w,y,C,F,D,A,z,L,M,Q=k;E&&(k=0);var G,H,I,J,K=this.rotate;if(0<Math.abs(h)||0<Math.abs(e))if(isNaN(t))z=q,q=f.adjustLuminosity(B,-.2),q=f.adjustLuminosity(B,-.2),w=f.polygon(b,[0,h,d+h,d,0],[0,e,e,0,0],
q,k,1,p,0,u),0<n&&(M=f.line(b,[0,h,d+h],[0,e,e],p,n,m,x)),y=f.polygon(b,[0,0,d,d,0],[0,g,g,0,0],q,k,1,p,0,u),y.translate(h,e),0<n&&(C=f.line(b,[h,h],[e,e+g],p,n,m,x)),F=f.polygon(b,[0,0,h,h,0],[0,g,g+e,e,0],q,k,1,p,0,u),D=f.polygon(b,[d,d,d+h,d+h,d],[0,g,g+e,e,0],q,k,1,p,0,u),0<n&&(A=f.line(b,[d,d+h,d+h,d],[0,e,g+e,g],p,n,m,x)),q=f.adjustLuminosity(z,.2),z=f.polygon(b,[0,h,d+h,d,0],[g,g+e,g+e,g,g],q,k,1,p,0,u),0<n&&(L=f.line(b,[0,h,d+h],[g,g+e,g+e],p,n,m,x));else{var N,O,P;K?(N=g/2,q=h/2,P=g/2,O=
d+h/2,H=Math.abs(g/2),G=Math.abs(h/2)):(q=d/2,N=e/2,O=d/2,P=g+e/2+1,G=Math.abs(d/2),H=Math.abs(e/2));I=G*t;J=H*t;.1<G&&.1<G&&(w=f.circle(b,G,B,k,m,p,n,!1,H),w.translate(q,N));.1<I&&.1<I&&(z=f.circle(b,I,f.adjustLuminosity(B,.5),k,m,p,n,!1,J),z.translate(O,P))}k=Q;1>Math.abs(g)&&(g=0);1>Math.abs(d)&&(d=0);!isNaN(t)&&(0<Math.abs(h)||0<Math.abs(e))?(l=[B],l={fill:l,stroke:p,"stroke-width":m,"stroke-opacity":n,"fill-opacity":k},K?(k="M0,0 L"+d+","+(g/2-g/2*t),m=" B",0<d&&(m=" A"),f.VML?(k+=m+Math.round(d-
I)+","+Math.round(g/2-J)+","+Math.round(d+I)+","+Math.round(g/2+J)+","+d+",0,"+d+","+g,k=k+(" L0,"+g)+(m+Math.round(-G)+","+Math.round(g/2-H)+","+Math.round(G)+","+Math.round(g/2+H)+",0,"+g+",0,0")):(k+="A"+I+","+J+",0,0,0,"+d+","+(g-g/2*(1-t))+"L0,"+g,k+="A"+G+","+H+",0,0,1,0,0"),G=90):(m=d/2-d/2*t,k="M0,0 L"+m+","+g,f.VML?(k="M0,0 L"+m+","+g,m=" B",0>g&&(m=" A"),k+=m+Math.round(d/2-I)+","+Math.round(g-J)+","+Math.round(d/2+I)+","+Math.round(g+J)+",0,"+g+","+d+","+g,k+=" L"+d+",0",k+=m+Math.round(d/
2+G)+","+Math.round(H)+","+Math.round(d/2-G)+","+Math.round(-H)+","+d+",0,0,0"):(k+="A"+I+","+J+",0,0,0,"+(d-d/2*(1-t))+","+g+"L"+d+",0",k+="A"+G+","+H+",0,0,1,0,0"),G=180),b=b.path(k).attr(l),b.gradient("linearGradient",[B,f.adjustLuminosity(B,-.3),f.adjustLuminosity(B,-.3),B],G),K?b.translate(h/2,0):b.translate(0,e/2)):b=0===g?f.line(b,[0,d],[0,0],p,n,m,x):0===d?f.line(b,[0,0],[0,g],p,n,m,x):0<v?f.rect(b,d,g,l,k,m,p,n,v,u,x):f.polygon(b,[0,0,d,d,0],[0,g,g,0,0],l,k,m,p,n,u,!1,x);d=isNaN(t)?0>g?[w,
M,y,C,F,D,A,z,L,b]:[z,L,y,C,F,D,w,M,A,b]:K?0<d?[w,b,z]:[z,b,w]:0>g?[w,b,z]:[z,b,w];f.setCN(c,b,r+"front");f.setCN(c,y,r+"back");f.setCN(c,z,r+"top");f.setCN(c,w,r+"bottom");f.setCN(c,F,r+"left");f.setCN(c,D,r+"right");for(w=0;w<d.length;w++)if(y=d[w])a.push(y),f.setCN(c,y,r+"element");E&&b.pattern(E,NaN,c.path)},width:function(a){isNaN(a)&&(a=0);this.w=Math.round(a);this.draw()},height:function(a){isNaN(a)&&(a=0);this.h=Math.round(a);this.draw()},animateHeight:function(a,b){var c=this;c.animationFinished=
!1;c.easing=b;c.totalFrames=a*f.updateRate;c.rh=c.h;c.frame=0;c.height(1);setTimeout(function(){c.updateHeight.call(c)},1E3/f.updateRate)},updateHeight:function(){var a=this;a.frame++;var b=a.totalFrames;a.frame<=b?(b=a.easing(0,a.frame,1,a.rh-1,b),a.height(b),window.requestAnimationFrame?window.requestAnimationFrame(function(){a.updateHeight.call(a)}):setTimeout(function(){a.updateHeight.call(a)},1E3/f.updateRate)):(a.height(a.rh),a.animationFinished=!0)},animateWidth:function(a,b){var c=this;c.animationFinished=
!1;c.easing=b;c.totalFrames=a*f.updateRate;c.rw=c.w;c.frame=0;c.width(1);setTimeout(function(){c.updateWidth.call(c)},1E3/f.updateRate)},updateWidth:function(){var a=this;a.frame++;var b=a.totalFrames;a.frame<=b?(b=a.easing(0,a.frame,1,a.rw-1,b),a.width(b),window.requestAnimationFrame?window.requestAnimationFrame(function(){a.updateWidth.call(a)}):setTimeout(function(){a.updateWidth.call(a)},1E3/f.updateRate)):(a.width(a.rw),a.animationFinished=!0)}})})();(function(){var f=window.AmCharts;f.CategoryAxis=f.Class({inherits:f.AxisBase,construct:function(a){this.cname="CategoryAxis";f.CategoryAxis.base.construct.call(this,a);this.minPeriod="DD";this.equalSpacing=this.parseDates=!1;this.position="bottom";this.startOnAxis=!1;this.gridPosition="middle";this.safeDistance=30;this.stickBalloonToCategory=!1;f.applyTheme(this,a,this.cname)},draw:function(){f.CategoryAxis.base.draw.call(this);this.generateDFObject();var a=this.chart.chartData;this.data=a;this.labelRotationR=
this.labelRotation;this.type=null;if(f.ifArray(a)){var b,c=this.chart;"scrollbar"!=this.id?(f.setCN(c,this.set,"category-axis"),f.setCN(c,this.labelsSet,"category-axis"),f.setCN(c,this.axisLine.axisSet,"category-axis")):this.bcn=this.id+"-";var d=this.start,g=this.labelFrequency,h=0,e=this.end-d+1,l=this.gridCountR,k=this.showFirstLabel,m=this.showLastLabel,p,n="",n=f.extractPeriod(this.minPeriod),u=f.getPeriodDuration(n.period,n.count),v,x,E,t,r,B=this.rotate,q=this.firstDayOfWeek,w=this.boldPeriodBeginning;
b=f.resetDateToMin(new Date(a[a.length-1].time+1.05*u),this.minPeriod,1,q).getTime();this.firstTime=c.firstTime;this.endTime>b&&(this.endTime=b);r=this.minorGridEnabled;x=this.gridAlpha;var y=0,C=0;if(this.widthField)for(b=this.start;b<=this.end;b++)if(t=this.data[b]){var F=Number(this.data[b].dataContext[this.widthField]);isNaN(F)||(y+=F,t.widthValue=F)}if(this.parseDates&&!this.equalSpacing)this.lastTime=a[a.length-1].time,this.maxTime=f.resetDateToMin(new Date(this.lastTime+1.05*u),this.minPeriod,
1,q).getTime(),this.timeDifference=this.endTime-this.startTime,this.parseDatesDraw();else if(!this.parseDates){if(this.cellWidth=this.getStepWidth(e),e<l&&(l=e),h+=this.start,this.stepWidth=this.getStepWidth(e),0<l)for(q=Math.floor(e/l),t=this.chooseMinorFrequency(q),e=h,e/2==Math.round(e/2)&&e--,0>e&&(e=0),w=0,this.widthField&&(e=this.start),this.end-e+1>=this.autoRotateCount&&(this.labelRotationR=this.autoRotateAngle),b=e;b<=this.end+2;b++){l=!1;0<=b&&b<this.data.length?(v=this.data[b],n=v.category,
l=v.forceShow):n="";if(r&&!isNaN(t))if(b/t==Math.round(b/t)||l)b/q==Math.round(b/q)||l||(this.gridAlpha=this.minorGridAlpha,n=void 0);else continue;else if(b/q!=Math.round(b/q)&&!l)continue;e=this.getCoordinate(b-h);l=0;"start"==this.gridPosition&&(e-=this.cellWidth/2,l=this.cellWidth/2);p=!0;E=l;"start"==this.tickPosition&&(E=0,p=!1,l=0);if(b==d&&!k||b==this.end&&!m)n=void 0;Math.round(w/g)!=w/g&&(n=void 0);w++;a=this.cellWidth;B&&(a=NaN,this.ignoreAxisWidth||!c.autoMargins)&&(a="right"==this.position?
c.marginRight:c.marginLeft,a-=this.tickLength+10);this.labelFunction&&v&&(n=this.labelFunction(n,v,this));n=f.fixBrakes(n);u=!1;this.boldLabels&&(u=!0);b>this.end&&"start"==this.tickPosition&&(n=" ");this.rotate&&this.inside&&(l-=2);isNaN(v.widthValue)||(v.percentWidthValue=v.widthValue/y*100,a=this.rotate?this.height*v.widthValue/y:this.width*v.widthValue/y,e=C,C+=a,E=l=a/2);p=new this.axisItemRenderer(this,e,n,p,a,l,void 0,u,E,!1,v.labelColor,v.className);p.serialDataItem=v;this.pushAxisItem(p);
this.gridAlpha=x}}else if(this.parseDates&&this.equalSpacing){h=this.start;this.startTime=this.data[this.start].time;this.endTime=this.data[this.end].time;this.timeDifference=this.endTime-this.startTime;b=this.choosePeriod(0);g=b.period;v=b.count;b=f.getPeriodDuration(g,v);b<u&&(g=n.period,v=n.count,b=u);x=g;"WW"==x&&(x="DD");this.currentDateFormat=this.dateFormatsObject[x];this.stepWidth=this.getStepWidth(e);l=Math.ceil(this.timeDifference/b)+1;n=f.resetDateToMin(new Date(this.startTime-b),g,v,q).getTime();
this.cellWidth=this.getStepWidth(e);e=Math.round(n/b);d=-1;e/2==Math.round(e/2)&&(d=-2,n-=b);e=this.start;e/2==Math.round(e/2)&&e--;0>e&&(e=0);C=this.end+2;C>=this.data.length&&(C=this.data.length);a=!1;a=!k;this.previousPos=-1E3;20<this.labelRotationR&&(this.safeDistance=5);F=e;if(this.data[e].time!=f.resetDateToMin(new Date(this.data[e].time),g,v,q).getTime()){var u=0,D=n;for(b=e;b<C;b++)t=this.data[b].time,this.checkPeriodChange(g,v,t,D)&&(u++,2<=u&&(F=b,b=C),D=t)}r&&1<v&&(t=this.chooseMinorFrequency(v),
f.getPeriodDuration(g,t));if(0<this.gridCountR)for(b=e;b<C;b++)if(t=this.data[b].time,this.checkPeriodChange(g,v,t,n)&&b>=F){e=this.getCoordinate(b-this.start);r=!1;this.nextPeriod[x]&&(r=this.checkPeriodChange(this.nextPeriod[x],1,t,n,x))&&f.resetDateToMin(new Date(t),this.nextPeriod[x],1,q).getTime()!=t&&(r=!1);u=!1;r&&this.markPeriodChange?(r=this.dateFormatsObject[this.nextPeriod[x]],u=!0):r=this.dateFormatsObject[x];n=f.formatDate(new Date(t),r,c);if(b==d&&!k||b==l&&!m)n=" ";a?a=!1:(w||(u=!1),
e-this.previousPos>this.safeDistance*Math.cos(this.labelRotationR*Math.PI/180)&&(this.labelFunction&&(n=this.labelFunction(n,new Date(t),this,g,v,E)),this.boldLabels&&(u=!0),p=new this.axisItemRenderer(this,e,n,void 0,void 0,void 0,void 0,u),r=p.graphics(),this.pushAxisItem(p),r=r.getBBox().width,f.isModern||(r-=e),this.previousPos=e+r));E=n=t}}for(b=k=0;b<this.data.length;b++)if(t=this.data[b])this.parseDates&&!this.equalSpacing?(m=t.time,d=this.cellWidth,"MM"==this.minPeriod&&(d=864E5*f.daysInMonth(new Date(m))*
this.stepWidth,t.cellWidth=d),m=Math.round((m-this.startTime)*this.stepWidth+d/2)):m=this.getCoordinate(b-h),t.x[this.id]=m;if(this.widthField)for(b=this.start;b<=this.end;b++)t=this.data[b],d=t.widthValue,t.percentWidthValue=d/y*100,this.rotate?(m=this.height*d/y/2+k,k=this.height*d/y+k):(m=this.width*d/y/2+k,k=this.width*d/y+k),t.x[this.id]=m;y=this.guides.length;for(b=0;b<y;b++)if(k=this.guides[b],q=q=q=r=d=NaN,m=k.above,k.toCategory&&(q=c.getCategoryIndexByValue(k.toCategory),isNaN(q)||(d=this.getCoordinate(q-
h),k.expand&&(d+=this.cellWidth/2),p=new this.axisItemRenderer(this,d,"",!0,NaN,NaN,k),this.pushAxisItem(p,m))),k.category&&(q=c.getCategoryIndexByValue(k.category),isNaN(q)||(r=this.getCoordinate(q-h),k.expand&&(r-=this.cellWidth/2),q=(d-r)/2,p=new this.axisItemRenderer(this,r,k.label,!0,NaN,q,k),this.pushAxisItem(p,m))),w=c.dataDateFormat,k.toDate&&(!w||k.toDate instanceof Date||(k.toDate=k.toDate.toString()+" |"),k.toDate=f.getDate(k.toDate,w),this.equalSpacing?(q=c.getClosestIndex(this.data,"time",
k.toDate.getTime(),!1,0,this.data.length-1),isNaN(q)||(d=this.getCoordinate(q-h))):d=(k.toDate.getTime()-this.startTime)*this.stepWidth,p=new this.axisItemRenderer(this,d,"",!0,NaN,NaN,k),this.pushAxisItem(p,m)),k.date&&(!w||k.date instanceof Date||(k.date=k.date.toString()+" |"),k.date=f.getDate(k.date,w),this.equalSpacing?(q=c.getClosestIndex(this.data,"time",k.date.getTime(),!1,0,this.data.length-1),isNaN(q)||(r=this.getCoordinate(q-h))):r=(k.date.getTime()-this.startTime)*this.stepWidth,q=(d-
r)/2,p=!0,k.toDate&&(p=!1),p="H"==this.orientation?new this.axisItemRenderer(this,r,k.label,p,2*q,NaN,k):new this.axisItemRenderer(this,r,k.label,!1,NaN,q,k),this.pushAxisItem(p,m)),p&&(q=p.label)&&this.addEventListeners(q,k),0<d||0<r){q=!1;if(this.rotate){if(d<this.height||r<this.height)q=!0}else if(d<this.width||r<this.width)q=!0;q&&(d=new this.guideFillRenderer(this,r,d,k),r=d.graphics(),this.pushAxisItem(d,m),k.graphics=r,r.index=b,this.addEventListeners(r,k))}if(c=c.chartCursor)B?c.fixHeight(this.cellWidth):
(c.fixWidth(this.cellWidth),c.fullWidth&&this.balloon&&(this.balloon.minWidth=this.cellWidth));this.previousHeight=A}this.axisCreated=!0;this.set.translate(this.x,this.y);this.labelsSet.translate(this.x,this.y);this.labelsSet.show();this.positionTitle();(B=this.axisLine.set)&&B.toFront();var A=this.getBBox().height;2<A-this.previousHeight&&this.autoWrap&&!this.parseDates&&(this.axisCreated=this.chart.marginsUpdated=!1)},xToIndex:function(a){var b=this.data,c=this.chart,d=c.rotate,g=this.stepWidth,
h;if(this.parseDates&&!this.equalSpacing)a=this.startTime+Math.round(a/g)-this.minDuration()/2,h=c.getClosestIndex(b,"time",a,!1,this.start,this.end+1);else if(this.widthField)for(c=Infinity,g=this.start;g<=this.end;g++){var e=this.data[g];e&&(e=Math.abs(e.x[this.id]-a),e<c&&(c=e,h=g))}else this.startOnAxis||(a-=g/2),h=this.start+Math.round(a/g);h=f.fitToBounds(h,0,b.length-1);var l;b[h]&&(l=b[h].x[this.id]);d?l>this.height+1&&h--:l>this.width+1&&h--;0>l&&h++;return h=f.fitToBounds(h,0,b.length-1)},
dateToCoordinate:function(a){return this.parseDates&&!this.equalSpacing?(a.getTime()-this.startTime)*this.stepWidth:this.parseDates&&this.equalSpacing?(a=this.chart.getClosestIndex(this.data,"time",a.getTime(),!1,0,this.data.length-1),this.getCoordinate(a-this.start)):NaN},categoryToCoordinate:function(a){if(this.chart){if(this.parseDates)return this.dateToCoordinate(new Date(a));a=this.chart.getCategoryIndexByValue(a);if(!isNaN(a))return this.getCoordinate(a-this.start)}else return NaN},coordinateToDate:function(a){return this.equalSpacing?
(a=this.xToIndex(a),new Date(this.data[a].time)):new Date(this.startTime+a/this.stepWidth)},coordinateToValue:function(a){a=this.xToIndex(a);if(a=this.data[a])return this.parseDates?a.time:a.category},getCoordinate:function(a){a*=this.stepWidth;this.startOnAxis||(a+=this.stepWidth/2);return Math.round(a)},formatValue:function(a,b){b||(b=this.currentDateFormat);this.parseDates&&(a=f.formatDate(new Date(a),b,this.chart));return a},showBalloonAt:function(a,b){void 0===b&&(b=this.parseDates?this.dateToCoordinate(new Date(a)):
this.categoryToCoordinate(a));return this.adjustBalloonCoordinate(b)},formatBalloonText:function(a,b,c){var d="",g="",h=this.chart,e=this.data[b];if(e)if(this.parseDates)d=f.formatDate(e.category,c,h),b=f.changeDate(new Date(e.category),this.minPeriod,1),g=f.formatDate(b,c,h),-1!=d.indexOf("fff")&&(d=f.formatMilliseconds(d,e.category),g=f.formatMilliseconds(g,b));else{var l;this.data[b+1]&&(l=this.data[b+1]);d=f.fixNewLines(e.category);l&&(g=f.fixNewLines(l.category))}a=a.replace(/\[\[category\]\]/g,
String(d));return a=a.replace(/\[\[toCategory\]\]/g,String(g))},adjustBalloonCoordinate:function(a,b){var c=this.xToIndex(a),d=this.chart.chartCursor;if(this.stickBalloonToCategory){var f=this.data[c];f&&(a=f.x[this.id]+1);this.stickBalloonToStart&&(a-=this.cellWidth/2);var h=0;if(d){var e=d.limitToGraph;if(e){var l=e.valueAxis.id;e.hidden||(h=f.axes[l].graphs[e.id].y)}this.rotate?("left"==this.position?(e&&(h-=d.width),0<h&&(h=0)):0>h&&(h=0),d.fixHLine(a,h)):("top"==this.position?(e&&(h-=d.height),
0<h&&(h=0)):0>h&&(h=0),d.fixVLine(a,h))}}d&&!b&&(d.setIndex(c),this.parseDates&&d.setTimestamp(this.coordinateToDate(a).getTime()));return a}})})();
